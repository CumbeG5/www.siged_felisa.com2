<?php
@session_start();
include "./classes/classes.php";
include('Connections/conexaosiged.php');
require_once("classes/Geral.php");

$query_comboempresass = "SELECT * FROM fornecedors WHERE activo='1'";
$comboempresass = $conexaosiged->prepare($query_comboempresass);
$comboempresass->execute();

$tipo_factura = "SELECT * FROM tipo_factura";
$factura = $conexaosiged->prepare($tipo_factura);
$factura->execute();

$query_combocentrodecusto = "SELECT * FROM centro_de_custo  LIMIT 10";
$combocentrodecusto = $conexaosiged->prepare($query_combocentrodecusto);
$combocentrodecusto->execute();

$meses = array(1 => "Janeiro", 2 => "Fevereiro", 3 => "Março", 4 => "Abril", 5 => "Maio", 6 => "Junho", 7 => "Julho", 8 => "Agosto", 9 => "Setembro", 10 => "Outubro", 11 => "Novembro", 12 => "Dezembro");
// echo $meses[date("n")];
?>
<!DOCTYPE html>
<html lang="en" dir="">
<meta http-equiv="content-type" content="text/html;charset=utf-8" />

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width,initial-scale=1" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>SIGED | Registar Facturas Fornecedor</title>
    <link href="./fonts.googleapis.com/csse27b.css?family=Nunito:300,400,400i,600,700,800,900" rel="stylesheet" />
    <link href="./dist-assets/css/themes/lite-purple.min.css" rel="stylesheet" />
    <link href="./dist-assets/css/plugins/perfect-scrollbar.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="./dist-assets/css/plugins/fontawesome-5.css" />
    <link href="./dist-assets/css/plugins/metisMenu.min.css" rel="stylesheet" />
    <link href="./dist-assets/flatpickr/dist/flatpickr.min.css" rel="stylesheet" />
    <link href="./dist-assets/fonts/fontawesome/css/fontawesome.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="./dist-assets/sweetalert2/dist/sweetalert2.css" />
</head>

<body class="text-left">
    <div class="app-admin-wrap layout-horizontal-bar">
        <?php include_once './topbar.php' ?>
        <!-- =============== Horizontal bar End ================-->
        <div class="main-content-wrap d-flex flex-column">
            <!-- ============ Body content start ============= -->
            <div class="main-content">
                <div class="breadcrumb">
                    <h1>Registar Factura</h1>
                </div>
                <div class="separator-breadcrumb border-top"></div>
                <div class="card">

                    <div class=" card-body ">
                        <form class="ui form" method="post" enctype="multipart/form-data" name="frmFactura" id="frmFactura">
                            <input type="hidden" id="user_id" name="user_id" value="<?= $_SESSION['MM_idUserSgr']; ?>">
                            <div class="row">
                                <div class="col-md-4 form-group mb-3">
                                    <label class="">Fornecedor:<span style="color:red;"> *</span></label>
                                    <select name="fornecedor_id" class="form-control" id="fornecedor_id">
                                        <option value="">--selecione um fornecedor--</option>
                                        <?php
                                        foreach ($comboempresass as $row_comboempresass) {
                                        ?>
                                            <option value="<?php echo $row_comboempresass['id']; ?>"><?php echo $row_comboempresass['nome']; ?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-md-4 form-group mb-3">
                                    <label class="">Numero da Factura:<span style="color:red;"> *</span></label>
                                    <input class="form-control" name="nr_factura" type="text" id="nr_factura" placeholder="Numero do recibo/Factura" />
                                </div>
                                <div class="col-md-4 form-group mb-3">
                                    <label class="">Valor:<span style="color:red;"> *</span></label>
                                    <input class="form-control" name="valorMask" type="text" id="valorMask" data-mask="#.####,###.##" />
                                    <input type="hidden" name="valor" id="valor" />
                                </div>



                                <div class="col-md-4 form-group mb-3">
                                    <label class="">Data da Factura:<span style="color:red;"> *</span></label>
                                    <input class="form-control data1" name="data_factura" value="" type="datetime" id="data_factura" />
                                </div>
                                <div class="col-md-4 form-group mb-3 ">
                                    <label class="">Farmacia:<span style="color:red;"> *</span></label>
                                    <select name="centro_custo_id" id="centro_custo_id" class="form-control">
                                        <option value="">--selecione aqui--</option>
                                        <?php
                                        foreach ($combocentrodecusto as $row_combocentrodecusto) {
                                        ?>
                                            <option value="<?php echo $row_combocentrodecusto['idcentrodecusto'] ?>"><?php echo $row_combocentrodecusto['titulo'] ?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="ui col-md-4 form-group mb-3">
                                    <label class="">Tipo de Factura:<span style="color:red;"> *</span></label>
                                    <select name="tipo_factura_id" id="tipo_factura_id" class="form-control">
                                        <option value="">--selecione aqui--</option>
                                        <?php
                                        foreach ($factura as $row_tipo_factura) {
                                        ?>
                                            <option value="<?php echo $row_tipo_factura['id'] ?>"><?php echo $row_tipo_factura['descricao'] ?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="ui col-md-4 form-group mb-3">
                                    <label class="">Estado:<span style="color:red;"></span></label>
                                    <select name="estado" id="estado" class="form-control">
                                        <option value="1">Aberto</option>
                                        <option value="2">Fechado</option>
                                    </select>
                                </div>
                                <div class="col-md-4 form-group mb-3">
                                    <label class="">Mes:<span style="color:red;"> *</span></label>
                                    <select name="mes" id="mes" class="form-control">
                                        <option value=""></option>
                                        <?php foreach ($meses as $key => $mes) : ?>
                                            <option value="<?= $key ?>"><?= $mes ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="col-md-4 form-group mb-3" hidden>
                                    <label class="">Saldo:<span style="color:red;"> *</span></label>
                                    <input class="uk-input" style="color: black; font-weight: bold;" name="saldo" type="text" id="saldo" readonly />
                                </div>

                                <div class="col-md-4 form-group mb-3">
                                    <label class="">Anexo:</label>
                                    <div class="">
                                        <input class="form-control" type="file" name="anexo_factura" id="anexo_factura" data-show-caption="true" data-msg-placeholder="Selecione um arquivo para carregar..." />
                                    </div>
                                </div>
                            </div>
                            <div class="ui error message"></div>
                            <div style="text-align: end;">
                                <button class="btn btn-primary ripple m-1" form="frmFactura" type="submit">Registar</button>
                            </div>
                            <!-- <button id="registar_factura" type="submit" class="btn uk-button-primary pull-right pointer">REGISTAR</button><br><br> -->
                        </form>


                        <br>
                        <br>

                        <!-- </div> -->
                    </div>
                </div>
                <!-- end of main-content -->
            </div>
            <!-- Footer Start -->
            <div class="flex-grow-1"></div>
        </div>
    </div>

    <!-- ============ Search UI End ============= -->
    <script src="./dist-assets/js/plugins/jquery-3.3.1.min.js"></script>
    <script src="./dist-assets/js/plugins/bootstrap.bundle.min.js"></script>
    <script src="./dist-assets/js/plugins/perfect-scrollbar.min.js"></script>
    <script src="./dist-assets/js/scripts/script.min.js"></script>
    <script src="./dist-assets/js/scripts/sidebar-horizontal.script.js"></script>
    <script src="./dist-assets/flatpickr/dist/flatpickr.min.js"></script>
    <script src="./js/dateformat.js"></script>
    <script src="./dist-assets/fonts/fontawesome/js/all.js"></script>
    <script src="./dist-assets/sweetalert2/dist/sweetalert2.all.js"></script>
    <script src="./js/rgFacturaFornecedor.js"></script>
    <!-- Factura -->
</body>


<!-- Mirrored from demos.ui-lib.com/gull/html/layout3/accordion.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 19 Aug 2021 10:04:12 GMT -->

</html>