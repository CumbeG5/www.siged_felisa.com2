<?php
@session_start();
include "./classes/classes.php";
include('Connections/conexaosiged.php');
require_once("classes/Geral.php");

unset($_SESSION['cart_facturas']);
unset($_SESSION['cart_tipos_pagamentos']);
// print_r($_POST);

$sql_banco = "SELECT * FROM banco";
$query_banco = $conexaosiged->prepare($sql_banco);
$query_banco->execute();

$query_comboempresass = "SELECT * FROM fornecedors";
$comboempresass = $conexaosiged->prepare($query_comboempresass);
$comboempresass->execute();

$query_comboformadepagamento = "SELECT * FROM formadepagamento";
$comboformadepagamento = $conexaosiged->prepare($query_comboformadepagamento);
$comboformadepagamento->execute();

$query_combocentrodecusto = "SELECT * FROM centro_de_custo  LIMIT 10";
$combocentrodecusto = $conexaosiged->prepare($query_combocentrodecusto);
$combocentrodecusto->execute();
?>
<!DOCTYPE html>
<html lang="en" dir="">
<meta http-equiv="content-type" content="text/html;charset=utf-8" />

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width,initial-scale=1" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>SIGED | Registar Pagamento Fornecedor</title>
    <link href="./fonts.googleapis.com/csse27b.css?family=Nunito:300,400,400i,600,700,800,900" rel="stylesheet" />
    <link href="./dist-assets/css/themes/lite-purple.min.css" rel="stylesheet" />
    <link href="./dist-assets/css/plugins/perfect-scrollbar.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="./dist-assets/css/plugins/fontawesome-5.css" />
    <link href="./dist-assets/css/plugins/metisMenu.min.css" rel="stylesheet" />
    <link href="./dist-assets/flatpickr/dist/flatpickr.min.css" rel="stylesheet" />
    <link href="./dist-assets/fonts/fontawesome/css/fontawesome.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="./dist-assets/sweetalert2/dist/sweetalert2.css" />
</head>

<body class="text-left">
    <div class="app-admin-wrap layout-horizontal-bar">
        <?php include_once './topbar.php' ?>
        <!-- =============== Horizontal bar End ================-->
        <div class="main-content-wrap d-flex flex-column">
            <!-- ============ Body content start ============= -->
            <div class="main-content">
                <div class="breadcrumb">
                    <h1>REGISTAR PAGAMENTO</h1>
                </div>
                <div class="separator-breadcrumb border-top"></div>
                <div class="card">

                    <div class="box-body">
                        <div class="card-body">
                            <form class="ui form" method="post" enctype="multipart/form-data" name="frmPagamento" id="frmPagamento">
                                <input type="hidden" id="user_id" name="user_id" value="<?= $_SESSION['MM_idUserSgr']; ?>">
                                <div class="row">
                                    <div class="col-md-3 form-group mb-3">
                                        <label class="">Fornecedor:<span style="color:red;"> *</span></label>
                                        <select name="fornecedor_id_pagamento" class="form-control" id="fornecedor_id_pagamento">
                                            <option value="">--selecione um fornecedor--</option>
                                            <?php
                                            foreach ($comboempresass as $row_comboempresass) {
                                            ?>
                                                <option value="<?php echo $row_comboempresass['id'] ?>"><?php echo $row_comboempresass['nome'] ?></option>
                                            <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-md-3 form-group mb-3">
                                        <label class="">Número de Recibo:<span style="color:red;"> *</span></label>
                                        <input class="form-control" name="nr_recibo" type="text" id="nr_recibo" placeholder="Numero do recibo/Factura" />
                                    </div>
                                    <div class="col-md-3 form-group mb-3">
                                        <label class="">Data do Recibo:<span style="color:red;"> *</span></label>
                                        <input style="background: white;" class="form-control data1" placeholder="yyyy-mm-dd" name="data_recibo" type="date" id="data_recibo" />
                                    </div>
                                    <div class="col-md-3 form-group mb-3">
                                        <label class="">Data do Pagamento:<span style="color:red;"> *</span></label>
                                        <input style="background: white;" class="form-control data1" placeholder="yyyy-mm-dd" name="data" type="date" id="data" />
                                    </div>
                                </div>
                            </form>
                            <br> <br>
                            <div class="box-header">
                                <h2 class="_500">TIPO DE PAGAMENTO</h2>
                            </div>
                            <fieldset>
                                <div class="uk-form-blank">
                                    <form class="ui form frmRgTiposPagamento" method="post" enctype="multipart/form-data" name="frmRgTiposPagamento" id="frmRgTiposPagamento">
                                        <input type="text" id="proveniencia" class="proveniencia" value="fornecedor" hidden>
                                        <!-- <div class="four fields"> -->
                                        <div class="row" id="formapagamento">
                                            <div class="col-md-3 form-group mb-3">
                                                <label class="">Forma de Pagamento:<span style="color:red;"> *</span></label>
                                                <select name="idformapagamento" id="idformapagamento" class="form-control">
                                                    <option value="">Selecione Aqui...</option>
                                                    <?php
                                                    foreach ($comboformadepagamento as $row_comboformadepagamento) {
                                                    ?>
                                                        <option value="<?php echo $row_comboformadepagamento['idformapagamento'] ?>"><?php echo $row_comboformadepagamento['titulo'] ?></option>
                                                    <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-md-3 form-group mb-3" style="display: none">
                                                <label>Banco:<span style="color:red;"> *</span></label>
                                                <select class="form-control" id="tipo_pagamento_id" name="tipo_pagamento_id">
                                                    <option value="">Selecione Aqui...</option>
                                                    <?php
                                                    foreach ($query_banco as $row) {
                                                    ?>
                                                        <option value="<?php echo $row['id'] ?>"><?php echo $row['nome'] ?></option>
                                                    <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-md-3 form-group mb-3" style="display: none;">
                                                <label class="">Número de cheque:</label>
                                                <div class="">
                                                    <input class="form-control" name="numerocheque" type="number" id="numerocheque" />
                                                </div>
                                            </div>
                                            <div id='' class="col-md-3 form-group mb-3" style="display: none">
                                                <label class=''>Caixa:</label>
                                                <div class=''>
                                                    <select name="caixa" id="idcaixa" class="form-control">
                                                        <option value="">Selecione Aqui...</option>
                                                        <?php
                                                        foreach ($combocaixas as $row) {
                                                        ?>
                                                            <option value="<?php echo $row['id'] ?>"><?php echo $row['nome'] ?></option>
                                                        <?php
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-3 form-group mb-3" hidden>
                                                <label class="">Número da Operação:<span style="color:red;"> *</span></label>
                                                <div class="">
                                                    <input name="nr_operacao" type="text" id="nr_operacao" class="form-control" />
                                                </div>
                                            </div>
                                            <div class="col-md-3 form-group mb-3">
                                                <label class="">Valor: <span style="color:red;"> *</span></label>
                                                <div class="">
                                                    <input class="form-control" step="0.01" name="valor_pagamento" type="number" id="valor_pagamento" />
                                                </div>
                                            </div>
                                            <div class="col-md-3 form-group mb-3">
                                                <label class="">Data:</label>
                                                <input style="background: white;" class="form-control data1" placeholder="yyyy-mm-dd" name="data_pagamento_cliente" type="date" id="data_pagamento_cliente" />
                                            </div>
                                            <div class="col-md-3 form-group mb-3 ">
                                                <label class="">Farmacia:<span style="color:red;"> *</span></label>
                                                <select name="centro_custo_id" id="centro_custo_id" class="form-control">
                                                    <option value="">--selecione aqui--</option>
                                                    <?php
                                                    foreach ($combocentrodecusto as $row_combocentrodecusto) {
                                                    ?>
                                                        <option value="<?php echo $row_combocentrodecusto['idcentrodecusto'] ?>"><?php echo $row_combocentrodecusto['titulo'] ?></option>
                                                    <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="field uk-width-1-4@s" hidden>
                                            <label class="">Total:</label>
                                            <input class="form-control" id="tab_total" placeholder="0.00" aria-label="25" readonly>
                                        </div>
                                        <!-- </div> -->
                                        <div style="text-align: right;">
                                            <button id="AdicionarTiposPagamentos" type="submit" form="frmRgTiposPagamento" class="btn btn-primary pull-right pointer">Adicionar</button><br><br>
                                        </div>
                                    </form>
                                </div>
                            </fieldset>
                            <div class="table-responsive" id="tabela_tiposPagamentos"></div>
                            <div class="box-header">
                                <h2 class="_500">REGISTAR FACTURA</h2>
                            </div>
                            <fieldset>
                                <div class="uk-form-blank">
                                    <form class="ui form" method="post" enctype="multipart/form-data" name="frmRgFacturaPagamento" id="frmRgFacturaPagamento">
                                        <input type="hidden" name="totalValorPagamentos" id="totalValorPagamentos" value="" />
                                        <input type="hidden" name="totalValorTiposPagamentos" id="totalValorTiposPagamentos" value="" />
                                        <div class="row">
                                            <div class="col-md-4 form-group mb-3">
                                                <label class="">Factura: <span style="color:red;"> *</span></label>
                                                <select name="factura_id" id="factura_id" class="form-control ">
                                                    <option value="">--selecione--</option>
                                                </select>
                                            </div>
                                            <div class="col-md-4 form-group mb-3">
                                                <label class="">Valor Total: <span style="color:red;"> *</span></label>
                                                <input style="background: white;" class="form-control" type="number" name="valor_total" id="valor_total" readonly />
                                            </div>
                                            <div class="col-md-4 form-group mb-3" hidden>
                                                <label class="">Desconto:</label>
                                                <input class="form-control" name="desconto" id="desconto" readonly />
                                            </div>
                                            <div class="col-md-4 form-group mb-3" hidden>
                                                <label class="">Valor T.Pagamento:</label>
                                                <input class="form-control" name="total_pagamento" id="total_pagamento" readonly />
                                            </div>
                                            <div class="col-md-4 form-group mb-3">
                                                <label class="">Valor Pago da Factura:</label>
                                                <input style="background: white;" class="form-control" name="total_pago" id="total_pago" readonly />
                                            </div>

                                            <div class="col-md-4 form-group mb-3">
                                                <label class="">Valor Pendente:</label>
                                                <input style="background: white;" class="form-control" name="total" id="total" readonly />
                                            </div>
                                            <div class="col-md-4 form-group mb-3 valor">
                                                <label class="">Valor a pagar: <span style="color:red;"> *</span></label>
                                                <input hidden name="tipo_remanescente" id="tipo_remanescente" type="text">
                                                <input class="form-control" step="0.01" name="remanescente" type="number" id="remanescente" />
                                            </div>
                                            <div class="col-md-4 form-group mb-3" hidden>
                                                <label class="">Tipo Factura:</label>
                                                <input class="form-control" name="tipo_factura" id="tipo_factura" readonly />
                                            </div>
                                        </div>
                                        <div style="text-align: right;">
                                            <button id="AdicionarPagamento" type="submit" form="frmRgFacturaPagamento" class="btn uk-button-primary pull-right pointer">Adicionar</button><br><br>
                                        </div>
                                    </form>
                                </div>
                            </fieldset>
                            <div class="table-responsive" id="tabela_facturas"></div>
                            <!-- <input type="hidden" name="MM_insert" value="form1" /> -->
                            <div style="text-align: right;">
                                <button id="registar" type="submit" form="frmPagamento" class="btn uk-button-primary pull-right pointer">REGISTAR</button><br><br>
                            </div>
                        </div>
                    </div </div>
                    <!-- end of main-content -->
                </div>
                <!-- Footer Start -->
                <div class="flex-grow-1"></div>
            </div>
        </div>

        <!-- ============ Search UI End ============= -->
        <script src="./dist-assets/js/plugins/jquery-3.3.1.min.js"></script>
        <script src="./dist-assets/js/plugins/bootstrap.bundle.min.js"></script>
        <script src="./dist-assets/js/plugins/perfect-scrollbar.min.js"></script>
        <script src="./dist-assets/js/scripts/script.min.js"></script>
        <script src="./dist-assets/js/scripts/sidebar-horizontal.script.js"></script>
        <script src="./dist-assets/flatpickr/dist/flatpickr.min.js"></script>
        <script src="./js/dateformat.js"></script>
        <script src="./dist-assets/fonts/fontawesome/js/all.js"></script>
        <script src="./dist-assets/sweetalert2/dist/sweetalert2.all.js"></script>
        <script src="./js/rgPagamentoFornecedor.js"></script>
        <!-- Factura -->
</body>


<!-- Mirrored from demos.ui-lib.com/gull/html/layout3/accordion.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 19 Aug 2021 10:04:12 GMT -->

</html>