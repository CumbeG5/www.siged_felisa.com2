<?php
@session_start();
// session_destroy();
// session_unset();

// print_r($_SESSION['cart_tipos_pagamentos']);

if (empty($_SESSION['cart_tipos_pagamentos'])) {
    $_SESSION['cart_tipos_pagamentos'] = [];
}


if (count($_SESSION['cart_tipos_pagamentos']) > 0) {
    if (!empty($_SESSION['cart_tipos_pagamentos'])) {
        // print_r($_SESSION['cart_tipos_pagamentos']);
?>
        <table class="table table-striped table-bordered" style="width: 50%;">
            <thead>
                <tr class="text-center">
                    <th>#</th>
                    <th>Forma de Pagamento</th>
                    <th>Banco</th>
                    <th>Nr. Operação</th>
                    <th>Nr. Cheque</th>
                    <th>Nr. documento</th>
                    <th>Caixa</th>
                    <!-- <th>Farmacia</th> -->
                    <th>Data Pagamento</th>
                    <th>Valor </th>
                </tr>
            </thead>
            <tbody>
                <?php $contar = 1;
                $total = 0;
                foreach ($_SESSION['cart_tipos_pagamentos'] as $key => $value) {
                ?>
                    <tr>
                        <td><?= $contar++; ?></td>
                        <td><?= $value['idformapagamento_text'] ?></td>
                        <td><?= $value['tipo_pagamento_text'] ?></td>
                        <td><?= $value['nr_operacao'] ?></td>
                        <td class="text-right"><?= $value['numerocheque'] ?></td>
                        <td class="text-right"><?= isset($value['numerodocumento']) ? $value['numerodocumento'] : "" ?></td>
                        <td class="text-right"><?= $value['caixa_text'] ?></td>
                        <!-- <td class="text-right"><?= $value['farmacia'] ?></td> -->
                        <td class="text-right"><?= $value['data_pagamento'] ?></td>
                        <td class="text-right"><?= number_format($value['valor'], 2, ",", ".") ?></td>
                        <td class="text-center">
                            <button value="<?= $key ?>" type="button" class="btn btn-danger remove_tipoPagamento pointer">
                                <i class="ui remove icon"></i>
                            </button>
                        </td>
                    </tr>
                <?php
                    $total += $value['valor'];
                }
                ?>
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="5"><input hidden id="rg_tipo_pagamentos" value="<?php echo $total ?>" type="number"></td>
                    <td><strong>Total: </strong></td>
                    <td class="text-right"><strong><?= number_format($total, 2, ",", ".") ?></strong></td>
                    <!-- <input type="hidden" id="TotalTipos" value="<?= $total ?>"> -->
                    <td></td>
                    <input type="hidden" id="total_tab" name="total_tab" value="<?= $total ?>">
                </tr>
            </tfoot>
        </table>
    <?php } else { ?>
        <div class="alert alert-info text-center">
            Nenhum registo encontrado!
        </div>
    <?php } ?>
<?php } else { ?>
    <div class="alert alert-info text-center">
        Nenhum registo encontrado!
    </div>
<?php } ?>

<script>
    $(function() {
        $(".remove_tipoPagamento").on('click', function() {
            var id = $(this).val();
            Swal.fire({
                icon: 'warning',
                title: 'Tem certeza que deseja eliminar esse pagamento?',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'Não',
                confirmButtonText: 'Sim,apague!',
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: "./sessao_facturas/remove_tipoPagamento.php",
                        method: "POST",
                        data: {
                            'id': id,
                        },
                        dataType: 'json',
                        success: function(data) {
                            Swal.fire({
                                icon: 'success',
                                title: 'Pagamento removido com sucesso!',
                                showConfirmButton: false,
                                timer: 1500
                            }).then((e) => {
                                listarTiposPagamento();
                            })
                            // alert("OK")
                        },
                        error: function(err) {
                            alert("Error")
                            console.log(err)
                        }
                    })
                }
            })

        });

        function listarTiposPagamento() {
            $.ajax({
                url: "./sessao_facturas/fetch_tiposPagamentos.php",
                method: "GET",
                data: {},
                dataType: 'html',
                success: function(data) {
                    $("#tabela_tiposPagamentos").html(data)
                },
                error: function(err) {
                    // alert("Error")
                    console.log(err)
                }
            });
        }
    })
</script>