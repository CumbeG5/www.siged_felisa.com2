<?php
include_once "../API/callAPI.php";
include_once "../API/include.php";

// print_r($_POST);

$response = CallApi($URL . 'api/factura_cliente/select_all.php?factura_global_id=' . (isset($_GET['factura_global_id']) ? $_GET['factura_global_id'] : 0), $METHOD = "GET");

if (count($response->data) > 0) {
    if (!empty($response->data)) {
        // print_r($_SESSION['cart_facturas']);
?>
        <div class="table-responsive mt-4">
            <table class="table table-sm table-bordered table-striped table-hover">
                <thead>
                    <tr class="text-center">
                        <th>#</th>
                        <th>Nome do Segurado</th>
                        <th>Número de Membro</th>
                        <th>Número da Guia</th>
                        <th>Codigo da Autorização</th>
                        <th>Nome do Tecnico</th>
                        <th>Valor Liquido</th>
                        <th>Data da Factura</th>
                        <th>Anexo</th>
                        <th>Acções</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $contar = 1;
                    $total = 0;
                    $ord = 1;
                    foreach ($response->data as $key => $value) {
                    ?>
                        <tr>
                            <td><?= $ord++; ?></td>
                            <td class="td_nome_segurado" factura_id="<?= $value->factura_global_id ?>" id_guia="<?= $value->id ?>" numerador="<?= $contar ?>" style="text-align: right"><input type="text" nome_segurado_anterior="<?= $value->nome_segurado ?>" class="form-control input_nome_segurado input_nome_segurado<?= $contar ?> " name="nome_segurado1[]" value="<?= $value->nome_segurado ?>" width="5" hidden><span class="span_nome_segurado<?= $contar ?>"><?= $value->nome_segurado ?></span></td>
                            <td class="td_nr_membro" factura_id="<?= $value->factura_global_id ?>" id_guia="<?= $value->id ?>" numerador="<?= $contar ?>" style="text-align: right"><input type="text" nr_membro_anterior="<?= $value->nr_membro ?>" class="form-control input_nr_membro input_nr_membro<?= $contar ?> " name="nr_membro1[]" value="<?= $value->nr_membro ?>" width="5" hidden><span class="span_nr_membro<?= $contar ?>"><?= $value->nr_membro ?></span></td>
                            <td class="td_nr_factura" factura_id="<?= $value->factura_global_id ?>" id_guia="<?= $value->id ?>" numerador="<?= $contar ?>" style="text-align: right"><input type="text" nr_factura_anterior="<?= $value->nr_factura ?>" class="form-control input_nr_factura input_nr_factura<?= $contar ?> " name="nr_factura1[]" value="<?= $value->nr_factura ?>" width="5" hidden><span class="span_nr_factura<?= $contar ?>"><?= $value->nr_factura ?></span></td>
                            <td class="td_codigo_autorizacao" factura_id="<?= $value->factura_global_id ?>" id_guia="<?= $value->id ?>" numerador="<?= $contar ?>" style="text-align: right"><input type="text" codigo_autorizacao_anterior="<?= $value->codigo_autorizacao ?>" class="form-control input_codigo_autorizacao input_codigo_autorizacao<?= $contar ?> " name="codigo_autorizacao1[]" value="<?= $value->codigo_autorizacao ?>" width="5" hidden><span class="span_codigo_autorizacao<?= $contar ?>"><?= $value->codigo_autorizacao ?></span></td>
                            <td class="td_nome_tecnico"> <?= $value->nome_do_tecnico ?> </td>
                            <td class="td_valor" factura_id="<?= $value->factura_global_id ?>" id_guia="<?= $value->id ?>" numerador="<?= $contar ?>" style="text-align: right"><input type="text" valor_anterior="<?= $value->valor ?>" class="form-control input_valor input_valor<?= $contar ?> " name="valor1[]" value="<?= $value->valor ?>" width="5" hidden><span class="span_valor<?= $contar ?>"><?= number_format($value->valor, 2, ".", ",") ?></span></td>
                            <td class="td_data" factura_id="<?= $value->factura_global_id ?>" id_guia="<?= $value->id ?>" numerador="<?= $contar ?>" style="text-align: right"><input type="datetime" data_anterior="<?= $value->data ?>" class="flatpickr ui input uk-input form-control input_data input_data<?= $contar ?>" name="data1[]" value="<?= $value->data ?>" width="5" hidden><span class="span_data<?= $contar++ ?>"><?= $value->data ?></span></td>
                            <td>
                                <a href="/www.siged_felisa.com2/<?= $value->anexo ?>" target="_blank"><?php $value->anexo ? print("anexo") : print("sem anexo") ?></a>
                            </td>
                            <td class="text-center">
                                <button value="<?= $value->id ?>" factura_id="<?= $value->factura_global_id ?>" type="button" class="btn btn-danger remove_guia pointer">
                                    <i class="fa-solid fa-x" style="color: #ffffff;"></i>
                                </button>
                            </td>
                        </tr>
                    <?php
                        $total += $value->valor;
                    }
                    ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="5"></td>
                        <td class="text-right"><strong>Total:</strong></td>
                        <td style="text-align: right;">
                            <input type="hidden" id="fetchTotal" name="fetchTotal" value="<?= $total ?>" />
                            <?= number_format($total, 2, ",", ".") ?>
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>
    <?php } else { ?>
        <div class="col-md-12 alert alert-info text-center">
            Nenhum registo encontrado!
        </div>
    <?php } ?>
<?php } else { ?>
    <div class="col-md-12 alert alert-info text-center">
        Nenhum registo encontrado!
    </div>
<?php } ?>

<script>
    $(document).ready(function() {
        function listarFacturasUpdate() {
            $.ajax({
                url: "./sessao_facturas/fetch_facturasUpdate.php",
                method: "GET",
                data: {},
                dataType: 'html',
                success: function(data) {
                    $("#tabela_facturasUpdate").html(data)
                },
                error: function(err) {
                    console.log({
                        err
                    });
                }
            });
        }
    })
</script>