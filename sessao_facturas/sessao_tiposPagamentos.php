<?php
@session_start();
// session_destroy();
// session_unset();

// print_r($_SESSION['cart_tipos_pagamentos'][0]);

$json['success'] = false;
$json['error'] = false;
$json['message'] = null;

function in_multiarray($elem, $array,$field)
{
    $top = sizeof($array) - 1;
    $bottom = 0;
    while($bottom <= $top)
    {
        if($array[$bottom][$field] == $elem)
            return true;
        else
            if(is_array($array[$bottom][$field]))
                if(in_multiarray($elem, ($array[$bottom][$field])))
                    return true;

        $bottom++;
    }
    return false;
}


if (empty($_SESSION['cart_tipos_pagamentos'])) {
    $_SESSION['cart_tipos_pagamentos'] = [];
}
$_POST['caixa_text'] = empty($_POST['caixa']) ? "" : $_POST['caixa_text'];
$_POST['tipo_pagamento_text'] = empty($_POST['tipo_pagamento_id']) ? "" : $_POST['tipo_pagamento_text'];
if(in_multiarray($_POST['nr_operacao'], $_SESSION['cart_tipos_pagamentos'],'nr_operacao')) {
    $json = array(
        'message' => 'Falha ao criar o registo!',
        'description' => 'Não pode adicionar o mesmo número da operação!',
        'Status' => 500,
        'status_text' => 'error'
        );
    // $json['error'] = true;
    // $json['message'] = "";

} else {
    if (array_push($_SESSION['cart_tipos_pagamentos'], $_POST)){
        $json['success'] = true;
        $json['message'] = "Adicionado com sucesso";
    }else{
        $json['error'] = true;
        $json['message'] = "Houve um erro ao tentar adicionar.";
    }
}

echo json_encode($json);
?>