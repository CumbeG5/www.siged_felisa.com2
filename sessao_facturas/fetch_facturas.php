<?php
@session_start();
// session_destroy();
// session_unset();

// print_r($_SESSION['cart_facturas']);

if (empty($_SESSION['cart_facturas'])) {
    $_SESSION['cart_facturas'] = [];
}


if (count($_SESSION['cart_facturas']) > 0) {
    if (!empty($_SESSION['cart_facturas'])) {
        // print_r($_SESSION['cart_facturas']);
?>
        <table class="table table-striped table-bordered" style="width: 100%;">
            <thead>
                <tr class="text-center">
                    <th>#</th>
                    <th>Factura</th>
                    <th>Valor Factura</th>
                    <th>Valor Pago</th>
                    <!-- <th>Desconto</th> -->
                    <th>Total</th>
                    <th>Pendente</th>
                    <th>Acções</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $contar = 1;
                $total = 0;
                $total_factura = 0;
                $total_pendente = 0;
                $total_nvc = 0;
                $total_vfa = 0;
                $total_desconto = 0;
                foreach ($_SESSION['cart_facturas'] as $key => $value) {
                    $desconto = $value['remanescente'];
                    if ($value['tipo_factura'] == 1) {
                        $total_vfa += $desconto;
                    } else if ($value['tipo_factura'] == 2) {
                        $total_nvc += $desconto;
                    }

                    if ($value['tipo_factura'] == 1) {
                        $remanescente = $value['remanescente'];
                        $valor = $value['valor'];
                        $saldo = $value['saldo'];
                    } else if ($value['tipo_factura'] == 2) {
                        $remanescente = $value['remanescente'];
                        $valor = $value['valor'] * (-1);
                        $saldo = $value['saldo'] * (-1);
                    }

                    $total_factura += $valor;
                    // $total_desconto += $value['desconto'];
                    $total += $remanescente;
                    $total_pendente += $saldo;
                ?>
                    <tr>
                        <td><?= $contar++; ?></td>
                        <td><?= $value['factura_text'] ?></td>
                        <td class="text-right"><?= number_format($valor, 2, ",", ".") ?></td>
                        <td class="text-right"><?= number_format($remanescente, 2, ",", ".") ?></td>
                        <!-- <td class="text-right">< //number_format($value['desconto'], 2, ",", ".") ?></td> -->
                        <td class="text-right"><?= number_format($desconto, 2, ",", ".") ?></td>
                        <td class="text-right"><?= number_format($saldo, 2, ",", ".") ?></td>
                        <td class="text-center">
                            <button value="<?= $key ?>" type="button" class="btn btn-danger remove_factura pointer">
                                Remover
                                <i class="nav-icon i-Close-Window font-weight-bold"></i>
                            </button>
                        </td>
                    </tr>
                <?php
                }
                $total_pago = (float)$total_vfa + (float)$total_nvc;
                // echo("Total F: " .$total_vfa);
                // print_r("Total N: "  .$total_nvc);
                ?>
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="1"><input hidden id="factura_fornecedor_total" value="<?php echo $total_pago ?>" type="number"></td>
                    <td class="text-right"><b>Total:</b></td>
                    <td class="text-right"><strong><?= number_format($total_factura, 2, ",", ".") ?></strong></td>
                    <td class="text-right"><strong><?= number_format($total, 2, ",", ".") ?></strong></td>
                    <!-- <td class="text-right"><strong><//number_format($total_desconto, 2, ",", ".") ?></strong></td> -->
                    <td class="text-right"><strong><?= number_format($total_pago, 2, ",", ".") ?> </strong></td>
                    <td class="text-right"><strong><?= number_format($total_pendente, 2, ",", ".") ?> </strong></td>
                    <input type="hidden" id="Pagou" name="Pagou" value="<?= $total ?>" />
                </tr>
            </tfoot>
        </table>
    <?php } else { ?>
        <div class="alert alert-info text-center">
            Nenhum registo encontrado!
        </div>
    <?php } ?>
<?php } else { ?>
    <div class="alert alert-info text-center">
        Nenhum registo encontrado!
    </div>
<?php } ?>