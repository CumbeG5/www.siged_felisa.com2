<div class="main-header">
    <div style="width:7rem; height: auto;" class=""><img src="./dist-assets/images/fme.png" alt="" /></div>
    <!-- <h2>SISTEMA DE GESTÃO DE DESPESAS</h2> -->
    <div class="menu-toggle">
        <div></div>
        <div></div>
        <div></div>
    </div>
    <div style="margin-left: 15px;" class="d-flex align-items-center">
        <!-- Mega menu-->
        <div class="dropdown mega-menu d-none d-md-block">
            SISTEMA DE GESTÃO DE DESPESAS
        </div>
        <!-- / Mega menu-->
        <div style="margin-left: 15px;" class="search-bar">
            <input type="text" placeholder="Search" /><i class="search-icon text-muted i-Magnifi-Glass1"></i>
        </div>
    </div>
    <div style="margin: auto"></div>
    <div class="header-part-right">
        <div class="dropdown">
            <div class="user col align-self-end"><img id="userDropdown" src="./dist-assets/images/user.png" alt="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" />
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                    <div class="dropdown-header">
                        <i class="i-Lock-User mr-1"></i> Timothy Carlson
                    </div>
                    <a class="dropdown-item">Account settings</a>
                    <a class="dropdown-item" href="signin.html">Sign out</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- header top menu end-->
<div class="horizontal-bar-wrap">
    <div class="header-topnav">
        <div class="container-fluid">
            <div class="topnav rtl-ps-none" id="" data-perfect-scrollbar="" data-suppress-scroll-x="true">
                <ul class="menu float-left">
                    <li>
                        <div>
                            <div>
                                <label class="toggle" for="drop-2">Dashboard</label><a href="#"><i class="nav-icon mr-2 i-Bar-Chart"></i> Dashboard</a>
                                <input id="drop-2" type="checkbox" />
                                <ul>
                                    <li><a href="dashboard1.html"><i class="nav-icon mr-2 i-Clock-3"></i><span class="item-name">Listar</span></a></li>
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div>
                            <div>
                                <label class="toggle" for="drop-2">Despesas</label><a href="#"><i class="nav-icon mr-2 i-Suitcase"></i> Despesas</a>
                                <input id="drop-2" type="checkbox" />
                                <ul>
                                    <!-- <li><a href="alerts.html"><i class="nav-icon mr-2 i-Add-File"></i><span class="item-name">Registar</span></a></li> -->
                                    <li><a href="listDespesas.php"><i class="nav-icon mr-2 i-File-Clipboard-File--Text"></i><span class="item-name">Despesas</span></a></li>
                                    <li><a href="badges.html"><i class="nav-icon mr-2 i-Folders"></i><span class="item-name">Resumo <br> Grupo-despesa</span></a></li>
                                </ul>
                            </div>
                        </div>
                    </li>
                    <!-- end ui kits-->
                    <li>
                        <div>
                            <div>
                                <label class="toggle" for="drop-2">Fornecedores</label><a href="#"><i class="nav-icon i-Library mr-2"></i> Fornecedores</a>
                                <input id="drop-2" type="checkbox" />
                                <ul>
                                    <li><a href="listFacturaFornecedor.php"><i class="nav-icon mr-2 i-File-Clipboard-File--Text"></i><span class="item-name">Factura</span></a></li>
                                    <li><a href="listPagamentosFornecedor.php"><i class="nav-icon mr-2 i-Money-2"></i><span class="item-name">Pagamento</span></a></li>
                                </ul>
                            </div>
                        </div>
                    </li>
                    <!-- end extra uikits-->
                    <li>
                        <div>
                            <div>
                                <label class="toggle" for="drop-2">Seguradoras/Empresas</label><a href="#"><i class="nav-icon mr-2 i-Computer-Secure"></i> Seguradoras/Empresas</a>
                                <input id="drop-2" type="checkbox" />
                                <ul>
                                    <li><a href="listFacturaSeguradora.php"><i class="nav-icon mr-2 i-File-Clipboard-File--Text"></i><span class="item-name">Factura</span></a></li>
                                    <li><a href="listPagamentosSeguradora.php"><i class="nav-icon mr-2 i-Money-2"></i><span class="item-name">Pagamento</span></a></li>
                                    <li><a href="inbox.html"><i class="nav-icon mr-2 i-Money-2"></i><span class="item-name">Nota de Crédito</span></a></li>
                                    <li><a href="inbox.html"><i class="nav-icon mr-2 i-Money-2"></i><span class="item-name">Técnico</span></a></li>
                                </ul>
                            </div>
                        </div>
                    </li>
                    <!-- end apps-->
                    <li>
                        <div>
                            <div>
                                <label class="toggle" for="drop-2">Parâmetros</label><a href="#"><i class="nav-icon mr-2 i-File-Clipboard-File--Text"></i> Parâmetros</a>
                                <input id="drop-2" type="checkbox" />
                                <ul>
                                    <li><a href="form.basic.html"><i class="nav-icon mr-2 i-File-Clipboard-Text--Image"></i><span class="item-name">Fornecedores</span></a></li>
                                    <li><a href="form.layouts.html"><i class="nav-icon mr-2 i-Split-Vertical"></i><span class="item-name">Grupo Despesas</span></a></li>
                                    <li><a href="form.input.group.html"><i class="nav-icon mr-2 i-Receipt-4"></i><span class="item-name">Centro Custos</span></a></li>
                                    <li><a href="form.validation.html"><i class="nav-icon mr-2 i-Close-Window"></i><span class="item-name">Forma Pagamento</span></a></li>
                                    <li><a href="smart.wizard.html"><i class="nav-icon mr-2 i-Width-Window"></i><span class="item-name">Unidade</span></a></li>
                                    <li><a href="tag.input.html"><i class="nav-icon mr-2 i-Tag-2"></i><span class="item-name">Caixas</span></a></li>
                                    <li><a href="editor.html"><i class="nav-icon mr-2 i-Pen-2"></i><span class="item-name">Técnico</span></a></li>
                                </ul>
                            </div>
                        </div>
                    </li>
                    <!-- end Forms-->

                </ul>
            </div>
        </div>
    </div>
</div>