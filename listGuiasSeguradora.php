<?php
@session_start();
include "./classes/classes.php";
include('Connections/conexaosiged.php');
require_once("classes/Geral.php");

?>
<!DOCTYPE html>
<html lang="en" dir="">
<meta http-equiv="content-type" content="text/html;charset=utf-8" />

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width,initial-scale=1" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>SIGED | Listar Guias</title>
    <!-- <link href="./fonts.googleapis.com/csse27b.css?family=Nunito:300,400,400i,600,700,800,900" rel="stylesheet" /> -->
    <link href="./dist-assets/css/themes/lite-purple.min.css" rel="stylesheet" />
    <link href="./dist-assets/css/plugins/perfect-scrollbar.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="./dist-assets/css/plugins/fontawesome-5.css" />
    <link href="./dist-assets/css/plugins/metisMenu.min.css" rel="stylesheet" />
    <link href="./dist-assets/flatpickr/dist/flatpickr.min.css" rel="stylesheet" />
    <link href="./dist-assets/fonts/fontawesome/css/fontawesome.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="./dist-assets/sweetalert2/dist/sweetalert2.css" />
</head>

<body class="text-left">
    <div class="app-admin-wrap layout-horizontal-bar">
        <?php include_once './topbar.php' ?>
        <!-- =============== Horizontal bar End ================-->
        <div class="main-content-wrap d-flex flex-column">
            <!-- ============ Body content start ============= -->
            <div class="main-content">
                <div class="breadcrumb">
                    <h1>Listagem Das Guias Pendentes</h1>
                </div>
                <div class="separator-breadcrumb border-top"></div>
                <div class="card">

                    <div class=" card-body ">
                        <div class="row">
                            <input hidden id="id_guia" value="<?= $_GET["id"] ?>" type="text">
                            <div class="col-md-3 form-group mb-3">
                                <label>Número da Guia:</label>
                                <input name="nr_factura" value="" type="text" id="nr_factura" class="search form-control" />
                            </div>
                            <div class="col-md-3 form-group mb-3">
                                <label>Data Inicio:</label>
                                <input placeholder="yyyy-mm-dd" style="background: white !important;" name="data_inicio" value="" type="datetime" id="data_inicio" class="data1 search form-control" />
                            </div>
                            <div class="col-md-3 form-group mb-3">
                                <label>Data Fim:</label>
                                <input placeholder="yyyy-mm-dd" style="background: white !important;" name="data_fim" value="" type="datetime" id="data_fim" class="data1 search form-control" />
                            </div>
                            <div class="col-md-3 form-group mb-3">
                                <label>Valor:</label>
                                <input name="valor" value="" type="number" id="valor" class="form-control search" />
                            </div>
                        </div>
                        <br>

                        <br>
                        <!-- Tabela -->
                        <div class="table-factura-guias table-responsive">

                        </div>
                        <!-- </div> -->
                    </div>
                </div>
                <!-- end of main-content -->
            </div>
            <div class="flex-grow-1"></div>
        </div>
    </div>

    <script src="./dist-assets/js/plugins/jquery-3.3.1.min.js"></script>
    <script src="./dist-assets/js/plugins/bootstrap.bundle.min.js"></script>
    <script src="./dist-assets/js/plugins/perfect-scrollbar.min.js"></script>
    <script src="./dist-assets/js/scripts/script.min.js"></script>
    <script src="./dist-assets/js/scripts/sidebar-horizontal.script.js"></script>
    <script src="./dist-assets/flatpickr/dist/flatpickr.min.js"></script>
    <script src="./js/dateformat.js"></script>
    <script src="./dist-assets/fonts/fontawesome/js/all.js"></script>
    <script src="./dist-assets/sweetalert2/dist/sweetalert2.all.js"></script>

    <!-- Factura -->
    <script src="./js/listar_guias.js"></script>
    <!-- pdf js -->
    <script src="./class/pdf/js/VentanaCentrada.js"></script>
    <!-- pdf js -->
    <script src="./class/pdf/js/pdf.js"></script>
</body>


<!-- Mirrored from demos.ui-lib.com/gull/html/layout3/accordion.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 19 Aug 2021 10:04:12 GMT -->

</html>