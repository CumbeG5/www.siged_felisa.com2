-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Mar 14, 2023 at 07:30 AM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sigedfarelisa`
--

-- --------------------------------------------------------

--
-- Table structure for table `banco`
--

CREATE TABLE `banco` (
  `id` int(11) NOT NULL,
  `nome` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `banco`
--

INSERT INTO `banco` (`id`, `nome`) VALUES
(1, 'BCI'),
(2, 'Millenium Bim'),
(3, 'Mozabanco'),
(4, 'MPesa'),
(5, 'Mkesh'),
(6, 'E-Mola'),
(7, 'Conta Móvel');

-- --------------------------------------------------------

--
-- Table structure for table `caixa`
--

CREATE TABLE `caixa` (
  `data_cadastro` datetime NOT NULL,
  `banco` int(11) NOT NULL,
  `nrCheque` double NOT NULL,
  `id` int(11) NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT 1,
  `valor` double NOT NULL,
  `data_cheque` date NOT NULL,
  `nome` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `caixa`
--

INSERT INTO `caixa` (`data_cadastro`, `banco`, `nrCheque`, `id`, `estado`, `valor`, `data_cheque`, `nome`) VALUES
('2022-10-06 08:22:39', 4, 1, 1, 1, 200, '2022-08-10', 'Farmácia Maria Elisa, EI');

-- --------------------------------------------------------

--
-- Table structure for table `centro_de_custo`
--

CREATE TABLE `centro_de_custo` (
  `idcentrodecusto` int(2) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `descricao` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `centro_de_custo`
--

INSERT INTO `centro_de_custo` (`idcentrodecusto`, `titulo`, `descricao`) VALUES
(1, 'TODOS', 'TODOS'),
(2, 'F.M.E KENNETH KAUNDA', 'F.M.E. KENNETH KAUNDA'),
(3, 'F.M.E BEACH FRONT', 'F.M.E BEACH FRONT'),
(4, 'F.M.E MACUTI', 'F.M.E MACUTI'),
(10, 'F.M.E PONTA GEA', 'F.M.E PONTA GEA'),
(12, 'F.M.E CRISTAL BOX', 'F.M.E CRISTAL BOX'),
(13, 'F.M.E REAL MACURUNGO', 'F.M.E REAL MACURUNGO'),
(14, 'F.M.E ACADEMICA', 'F.M.E ACADEMICA'),
(15, 'F.M.E PASSAGEM DE NIVEL', 'F.M.E PASSAGEM DE NIVEL'),
(16, 'F.M.E MUALY', 'F.M.E MUALY'),
(17, 'Marco Aurelio Pinto Monteiro', 'Marco Aurelio Pinto Monteiro'),
(18, 'Anny Shirley do Carmo Mausse', 'Anny Shirley do Carmo Mausse'),
(19, 'Residencia Sr. Marco Aurelio', 'Residencia Sr. Marco Aurelio');

-- --------------------------------------------------------

--
-- Table structure for table `cliente`
--

CREATE TABLE `cliente` (
  `id` int(11) NOT NULL,
  `nome` varchar(50) NOT NULL,
  `apelido` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `cliente`
--

INSERT INTO `cliente` (`id`, `nome`, `apelido`, `created_at`, `updated_at`) VALUES
(1, 'Rafik', 'Macou', '2023-03-02 12:31:38', '0000-00-00 00:00:00'),
(2, 'Rafi', 'G', '2023-03-02 12:34:00', '0000-00-00 00:00:00'),
(3, 'Ababug', 'Fakir', '2023-03-03 17:00:17', '0000-00-00 00:00:00'),
(4, 'jhdsa', 'sdas', '2023-03-03 17:25:23', '2023-03-13 10:14:00'),
(5, '', '', '2023-03-03 17:25:53', '0000-00-00 00:00:00'),
(6, 'jhas', 'aa', '2023-03-12 21:43:44', '0000-00-00 00:00:00'),
(7, 'Ababug', 'G', '2023-03-12 22:09:48', '0000-00-00 00:00:00'),
(8, 'Ababug', 'G', '2023-03-12 22:09:51', '0000-00-00 00:00:00'),
(9, 'Ababug', 'G', '2023-03-12 22:09:59', '0000-00-00 00:00:00'),
(10, 'F', 'K', '2023-03-12 22:11:56', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `empresas`
--

CREATE TABLE `empresas` (
  `idempresa` int(3) NOT NULL,
  `descricaoempresa` varchar(255) NOT NULL,
  `cidade` varchar(255) NOT NULL,
  `Nuit` int(12) NOT NULL,
  `localizacao` varchar(255) NOT NULL,
  `contacto` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `empresas`
--

INSERT INTO `empresas` (`idempresa`, `descricaoempresa`, `cidade`, `Nuit`, `localizacao`, `contacto`, `email`) VALUES
(1, 'ACSUN - Academia, Consultoria & Serviços Universo ', 'Maputo', 5310, 'Distrito de Marracuente, Bairro Zinthava, Q2A Nº. 39, próximo à Facim', '844370150', 'info@acsun.co.mz'),
(2, 'CA INNOVATION & MARKETING', 'Maputo', 5310, 'Rua Armando Tivane', '843220563', 'albasinirogerio@gmail.com'),
(3, 'Tmcel - Moçambique Telecom, SA', 'Maputo', 5310, 'Rua Belmiro Obadias Muianga, Nº 384', '821351100', 'corporate@tmcel.mz'),
(4, 'Vodacom', 'Maputo', 5310, 'AV EDUARDO MONDLANE NUMERO 1441', '84111', 'vodacom@gmail.com'),
(5, 'Movitel S.A', 'Maputo', 5310, 'Av. Guerra Popular, 1086 Maputo, Mozambique', '100', 'movitel@gmail.com'),
(6, 'A.J.A Consultantes Mocambique, Lda', 'Cidade Maputo', 400057249, 'Praca da OMM', '829551617', 'diana.timnana@gmail.com'),
(7, 'Donald Txopela', 'Maputo Cidade', 11111, 'Costa do Sol', '877742605', 'hiltoninaciojarro7500@gmail.com'),
(8, 'Seguradora Internacional de Moçambique ', 'Maputo', 2147483647, 'Sede Rua dos Desportistas, Nr.873-879', '21354500', 'hiltoninaciojarro7500@gmail.com'),
(9, 'Estacão de Serviços Rola, Grupo Vali Manica, LDA', 'Beira', 400502994, 'Rua Major Serpa Pinto', '841111111', 'naotem@email'),
(10, 'City Link', 'Maputo', 400131546, 'Rua Joao Mulungo. Nr.481', '846068783', 'naotem@email'),
(11, 'Txopela (Genérico)', 'Maputo Cidade', 11111, 'Maputo Cidade', '123456', 'naotem@email'),
(12, 'Posto de Abastecimento (Combustível)', 'Maputo Cidade', 1111111, 'Maputo Cidade', '123456', 'naotem@email'),
(13, 'Fundo de Maneio Farmácia Maria Elisa', 'Maputo Cidade', 1111111, 'Maputo Cidade', '82111111', 'naotem@email'),
(14, 'Manutenção (Eletricidade)', 'Maputo Cidade', 1111111, 'Maputo Cidade', '82111111', 'hiltoninaciojarro7500@gmail.com'),
(15, 'Outras Despesas', 'Maputo Cidade', 1111111, 'Maputo Cidade', '123456', 'hiltoninaciojarro7500@gmail.com'),
(16, 'Electro Ferragem', 'Maputo Cidade', 400545865, 'Avenida Emilia Dausse', '82458698', 'naotem@email'),
(17, 'Eletricidade de Moçambique ', 'Maputo Cidade', 600000063, 'Avenida agostinho Neto Nr. 70', '82111111', 'naotem@email'),
(18, 'Supermercado Primavera, Lda', 'Maputo', 400320284, 'Sommarshield', '847199116', 'naotemdados@gmail.com'),
(19, 'Movitel, S.A', 'Maputo', 400268177, 'Avenida Guerra Popular', '00258100', 'cc@movitel.co.mz'),
(20, 'Maputo Copy, Lda', 'Maputo Cidade', 400409387, 'Avenida Eduardo Mondlane Nr. 40 R/C', '873856402', 'naotemadados1@gmail.com'),
(21, 'Vhonani B.S', 'Maputo Cidade', 103846224, 'Avenida Acordos de Lusaka Nr 1883', '873856427', 'central.vbs@gmail.com'),
(22, 'Taverna Doce 5, Taverna, Lda', 'Maputo Cidade', 401380371, 'Avenida Mao Tse Tung', '8211111', 'naotememail2@gmail.com'),
(23, 'Papelaria Oriental SU, Lda', 'Beira', 401135820, 'Rua da Madeira, Nr 134', '845082152', 'naotememail3@gmail.com'),
(24, 'EDT Empresa de Distribuição de Tete Sociedade Unipessoal Lda', 'Beira', 400724520, 'EN7, Bairro Samora Machel-Tete', '82111', 'naotememail4@gmail.com'),
(25, 'Motivate ', 'Maputo', 400782156, 'Avenida Ho Chi Min', '843856427', 'naotememail5@gmail.com'),
(26, 'Petroli, Combustíveis e Lubrificantes E.I', 'Beira', 102334787, 'Avenida Mártires de Revolução, Nr.1', '21016462', 'fjanfar@petroli.co.mz'),
(27, 'TVCABO-Comunicacoes Multimedia, Lda', 'Maputo Cidade', 400062773, 'Avenida dos Presidentes Nr. 68 ', '21480550', 'apoio@tvcabo.co.mz'),
(28, 'Khurula Investimentos, Lda', 'Maputo Cidade', 400389136, 'Avenida 24 de Julho Nr. 1090 Bairro Alto Mae', '848829082', 'naotem@email'),
(29, 'FOOD, Lda (Sucursal)', 'Maputo Cidade', 401071717, 'Avenida Karl Max', '8222222', 'naotem@email'),
(30, 'Platinum Eventos, Lda', 'Maputo Cidade', 401174907, 'Rua Antonio da Conceicao Nr. 12', '847486666', 'hugo.monteiro3003@gmail.com'),
(31, 'Car Wash Moses', 'Maputo Cidade ', 131550502, 'Avenida Marginal, Triunfo', '862223618', 'naotem@email'),
(32, 'Brithol Michcoma Mocambique, Lda', 'Maputo Cidade', 400002842, 'Avenida Mao Tse Tung', '21493365', 'naotem@emai'),
(33, 'Witrans Services Moçambique, Lda', 'Cidade Maputo', 400389810, 'Avenida 25 de Setembro, Nr. 1020', '874040063', 'info@witrans.co.mz'),
(34, 'Flexível Plástica, Lda', 'Beira', 400240884, 'Rua Martires Massangano, Nr.557', '828693136', 'naotem@email'),
(35, '87 BAR, Lda', 'Beira', 400961301, 'Av. Eduardo Mondlane, Ponta Gea. ', '82111111', 'naotem@email'),
(36, 'FOCO Serigráfica, Lda', 'Maputo Cidade', 401077383, 'Av. Vlademir Lenine, bairro da Coop.', '843285055', 'geral@focoseregrafica.co.mz'),
(37, 'AMPEA Electronica', 'Beira', 102783034, 'Maquinino, Beira', '877067070', 'naotem@email'),
(38, 'Tok DCanela Cafe & Take Away, Soc. Unip., Lda', 'Beira', 400910286, 'Rua Correia de Brito', '863616517', 'naotem@email'),
(39, 'Comilao Take Away.', 'Beira', 114326968, 'Rua Poder Popular', '821727812', 'naotem@email'),
(40, 'VIP Supermercado,Lda', 'Beira', 400282072, 'Bairro dos Pioneiros, Beira', '845909999', 'naotem@email'),
(41, 'LUXOr, SA. Estacão de Serviço TOTAL Coop', 'Maputo Cidade ', 400376360, 'Av. Vlademir lenine Nr. 2576', '21415919', 'naotem@email'),
(42, 'FANTAZIA', 'Maputo', 400096430, 'Av Acordos de Lusaka Nr.242', '21759252', 'info@premier-gr.com'),
(43, 'Ultra Grafica, Lda.', 'Beira', 400757232, 'Av. Alfredo Lawley Nr. 769', '847611887', 'ultragraficacomercial@gmail.com'),
(44, 'Posto de Abastecimento de Khongolote', 'Maputo Cidade', 400545146, 'Av. de Khongolote, Bairro 1 de Maio', '848296', 'naotem@email'),
(45, 'Good Fittings & Services Sociedade Unipessoal, Lda', 'Maputo Cidade', 401141685, 'Av. Julius Nyerere, Mercado 1 de Junho', '848744444', 'goodfser@gmail.com'),
(46, 'AdRM Águas de Maputo', 'Maputo Cidade', 401337881, 'Avenida Eduardo Mondlane, Nr2952', '21302432', 'naotem@email'),
(47, 'Mini Papelaria, Lda', 'Beira - Chaimite', 401016902, 'Rua Correia de Brito, Nr.1854', '8211111', 'minipapelarialda@gmail.com'),
(48, 'Electro CHEL', 'Maputo Cidade', 400989338, 'Avenida Emilia Dausse, Nr. 127 R/C', '842819163', 'electrochel@gmail.com'),
(49, 'Afonso Rafael Chambane', 'Beira', 116299100, 'Rua Martires da Revolucao ', '848866747', 'naotem@email'),
(50, 'Tudo MZ', 'Beira', 401250621, 'Beira', '846473331', 'tecnologia@tudomoz.co.mz'),
(51, 'S.T FOOD, Lda', 'Maputo', 401071717, 'Avenida Karl Max', '8211111', 'naotem@email'),
(52, 'Ferragem Reyad', 'Maputo Cidade', 1046433675, 'Avenida Guerra Popular', '821111', 'naotem@email'),
(53, 'NOVATO, Lda', 'Maputo Cidade', 400753458, 'Avenida Samuel Magaia', '821222250', 'naotem@email'),
(54, 'MED Mozambique Energy Drinks', 'Maputo Cidade', 400088985, 'Av. das Estâncias ', '21333703', 'medl.mohans@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `estado`
--

CREATE TABLE `estado` (
  `idestado` int(1) NOT NULL,
  `descricao` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `execucaodespesas`
--

CREATE TABLE `execucaodespesas` (
  `idexecucaodespesas` int(8) NOT NULL,
  `descricaodespesa` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `idrequisicaodefundos` int(8) DEFAULT NULL,
  `data` date NOT NULL,
  `mes` int(11) NOT NULL,
  `ano` int(11) NOT NULL,
  `idempresa` int(8) NOT NULL,
  `valorinicial` double(10,2) NOT NULL,
  `iva` double(10,2) DEFAULT NULL,
  `valortotal` double(10,2) NOT NULL,
  `idformapagamento` int(2) DEFAULT NULL,
  `idcentrodecusto` int(2) NOT NULL,
  `idgrupodespesas` int(3) DEFAULT NULL,
  `observacoes` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `anexo` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `recibo` varchar(15) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `numerocheque` varchar(45) DEFAULT NULL,
  `transfcontadestino` varchar(45) DEFAULT NULL,
  `transferenciabanco` varchar(15) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `caixa` int(5) DEFAULT NULL,
  `grupo` int(3) DEFAULT NULL,
  `subgrupo` int(5) NOT NULL,
  `banco` int(4) DEFAULT NULL,
  `quantidade` int(11) DEFAULT 0,
  `unidade` int(11) DEFAULT NULL,
  `tipo_documento_id` int(11) DEFAULT NULL,
  `data_criacao` timestamp NOT NULL DEFAULT current_timestamp(),
  `data_atualizacao` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `execucaodespesas`
--

INSERT INTO `execucaodespesas` (`idexecucaodespesas`, `descricaodespesa`, `idrequisicaodefundos`, `data`, `mes`, `ano`, `idempresa`, `valorinicial`, `iva`, `valortotal`, `idformapagamento`, `idcentrodecusto`, `idgrupodespesas`, `observacoes`, `anexo`, `recibo`, `numerocheque`, `transfcontadestino`, `transferenciabanco`, `caixa`, `grupo`, `subgrupo`, `banco`, `quantidade`, `unidade`, `tipo_documento_id`, `data_criacao`, `data_atualizacao`) VALUES
(1, 'Combustivel para viatura.', NULL, '2023-01-14', 1, 2022, 12, 2515.00, 485.00, 3000.00, 6, 1, 100, '', '', '09529', NULL, '844396996', NULL, 0, 100, 116, 4, 35, 3, 0, '2022-09-30 11:05:08', '2023-01-14 16:05:25'),
(2, 'valor solicitado por Monica para deslocação ao Banco, com o custo de transação de 10,00MT.', NULL, '2023-02-14', 2, 2022, 7, 210.00, 0.00, 210.00, 6, 3, 7, '', '', '', NULL, '877742605', NULL, 0, 7, 11, 6, 2, 9, NULL, '2022-09-30 11:05:08', '2023-01-14 16:06:30'),
(3, 'Pagamento de Documento de Livrança, solicitado pelo Ambasse. Não contem recibo. Foi reconhecer no Notário.  ', NULL, '2023-01-14', 1, 2023, 15, 3500.00, 0.00, 3500.00, 7, 1, 99, '', '', '02', NULL, '', NULL, 0, 99, 99, 1, 1, 1, 0, '2022-09-30 11:05:08', '2023-01-14 16:07:32'),
(4, 'Pagamento de Seguro de Viagem. Custo adicional de 174.28,00MT ', NULL, '2022-08-02', 9, 2022, 8, 1400.00, 0.00, 1400.00, 6, 17, 103, '', '', '20227351/1', NULL, '845533809', NULL, 0, 103, 162, 4, 0, 9, NULL, '2022-09-30 11:05:08', '2022-10-18 13:06:34'),
(6, 'Pagamento feito ao Sr. Jose', NULL, '2022-08-17', 9, 2022, 14, 1000.00, 0.00, 1000.00, 1, 3, 13, '', '', '', NULL, '', NULL, 0, 13, 102, 1, 1, 1, NULL, '2022-09-30 11:05:08', '2022-10-18 06:52:29'),
(9, 'Pagamento de Txopela para Recolha de Oscar.', NULL, '2022-08-03', 9, 2022, 11, 660.00, 0.00, 660.00, 6, 1, 7, '', '', '', NULL, '844265839', NULL, 0, 7, 11, 4, 1, 1, NULL, '2022-09-30 11:05:08', '2022-11-07 08:07:56'),
(11, 'Valor enviado para a Sra. Ana Bela, com o custo de transação de 10,00MT.', NULL, '2022-08-04', 9, 2022, 13, 510.00, 0.00, 510.00, 6, 19, 104, '', '', '', NULL, '851588285', NULL, 0, 104, 124, 4, 3, 1, NULL, '2022-09-30 11:05:08', '2022-10-17 14:26:02'),
(14, 'Valor enviado para o Rosário.', NULL, '2022-08-04', 9, 2022, 13, 400.00, 0.00, 400.00, 6, 4, 5, '', '', '', NULL, '875052683', NULL, 0, 5, 80, 4, 1, 1, NULL, '2022-09-30 11:05:08', '2022-10-18 14:13:11'),
(15, 'Valor enviado para o Rosario.', NULL, '2022-08-04', 9, 2022, 13, 400.00, 0.00, 400.00, 6, 10, 5, '', '', '', NULL, '870502683', NULL, 0, 5, 84, 4, 1, 1, NULL, '2022-09-30 11:05:08', '2022-10-18 14:33:21'),
(16, 'Valor enviado ao Rosario.', NULL, '2022-08-04', 9, 2022, 13, 400.00, 0.00, 400.00, 6, 12, 5, '', '', '', NULL, '870502683', NULL, 0, 5, 93, 4, 1, 1, NULL, '2022-09-30 11:05:08', '2022-10-18 14:53:30'),
(17, 'Valoe enviado para o Rosario.', NULL, '2022-08-04', 9, 2022, 13, 1040.00, 0.00, 1040.00, 6, 15, 5, '', '', '', NULL, '870502683', NULL, 0, 5, 97, 4, 1, 1, NULL, '2022-09-30 11:05:08', '2022-10-18 15:00:22'),
(18, 'Valor enviado para o Rosario.', NULL, '2022-08-04', 9, 2022, 13, 400.00, 0.00, 400.00, 6, 14, 5, '', '', '', NULL, '870502683', NULL, 0, 5, 169, 4, 1, 1, NULL, '2022-09-30 11:05:08', '2022-10-31 11:28:30'),
(19, 'Valor enviado para o Rosário, com o Custo de transação de 50,00MT.', NULL, '2022-08-04', 9, 2022, 13, 850.00, 0.00, 850.00, 6, 16, 5, '', '', '', NULL, '870502683', NULL, 0, 5, 167, 4, 1, 1, NULL, '2022-09-30 11:05:08', '2022-10-18 15:06:51'),
(22, 'Valor enviado via Mpesa para o Sr. Albino.\r\n', NULL, '2022-08-05', 9, 2022, 12, 2010.00, 0.00, 2010.00, 7, 1, NULL, '', '', '0125378', '', '', NULL, NULL, 100, 115, NULL, 23, 3, 0, '2022-09-30 11:05:08', '0000-00-00 00:00:00'),
(23, 'Valor enviado para Rosário, com o custo de transferência de 20,00MT.', NULL, '2022-08-05', 9, 2022, 23, 6420.00, 0.00, 6420.00, 6, 10, 106, '', '', '00193', NULL, '870502683', NULL, 0, 106, 130, 4, 1, 1, NULL, '2022-09-30 11:05:08', '2022-10-18 13:36:10'),
(24, 'Valor envia para o Ambasse via a conta M-pesa, com o valor de transferência de 20,00MT.', NULL, '2022-08-05', 9, 2022, 12, 1520.00, 0.00, 1520.00, 7, 1, NULL, '', '', '0125335', '', '', NULL, NULL, 100, 116, NULL, 17, 3, 0, '2022-09-30 11:05:08', '0000-00-00 00:00:00'),
(25, 'Valor enviado para o Rosário com o valor de transação adicional de 10,00MT.', NULL, '2022-08-05', 9, 2022, 26, 1710.00, 0.00, 1710.00, 1, 4, 8, '', '', '94853', NULL, '', NULL, 0, 8, 146, 1, 20, 3, NULL, '2022-09-30 11:05:08', '2022-10-18 06:21:55'),
(29, 'Valor enviado para o Taverna com a taxa adicional de transferência de 10,00Mt.', NULL, '2022-08-05', 9, 2022, 22, 316.24, 53.76, 370.00, 1, 17, 102, '', '', '100469261', NULL, '', NULL, 0, 102, 145, 1, 2, 1, NULL, '2022-09-30 11:05:08', '2022-10-18 05:30:27'),
(31, 'Valor enviado ao Francisco através da conta M-pesa, com a taxa de envio adicional de 10.00MT.', NULL, '2022-08-05', 10, 2022, 13, 710.00, NULL, 710.00, 7, 1, NULL, '', '', '   ', '', '', NULL, NULL, 7, 12, NULL, 1, 1, NULL, '2022-10-03 05:54:51', '0000-00-00 00:00:00'),
(32, 'Valor enviado para o Ambasse através da conta M-pesa, com o valor de Transação de 10.00MT', NULL, '2022-08-05', 10, 2022, 13, 510.00, NULL, 510.00, 1, 1, 7, '', '', '', NULL, '', NULL, 0, 7, 13, 1, 0, NULL, NULL, '2022-10-03 06:27:10', '2022-10-18 05:35:08'),
(33, 'Valor enviado para o Latifo Espuma para compra de Bilhete para viagem de Beira a Maputo, valor adicional de transição 20,00MT.', NULL, '2022-08-05', 10, 2022, 28, 2520.00, NULL, 2520.00, 7, 1, NULL, '', '', '012999', '', '', NULL, NULL, 7, 11, NULL, 1, 1, 1, '2022-10-03 06:34:40', '0000-00-00 00:00:00'),
(34, 'Valor enviado para o Latifo Espuma para alimentação durante a viagem. Custo de transação de 05.00MT.', NULL, '2022-08-05', 10, 2022, 13, 505.00, NULL, 505.00, 7, 1, NULL, '', '', '', '', '', NULL, NULL, 101, 165, NULL, 1, 1, NULL, '2022-10-03 06:47:33', '2022-10-18 13:53:50'),
(35, 'Valor enviado para o Rosario, através da conta M-pesa. Custo de transação de 10,00MT.', NULL, '2022-08-06', 10, 2022, 11, 660.00, NULL, 660.00, 7, 1, NULL, '', '', '', '', '', NULL, NULL, 108, 128, NULL, 1, 1, NULL, '2022-10-03 07:06:15', '0000-00-00 00:00:00'),
(36, 'Valor enviado para o Rosário através do M-pesa. Custo de transação de 30,00MT.', NULL, '2022-08-06', 10, 2022, 13, 3030.00, NULL, 3030.00, 7, 10, NULL, '', '', '', '', '', NULL, NULL, 4, 62, NULL, NULL, NULL, NULL, '2022-10-03 07:37:29', '0000-00-00 00:00:00'),
(37, 'Valor enviado para o Rosário através do M-pesa. Custo de envio 05,00MT', NULL, '2022-08-06', 10, 2022, 13, 505.00, NULL, 505.00, 1, 4, 111, '', '', '', NULL, '', NULL, 0, 111, 156, 1, 1, 1, NULL, '2022-10-03 09:04:05', '2022-10-18 06:28:35'),
(38, ' Valor enviado para o Rosario através da conta M-pesa. Custo de envio 05,00MT.', NULL, '2022-08-06', 10, 2022, 13, 505.00, NULL, 505.00, 7, 12, NULL, '', '', '', '', '', NULL, NULL, 111, 156, NULL, 1, 1, NULL, '2022-10-03 09:39:17', '2022-10-18 13:48:58'),
(39, 'Valor enviado para o Rosário através da conta M-pesa. Sem custo de transação.', NULL, '2022-08-06', 10, 2022, 13, 400.00, NULL, 400.00, 6, 14, 111, '', '', '', NULL, '870502683', NULL, 0, 111, 156, 4, 1, 1, NULL, '2022-10-03 09:49:29', '2022-10-17 15:20:53'),
(40, 'Valor enviado para o Milton. Custo de envio adicional de 6.17,00MT.', NULL, '2022-08-06', 10, 2022, 8, 2550.00, NULL, 2550.00, 7, 1, NULL, '', '', '', '', '', NULL, NULL, 103, 132, NULL, NULL, NULL, 2, '2022-10-04 07:38:27', '0000-00-00 00:00:00'),
(41, 'Valor enviado para o Rosário. Custo de envio adicional de 5,00MT', NULL, '2022-08-06', 10, 2022, 13, 2005.00, NULL, 2005.00, 7, 14, NULL, '', '', '', '', '', NULL, NULL, 4, 62, NULL, NULL, NULL, NULL, '2022-10-04 08:01:18', '0000-00-00 00:00:00'),
(42, 'valor enviado para o Sr. Albino. Custo de transação de 20,00MT.', NULL, '2022-08-06', 10, 2022, 17, 2020.00, NULL, 2020.00, 7, 19, NULL, '', '', '', '', '', NULL, NULL, 7, 15, NULL, NULL, NULL, NULL, '2022-10-04 10:22:13', '0000-00-00 00:00:00'),
(43, 'Valor enviado para o Rosário. Custo de transação de 20,00MT.', NULL, '2022-08-07', 10, 2022, 12, 2020.00, NULL, 2020.00, 7, 1, NULL, '', '', '', '', '', NULL, NULL, 100, 119, NULL, NULL, NULL, NULL, '2022-10-04 11:34:22', '0000-00-00 00:00:00'),
(44, 'Valor enviado para o Sr. Mariano através da conta M-pesa. Custo adicional de transação 10,00Mt.', NULL, '2022-08-07', 10, 2022, 31, 210.00, NULL, 210.00, 7, 1, NULL, '', '', '000146', '', '', NULL, NULL, 112, 133, NULL, NULL, NULL, 2, '2022-10-04 19:33:31', '0000-00-00 00:00:00'),
(45, 'Valor enviado para Bernardo através da conta M-pesa. Custo de transação 10,00MT.', NULL, '2022-08-07', 10, 2022, 15, 1510.00, NULL, 1510.00, 7, 1, NULL, '', '', '', '', '', NULL, NULL, 112, 135, NULL, NULL, NULL, NULL, '2022-10-04 19:51:07', '0000-00-00 00:00:00'),
(46, 'Valor enviado para Bernardo através da conta M-pesa. Custo de transação de 10,00MT.', NULL, '2022-08-07', 10, 2022, 15, 1510.00, NULL, 1510.00, 1, 1, 112, '', '', '', NULL, '', NULL, 0, 112, 135, 1, 0, NULL, NULL, '2022-10-04 19:52:40', '2022-10-25 10:37:21'),
(47, 'Valor transferido para o Rosário através da conta M-pesa. Custo de transição 10,00MT.', NULL, '2022-08-08', 10, 2022, 13, 660.00, NULL, 660.00, 7, 1, NULL, '', '', '', '', '', NULL, NULL, 108, 128, NULL, NULL, NULL, NULL, '2022-10-04 19:58:51', '0000-00-00 00:00:00'),
(48, 'Valor enviado para o Carlos através da conta M-pesa. Custo de transação de 10,00MT.', NULL, '2022-08-08', 10, 2022, 11, 610.00, NULL, 610.00, 7, 1, NULL, '', '', '', '', '', NULL, NULL, 7, 11, NULL, 2, 1, NULL, '2022-10-04 20:04:55', '0000-00-00 00:00:00'),
(49, 'Compra de envelopes, solicitado pelo Francisco. Valor enviado para o Rosário através da conta M-pesa, custo de transação de 18,00MT.', NULL, '2022-08-08', 10, 2022, 15, 618.00, NULL, 618.00, 7, 10, NULL, '', '', '', '', '', NULL, NULL, 99, 99, NULL, NULL, NULL, NULL, '2022-10-04 20:27:56', '0000-00-00 00:00:00'),
(50, 'Pagamento de Encadernação de 6 processos, solicitado pelo Leonel Magaia. Valor enviado para o Rosário, custo de transação de 10,00MT.', NULL, '2022-08-08', 10, 2022, 15, 1010.00, NULL, 1010.00, 7, 1, NULL, '', '', '', '', '', NULL, NULL, 99, 99, NULL, NULL, NULL, NULL, '2022-10-04 20:36:04', '0000-00-00 00:00:00'),
(51, ' Valor enviado para o sr. Albino através da conta M-pesa. Custo de transação de 15,00MT.', NULL, '2022-08-08', 10, 2022, 12, 2015.00, NULL, 2015.00, 7, 1, NULL, '', '', '0125528', '', '', NULL, NULL, 100, 115, NULL, 34, 3, 1, '2022-10-04 20:40:07', '0000-00-00 00:00:00'),
(52, 'Valor enviado para o Ambasse através da conta M-pesa. custo de transação de 30,00MT.', NULL, '2022-08-08', 10, 2022, 12, 3030.00, NULL, 3030.00, 7, 1, NULL, '', '', '0125545', '', '', NULL, NULL, 100, 116, NULL, 34, 3, 1, '2022-10-04 20:55:06', '0000-00-00 00:00:00'),
(53, 'Valor enviado para o Milton, através da conta M-pesa. Custo de transação de 30,00MT.', NULL, '2022-08-08', 10, 2022, 6, 3030.00, NULL, 3030.00, 7, 1, NULL, '', '', '8501', '', '', NULL, NULL, 100, 117, NULL, 34, 3, 1, '2022-10-04 20:57:42', '0000-00-00 00:00:00'),
(54, 'Valor enviado para o Rosário através da conta M-pesa. Custo de transição de 05,00MT.', NULL, '2022-08-08', 10, 2022, 13, 405.00, NULL, 405.00, 6, 12, 5, '', '', '', NULL, '870502683', NULL, 0, 5, 93, 4, 1, 1, NULL, '2022-10-04 20:59:31', '2022-10-19 11:15:06'),
(55, 'Valor enviado para Ambasse através da conta M-pesa. Custo de transação de 10,00MT.', NULL, '2022-08-08', 10, 2022, 4, 1010.00, 0.00, 1010.00, 7, 1, 3, '', '', '0015155', NULL, '', NULL, 0, 3, 137, 1, 1, 1, 1, '2022-10-04 21:04:37', '2022-10-04 23:31:34'),
(56, 'Valor enviado para Ambasse, através da conta M-pesa. Custo de transação de 10,00MT.', NULL, '2022-08-08', 10, 2022, 5, 1010.00, NULL, 1010.00, 7, 1, NULL, '', '', '', '', '', NULL, NULL, 3, 52, NULL, 1, 1, NULL, '2022-10-04 21:06:52', '0000-00-00 00:00:00'),
(57, 'Valor enviado para o Rosário através da conta m-pesa.', NULL, '2022-08-08', 10, 2022, 13, 8000.00, NULL, 8000.00, 7, 4, NULL, '', '', '', '', '', NULL, NULL, 4, 62, NULL, NULL, NULL, NULL, '2022-10-04 21:08:57', '0000-00-00 00:00:00'),
(58, 'Valor enviado para Monica. Transporte para o Banco.', NULL, '2022-08-08', 10, 2022, 11, 217.00, NULL, 217.00, 7, 1, NULL, '', '', '', '', '', NULL, NULL, 7, 11, NULL, NULL, NULL, NULL, '2022-10-04 21:14:45', '0000-00-00 00:00:00'),
(59, 'Valor enviado para a Sra. Ana Bela através do M-pesa. Custo de transação.', NULL, '2022-08-08', 10, 2022, 13, 510.00, NULL, 510.00, 7, 19, NULL, '', '', '', '', '', NULL, NULL, 104, 124, NULL, NULL, NULL, NULL, '2022-10-04 21:17:11', '0000-00-00 00:00:00'),
(60, 'Valor enviado para a Sra. Ana Bela através do M-pesa. Custo de transação de 5,00MT.', NULL, '2022-08-08', 10, 2022, 13, 505.00, NULL, 505.00, 7, 19, NULL, '', '', '', '', '', NULL, NULL, 104, 125, NULL, NULL, NULL, NULL, '2022-10-04 21:20:44', '0000-00-00 00:00:00'),
(61, 'Valor enviado para o sr. Albino. Custo de transação de 10,00MT.', NULL, '2022-08-08', 10, 2022, 13, 510.00, NULL, 510.00, 7, 19, NULL, '', '', '', '', '', NULL, NULL, 104, 136, NULL, 1, 1, NULL, '2022-10-04 21:24:12', '0000-00-00 00:00:00'),
(62, 'Valor enviado para a Sra. Ana Bela. Custo de transação de 5,00MT.', NULL, '2022-08-08', 10, 2022, 13, 505.00, NULL, 505.00, 7, 19, NULL, '', '', '', '', '', NULL, NULL, 104, 138, NULL, 1, NULL, NULL, '2022-10-04 21:28:11', '0000-00-00 00:00:00'),
(63, 'Valor enviado para o Rosário para Impressão Colorida. Custo adicional de transição de 20,00MT.', NULL, '2022-08-08', 10, 2022, 32, 11270.00, 0.00, 11270.00, 7, 10, 99, '', '', '135968', NULL, '', NULL, 0, 99, 99, 1, 3, 1, 1, '2022-10-04 21:42:08', '2022-10-04 23:46:38'),
(64, 'Valor enviado para o Rosário para Encadernação. Custo de transação de 20,00MT.', NULL, '2022-08-08', 10, 2022, 32, 15020.00, NULL, 15020.00, 7, 10, NULL, '', '', '135968', '', '', NULL, NULL, 99, 99, NULL, 3, 1, 1, '2022-10-04 21:44:57', '0000-00-00 00:00:00'),
(65, 'Valor enviado para o Taverna. Custo de transação de 10,00MT não adicionado.', NULL, '2022-08-09', 10, 2022, 22, 333.34, 56.66, 390.00, 7, 17, NULL, '', '', '100469261', '', '', NULL, NULL, 102, 145, NULL, 4, 1, 1, '2022-10-04 21:57:44', '2022-10-18 13:50:38'),
(66, 'Transporte de Diana Timana do Baia Mall para FME Kenneth Kaunda. Custo de transação de 5,00MT.', NULL, '2022-08-09', 10, 2022, 33, 220.00, NULL, 220.00, 7, 1, NULL, '', '', '002812', '', '', NULL, NULL, 7, 11, NULL, 1, 1, 1, '2022-10-04 22:04:29', '0000-00-00 00:00:00'),
(67, 'Valor enviado para o Rosário. Custo de transação de 20,00MT', NULL, '2022-08-09', 10, 2022, 9, 2520.00, NULL, 2520.00, 7, 1, NULL, '', '', '0108671', '', '', NULL, NULL, 100, 139, NULL, 29, 3, 1, '2022-10-04 22:13:36', '0000-00-00 00:00:00'),
(68, 'Valor enviado para o Rosário. Custo de transição 05,00MT.', NULL, '2022-08-09', 10, 2022, 13, 405.00, NULL, 405.00, 6, 10, 5, '', '', '', NULL, '870502683', NULL, 0, 5, 84, 4, 1, 1, NULL, '2022-10-04 22:16:14', '2022-10-19 10:16:29'),
(69, 'Valor enviado para o Rosário. Custo de transação de 10,00MT.', NULL, '2022-08-09', 10, 2022, 11, 660.00, NULL, 660.00, 7, 1, NULL, '', '', '', '', '', NULL, NULL, 108, 128, NULL, NULL, NULL, NULL, '2022-10-04 22:18:21', '0000-00-00 00:00:00'),
(70, 'Valor enviado para o Rosário.', NULL, '2022-08-09', 10, 2022, 15, 3650.00, NULL, 3650.00, 7, 10, NULL, '', '', '', '', '', NULL, NULL, 7, 9, NULL, NULL, NULL, NULL, '2022-10-04 22:24:33', '0000-00-00 00:00:00'),
(71, 'Valor enviado para o Rosário.', NULL, '2022-08-09', 10, 2022, 15, 2900.00, NULL, 2900.00, 7, 14, NULL, '', '', '', '', '', NULL, NULL, 7, 9, NULL, NULL, NULL, NULL, '2022-10-04 22:27:54', '0000-00-00 00:00:00'),
(72, 'Valor enviado para o Rosário.', NULL, '2022-08-09', 10, 2022, 17, 1000.00, NULL, 1000.00, 6, 10, 7, '', '', '', NULL, '870502683', NULL, 0, 7, 15, 4, 0, NULL, 2, '2022-10-04 22:29:59', '2022-10-19 10:35:24'),
(73, 'Valor enviado para o Rosário. ', NULL, '2022-08-09', 10, 2022, 17, 830.00, 170.00, 1000.00, 7, 14, NULL, '', '', '', '', '', NULL, NULL, 7, 15, NULL, 111, 5, 2, '2022-10-04 22:38:56', '0000-00-00 00:00:00'),
(74, 'Valor enviado para o Rosario.', NULL, '2022-08-09', 10, 2022, 10, 500.00, NULL, 500.00, 7, 1, NULL, '', '', '02004', '', '', NULL, NULL, 7, 12, NULL, NULL, NULL, 1, '2022-10-04 22:42:15', '0000-00-00 00:00:00'),
(75, 'Valor enviado pra o Rosário.', NULL, '2022-08-06', 10, 2022, 9, 1500.00, NULL, 1500.00, 1, 1, 100, '', '', '0108161', NULL, '', NULL, 0, 100, 119, 1, 17, 3, 1, '2022-10-04 22:45:32', '2022-10-28 06:02:13'),
(76, 'Valor enviado para o Rosário.', NULL, '2022-08-09', 10, 2022, 9, 3000.00, NULL, 3000.00, 7, 1, NULL, '', '', '079837', '', '', NULL, NULL, 100, 119, NULL, 34, 3, 1, '2022-10-04 22:47:46', '0000-00-00 00:00:00'),
(77, 'Valor enviado para o Rosário para Impressão A4. Custo de transação de 4,00MT.', NULL, '2022-08-09', 10, 2022, 32, 165.25, NULL, 165.25, 7, 10, NULL, '', '', '135971', '', '', NULL, NULL, 99, 99, NULL, 3, 1, 1, '2022-10-04 22:51:13', '0000-00-00 00:00:00'),
(78, 'Valor enviado para o Rosário para Encadernação. Custo de transação de 4,00MT. ', NULL, '2022-08-09', 10, 2022, 32, 165.25, NULL, 165.25, 7, 10, NULL, '', '', '135971', '', '', NULL, NULL, 99, 99, NULL, 3, 1, 1, '2022-10-04 22:54:07', '0000-00-00 00:00:00'),
(79, 'Valor enviado para o Rosário. Custo de transação de 10,00MT.', NULL, '2022-08-10', 10, 2022, 13, 660.00, NULL, 660.00, 7, 1, NULL, '', '', '', '', '', NULL, NULL, 108, 128, NULL, NULL, NULL, NULL, '2022-10-04 22:58:01', '0000-00-00 00:00:00'),
(80, 'Valor enviado para o Taverna. Custo de transação de 10,00MT não adicionado.', NULL, '2022-08-10', 10, 2022, 22, 299.15, 50.85, 350.00, 7, 17, NULL, '', '', '', '', '', NULL, NULL, 102, 145, NULL, 1, 1, 2, '2022-10-04 23:12:28', '2022-10-18 13:50:38'),
(81, 'Valor enviado para o Rosário. ', NULL, '2022-08-10', 10, 2022, 17, 1815.22, 184.78, 2000.00, 6, 15, 7, '', '', '', NULL, '870502683', NULL, 0, 7, 15, 4, 132, 5, 2, '2022-10-04 23:26:09', '2022-10-19 14:06:38'),
(82, 'Valor enviado para o Ambasse para envio de Cremes para Beira. Custo de transação de 10,00MT.', NULL, '2022-08-10', 10, 2022, 10, 510.00, NULL, 510.00, 7, 1, NULL, '', '', '02085', '', '', NULL, NULL, 7, 13, NULL, NULL, NULL, 1, '2022-10-04 23:29:03', '0000-00-00 00:00:00'),
(83, 'Valor enviado para o Rosário através do M-pesa, custo de transição não adicionado de 20,00MT.', NULL, '2022-08-10', 10, 2022, 17, 1809.30, 190.70, 2000.00, 7, 4, NULL, '', '', '', '', '', NULL, NULL, 7, 15, NULL, 136, 5, 1, '2022-10-04 23:33:47', '0000-00-00 00:00:00'),
(84, 'Valor enviado para o Rosário. Custo de transação de 14,00MT.', NULL, '2022-08-10', 10, 2022, 13, 8120.00, NULL, 8120.00, 1, 14, 10, '', '', '', NULL, '', NULL, 0, 10, 163, 1, 0, NULL, NULL, '2022-10-04 23:37:57', '2022-10-18 13:09:53'),
(85, 'Valor enviado para o Rosário. Custo de transação de 10,00MT.', NULL, '2022-08-10', 10, 2022, 13, 9292.65, NULL, 9292.65, 7, 14, NULL, '', '', '', '', '', NULL, NULL, 10, 163, NULL, NULL, NULL, NULL, '2022-10-04 23:40:22', '2022-10-18 13:13:14'),
(86, 'Valor enviado para o Rosário. Custo de transação de 10,00MT.', NULL, '2022-08-10', 10, 2022, 13, 1722.95, NULL, 1722.95, 7, 13, NULL, '', '', '', '', '', NULL, NULL, 10, 163, NULL, NULL, NULL, NULL, '2022-10-04 23:42:54', '2022-10-18 13:13:14'),
(87, 'Valor enviado para o Rosário. Custo de transação de 15.88, MT.', NULL, '2022-08-10', 10, 2022, 13, 1823.00, NULL, 1823.00, 7, 13, NULL, '', '', '', '', '', NULL, NULL, 10, 163, NULL, NULL, NULL, NULL, '2022-10-04 23:44:41', '2022-10-18 13:13:14'),
(88, 'Valor enviado para o Sr. Albino. Custo de transação 15,00MT.', NULL, '2022-08-11', 10, 2022, 12, 515.00, NULL, 515.00, 6, 1, 100, '', '', '0125775', NULL, '845629961', NULL, 0, 100, 115, 4, 6, 3, 1, '2022-10-05 02:05:46', '2022-10-27 21:22:38'),
(89, 'Pequeno-almoço do Sr. Marco. Valor enviado para o M-pesa do Taverna com custo de transação não adicionado de 10,00MT.', NULL, '2022-08-11', 10, 2022, 22, 504.28, 85.72, 590.00, 7, 17, NULL, '', '', '', '', '', NULL, NULL, 102, 145, NULL, 6, 1, 2, '2022-10-05 02:11:55', '2022-10-18 13:50:38'),
(90, 'Valor enviado para o Rosário. Custo de transição e de 10,00MT não adicionado.', NULL, '2022-08-11', 10, 2022, 17, 1809.30, 190.70, 2000.00, 7, 10, NULL, '', '', '', '', '', NULL, NULL, 7, 15, NULL, 136, 5, 1, '2022-10-05 02:18:15', '0000-00-00 00:00:00'),
(91, 'Valor enviado para o Rosário.', NULL, '2022-08-11', 10, 2022, 17, 1809.30, 190.70, 2000.00, 7, 4, NULL, '', '', '', '', '', NULL, NULL, 7, 15, NULL, 136, 5, 1, '2022-10-05 02:20:36', '0000-00-00 00:00:00'),
(92, 'Valor entregue ao sr. Albino.', NULL, '2022-08-11', 10, 2022, 13, 179.94, NULL, 179.94, 3, 19, 7, '', '', '16A52636', NULL, '', NULL, 1, 7, 16, 1, 0, NULL, 2, '2022-10-05 02:26:55', '2022-10-24 09:47:56'),
(93, 'Compra de plástico P15 branco. Valor enviado para o Rosário, ccusto de transação de 10,00MT.', NULL, '2022-08-11', 10, 2022, 34, 4510.00, 0.00, 4510.00, 7, 4, 99, '', '', '61586', NULL, '', NULL, 0, 99, 99, 1, 10, 1, 1, '2022-10-05 02:41:10', '2022-10-05 04:44:53'),
(94, 'Compra de plástico Q28 branco. Valor enviado para o Rosário, custo de transição de 10,00MT.', NULL, '2022-08-11', 10, 2022, 34, 6510.00, NULL, 6510.00, 7, 4, NULL, '', '', '61586', '', '', NULL, NULL, 99, 99, NULL, 10, 1, 1, '2022-10-05 02:43:37', '0000-00-00 00:00:00'),
(95, 'Valor enviado para o Rosário.', NULL, '2022-08-11', 10, 2022, 11, 650.00, NULL, 650.00, 7, 1, NULL, '', '', '', '', '', NULL, NULL, 108, 128, NULL, NULL, NULL, NULL, '2022-10-05 02:46:44', '0000-00-00 00:00:00'),
(96, 'Valor enviado para o Rosário. ', NULL, '2022-08-11', 10, 2022, 17, 1809.30, 190.70, 2000.00, 7, 14, NULL, '', '', '', '', '', NULL, NULL, 7, 15, NULL, NULL, NULL, 2, '2022-10-05 02:49:45', '0000-00-00 00:00:00'),
(97, 'Valor entregue ao Fenias para deslocação as Importadoras.', NULL, '2022-08-11', 10, 2022, 13, 200.00, NULL, 200.00, 3, 1, 7, '', '', '', NULL, '', NULL, 1, 7, 11, 1, 2, 1, NULL, '2022-10-05 02:57:14', '2022-10-25 12:33:48'),
(98, 'Valor transferido para o Milton.', NULL, '2022-08-11', 10, 2022, 12, 3000.00, NULL, 3000.00, 1, 1, 100, '', '', '0125855', NULL, '', NULL, 0, 100, 117, 1, 34, 3, 1, '2022-10-05 07:07:44', '2022-10-19 14:34:48'),
(99, 'Valor enviado para Sra. Emilia Jacinto a mando do Sr. Marco.', NULL, '2022-08-12', 10, 2022, 13, 518.00, NULL, 518.00, 7, 1, NULL, '', '', '', '', '', NULL, NULL, 99, 99, NULL, NULL, NULL, NULL, '2022-10-05 07:12:33', '0000-00-00 00:00:00'),
(100, 'Valor entregue ao Rosário.', NULL, '2022-08-13', 10, 2022, 11, 650.00, NULL, 650.00, 7, 1, NULL, '', '', '', '', '', NULL, NULL, 108, 128, NULL, 1, 1, NULL, '2022-10-05 13:03:01', '0000-00-00 00:00:00'),
(101, 'Valor enviado para Pedro. Custo de transação de 20,00MT.', NULL, '2022-08-13', 10, 2022, 13, 6020.00, NULL, 6020.00, 7, 1, NULL, '', '', '', '', '', NULL, NULL, 6, 3, NULL, 1, 1, NULL, '2022-10-05 13:31:01', '0000-00-00 00:00:00'),
(102, 'Valor enviado para o Rosario.', NULL, '2022-08-13', 10, 2022, 9, 3000.00, NULL, 3000.00, 7, 1, NULL, '', '', '0106746', '', '', NULL, NULL, 100, 119, NULL, 34, 3, 1, '2022-10-05 13:46:27', '0000-00-00 00:00:00'),
(103, 'Valor enviado para Sra. Márcia, custo de transação de 18,00MT.', NULL, '2022-08-13', 10, 2022, 13, 1018.00, NULL, 1018.00, 7, 19, NULL, '', '', '', '', '', NULL, NULL, 104, 138, NULL, 1, 1, NULL, '2022-10-05 16:47:14', '0000-00-00 00:00:00'),
(104, 'A despesa não apresenta recibo/VD.', NULL, '2022-08-13', 10, 2022, 13, 1010.00, NULL, 1010.00, 7, 3, NULL, '', '', '', '', '', NULL, NULL, 111, 156, NULL, 1, 1, NULL, '2022-10-05 16:53:18', '2022-10-18 13:48:58'),
(105, 'Valor Solicitado pela Sra. Florinda. Montagem e desmontagem de Pneu.', NULL, '2022-08-13', 10, 2022, 13, 750.00, NULL, 750.00, 6, 1, 1, '', '', '', NULL, '842173242', NULL, 0, 1, 46, 4, 0, NULL, NULL, '2022-10-05 17:03:17', '2022-11-07 11:55:32'),
(106, 'Valor enviado para o Rosário, custo de transação de 5,00MT.', NULL, '2022-08-14', 10, 2022, 15, 505.00, NULL, 505.00, 6, 10, 111, '', '', '59409', NULL, '870502683', NULL, 0, 111, 156, 4, 0, NULL, 1, '2022-10-05 17:09:01', '2022-10-18 13:46:11'),
(107, 'Valor enviado para o Rosário, custo de transação de 5,00MT.', NULL, '2022-08-14', 10, 2022, 13, 505.00, NULL, 505.00, 7, 4, NULL, '', '', '00504', '', '', NULL, NULL, 111, 156, NULL, NULL, NULL, 1, '2022-10-05 17:10:37', '2022-10-18 13:48:58'),
(108, 'Valor enviado para o Rosario.', NULL, '2022-08-14', 10, 2022, 35, 500.00, NULL, 500.00, 7, 12, NULL, '', '', '000736', '', '', NULL, NULL, 111, 156, NULL, NULL, NULL, 1, '2022-10-05 17:14:44', '2022-10-18 13:48:58'),
(109, 'Pagamento da viagem de Latifo Espuma de Beira a Maputo.', NULL, '2022-08-14', 10, 2022, 28, 2500.00, NULL, 2500.00, 7, 1, NULL, '', '', '17060', '', '', NULL, NULL, 7, 11, NULL, 1, 1, 1, '2022-10-05 17:16:37', '0000-00-00 00:00:00'),
(110, 'Valor Solicitado e enviado para o Latifo Espuma para alimentação durante a viagem. A despesa não apresenta recibo.', NULL, '2022-08-14', 10, 2022, 15, 500.00, NULL, 500.00, 7, 1, NULL, '', '', '', '', '', NULL, NULL, 101, 165, NULL, NULL, NULL, NULL, '2022-10-05 17:18:15', '2022-10-18 13:53:50'),
(111, 'Valor enviado para o Rosário para o pagamento da refeição de Domingo. A despesa não apresenta Recibo.', NULL, '2022-08-14', 10, 2022, 13, 410.00, NULL, 410.00, 7, 14, NULL, '', '', '', '', '', NULL, NULL, 111, 156, NULL, NULL, NULL, NULL, '2022-10-05 17:22:26', '2022-10-18 13:48:58'),
(112, 'Valor transferido para o Sr. albino. Custo de transação de 10,00MT.', NULL, '2022-08-15', 10, 2022, 17, 1812.17, 187.83, 2000.00, 1, 19, 7, '', '', '', NULL, '', NULL, 0, 7, 15, 1, 0, NULL, 1, '2022-10-05 17:36:30', '2022-10-21 10:19:01'),
(113, 'Valor enviado para Anilza Mazoio.', NULL, '2022-08-15', 10, 2022, 5, 1000.00, NULL, 1000.00, 3, 1, 3, '', '', '', NULL, '', NULL, 1, 3, 54, 1, 0, NULL, 1, '2022-10-05 17:38:38', '2022-10-20 10:41:50'),
(115, 'Valor enviado para Milton. Custo de transação de 15,00MT.', NULL, '2022-08-15', 10, 2022, 12, 3015.00, NULL, 3015.00, 7, 1, NULL, '', '', '0126067', '', '', NULL, NULL, 100, 117, NULL, 34, 3, 1, '2022-10-05 17:40:37', '0000-00-00 00:00:00'),
(116, 'Valor solicitado por Nelson João, valor enviado para Witrans. Custo de transação de 10,00MT.', NULL, '2022-08-15', 10, 2022, 33, 163.00, NULL, 163.00, 6, 1, 7, '', '', '01177', NULL, '855895647', NULL, 0, 7, 11, 4, 0, NULL, 1, '2022-10-05 17:51:34', '2022-10-18 12:58:20'),
(117, 'Valor enviado para Carlos Bento. A despesa não apresenta Recibo, custo de transação de 15,00MT', NULL, '2022-08-15', 10, 2022, 15, 5015.00, NULL, 5015.00, 6, 3, 4, '', '', '', NULL, '843910518', NULL, 0, 4, 166, 4, 0, NULL, NULL, '2022-10-05 17:54:49', '2022-10-18 14:09:03'),
(118, 'Pagamento de combustível para o Electricista Tomas.  A despesa não apresenta VD/Recibo.', NULL, '2022-08-15', 10, 2022, 13, 1000.00, NULL, 1000.00, 6, 2, 13, '', '', '', NULL, '846476730', NULL, 0, 13, 102, 4, 0, NULL, NULL, '2022-10-05 17:58:15', '2022-11-07 12:36:24'),
(119, 'Valor enviado para Sra. Ana Bela. Custo de transação de 5,00MT.', NULL, '2022-08-15', 10, 2022, 13, 505.00, NULL, 505.00, 7, 19, NULL, '', '', '', '', '', NULL, NULL, 104, 124, NULL, NULL, NULL, NULL, '2022-10-05 18:13:01', '0000-00-00 00:00:00'),
(120, 'Valor enviado para Sra. Ana Bela. Custo de transação de 5,00MT.', NULL, '2022-08-15', 10, 2022, 13, 505.00, NULL, 505.00, 7, 19, NULL, '', '', '', '', '', NULL, NULL, 104, 138, NULL, NULL, NULL, NULL, '2022-10-05 18:14:29', '0000-00-00 00:00:00'),
(121, 'Valor enviado para Sra. Ana Bela. A despesa não apresenta Recibo/VD.', NULL, '2022-08-15', 10, 2022, 13, 505.00, NULL, 505.00, 7, 19, NULL, '', '', '', '', '', NULL, NULL, 104, 125, NULL, NULL, NULL, NULL, '2022-10-05 18:16:18', '0000-00-00 00:00:00'),
(122, 'Valor enviado para o Rosário. Custo de transição de 10,00MT.', NULL, '2022-08-15', 10, 2022, 37, 960.00, NULL, 960.00, 7, 10, NULL, '', '', '5599', '', '', NULL, NULL, 13, 102, NULL, NULL, NULL, 1, '2022-10-05 18:26:51', '0000-00-00 00:00:00'),
(123, 'Valor enviado para o Sr. Albino. Custo de transação de 10,00MT.', NULL, '2022-08-15', 10, 2022, 12, 2010.00, NULL, 2010.00, 7, 1, NULL, '', '', '', '', '', NULL, NULL, 100, 115, NULL, NULL, NULL, NULL, '2022-10-05 18:29:00', '0000-00-00 00:00:00'),
(124, 'valor enviado para o Sr. Albino. Custo de transação de 5,00MT.', NULL, '2022-08-15', 10, 2022, 13, 505.00, NULL, 505.00, 7, 19, NULL, '', '', '', '', '', NULL, NULL, 104, 136, NULL, NULL, NULL, NULL, '2022-10-05 18:32:53', '0000-00-00 00:00:00'),
(125, 'Pagamento do Ligador DC3. Valor enviado para o Rosario com o custo de transicao de 10,00MT. ', NULL, '2022-08-15', 10, 2022, 37, 850.00, NULL, 850.00, 7, 10, NULL, '', '', '05599', '', '', NULL, NULL, 13, 102, NULL, NULL, NULL, 1, '2022-10-05 18:39:12', '0000-00-00 00:00:00'),
(126, 'Pagamento de Base NH1. Valor enviado para o Rosário com o custo de transação de 10,00MT.', NULL, '2022-08-15', 10, 2022, 13, 770.00, NULL, 770.00, 7, 10, NULL, '', '', '05577', '', '', NULL, NULL, 13, 102, NULL, NULL, NULL, 1, '2022-10-05 18:41:51', '0000-00-00 00:00:00'),
(128, 'Valor enviado para o Sr. Albino. custo de transação de 10,00MT.', NULL, '2022-08-16', 10, 2022, 12, 2010.00, NULL, 2010.00, 7, 1, NULL, '', '', '0126079', '', '', NULL, NULL, 100, 115, NULL, 23, 3, 1, '2022-10-05 19:01:13', '0000-00-00 00:00:00'),
(129, 'valor solicitado pelo Ambasse. Custo de transição de 10,00MT.', NULL, '2022-08-16', 10, 2022, 10, 2010.00, NULL, 2010.00, 7, 1, NULL, '', '', '04052', '', '', NULL, NULL, 7, 13, NULL, NULL, NULL, 1, '2022-10-05 19:03:33', '0000-00-00 00:00:00'),
(131, 'Compra de uma caixa de Resma. Valor enviado para o Milton, custo de transação de 20,00MT.', NULL, '2022-08-16', 10, 2022, 20, 2200.00, 0.00, 2200.00, 6, 3, 106, '', '', '003762', NULL, '855533809', NULL, 0, 106, 171, 4, 1, 1, 1, '2022-10-05 23:14:14', '2022-11-07 13:24:01'),
(132, 'Compra de 03 caixas de Clips grandes. Valor enviado para o Milton.', NULL, '2022-08-16', 10, 2022, 20, 400.00, 0.00, 400.00, 7, 3, 110, '', '', '003762', NULL, '', NULL, 0, 106, 130, 1, 3, 1, 1, '2022-10-05 23:17:44', '2022-10-18 13:34:28'),
(133, 'Deslocacao de Nelson Joao para FME Beach Front ide e volta. Valor enviado a Witrans, custo de transação de 10,00MT.', NULL, '2022-08-16', 10, 2022, 33, 320.00, 0.00, 320.00, 7, 1, 7, '', '', '001906', NULL, '', NULL, 0, 7, 11, 1, 2, 1, 1, '2022-10-05 23:22:39', '2022-10-06 01:24:40'),
(134, 'Valor enviado para o Rosário.', NULL, '2022-08-16', 10, 2022, 27, 2051.28, 348.72, 2400.00, 6, 4, 7, '', '', '', NULL, '870502683', NULL, 0, 7, 9, 4, 0, NULL, 2, '2022-10-05 23:26:03', '2022-10-20 14:21:24'),
(135, 'Valor enviado para o Rosário. Custo de transação de 50,00MT.', NULL, '2022-08-16', 10, 2022, 23, 6550.00, NULL, 6550.00, 1, 10, 7, '', '', '', NULL, '', NULL, 0, 7, 8, 1, 0, NULL, NULL, '2022-10-05 23:27:37', '2022-10-18 06:51:29'),
(136, 'Valor enviado para o Rosário. Custo de transação de 20,00MT.', NULL, '2022-08-16', 10, 2022, 17, 1809.30, 190.70, 2000.00, 7, 4, NULL, '', '', '', '', '', NULL, NULL, 7, 15, NULL, NULL, NULL, 1, '2022-10-05 23:30:44', '0000-00-00 00:00:00'),
(137, 'Compra efectuada directamente no do M-pesa. A despesa não apresenta VD/Recibo.', NULL, '2022-08-16', 10, 2022, 17, 1809.30, 190.70, 2000.00, 7, 2, NULL, '', '', '', '', '', NULL, NULL, 7, 15, NULL, 136, 5, NULL, '2022-10-05 23:33:34', '0000-00-00 00:00:00'),
(138, 'Valor enviado para o Rosario refente a dois dias. Custo de transação de 10,00MT.', NULL, '2022-08-16', 10, 2022, 11, 1310.00, NULL, 1310.00, 7, 1, NULL, '', '', '', '', '', NULL, NULL, 108, 128, NULL, 2, NULL, NULL, '2022-10-05 23:35:56', '0000-00-00 00:00:00'),
(139, 'Valor solicitado pela Sra. Florinda. Custo de transição de 10,00MT, a despesa nao apresenta Recibo/VD.', NULL, '2022-08-16', 10, 2022, 13, 760.00, NULL, 760.00, 7, 1, NULL, '', '', '', '', '', NULL, NULL, 99, 99, NULL, NULL, NULL, NULL, '2022-10-05 23:41:59', '0000-00-00 00:00:00'),
(140, 'Valor enviado para o Rosário,com o custo de transação de 10,00MT.', NULL, '2022-08-16', 10, 2022, 13, 410.00, NULL, 410.00, 6, 4, 5, '', '', '', NULL, '870502683', NULL, 0, 5, 80, 4, 1, 1, NULL, '2022-10-05 23:45:29', '2022-10-19 09:49:01'),
(141, 'Valor enviado para o Rosário. Custo de transação de 10,00MT.', NULL, '2022-08-16', 10, 2022, 13, 410.00, NULL, 410.00, 6, 12, 5, '', '', '', NULL, '870502683', NULL, 0, 5, 93, 4, 1, 1, NULL, '2022-10-05 23:46:59', '2022-10-19 09:59:49'),
(142, 'Valor enviado para o Rosário com o custo de transação de 05,00MT.', NULL, '2022-08-16', 10, 2022, 13, 405.00, NULL, 405.00, 6, 14, 5, '', '', '', NULL, '870502683', NULL, 0, 5, 169, 4, 1, 1, NULL, '2022-10-05 23:49:42', '2022-10-31 11:27:37'),
(143, 'Valor solicitado pelo Sr. Marco Aurelio e enviado para o Sr. Arthur. Custo de transação de 15,00MT.', NULL, '2022-08-16', 10, 2022, 13, 2415.00, NULL, 2415.00, 6, 1, 99, '', '', '', NULL, '', NULL, 0, 99, 99, 1, 0, NULL, NULL, '2022-10-05 23:51:56', '2022-10-20 14:32:31'),
(144, 'Valor enviado para o Milton. custo de transação de 10,00MT. Pagamento referente a duas semanas sendo, a anterior e a que se segue.', NULL, '2022-08-16', 10, 2022, 13, 610.00, NULL, 610.00, 7, 3, NULL, '', '', '', '', '', NULL, NULL, 5, 77, NULL, NULL, NULL, NULL, '2022-10-06 00:13:56', '0000-00-00 00:00:00'),
(145, 'Valor enviado para o Milton. Pagamento referente a duas semanas sendo, a anterior e a que se encontra.', NULL, '2022-08-16', 10, 2022, 13, 1010.00, NULL, 1010.00, 7, 3, NULL, '', '', '', '', '', NULL, NULL, 5, 78, NULL, NULL, NULL, NULL, '2022-10-06 00:17:09', '0000-00-00 00:00:00'),
(146, 'Valor enviado para o Milton. Custo de transação de 10,00MT.', NULL, '2022-08-16', 10, 2022, 13, 510.00, NULL, 510.00, 7, 3, NULL, '', '', '', '', '', NULL, NULL, 5, 79, NULL, NULL, NULL, NULL, '2022-10-06 00:18:46', '0000-00-00 00:00:00'),
(147, 'Valor enviado para o Milton. Custo de transação de 10,00MT.', NULL, '2022-08-16', 10, 2022, 13, 1010.00, NULL, 1010.00, 6, 3, 5, '', '', '', NULL, '855533809', NULL, 0, 5, 76, 4, 0, NULL, NULL, '2022-10-06 00:22:37', '2022-10-19 09:41:20'),
(148, 'Valor solicitado pela Monica para deslocação paro o Banco. Valor enviado para o Milton.', NULL, '2022-08-16', 10, 2022, 7, 200.00, NULL, 200.00, 7, 3, NULL, '', '', '', '', '', NULL, NULL, 7, 11, NULL, NULL, NULL, NULL, '2022-10-06 00:25:20', '0000-00-00 00:00:00'),
(149, 'Valor de transição não adicionado de 20,00MT. Valor enviado para o Rosario. ', NULL, '2022-08-17', 10, 2022, 17, 2735.08, 264.92, 3000.00, 7, 10, NULL, '', '', '', '', '', NULL, NULL, 7, 15, NULL, NULL, NULL, 1, '2022-10-06 00:33:28', '0000-00-00 00:00:00'),
(150, 'Valor enviado para o Milton, custo de transação de 4,00MT.', NULL, '2022-08-17', 10, 2022, 25, 6810.00, NULL, 6810.00, 7, 3, NULL, '', '', '568', '', '', NULL, NULL, 7, 8, NULL, NULL, NULL, 1, '2022-10-06 00:36:12', '0000-00-00 00:00:00'),
(151, 'Pagamento Parcial, valor enviado para o Rosário com o custo de transação de 25,00MT.', NULL, '2022-08-17', 10, 2022, 13, 6350.00, NULL, 6350.00, 7, 4, NULL, '', '', '', '', '', NULL, NULL, 7, 8, NULL, NULL, NULL, NULL, '2022-10-06 00:45:06', '0000-00-00 00:00:00'),
(152, 'Valor enviado para o Rosário com custo de transação de 10,00MT. Pagamento de Jornal.', NULL, '2022-08-17', 10, 2022, 15, 1510.00, NULL, 1510.00, 1, 1, 99, '', '', '', NULL, '', NULL, 0, 99, 99, 1, 0, NULL, NULL, '2022-10-06 00:48:12', '2022-11-07 12:52:25'),
(153, 'Valor enviado para o Rosário. Custo de transação de 05,00MT.', NULL, '2022-08-17', 10, 2022, 13, 405.00, NULL, 405.00, 6, 10, 5, '', '', '', NULL, '870502683', NULL, 0, 5, 84, 4, 1, 1, NULL, '2022-10-06 00:51:08', '2022-10-19 11:43:07'),
(154, 'Valor enviado para o Sr.Jose da EDM.', NULL, '2022-08-17', 10, 2022, 13, 1000.00, NULL, 1000.00, 6, 3, 13, '', '', '', NULL, '840548473', NULL, 0, 13, 102, 4, 0, NULL, NULL, '2022-10-06 00:52:49', '2022-11-07 13:25:06'),
(155, 'Valor enviado para o Rosário, custo de transação de 10,00MT.', NULL, '2022-08-17', 10, 2022, 13, 660.00, NULL, 660.00, 7, 1, NULL, '', '', '', '', '', NULL, NULL, 108, 128, NULL, NULL, NULL, NULL, '2022-10-06 00:54:18', '0000-00-00 00:00:00'),
(156, 'Valor enviado para o sr. Mariano. Custo de transação de 10,00MT.', NULL, '2022-08-17', 10, 2022, 31, 210.00, NULL, 210.00, 7, 1, NULL, '', '', '000155', '', '', NULL, NULL, 112, 133, NULL, NULL, NULL, 1, '2022-10-06 00:56:51', '0000-00-00 00:00:00'),
(157, 'Valor enviado para Ambasse para a deslocação de mercadoria com o custo de transação de 10,00MT.', NULL, '2022-08-17', 10, 2022, 13, 610.00, NULL, 610.00, 7, 1, NULL, '', '', '', '', '', NULL, NULL, 7, 13, NULL, NULL, NULL, NULL, '2022-10-06 00:58:38', '0000-00-00 00:00:00'),
(158, 'Valor entregue ao Ambasse.', NULL, '2022-08-18', 10, 2022, 12, 3000.00, 0.00, 3000.00, 7, 1, 100, '', '', '0126268', NULL, '', NULL, 0, 100, 116, 1, 34, 3, 1, '2022-10-06 01:01:48', '2022-10-06 03:04:25'),
(159, 'Valor enviado para o Rosário. Custo de transação de 10,00MT.', NULL, '2022-08-18', 10, 2022, 9, 3010.00, NULL, 3010.00, 7, 1, NULL, '', '', '0106851', '', '', NULL, NULL, 100, 119, NULL, 34, 3, 1, '2022-10-06 01:06:47', '0000-00-00 00:00:00'),
(160, 'Pagamento feito directamente do M-pesa. A despesa não apresenta Recibo/VD. ', NULL, '2022-08-18', 10, 2022, 17, 1809.30, 190.70, 2000.00, 7, 2, NULL, '', '', '', '', '', NULL, NULL, 7, 15, NULL, 136, 5, NULL, '2022-10-06 01:10:21', '0000-00-00 00:00:00'),
(161, 'Valor enviado para o Rosário, Custo de transação de 30,00MT.', NULL, '2022-08-18', 10, 2022, 15, 9040.00, NULL, 9040.00, 6, 1, NULL, '', '', '', '', '', NULL, NULL, 99, 99, NULL, NULL, NULL, NULL, '2022-10-06 01:15:45', '2022-10-18 13:36:35'),
(162, 'Valor enviado para Eurico através do M-pesa.', NULL, '2022-08-18', 10, 2022, 13, 210.00, NULL, 210.00, 7, 2, NULL, '', '', '', '', '', NULL, NULL, 5, 64, NULL, 1, NULL, NULL, '2022-10-06 01:18:45', '0000-00-00 00:00:00'),
(163, 'Valor enviado para Ambasse para o envio de mercadoria. Custo de transação de 10,00MT.', NULL, '2022-08-18', 10, 2022, 13, 710.00, NULL, 710.00, 7, 1, NULL, '', '', '', '', '', NULL, NULL, 7, 13, NULL, NULL, NULL, NULL, '2022-10-06 01:25:42', '0000-00-00 00:00:00'),
(164, 'Valor enviado para o Sr. Albino. Custo de transação de 15,00MT.', NULL, '2022-08-19', 10, 2022, 12, 2015.00, NULL, 2015.00, 7, 1, NULL, '', '', '0126371', '', '', NULL, NULL, 100, 115, NULL, 23, 5, 1, '2022-10-06 01:29:05', '0000-00-00 00:00:00'),
(165, 'Valor enviado para o Ambasse. custo de transação de 10,00MT.', NULL, '2022-08-19', 10, 2022, 10, 1310.00, NULL, 1310.00, 7, 1, NULL, '', '', '04113', '', '', NULL, NULL, 7, 13, NULL, NULL, NULL, 1, '2022-10-06 01:30:48', '0000-00-00 00:00:00'),
(166, 'Valor solicitado pelo Fenias para deslocação a Importadora. ', NULL, '2022-08-08', 10, 2022, 13, 200.00, NULL, 200.00, 1, 1, 7, '', '', '', NULL, '', NULL, 0, 7, 11, 1, 0, NULL, NULL, '2022-10-06 01:32:51', '2022-10-18 06:58:35'),
(167, 'Valor enviado para o Milton. Custo de transação de 20,00MT.', NULL, '2022-08-19', 10, 2022, 21, 3770.00, NULL, 3770.00, 1, 2, 9, '', '', '002012', NULL, '', NULL, 0, 9, 147, 1, 5, 1, 1, '2022-10-06 01:34:51', '2022-10-18 13:20:56'),
(168, 'Valor transferido para Florinda. Custo de transação de 20.00MT.', NULL, '2022-08-19', 10, 2022, 13, 9035.00, NULL, 9035.00, 6, 10, 105, '', '', '132/2022', NULL, '842173242', NULL, 0, 105, 157, 4, 0, NULL, 4, '2022-10-06 01:36:34', '2022-10-25 11:47:09'),
(169, 'Valor enviado para o Rosário, referente a dois dias de recolha.', NULL, '2022-08-19', 10, 2022, 11, 1310.00, NULL, 1310.00, 7, 1, NULL, '', '', '', '', '', NULL, NULL, 108, 128, NULL, 2, NULL, NULL, '2022-10-06 01:39:42', '0000-00-00 00:00:00'),
(170, 'Conclusão do pagamento parcial. Valor enviado para o Rosário com o custo de transação de 25,00MT. ', NULL, '2022-08-19', 10, 2022, 1, 6150.00, NULL, 6150.00, 7, 4, NULL, '', '', '', '', '', NULL, NULL, 7, 8, NULL, NULL, NULL, NULL, '2022-10-06 01:42:25', '0000-00-00 00:00:00'),
(171, 'Valor solicitado pelo Sr. Marco para o Sr. Baptista. Custo de transação de 10,00MT.', NULL, '2022-08-19', 10, 2022, 13, 1320.00, NULL, 1320.00, 7, 1, NULL, '', '', '', '', '', NULL, NULL, 99, 99, NULL, NULL, NULL, NULL, '2022-10-06 01:44:46', '2022-10-18 13:36:35'),
(172, 'Valor entregue ao Rosário. a despesa não apresenta VD.', NULL, '2022-08-21', 10, 2022, 13, 400.00, NULL, 400.00, 7, 14, NULL, '', '', '', '', '', NULL, NULL, 4, 61, NULL, NULL, NULL, NULL, '2022-10-06 02:25:12', '0000-00-00 00:00:00'),
(173, 'Valor enviado para o Rosário.', NULL, '2022-08-21', 10, 2022, 38, 500.00, NULL, 500.00, 7, 12, NULL, '', '', '00507', '', '', NULL, NULL, 4, 61, NULL, NULL, NULL, 1, '2022-10-06 02:37:14', '0000-00-00 00:00:00'),
(174, 'Valor entregue ao Rosario.', NULL, '2022-08-21', 10, 2022, 40, 500.00, NULL, 500.00, 7, 10, NULL, '', '', '59593', '', '', NULL, NULL, 4, 61, NULL, NULL, NULL, 1, '2022-10-06 02:40:02', '0000-00-00 00:00:00'),
(175, 'Valor enviado ao Rosário. a despesa não apresenta Recibo/VD.', NULL, '2022-08-21', 10, 2022, 17, 3000.00, NULL, 3000.00, 7, 16, NULL, '', '', '', '', '', NULL, NULL, 7, 15, NULL, NULL, NULL, NULL, '2022-10-06 02:41:47', '0000-00-00 00:00:00'),
(176, 'Pagamento do transporte para Técnico de Farmácia, no Domingo. Valor enviado para o Rosário.', NULL, '2022-08-21', 10, 2022, 13, 150.00, NULL, 150.00, 7, 10, NULL, '', '', '', '', '', NULL, NULL, 7, 11, NULL, NULL, NULL, NULL, '2022-10-06 02:45:04', '0000-00-00 00:00:00'),
(177, 'Pagamento de deslocação para Caixa. Valor enviado para Rosario.', NULL, '2022-08-21', 10, 2022, 13, 150.00, NULL, 150.00, 7, 10, NULL, '', '', '', '', '', NULL, NULL, 7, 11, NULL, NULL, NULL, NULL, '2022-10-06 02:46:55', '0000-00-00 00:00:00'),
(178, 'Valor enviado para o Ambasse, custo de transação de 15,00MT.', NULL, '2022-08-22', 10, 2022, 12, 3015.00, NULL, 3015.00, 7, 1, NULL, '', '', '00398', '', '', NULL, NULL, 100, 116, NULL, NULL, NULL, 1, '2022-10-06 02:59:11', '0000-00-00 00:00:00'),
(179, 'valor enviado ao Sr. Albino. Custo de transição de 10,00MT.', NULL, '2022-08-22', 10, 2022, 41, 2010.00, NULL, 2010.00, 7, 1, NULL, '', '', '0125572', '', '', NULL, NULL, 100, 115, NULL, 23, 3, 1, '2022-10-06 03:03:01', '0000-00-00 00:00:00'),
(180, 'Valor enviado para Rosário. Custo de transação de 10,00MT.', NULL, '2022-08-22', 10, 2022, 17, 3010.00, NULL, 3010.00, 7, 16, NULL, '', '', '', '', '', NULL, NULL, 7, 15, NULL, 264, 5, 1, '2022-10-06 03:04:58', '0000-00-00 00:00:00'),
(181, 'Valor enviado para o Shamir, para efeitos de compra de Tabuleiro Opac. Custo de transação 10,00 MT', NULL, '2022-08-22', 10, 2022, 42, 1690.00, NULL, 1690.00, 7, 3, NULL, '', '', 'AA187692', '', '', NULL, NULL, 106, 130, NULL, 7, 1, 1, '2022-10-06 03:13:14', '2022-10-18 13:34:28'),
(182, 'Valor enviado para o Shamir para a compra de Tabuleiro Opac para o Sr. Marco, custo de transação 10,00MT', NULL, '2022-08-22', 10, 2022, 42, 2890.00, NULL, 2890.00, 7, 3, NULL, '', '', 'AA187692', '', '', NULL, NULL, 106, 130, NULL, 12, 1, 1, '2022-10-06 03:16:19', '2022-10-18 13:34:28'),
(183, 'Pagamento de Redbull. Custo de transação 15,00MT', NULL, '2022-08-22', 10, 2022, 13, 3415.00, NULL, 3415.00, 7, 2, NULL, '', '', '', '', '', NULL, NULL, 99, 99, NULL, 24, 1, NULL, '2022-10-06 03:19:37', '2022-10-18 13:36:35'),
(184, 'Valor enviado para Rosario, custo de transação 10,00MT', NULL, '2022-08-22', 10, 2022, 13, 410.00, NULL, 410.00, 6, 4, 5, '', '', '', NULL, '870502683', NULL, 0, 5, 80, 5, 1, 1, NULL, '2022-10-06 03:21:08', '2022-10-19 10:43:45'),
(185, 'Valor enviado para Rosário, custo de transação 5,00MT.', NULL, '2022-08-22', 10, 2022, 13, 405.00, NULL, 405.00, 6, 14, 5, '', '', '', NULL, '870502683', NULL, 0, 5, 168, 4, 1, NULL, NULL, '2022-10-06 03:23:40', '2022-10-31 11:19:49'),
(186, 'Valor enviado para o Rosário.', NULL, '2022-08-22', 10, 2022, 13, 400.00, NULL, 400.00, 6, 14, 5, '', '', '', NULL, '870502683', NULL, 0, 5, 169, 4, 1, 1, NULL, '2022-10-06 03:27:43', '2022-10-31 11:21:26'),
(187, 'Valor enviado para o Sr. Albino, custo de transação 05.00MT.', NULL, '2022-08-22', 10, 2022, 13, 505.00, NULL, 505.00, 6, 19, 104, '', '', '', NULL, '84569961', NULL, 0, 104, 124, 4, 0, NULL, NULL, '2022-10-06 03:31:20', '2022-10-18 07:11:34'),
(188, 'Valor enviado para Rosário, custo de transação não adicionado de 10,00MT', NULL, '2022-08-22', 10, 2022, 43, 3112.50, 637.50, 3750.00, 1, 10, 9, '', '', '000437', NULL, '', NULL, 0, 9, 147, 1, 5, 1, 1, '2022-10-06 03:44:41', '2022-10-18 13:19:28'),
(189, 'Valor enviado para o Rosário, Custo de transação não adicional de 10,00MT', NULL, '2022-08-22', 10, 2022, 43, 3112.50, 637.50, 3750.00, 1, 12, 9, '', '', '000437', NULL, '', NULL, 0, 9, 147, 1, 5, 1, 1, '2022-10-06 03:47:02', '2022-10-18 13:20:07'),
(190, 'Compra de Guia de Remessa para Florinda, Valor enviado para Rosário, Custo de transação não adicional de 10,00MT', NULL, '2022-08-22', 10, 2022, 43, 3112.50, 637.50, 3750.00, 1, 1, 9, '', '', '000437', NULL, '', NULL, 0, 9, 164, 1, 5, 1, 1, '2022-10-06 03:49:47', '2022-10-18 13:18:02'),
(191, 'Valor enviado para Isabel, Custo de transacao 14.00MT', NULL, '2022-08-22', 10, 2022, 13, 314.00, NULL, 314.00, 7, 2, NULL, '', '', '', '', '', NULL, NULL, 5, 65, NULL, 1, 1, NULL, '2022-10-06 03:51:47', '0000-00-00 00:00:00'),
(192, 'Valor enviado para Ambasse, para envio de mercadorias para a Beira, Custo de transação 10.00MT ', NULL, '2022-08-22', 10, 2022, 13, 1310.00, NULL, 1310.00, 7, 1, NULL, '', '', '', '', '', NULL, NULL, 7, 13, NULL, NULL, NULL, NULL, '2022-10-06 03:53:44', '0000-00-00 00:00:00'),
(193, 'Valor enviado para o Rosário, com o custo de transação de 05,00MT.', NULL, '2022-08-22', 10, 2022, 13, 405.00, NULL, 405.00, 6, 10, 5, '', '', '', NULL, '870502683', NULL, 0, 5, 84, 4, 1, 1, NULL, '2022-10-06 03:55:17', '2022-10-19 11:00:44'),
(194, 'Valor enviado para Rosário, Custo de transação de 5.00MT', NULL, '2022-08-22', 10, 2022, 5, 1005.00, NULL, 1005.00, 6, 10, 3, '', '', '', NULL, '870502683', NULL, 0, 3, 100, 4, 1, 1, NULL, '2022-10-06 03:57:09', '2022-10-18 13:40:18'),
(195, 'Valor enviado para Rosário, Custo de transação de 10.00MT', NULL, '2022-08-22', 10, 2022, 17, 3010.00, NULL, 3010.00, 7, 10, NULL, '', '', '', '', '', NULL, NULL, 7, 15, NULL, NULL, NULL, NULL, '2022-10-06 03:58:51', '0000-00-00 00:00:00'),
(196, 'Valor enviado para Monica, deslocação para o Banco. A despesa não apresenta VD/Recibo.', NULL, '2022-08-22', 10, 2022, 7, 110.00, NULL, 110.00, 7, 1, NULL, '', '', '', '', '', NULL, NULL, 7, 11, NULL, 1, NULL, NULL, '2022-10-06 04:38:32', '0000-00-00 00:00:00'),
(197, 'Vale efectuado pelo Rosário. ', NULL, '2022-08-22', 10, 2022, 13, 4050.00, NULL, 4050.00, 7, 1, NULL, '', '', '', '', '', NULL, NULL, 99, 99, NULL, NULL, NULL, NULL, '2022-10-06 04:52:14', '0000-00-00 00:00:00'),
(198, 'Valor enviado para Sra. Anilsa para deslocação as Importadoras e FME Beach Front. Custo de transação de 15,00MT.', NULL, '2022-08-23', 10, 2022, 44, 2015.00, NULL, 2015.00, 7, 1, NULL, '', '', '015955', '', '', NULL, NULL, 100, 143, NULL, 23, 3, 1, '2022-10-06 05:18:37', '0000-00-00 00:00:00'),
(199, 'Pagamento de rolos de Etiqueta. Valor enviado para Milton com custo de transação de 20,00MT.', NULL, '2022-08-23', 10, 2022, 20, 1520.00, NULL, 1520.00, 7, 2, NULL, '', '', '003798', '', '', NULL, NULL, 106, 130, NULL, 3, 1, 1, '2022-10-06 05:23:38', '2022-10-18 13:34:28'),
(200, 'Valor entregue ao Milton.', 0, '2022-08-10', 10, 2022, 31, 200.00, NULL, 200.00, 3, 1, NULL, '', '', '000153', '', '', NULL, 1, 112, 133, 4, 1, 1, 1, '2022-10-06 06:24:17', '0000-00-00 00:00:00'),
(201, 'Valor enviado ao Taverna. Custo de transação de 10,00MT não adicional.', NULL, '2022-08-10', 10, 2022, 22, 299.15, 50.85, 350.00, 7, 3, NULL, '', '', '100469261', '', '', NULL, NULL, 102, 145, NULL, 1, 1, 2, '2022-10-06 06:30:36', '0000-00-00 00:00:00'),
(202, 'Valor enviado para o Rosário. Custo de transação de 10,00MT.', NULL, '2022-08-23', 10, 2022, 26, 1710.00, NULL, 1710.00, 7, 1, NULL, '', '', '95160', '', '', NULL, NULL, 8, 146, NULL, 20, 3, 1, '2022-10-06 12:25:51', '0000-00-00 00:00:00'),
(203, 'Valor enviado para o Milton, custo de transição de 9,00MT não adicional.', NULL, '2022-08-23', 10, 2022, 25, 8300.00, 1411.00, 9711.00, 7, 3, NULL, '', '', '486/2022', '', '', NULL, NULL, 7, 8, NULL, 1, 1, 1, '2022-10-06 12:39:15', '0000-00-00 00:00:00'),
(204, 'Valor entregue ao Ambasse.', 0, '2022-08-12', 10, 2022, 13, 500.00, NULL, 500.00, 3, 3, 5, '', '', '', NULL, '', NULL, 1, 5, 70, 1, 1, 1, NULL, '2022-10-06 13:21:19', '2022-10-18 07:51:19'),
(205, 'Valor enviado ao Milton.', NULL, '2022-08-23', 10, 2022, 13, 510.00, NULL, 510.00, 7, 3, NULL, '', '', '', '', '', NULL, NULL, 5, 70, NULL, 1, 1, NULL, '2022-10-07 08:20:50', '0000-00-00 00:00:00'),
(206, 'Valor enviado ao Milton.', NULL, '2022-08-23', 10, 2022, 13, 510.00, NULL, 510.00, 7, 3, NULL, '', '', '', '', '', NULL, NULL, 5, 73, NULL, 1, 1, NULL, '2022-10-07 08:24:58', '0000-00-00 00:00:00'),
(207, 'Valor enviado para o Milton.', NULL, '2022-08-23', 10, 2022, 13, 510.00, NULL, 510.00, 7, 3, NULL, '', '', '', '', '', NULL, NULL, 5, 74, NULL, 1, 1, NULL, '2022-10-07 09:40:43', '0000-00-00 00:00:00'),
(208, 'Valor entregue ao Isaac.', 0, '2022-08-23', 10, 2022, 13, 300.00, NULL, 300.00, 3, 3, NULL, '', '', '', '', '', NULL, 1, 5, 75, 4, 1, 1, NULL, '2022-10-07 09:45:16', '0000-00-00 00:00:00'),
(209, 'Valor enviado ao Milton.', 0, '2022-08-23', 10, 2022, 13, 1000.00, NULL, 1000.00, 3, 3, NULL, '', '', '', '', '', NULL, 1, 5, 76, 4, 1, 1, NULL, '2022-10-07 09:58:08', '0000-00-00 00:00:00'),
(210, 'Valor enviado ao Milton.', NULL, '2022-08-23', 10, 2022, 13, 310.00, 0.00, 310.00, 7, 3, 5, '', '', '', NULL, '', NULL, 0, 5, 77, 1, 1, 1, 0, '2022-10-07 12:18:10', '2022-10-08 09:44:58'),
(211, 'Valor enviado para o Milton.', NULL, '2022-08-23', 10, 2022, 13, 510.00, NULL, 510.00, 7, 3, NULL, '', '', '', '', '', NULL, NULL, 5, 79, NULL, 1, 1, NULL, '2022-10-08 07:43:44', '0000-00-00 00:00:00'),
(212, 'Valor enviado ao Milton.', NULL, '2022-08-23', 10, 2022, 13, 500.00, NULL, 500.00, 7, 3, NULL, '', '', '', '', '', NULL, NULL, 5, 78, NULL, 1, 1, NULL, '2022-10-08 07:46:03', '0000-00-00 00:00:00'),
(213, 'Valor enviado para a Sra. Ana Dias.', NULL, '2022-08-23', 10, 2022, 13, 505.00, NULL, 505.00, 7, 2, NULL, '', '', '', '', '', NULL, NULL, 5, 63, NULL, 1, 1, NULL, '2022-10-08 07:53:19', '0000-00-00 00:00:00');
INSERT INTO `execucaodespesas` (`idexecucaodespesas`, `descricaodespesa`, `idrequisicaodefundos`, `data`, `mes`, `ano`, `idempresa`, `valorinicial`, `iva`, `valortotal`, `idformapagamento`, `idcentrodecusto`, `idgrupodespesas`, `observacoes`, `anexo`, `recibo`, `numerocheque`, `transfcontadestino`, `transferenciabanco`, `caixa`, `grupo`, `subgrupo`, `banco`, `quantidade`, `unidade`, `tipo_documento_id`, `data_criacao`, `data_atualizacao`) VALUES
(214, 'Valor enviado para a Sra Eufrásia com o custo de transação de 30,00MT.', NULL, '2022-08-23', 10, 2022, 12, 2730.00, NULL, 2730.00, 6, 1, 100, '', '', '', NULL, '846538861', NULL, 0, 100, 120, 4, 0, NULL, NULL, '2022-10-08 08:53:38', '2022-10-28 05:59:48'),
(215, 'Valor enviado ao Rosário. Custo de transação de 10,00MT.', NULL, '2022-08-23', 10, 2022, 11, 660.00, NULL, 660.00, 7, 1, NULL, '', '', '', '', '', NULL, NULL, 108, 128, NULL, 1, 1, NULL, '2022-10-08 09:08:08', '0000-00-00 00:00:00'),
(216, 'Valor enviado para o Milton, custo de transação de 20,00MT. ', NULL, '2022-08-23', 10, 2022, 15, 3770.00, NULL, 3770.00, 7, 3, NULL, '', '', '', '', '', NULL, NULL, 9, 147, NULL, 5, 1, NULL, '2022-10-10 05:49:14', '0000-00-00 00:00:00'),
(217, 'Valor enviado para o Milton para efectuar a compra, com o custo de transação de 10,00MT.', NULL, '2022-08-23', 10, 2022, 17, 2010.00, NULL, 2010.00, 7, 2, NULL, '', '', '', '', '', NULL, NULL, 7, 15, NULL, NULL, NULL, NULL, '2022-10-10 05:53:54', '0000-00-00 00:00:00'),
(218, 'Pagamento de Copias Coloridas A3. Valor entregue a Nelson.', NULL, '2022-08-23', 10, 2022, 13, 40.00, NULL, 40.00, 3, 2, 99, '', '', '', NULL, '', NULL, 1, 99, 99, 1, 4, 1, NULL, '2022-10-10 06:03:08', '2022-10-20 22:41:11'),
(219, 'Valor enviado para o Rosario con o custo de 50,00MT.', NULL, '2022-08-23', 10, 2022, 15, 7700.00, NULL, 7700.00, 7, 12, NULL, '', '', '', '', '', NULL, NULL, 7, 8, NULL, NULL, NULL, NULL, '2022-10-10 06:12:26', '0000-00-00 00:00:00'),
(220, 'Valor enviado para o Ambasse para o envio de Medicamento Maputo-Beira, com o custo de transacao de 30,00MT.', NULL, '2022-08-23', 10, 2022, 13, 2730.00, NULL, 2730.00, 7, 1, NULL, '', '', '', '', '', NULL, NULL, 7, 13, NULL, NULL, NULL, NULL, '2022-10-10 06:16:02', '0000-00-00 00:00:00'),
(221, 'Valor enviado para o Rosario, com o custo transacao de 50,00MT.', NULL, '2022-08-23', 10, 2022, 12, 2800.00, NULL, 2800.00, 1, 1, 100, '', '', '', NULL, '', NULL, 0, 100, 139, 1, 0, NULL, NULL, '2022-10-10 06:20:03', '2022-10-28 06:20:54'),
(222, 'Valor entregue ao Ambasse.\r\n', 0, '2022-08-24', 10, 2022, 12, 3000.00, NULL, 3000.00, 3, 1, NULL, '', '', '0043210', '', '', NULL, 1, 100, 116, 4, 34, 3, 1, '2022-10-10 09:27:15', '0000-00-00 00:00:00'),
(223, 'Valor entregue ao Milton.', 0, '2022-08-24', 10, 2022, 46, 2019.96, 185.73, 2205.69, 3, 3, NULL, '', '', '31004758722', '', '', NULL, 1, 7, 16, 4, NULL, NULL, 2, '2022-10-10 11:08:19', '0000-00-00 00:00:00'),
(224, 'Valor enviado para o Milton com o custo de transação de 10,00MT.', NULL, '2022-08-24', 10, 2022, 20, 760.00, NULL, 760.00, 7, 2, NULL, '', '', '004135', '', '', NULL, NULL, 99, 99, NULL, 5, 1, 1, '2022-10-10 13:07:49', '0000-00-00 00:00:00'),
(225, 'Compra de: GL LEVER ARCH FILE BLACK MOTT, valor enviado para o Milton com o custo de transação de 15,00MT.', NULL, '2022-08-24', 10, 2022, 42, 1395.00, 0.00, 1395.00, 6, 2, 110, '', '', 'AA187692', NULL, '845533809', NULL, 0, 106, 130, 4, 6, 1, 1, '2022-10-10 13:18:50', '2022-10-18 13:34:28'),
(226, 'Valor enviado para o Milton com o custo de transação de 20,00MT. ', NULL, '2022-08-24', 10, 2022, 42, 1360.00, NULL, 1360.00, 7, 3, NULL, '', '', 'AA187692', '', '', NULL, NULL, 106, 130, NULL, 4, 1, 1, '2022-10-10 13:24:52', '2022-10-18 13:34:28'),
(227, 'Valor enviado para o Rosário.', NULL, '2022-08-24', 10, 2022, 12, 3000.00, NULL, 3000.00, 7, 1, NULL, '', '', '16728', '', '', NULL, NULL, 100, 119, NULL, NULL, NULL, 1, '2022-10-10 13:30:49', '0000-00-00 00:00:00'),
(228, 'Valor enviado para o Rosário.', NULL, '2022-08-24', 10, 2022, 17, 2000.00, NULL, 2000.00, 7, 4, NULL, '', '', '789029334', '', '', NULL, NULL, 7, 15, NULL, 136, 5, 1, '2022-10-10 13:38:11', '0000-00-00 00:00:00'),
(229, 'Valor enviado para o Rosário com o custo de transação não adicionado de 10,00MT.', NULL, '2022-08-24', 10, 2022, 19, 854.70, 145.30, 1000.00, 7, 10, NULL, '', '', '52745942', '', '', NULL, NULL, 3, 100, NULL, 1, 1, 1, '2022-10-10 13:45:05', '0000-00-00 00:00:00'),
(230, 'Valor enviado para o Rosário com o custo de transação de 10,00MT.', NULL, '2022-08-24', 10, 2022, 47, 460.00, NULL, 460.00, 7, 10, NULL, '', '', '04434', '', '', NULL, NULL, 106, 130, NULL, 1, 1, 1, '2022-10-10 13:57:28', '2022-10-18 13:34:28'),
(231, 'Pagamento de: Encadernação de documento da Contabilidade FME Beira.', NULL, '2022-08-24', 10, 2022, 15, 640.62, 108.91, 749.53, 7, 1, NULL, '', '', '2945/2022A', '', '', NULL, NULL, 99, 99, NULL, NULL, NULL, 2, '2022-10-10 14:05:32', '0000-00-00 00:00:00'),
(232, 'Valor entregue ao Eurico.', 0, '2022-08-24', 10, 2022, 13, 200.00, NULL, 200.00, 3, 2, NULL, '', '', '', '', '', NULL, 1, 5, 64, 4, 1, 1, NULL, '2022-10-11 07:29:04', '0000-00-00 00:00:00'),
(233, 'Valor entregue ao Sr. Jorge.', 0, '2022-08-24', 10, 2022, 13, 200.00, NULL, 200.00, 3, 2, NULL, '', '', '', '', '', NULL, 1, 5, 66, 4, 1, 1, NULL, '2022-10-11 08:40:35', '0000-00-00 00:00:00'),
(234, 'Valor entregue ao Sr. Raimundo.\r\n', 0, '2022-08-24', 10, 2022, 13, 200.00, NULL, 200.00, 3, 2, NULL, '', '', '', '', '', NULL, 1, 5, 68, 4, 1, 1, NULL, '2022-10-11 08:58:03', '0000-00-00 00:00:00'),
(235, 'Valor entregue ao Sr. Manuel.', 0, '2022-08-24', 10, 2022, 13, 200.00, NULL, 200.00, 3, 2, NULL, '', '', '', '', '', NULL, 1, 5, 67, 4, 1, 1, NULL, '2022-10-11 09:06:49', '0000-00-00 00:00:00'),
(236, 'Valor enviado para a Sra. Monica com o custo de transação de 10,00MT. Deslocação para o Banco.', NULL, '2022-08-24', 10, 2022, 7, 210.00, NULL, 210.00, 7, 1, NULL, '', '', '', '', '', NULL, NULL, 7, 11, NULL, NULL, NULL, NULL, '2022-10-11 09:28:16', '0000-00-00 00:00:00'),
(237, 'Pagamento de caixa de Canetas. Valor enviado para o Rosário, custo de transação de 10,00MT.', NULL, '2022-08-24', 10, 2022, 15, 1510.00, NULL, 1510.00, 7, 10, NULL, '', '', '', '', '', NULL, NULL, 106, 130, NULL, 1, 9, NULL, '2022-10-11 17:50:33', '2022-10-18 13:34:28'),
(238, 'Compra de Pasta de Arquivo. Valor entregue ao Milton.', 0, '2022-08-24', 10, 2022, 42, 1340.00, NULL, 1340.00, 3, 3, NULL, '', '', '', '', '', NULL, 1, 106, 130, 4, 4, 1, NULL, '2022-10-11 18:06:29', '2022-10-18 13:34:28'),
(239, 'Compra de 02 caixas de Resma. Valor enviado para o Rosário com o custo de transação de 20,00MT.', NULL, '2022-08-24', 10, 2022, 15, 4220.00, NULL, 4220.00, 7, 10, NULL, '', '', '', '', '', NULL, NULL, 106, 130, NULL, 2, 9, NULL, '2022-10-11 18:18:37', '2022-10-18 13:34:28'),
(240, 'Compra de pastas de Arquivo. Valor entregue ao Milton.', 0, '2022-08-24', 10, 2022, 42, 1380.00, NULL, 1380.00, 3, 3, NULL, '', '', '', '', '', NULL, 1, 106, 130, 4, 6, 1, NULL, '2022-10-11 18:26:15', '2022-10-18 13:34:28'),
(241, 'Pagamento de manutenção de Impressora. Valor entregue ao Milton.', 0, '2022-08-24', 10, 2022, 15, 3180.00, NULL, 3180.00, 3, 2, 13, '', '', '', NULL, '', NULL, 1, 13, 144, 4, 0, NULL, NULL, '2022-10-11 19:02:32', '2022-10-18 13:26:45'),
(242, 'Valor entregue ao Mohamad.\r\n', 0, '2022-08-24', 10, 2022, 13, 500.00, NULL, 500.00, 3, 2, NULL, '', '', '', '', '', NULL, 1, 5, 152, 4, 1, 1, NULL, '2022-10-11 19:28:35', '0000-00-00 00:00:00'),
(243, 'Valor entregue ao Milton.\r\n', 0, '2022-08-25', 10, 2022, 6, 3000.00, NULL, 3000.00, 3, 1, NULL, '', '', '09276', '', '', NULL, 1, 100, 117, 4, 54, 3, 1, '2022-10-11 20:31:48', '0000-00-00 00:00:00'),
(244, 'Valor entregue ao Ambasse.', 0, '2022-08-25', 10, 2022, 19, 854.70, 145.30, 1000.00, 3, 3, NULL, '', '', '09549567', '', '', NULL, 1, 3, 51, 4, 1, 1, 1, '2022-10-11 20:39:12', '0000-00-00 00:00:00'),
(245, 'Valor entregue ao Ambasse.', 0, '2022-08-25', 10, 2022, 19, 1000.00, NULL, 1000.00, 3, 2, NULL, '', '', '', '', '', NULL, 1, 3, 52, 4, 1, 1, NULL, '2022-10-11 20:45:39', '0000-00-00 00:00:00'),
(246, 'Valor solicitado pelo Fenias, para deslocação as Seguradoras.', NULL, '2022-08-25', 10, 2022, 13, 214.00, NULL, 214.00, 7, 1, NULL, '', '', '', '', '', NULL, NULL, 7, 11, NULL, 1, 1, NULL, '2022-10-11 20:50:11', '0000-00-00 00:00:00'),
(247, 'Valor solicitado pela Sra. Florinda, com o custo de transação de 18,00MT.', NULL, '2022-08-25', 10, 2022, 15, 718.00, NULL, 718.00, 7, 1, NULL, '', '', '', '', '', NULL, NULL, 99, 99, NULL, NULL, NULL, NULL, '2022-10-11 20:54:04', '0000-00-00 00:00:00'),
(248, 'Valor solicitado pelo Ambasse para o envio de medicamento Maputo-Beira.', 0, '2022-08-25', 10, 2022, 15, 1500.00, NULL, 1500.00, 3, 1, NULL, '', '', '', '', '', NULL, 1, 7, 13, 4, NULL, NULL, NULL, '2022-10-11 21:01:30', '0000-00-00 00:00:00'),
(249, 'Valor transferido para o Rosario. ', NULL, '2022-08-25', 10, 2022, 11, 650.00, NULL, 650.00, 7, 1, NULL, '', '', '', '', '', NULL, NULL, 108, 129, NULL, 1, 1, NULL, '2022-10-11 21:06:38', '0000-00-00 00:00:00'),
(250, 'Valor entregue ao Shamir.', 0, '2022-08-25', 10, 2022, 31, 200.00, NULL, 200.00, 3, 1, NULL, '', '', '', '', '', NULL, 1, 112, 133, 4, 1, 1, NULL, '2022-10-11 21:15:05', '0000-00-00 00:00:00'),
(251, 'Compra de Redbull.', 0, '2022-08-25', 10, 2022, 13, 1600.00, NULL, 1600.00, 3, 3, NULL, '', '', '', '', '', NULL, 1, 99, 99, 4, NULL, NULL, NULL, '2022-10-11 21:23:01', '2022-10-18 13:36:35'),
(252, 'Valor entregue ao Milton para compra de Extensao para o Sr. Mariano.', 0, '2022-08-25', 10, 2022, 13, 1780.00, NULL, 1780.00, 3, 3, NULL, '', '', '', '', '', NULL, 1, 99, 99, 4, 1, 1, NULL, '2022-10-11 21:28:32', '0000-00-00 00:00:00'),
(253, 'Valor entregue ao Milton para compra de Fixa elétrica.', 0, '2022-08-25', 10, 2022, 13, 130.00, NULL, 130.00, 3, 3, NULL, '', '', '', '', '', NULL, 1, 99, 99, 4, 1, 1, NULL, '2022-10-11 21:35:29', '0000-00-00 00:00:00'),
(254, 'Compra efectuada pela Diana.', 0, '2022-08-26', 10, 2022, 17, 1809.30, 190.70, 2000.00, 3, 2, 7, '', '', '', NULL, '', NULL, 1, 7, 15, 4, 136, 5, 1, '2022-10-11 21:43:06', '2022-10-11 23:43:48'),
(255, 'Valor solicitado e entregue a Milton.', 0, '2022-08-26', 10, 2022, 21, 3750.00, NULL, 3750.00, 3, 3, NULL, '', '', '002014', '', '', NULL, 1, 9, 147, 4, 5, 1, 1, '2022-10-11 21:45:55', '0000-00-00 00:00:00'),
(256, 'Compra de Transformador para o Letreiro. ', 0, '2022-08-26', 10, 2022, 48, 2350.00, NULL, 2350.00, 3, 2, NULL, '', '', '00273', '', '', NULL, 1, 13, 102, 4, 1, 1, 1, '2022-10-12 01:25:52', '0000-00-00 00:00:00'),
(257, 'Compra de bebida para o Sr. Marco, valor entregue ao Ambasse. A despesa não apresenta Recibo/VD.', 0, '2022-08-26', 10, 2022, 15, 2000.00, NULL, 2000.00, 3, 17, NULL, '', '', '', '', '', NULL, 1, 99, 99, 4, 2, 9, NULL, '2022-10-12 01:30:12', '0000-00-00 00:00:00'),
(258, 'Valor enviado para o Rosário, com o custo de transação de 10,00MT. A despesa não apresenta recibo?VD.', NULL, '2022-08-26', 10, 2022, 11, 660.00, NULL, 660.00, 7, 1, NULL, '', '', '', '', '', NULL, NULL, 108, 129, NULL, 1, 1, NULL, '2022-10-12 01:33:14', '0000-00-00 00:00:00'),
(259, 'Valor enviado para o Rosário com o custo de transação de 10,00MT.', NULL, '2022-08-26', 10, 2022, 12, 2710.00, NULL, 2710.00, 7, 1, NULL, '', '', '', '', '', NULL, NULL, 100, 139, NULL, NULL, NULL, NULL, '2022-10-12 01:35:44', '0000-00-00 00:00:00'),
(260, 'Compra de Marcadores para a FME Beira, valor solicitado pelo Francisco. Custo de transação de 50,00MT.', NULL, '2022-08-26', 10, 2022, 13, 1050.00, NULL, 1050.00, 6, 10, 106, '', '', '', NULL, '870502683', NULL, 0, 106, 130, 4, 1, 9, NULL, '2022-10-12 01:42:25', '2022-10-20 22:58:13'),
(261, 'Valor enviado para o Rosário com o custo de transacao de 10,00MT.', NULL, '2022-08-27', 10, 2022, 11, 660.00, NULL, 660.00, 7, 1, NULL, '', '', '', '', '', NULL, NULL, 108, 128, NULL, 1, 1, NULL, '2022-10-12 02:53:17', '0000-00-00 00:00:00'),
(262, 'Valor solicitado pela Florindaa, com o custo de transação de 50,00MT', NULL, '2022-08-27', 10, 2022, 15, 7050.00, NULL, 7050.00, 7, 1, NULL, '', '', '', '', '', NULL, NULL, 99, 99, NULL, NULL, NULL, NULL, '2022-10-12 03:00:11', '0000-00-00 00:00:00'),
(263, 'Compra de 01 caixinha de Clips. Valor solicitado por Francisco, valor enviado para o Rosário.', NULL, '2022-08-27', 10, 2022, 15, 660.00, NULL, 660.00, 7, 10, NULL, '', '', '', '', '', NULL, NULL, 106, 130, NULL, 1, 9, NULL, '2022-10-12 03:05:00', '2022-10-18 13:34:28'),
(264, 'Compra de Caixinha de agrafos, requisição feita pelo Francisco. Valor enviado para o Rosário.', NULL, '2022-08-27', 10, 2022, 15, 600.00, NULL, 600.00, 7, 10, NULL, '', '', '', '', '', NULL, NULL, 106, 130, NULL, 1, 9, NULL, '2022-10-12 03:08:21', '2022-10-18 13:34:28'),
(265, 'Valor enviado para o Rosário com o custo de transação de 10,00MT.', NULL, '2022-08-27', 10, 2022, 35, 510.00, NULL, 510.00, 7, 10, NULL, '', '', '000751', '', '', NULL, NULL, 4, 61, NULL, 1, 1, 1, '2022-10-12 03:14:20', '0000-00-00 00:00:00'),
(266, 'Valor enviado para o Rosario.', NULL, '2022-08-27', 10, 2022, 40, 500.00, 0.00, 500.00, 7, 14, 4, '', '', '59871', NULL, '', NULL, 0, 4, 61, 1, 1, 9, 1, '2022-10-12 03:19:57', '2022-10-12 05:31:16'),
(267, 'Valor enviado para o Rosário.', NULL, '2022-08-27', 10, 2022, 49, 500.00, NULL, 500.00, 7, 4, NULL, '', '', '0000061', '', '', NULL, NULL, 4, 61, NULL, 1, 1, 1, '2022-10-12 03:30:21', '0000-00-00 00:00:00'),
(268, 'Valor enviado para o Rosário com o de transação de 10,00MT. A despesa não apresenta Recibo/VD.', NULL, '2022-08-27', 10, 2022, 15, 410.00, NULL, 410.00, 7, 12, NULL, '', '', '', '', '', NULL, NULL, 4, 61, NULL, 1, 9, NULL, '2022-10-12 03:34:34', '0000-00-00 00:00:00'),
(269, 'Valor entregue a Milton.', 1, '2022-08-27', 10, 2022, 50, 875.00, NULL, 875.00, 3, 3, NULL, '', '', '2096', '', '', NULL, 1, 106, 130, 4, 1, 1, 1, '2022-10-12 04:01:24', '2022-10-18 13:34:28'),
(270, 'Valor entregue ao Milton.', 1, '2022-08-27', 10, 2022, 27, 4100.00, NULL, 4100.00, 3, 2, NULL, '', '', '', '', '', NULL, 1, 7, 9, 4, NULL, NULL, 2, '2022-10-12 04:06:00', '0000-00-00 00:00:00'),
(271, 'Valor entregue ao Caixa de Serviço. ', 1, '2022-08-27', 10, 2022, 13, 1000.00, NULL, 1000.00, 3, 2, NULL, '', '', '', '', '', NULL, 1, 4, 61, 4, 1, 1, NULL, '2022-10-12 04:08:38', '0000-00-00 00:00:00'),
(272, 'Valor entregue ao caixa de serviço.', 1, '2022-08-27', 10, 2022, 51, 1000.00, NULL, 1000.00, 3, 3, NULL, '', '', '1168', '', '', NULL, 1, 4, 61, 4, 1, 9, 1, '2022-10-12 04:14:01', '0000-00-00 00:00:00'),
(273, 'Valor enviado para o Rosário.', NULL, '2022-08-27', 10, 2022, 17, 1809.30, 190.70, 2000.00, 7, 4, NULL, '', '', '', '', '', NULL, NULL, 7, 15, NULL, 136, 5, 1, '2022-10-12 04:23:25', '0000-00-00 00:00:00'),
(274, 'Valor entregue ao Ambasse.', 1, '2022-08-28', 10, 2022, 12, 3000.00, NULL, 3000.00, 3, 1, NULL, '', '', '0127116', '', '', NULL, 1, 100, 116, 4, 34, 3, 1, '2022-10-12 04:38:03', '0000-00-00 00:00:00'),
(275, 'Valor enviado para a Sra. Marcia.', NULL, '2022-08-28', 10, 2022, 13, 510.00, NULL, 510.00, 7, 19, NULL, '', '', '', '', '', NULL, NULL, 104, 138, NULL, 1, 1, NULL, '2022-10-12 04:41:47', '0000-00-00 00:00:00'),
(276, 'Valor enviado para o Rosário.', NULL, '2022-08-28', 10, 2022, 17, 1809.30, 190.70, 2000.00, 7, 14, NULL, '', '', '', '', '', NULL, NULL, 7, 15, NULL, NULL, 5, NULL, '2022-10-12 04:55:10', '0000-00-00 00:00:00'),
(277, 'Valor enviado para o Rosário para o envio de Documentos e mercadoria de Beira-Maputo.', NULL, '2022-08-28', 10, 2022, 15, 700.00, NULL, 700.00, 7, 1, NULL, '', '', '', '', '', NULL, NULL, 7, 13, NULL, NULL, NULL, NULL, '2022-10-12 05:01:57', '0000-00-00 00:00:00'),
(279, 'Valor entregue ao Rosário para deslocação de um Técnico de Farmácia e um caixa.', NULL, '2022-08-28', 10, 2022, 13, 300.00, NULL, 300.00, 7, 10, NULL, '', '', '', '', '', NULL, NULL, 7, 11, NULL, 2, 1, NULL, '2022-10-12 05:07:10', '0000-00-00 00:00:00'),
(280, 'Valor entregue ao Sr. Albino.', 1, '2022-08-29', 10, 2022, 12, 2000.00, NULL, 2000.00, 3, 1, NULL, '', '', '0127120', '', '', NULL, 1, 100, 115, 4, 23, 3, 1, '2022-10-12 05:40:34', '0000-00-00 00:00:00'),
(281, 'Valor entregue a Milton.', 1, '2022-08-29', 10, 2022, 12, 3000.00, NULL, 3000.00, 3, 1, NULL, '', '', '327942', '', '', NULL, 1, 100, 117, 4, 35, 3, 1, '2022-10-12 05:43:50', '0000-00-00 00:00:00'),
(282, 'Valor entregue a Sra. Ana Dias.', 1, '2022-08-29', 10, 2022, 13, 500.00, NULL, 500.00, 3, 2, NULL, '', '', '', '', '', NULL, 1, 5, 63, 4, 1, 1, NULL, '2022-10-12 05:49:13', '0000-00-00 00:00:00'),
(283, 'Valor entregue a Sra. Isabel.', 1, '2022-08-29', 10, 2022, 13, 300.00, NULL, 300.00, 3, 2, NULL, '', '', '', '', '', NULL, 1, 5, 65, 4, 1, 1, NULL, '2022-10-12 05:50:47', '0000-00-00 00:00:00'),
(284, 'Valor entregue a Sra. Rufina Simbine.', 1, '2022-08-29', 10, 2022, 13, 500.00, NULL, 500.00, 3, 2, NULL, '', '', '', '', '', NULL, 1, 5, 123, 4, 1, 1, NULL, '2022-10-12 05:52:45', '0000-00-00 00:00:00'),
(285, 'Valor entregue ao Mohamad.', 1, '2022-08-29', 10, 2022, 13, 500.00, NULL, 500.00, 3, 2, 5, '', '', '', NULL, '', NULL, 1, 5, 152, 4, 1, 1, NULL, '2022-10-13 05:53:58', '2022-10-13 07:57:54'),
(286, 'Valor entregue a Nelson.', 1, '2022-08-12', 10, 2022, 13, 500.00, NULL, 500.00, 3, 2, NULL, '', '', '', '', '', NULL, 1, 5, 153, 4, 1, 1, NULL, '2022-10-13 06:20:41', '0000-00-00 00:00:00'),
(287, 'Valor entregue a Nelson.', 1, '2022-08-29', 10, 2022, 13, 500.00, NULL, 500.00, 3, 2, NULL, '', '', '', '', '', NULL, 1, 5, 153, 4, 1, 1, NULL, '2022-10-13 06:25:41', '0000-00-00 00:00:00'),
(288, 'Valor entregue a Fenias para deslocação as Importadoras.', 1, '2022-08-29', 10, 2022, 13, 200.00, NULL, 200.00, 3, 1, NULL, '', '', '', '', '', NULL, 1, 7, 11, 4, 1, 1, NULL, '2022-10-13 06:58:51', '0000-00-00 00:00:00'),
(289, 'Valor entregue ao Sr. Antonio.', 1, '2022-08-29', 10, 2022, 52, 1960.00, NULL, 1960.00, 3, 19, NULL, '', '', '001026', '', '', NULL, 1, 13, 104, 4, 6, 1, 1, '2022-10-13 18:52:01', '0000-00-00 00:00:00'),
(290, 'Valor enviado para o Milton para a troca dos 02 pneus de frente. Custo de transação de 10,00MT.', NULL, '2022-08-29', 10, 2022, 15, 210.00, NULL, 210.00, 7, 1, NULL, '', '', '', '', '', NULL, NULL, 1, 44, NULL, 1, 1, NULL, '2022-10-13 18:59:00', '0000-00-00 00:00:00'),
(291, 'Valor enviado para Mónica para deslocação ao Banco. Custo de transação de 10,00MT.', NULL, '2022-08-29', 10, 2022, 11, 210.00, NULL, 210.00, 7, 1, NULL, '', '', '', '', '', NULL, NULL, 7, 11, NULL, 2, 1, NULL, '2022-10-13 19:01:17', '0000-00-00 00:00:00'),
(292, 'Valor entregue ao Francisco para deslocação as seguradoras.', 1, '2022-08-29', 10, 2022, 11, 150.00, NULL, 150.00, 3, 1, NULL, '', '', '', '', '', NULL, 1, 7, 11, 4, 2, NULL, NULL, '2022-10-13 19:04:08', '0000-00-00 00:00:00'),
(293, 'Valor enviado para o Milton, com o custo de transação de 10,00MT.', 1, '2022-08-29', 10, 2022, 13, 510.00, NULL, 510.00, 6, 3, 5, '', '', '', NULL, '845533809', NULL, 1, 5, 70, 4, 1, 1, NULL, '2022-10-13 19:10:43', '2022-10-21 00:30:57'),
(294, 'Valor enviado para o Sr. Albino com o custo de transação de 10,00MT.', NULL, '2022-08-29', 10, 2022, 13, 510.00, NULL, 510.00, 6, 19, 104, '', '', '', NULL, '845629961', NULL, 0, 104, 136, 4, 1, 1, NULL, '2022-10-13 19:13:14', '2022-10-20 23:30:41'),
(295, 'Valor enviado para o Fenias com o custo de transação de 10,00MT.', NULL, '2022-08-29', 10, 2022, 13, 510.00, NULL, 510.00, 7, 3, NULL, '', '', '', '', '', NULL, NULL, 5, 72, NULL, 1, 1, NULL, '2022-10-13 19:17:06', '0000-00-00 00:00:00'),
(296, 'Valor enviado para Geni com o custo de transação de 10,00MT.', NULL, '2022-08-29', 10, 2022, 13, 510.00, NULL, 510.00, 7, 3, NULL, '', '', '', '', '', NULL, NULL, 5, 73, NULL, 1, NULL, NULL, '2022-10-13 19:19:04', '0000-00-00 00:00:00'),
(297, 'Valor entregue a Sra. Joana Torquino para transporte.', 1, '2022-08-29', 10, 2022, 13, 500.00, NULL, 500.00, 3, 3, NULL, '', '', '', '', '', NULL, 1, 5, 74, 4, 1, 1, NULL, '2022-10-13 19:21:15', '0000-00-00 00:00:00'),
(299, 'Valor entregue ao Sr. Milton.', 1, '2022-08-29', 10, 2022, 13, 1000.00, NULL, 1000.00, 3, 3, NULL, '', '', '', '', '', NULL, 1, 5, 76, 4, 1, 1, NULL, '2022-10-13 19:31:25', '0000-00-00 00:00:00'),
(300, 'Valor entregue a Sra. Mónica.', 1, '2022-08-29', 10, 2022, 13, 300.00, NULL, 300.00, 3, 3, NULL, '', '', '', '', '', NULL, 1, 5, 77, 4, 1, 1, NULL, '2022-10-13 19:34:49', '0000-00-00 00:00:00'),
(301, 'Valor enviado para o Milton, com o custo de transação de 10,00MT.', 1, '2022-08-29', 10, 2022, 13, 510.00, NULL, 510.00, 6, 3, 5, '', '', '', NULL, '845533809', NULL, 1, 5, 78, 4, 1, 1, NULL, '2022-10-13 19:36:38', '2022-10-21 00:29:23'),
(302, 'Valor entregue ao Sr. Shamir.', 1, '2022-08-29', 10, 2022, 13, 500.00, NULL, 500.00, 3, 3, NULL, '', '', '', '', '', NULL, 1, 5, 79, 4, 1, 1, NULL, '2022-10-13 19:47:19', '0000-00-00 00:00:00'),
(303, 'Valor enviado para o Rosário, com o custo de transação de 10,00MT.', NULL, '2022-08-29', 10, 2022, 13, 410.00, NULL, 410.00, 6, 4, 5, '', '', '', NULL, '870502683', NULL, 0, 5, 80, 4, 1, 1, NULL, '2022-10-13 23:15:06', '2022-10-18 15:34:36'),
(304, 'Valor enviado para o Rosário.', NULL, '2022-08-29', 10, 2022, 13, 400.00, NULL, 400.00, 6, 10, 5, '', '', '', NULL, '870502683', NULL, 0, 5, 84, 4, 1, 1, NULL, '2022-10-13 23:17:53', '2022-10-19 11:56:00'),
(305, 'Valor enviado para o Rosário com o custo de transação de 05,00MT.', NULL, '2022-08-29', 10, 2022, 13, 405.00, NULL, 405.00, 6, 12, 5, '', '', '', NULL, '870502683', NULL, 0, 5, 93, 4, 1, 1, NULL, '2022-10-13 23:24:07', '2022-10-19 11:27:33'),
(306, 'Valor enviado para o Rosário.', NULL, '2022-08-29', 10, 2022, 13, 400.00, NULL, 400.00, 6, 14, 5, '', '', '', NULL, '870502683', NULL, 0, 5, 169, 4, 1, 1, NULL, '2022-10-13 23:26:25', '2022-10-31 11:20:46'),
(307, 'Compra de rolos. Valor enviado para o Milton, com custo de transação de 20.00MT.', NULL, '2022-08-29', 10, 2022, 15, 2720.00, NULL, 2720.00, 7, 3, NULL, '', '', '', '', '', NULL, NULL, 106, 130, NULL, NULL, NULL, NULL, '2022-10-13 23:31:11', '2022-10-18 13:34:28'),
(308, 'Valor transferido para a Sra. Ana Bela com o custo de transação de 10,00MT.', NULL, '2022-08-29', 10, 2022, 13, 510.00, NULL, 510.00, 7, 19, NULL, '', '', '', '', '', NULL, NULL, 104, 124, NULL, 1, 1, NULL, '2022-10-13 23:34:01', '0000-00-00 00:00:00'),
(309, 'Valor transferido para a Sra Ana Bela, com o custo de transação de 5.00MT.', NULL, '2022-08-29', 10, 2022, 13, 505.00, NULL, 505.00, 7, 19, NULL, '', '', '', '', '', NULL, NULL, 104, 125, NULL, 1, 1, NULL, '2022-10-13 23:35:50', '0000-00-00 00:00:00'),
(310, 'Valor enviado para a Sra. Ana Bela.', NULL, '2022-08-29', 10, 2022, 13, 500.00, NULL, 500.00, 7, 19, NULL, '', '', '', '', '', NULL, NULL, 104, 138, NULL, NULL, NULL, NULL, '2022-10-13 23:36:59', '0000-00-00 00:00:00'),
(311, 'Compra efectuada por Eurico', NULL, '2022-08-29', 10, 2022, 13, 1812.65, 187.35, 2000.00, 3, 2, 7, '', '', '', NULL, '', NULL, 1, 7, 15, 1, 233, 5, NULL, '2022-10-13 23:39:04', '2022-10-21 00:14:35'),
(312, 'Pagamento pelo serviço prestado ao Sr. António.', 1, '2022-08-29', 10, 2022, 13, 1000.00, NULL, 1000.00, 3, 19, NULL, '', '', '', '', '', NULL, 1, 13, 104, 4, 1, 1, NULL, '2022-10-13 23:41:13', '0000-00-00 00:00:00'),
(313, 'Valor enviado para o Sr. Albino.', NULL, '2022-08-29', 10, 2022, 17, 1809.30, 190.70, 2000.00, 7, 19, NULL, '', '', '', '', '', NULL, NULL, 7, 15, NULL, 136, 5, 1, '2022-10-13 23:44:01', '0000-00-00 00:00:00'),
(314, 'Compra de almoço. Valor enviado para o Sr. Albino.', NULL, '2022-08-29', 10, 2022, 13, 500.00, NULL, 500.00, 7, 19, NULL, '', '', '', '', '', NULL, NULL, 99, 99, NULL, NULL, NULL, NULL, '2022-10-13 23:46:12', '0000-00-00 00:00:00'),
(315, 'Valor enviado para o Rosário com o custo de transação de 10.00MT.', NULL, '2022-08-29', 10, 2022, 12, 3010.00, NULL, 3010.00, 7, 1, NULL, '', '', '', '', '', NULL, NULL, 100, 119, NULL, NULL, NULL, 1, '2022-10-13 23:48:40', '0000-00-00 00:00:00'),
(316, 'Valor entregue ao Eurico para compra de Energia.', 1, '2022-08-29', 10, 2022, 13, 50.00, NULL, 50.00, 3, 2, 7, '', '', '', NULL, '', NULL, 1, 7, 11, 4, 1, 1, NULL, '2022-10-14 00:03:32', '2022-10-14 02:10:27'),
(317, 'Valor enviado ao Rosario.', 1, '2022-08-29', 10, 2022, 11, 650.00, NULL, 650.00, 7, 1, 108, '', '', '', NULL, '', NULL, 1, 108, 129, 4, 1, 1, NULL, '2022-10-14 00:05:05', '2022-10-14 02:12:51'),
(318, 'Valor entregue ao Sr. António para compra de Material.', 1, '2022-08-29', 10, 2022, 13, 50.00, NULL, 50.00, 3, 19, NULL, '', '', '', '', '', NULL, 1, 7, 11, 4, 1, 1, NULL, '2022-10-14 00:22:31', '0000-00-00 00:00:00'),
(319, 'Compra efectuada no Mpesa. A despesa não apresenta Recibo/VD.', NULL, '2022-08-30', 10, 2022, 4, 1000.00, NULL, 1000.00, 6, 17, 3, '', '', '', NULL, '843988581', NULL, 0, 3, 56, 4, 1, 1, NULL, '2022-10-14 01:45:34', '2022-10-18 13:43:33'),
(320, 'Compra de papel higienico.', 1, '2022-08-30', 10, 2022, 15, 200.00, NULL, 200.00, 3, 2, 105, '', '', '', NULL, '', NULL, 1, 105, 157, 4, 4, 1, 2, '2022-10-14 01:47:04', '2022-10-18 13:37:44'),
(321, 'Compra de pneus: P195/50 R15 Compasal.', 1, '2022-08-30', 10, 2022, 53, 7100.00, NULL, 7100.00, 3, 1, NULL, '', '', '00155000', '', '', NULL, 1, 1, 44, 4, 2, 1, 1, '2022-10-14 02:08:27', '0000-00-00 00:00:00'),
(322, 'Valor entregue ao guarda de serviço. ', 1, '2022-08-30', 10, 2022, 17, 800.00, NULL, 800.00, 3, 2, NULL, '', '', '', '', '', NULL, 1, 7, 15, 4, NULL, NULL, 2, '2022-10-14 02:10:34', '0000-00-00 00:00:00'),
(323, 'Valor entregue ao Rosario.', 1, '2022-08-30', 10, 2022, 11, 650.00, NULL, 650.00, 3, 1, NULL, '', '', '', '', '', NULL, 1, 108, 129, 4, 1, 1, NULL, '2022-10-14 02:14:33', '0000-00-00 00:00:00'),
(324, 'Valor entregue ao Rosário.', 1, '2022-08-29', 10, 2022, 17, 1809.30, 190.70, 2000.00, 3, 14, NULL, '', '', '', '', '', NULL, 1, 7, 15, 4, 136, 5, 2, '2022-10-14 02:17:09', '0000-00-00 00:00:00'),
(325, 'Envio de Relatório e Medicamentos de Beira a Maputo.', 1, '2022-08-29', 10, 2022, 13, 700.00, NULL, 700.00, 3, 1, 7, '', '', '', NULL, '', NULL, 1, 7, 13, 4, 1, 1, NULL, '2022-10-14 02:20:10', '2022-10-21 10:37:20'),
(326, 'Transporte de Técnico e Caixa no Domingo.', 1, '2022-08-29', 10, 2022, 13, 300.00, NULL, 300.00, 3, 10, NULL, '', '', '', '', '', NULL, 1, 7, 11, 4, 2, 1, NULL, '2022-10-14 02:23:02', '0000-00-00 00:00:00'),
(327, ' Valor entregue ao Sr. Albino', 1, '2022-08-31', 10, 2022, 46, 180.00, NULL, 180.00, 3, 19, NULL, '', '', '', '', '', NULL, 1, 7, 16, 4, NULL, NULL, 2, '2022-10-14 02:37:19', '0000-00-00 00:00:00'),
(328, 'Compra de Redbull. 01Caixa.', 1, '2022-08-31', 10, 2022, 54, 1700.00, NULL, 1700.00, 3, 3, NULL, '', '', '038158', '', '', NULL, 1, 99, 99, 4, 1, 9, 1, '2022-10-14 02:40:10', '0000-00-00 00:00:00'),
(329, 'Valor entregue ao Ambasse.', 1, '2022-08-31', 10, 2022, 12, 3000.00, NULL, 3000.00, 3, 1, 100, '', '', '0127271', NULL, '', NULL, 1, 100, 116, 4, 34, 3, 1, '2022-10-14 02:41:59', '2022-10-28 02:15:39'),
(330, 'Valor entregue ao Eurico.', 1, '2022-08-31', 10, 2022, 13, 200.00, NULL, 200.00, 3, 2, NULL, '', '', '', '', '', NULL, 1, 5, 64, 4, 1, 1, NULL, '2022-10-14 02:44:22', '0000-00-00 00:00:00'),
(331, 'Valor entregue ao Sr. Jorge.', 1, '2022-08-31', 10, 2022, 13, 200.00, NULL, 200.00, 3, 2, NULL, '', '', '', '', '', NULL, 1, 5, 66, 4, 1, 1, NULL, '2022-10-14 02:45:30', '0000-00-00 00:00:00'),
(332, 'Valor entregue ao Sr. Manuel.', 1, '2022-08-31', 10, 2022, 13, 200.00, NULL, 200.00, 3, 2, NULL, '', '', '', '', '', NULL, 1, 5, 67, 4, 1, 1, NULL, '2022-10-14 02:46:42', '0000-00-00 00:00:00'),
(333, 'Valor entregue ao Raimundo.', 1, '2022-08-31', 10, 2022, 13, 200.00, NULL, 200.00, 3, 2, NULL, '', '', '', '', '', NULL, 1, 5, 68, 4, 1, 1, NULL, '2022-10-14 02:47:41', '0000-00-00 00:00:00'),
(334, 'Valor entregue ao Milton.', 1, '2022-08-01', 10, 2022, 6, 3000.00, NULL, 3000.00, 3, 1, NULL, '', '', '09529', '', '', NULL, 1, 100, 117, 4, 35, 5, 1, '2022-10-14 05:16:28', '0000-00-00 00:00:00'),
(335, 'Custo adicional de 10,00MT.', NULL, '2022-08-02', 10, 2022, 12, 2010.00, NULL, 2010.00, 6, 1, NULL, '', '', '', '', '845629961', NULL, NULL, 100, 115, 4, 22, 3, 1, '2022-10-14 05:29:45', '0000-00-00 00:00:00'),
(336, 'Valor entregue ao Sr. Jose.', 1, '2022-08-02', 10, 2022, 13, 1000.00, NULL, 1000.00, 3, 3, NULL, '', '', '', '', '', NULL, 1, 13, 102, 4, 1, 1, NULL, '2022-10-14 05:31:30', '0000-00-00 00:00:00'),
(337, 'Valor envado para Florinda.', NULL, '2022-08-02', 10, 2022, 9, 3000.00, NULL, 3000.00, 6, 1, NULL, '', '', '0108083', '', '842173242', NULL, NULL, 100, 119, 4, 34, 3, 1, '2022-10-14 05:35:48', '0000-00-00 00:00:00'),
(338, 'Envio de Documentos para Beira. Valor enviado para o Ambasse com o custo de transação de 10,00MT.', NULL, '2022-08-02', 10, 2022, 13, 510.00, NULL, 510.00, 6, 1, NULL, '', '', '', '', '844396996', NULL, NULL, 7, 12, 4, 1, 1, NULL, '2022-10-14 05:41:57', '0000-00-00 00:00:00'),
(339, 'Valor solicitado por Milton para Autenticar a Carta dirigida ao BCI, com o custo de transação de 10,00MT.', NULL, '2022-08-02', 10, 2022, 15, 210.00, NULL, 210.00, 6, 1, NULL, '', '', '', '', '845533809', NULL, NULL, 99, 99, 4, 1, 1, NULL, '2022-10-14 05:49:30', '0000-00-00 00:00:00'),
(340, 'Valor entregue a Sra. Ana Dias.', 1, '2022-08-02', 10, 2022, 13, 500.00, NULL, 500.00, 3, 2, 5, '', '', '', NULL, '', NULL, 1, 5, 63, 4, 1, 1, NULL, '2022-10-14 05:51:30', '2022-10-14 08:16:42'),
(341, 'Valor entregue a Isabel Carlos.', 1, '2022-08-02', 10, 2022, 13, 300.00, NULL, 300.00, 3, 2, NULL, '', '', '', '', '', NULL, 1, 5, 65, 4, 1, 1, NULL, '2022-10-14 05:52:53', '0000-00-00 00:00:00'),
(342, 'Valor entregue ao Leandro.', 1, '2022-08-02', 10, 2022, 13, 500.00, NULL, 500.00, 3, 2, NULL, '', '', '', '', '', NULL, 1, 5, 69, 4, 1, 1, NULL, '2022-10-14 05:54:13', '0000-00-00 00:00:00'),
(343, 'Valor entregue a Rufina.', 1, '2022-08-02', 10, 2022, 13, 500.00, NULL, 500.00, 3, 2, NULL, '', '', '', '', '', NULL, 1, 5, 123, 4, 1, 1, NULL, '2022-10-14 05:55:23', '0000-00-00 00:00:00'),
(344, 'Valor entregue ao Nelson Joao.', 1, '2022-08-02', 10, 2022, 13, 500.00, NULL, 500.00, 3, 2, NULL, '', '', '', '', '', NULL, 1, 5, 153, 4, 1, 1, NULL, '2022-10-14 05:57:40', '0000-00-00 00:00:00'),
(345, 'Valor entregue ao Sr. Eurico.', 1, '2022-08-02', 10, 2022, 13, 200.00, NULL, 200.00, 3, 2, NULL, '', '', '', '', '', NULL, 1, 5, 64, 4, 1, 1, NULL, '2022-10-14 05:59:12', '0000-00-00 00:00:00'),
(346, 'Valor entregue ao Sr. Jorge.', 1, '2022-08-02', 10, 2022, 13, 200.00, NULL, 200.00, 3, 2, NULL, '', '', '', '', '', NULL, 1, 5, 66, 4, 1, 1, NULL, '2022-10-14 06:00:32', '0000-00-00 00:00:00'),
(347, 'Valor entregue ao Sr. Manuel Tinosse.', 1, '2022-08-02', 10, 2022, 13, 200.00, NULL, 200.00, 3, 2, NULL, '', '', '', '', '', NULL, 1, 5, 67, 4, 1, 1, NULL, '2022-10-14 06:04:36', '0000-00-00 00:00:00'),
(348, 'Valor entregue ao Sr. Raimundo.', 1, '2022-08-02', 10, 2022, 13, 200.00, NULL, 200.00, 3, 2, NULL, '', '', '', '', '', NULL, 1, 5, 68, 4, 1, 1, NULL, '2022-10-14 06:05:47', '0000-00-00 00:00:00'),
(349, 'Deslocação da Diana ao Banco BCI. Valor enviado com o Custo adicional de 15,00MT.', NULL, '2022-08-02', 10, 2022, 11, 165.00, NULL, 165.00, 6, 1, NULL, '', '', '', '', '845052024', NULL, NULL, 7, 11, 4, 1, 1, NULL, '2022-10-14 06:09:16', '0000-00-00 00:00:00'),
(350, 'Valor entregue ao Eletricista Dinho para compra de: Lâmpada 24w Led.', 1, '2022-08-03', 10, 2022, 16, 2100.00, NULL, 2100.00, 3, 2, 13, '', '', '00398', NULL, '', NULL, 1, 13, 102, 4, 2, 1, 1, '2022-10-14 06:30:20', '2022-10-14 08:33:47'),
(351, 'Valor entregue ao Eletricista Dinho para compra de: Lâmpada de 18W Led (Varanda).', 1, '2022-08-03', 10, 2022, 16, 850.00, NULL, 850.00, 3, 3, NULL, '', '', '00398', '', '', NULL, 1, 13, 102, 4, 1, 1, 1, '2022-10-14 06:32:49', '0000-00-00 00:00:00'),
(352, 'Valor enviado ao funcionário de serviço. Esta despesa não tem Recibo/VD.', NULL, '2022-08-03', 10, 2022, 12, 3210.00, NULL, 3210.00, 6, 17, NULL, '', '', '', '', '', NULL, NULL, 100, 122, 4, NULL, NULL, NULL, '2022-10-14 06:37:06', '0000-00-00 00:00:00'),
(353, 'Valor enviado para Milton, com custo de transação de 10,00MT.', NULL, '2022-08-03', 10, 2022, 4, 1010.00, NULL, 1010.00, 6, 1, NULL, '', '', '0063947', '', '845533809', NULL, NULL, 3, 55, 4, 1, 1, 1, '2022-10-14 06:44:49', '0000-00-00 00:00:00'),
(354, 'Valor enviado para Florinda para pagamento de água (contribuição).', NULL, '2022-08-03', 10, 2022, 15, 510.00, NULL, 510.00, 6, 16, NULL, '', '', '', '', '842173242', NULL, NULL, 7, 16, 4, NULL, NULL, NULL, '2022-10-15 08:44:30', '0000-00-00 00:00:00'),
(355, 'Valor enviado para o Taverna.', NULL, '2022-08-03', 10, 2022, 22, 299.15, 50.85, 350.00, 6, 17, NULL, '', '', '', '', '849435147', NULL, NULL, 102, 145, 4, 1, 1, 2, '2022-10-15 09:14:14', '0000-00-00 00:00:00'),
(356, 'Valor enviado para o Rosário, a despesa apresenta um e único recibo para Recarga da Sra. Florinda e Eufrasia.  ', NULL, '2022-08-03', 10, 2022, 19, 854.70, 145.30, 1000.00, 6, 1, NULL, '', '', '5274296', '', '870502683', NULL, NULL, 3, 60, 4, 1, 1, 2, '2022-10-15 09:41:32', '0000-00-00 00:00:00'),
(357, 'Valor enviado para o Rosário. A despesa apresenta um e único recibo para as recargas da Sra. Florinda e Eufrásia.', NULL, '2022-08-03', 10, 2022, 19, 854.70, 145.30, 1000.00, 6, 1, NULL, '', '', '5274295', '', '870502683', NULL, NULL, 3, 59, 4, 1, 1, 2, '2022-10-15 09:47:49', '0000-00-00 00:00:00'),
(358, 'Valor enviado para o Rosario.', NULL, '2022-08-03', 10, 2022, 4, 1000.00, NULL, 1000.00, 6, 1, NULL, '', '', '0108127', '', '870502683', NULL, NULL, 3, 58, 4, 1, 1, 1, '2022-10-15 10:02:50', '0000-00-00 00:00:00'),
(359, 'Valor entregue ao Ambasse. A despesa apesenta o mesmo recibo para as FME Beach Front e FME Kenneth Kaunda.', 1, '2022-08-03', 10, 2022, 19, 854.70, 145.30, 1000.00, 3, 3, NULL, '', '', '0363655', '', '', NULL, 1, 3, 100, 4, 1, 1, 2, '2022-10-15 10:08:15', '0000-00-00 00:00:00'),
(360, 'valor entregue ao Ambasse. A despesa apresenta um e unico recibo para a FME Beach Front e FME Kenneth. Kaunda.', 1, '2022-08-03', 10, 2022, 19, 854.70, 145.30, 1000.00, 3, 2, NULL, '', '', '0363655', '', '', NULL, 1, 3, 100, 4, 1, 1, 2, '2022-10-15 10:11:50', '0000-00-00 00:00:00'),
(361, 'Compra efectuada pela Diana.', 1, '2022-08-03', 10, 2022, 17, 1809.30, 190.70, 2000.00, 3, 2, NULL, '', '', '', '', '', NULL, 1, 7, 15, 4, 136, 5, 1, '2022-10-17 05:42:54', '0000-00-00 00:00:00'),
(362, 'Valor entregue ao Ambasse.', 1, '2022-08-03', 10, 2022, 13, 500.00, NULL, 500.00, 3, 3, NULL, '', '', '', '', '', NULL, 1, 5, 70, 4, 1, 1, NULL, '2022-10-17 06:08:28', '0000-00-00 00:00:00'),
(363, 'Valor enviado para o Sr. Albino com o custo de transação de 10,00MT.', NULL, '2022-08-03', 10, 2022, 13, 510.00, NULL, 510.00, 6, 3, NULL, '', '', '', '', '845629961', NULL, NULL, 5, 71, 4, 1, 1, NULL, '2022-10-17 06:11:53', '0000-00-00 00:00:00'),
(364, 'Valor enviado para Fenias, com o custo de transação de 10,00MT.', NULL, '2022-08-03', 10, 2022, 13, 510.00, NULL, 510.00, 6, 3, NULL, '', '', '', '', '846852356', NULL, NULL, 5, 72, 4, 1, 1, NULL, '2022-10-17 06:16:11', '0000-00-00 00:00:00'),
(365, 'Valor enviado para Milton.', 1, '2022-08-03', 10, 2022, 13, 510.00, NULL, 510.00, 6, 3, NULL, '', '', '', '', '845533809', NULL, 1, 5, 73, 4, 1, 1, NULL, '2022-10-17 06:20:47', '0000-00-00 00:00:00'),
(366, 'Valor enviado para Milton com o de transação de 10,00MT.', NULL, '2022-08-03', 10, 2022, 13, 510.00, NULL, 510.00, 6, 3, NULL, '', '', '', '', '845533809', NULL, NULL, 5, 74, 4, 1, 1, NULL, '2022-10-17 06:26:26', '0000-00-00 00:00:00'),
(367, 'Valor entregue ao Isaac.', 1, '2022-08-03', 10, 2022, 13, 300.00, NULL, 300.00, 3, 3, NULL, '', '', '', '', '', NULL, 1, 5, 75, 4, 300, 1, NULL, '2022-10-17 06:31:34', '0000-00-00 00:00:00'),
(368, 'Valor enviado para Milton.', NULL, '2022-08-03', 10, 2022, 13, 1000.00, NULL, 1000.00, 6, 3, 5, '', '', '', NULL, '845533809', NULL, 0, 5, 76, 4, 1, 1, NULL, '2022-10-17 06:34:39', '2022-10-17 10:24:15'),
(369, 'Valor enviado para Milton.', 1, '2022-08-03', 10, 2022, 13, 300.00, NULL, 300.00, 6, 3, NULL, '', '', '', '', '845533809', NULL, 1, 5, 77, 4, 1, 1, NULL, '2022-10-17 06:37:58', '0000-00-00 00:00:00'),
(370, 'Valor enviado para Milton.', NULL, '2022-08-03', 10, 2022, 13, 300.00, NULL, 300.00, 6, 3, 5, '', '', '', NULL, '845533809', NULL, 0, 5, 78, 4, 1, 1, NULL, '2022-10-17 06:40:05', '2022-10-17 08:53:11'),
(371, 'Valor enviado para Milton.', NULL, '2022-08-03', 10, 2022, 13, 500.00, NULL, 500.00, 6, 3, NULL, '', '', '', '', '845533809', NULL, NULL, 5, 79, 4, 1, 1, NULL, '2022-10-17 06:56:54', '0000-00-00 00:00:00'),
(372, 'Valor enviado para a Sra. Ana Bela, com o custo de transação de 5,00MT.', NULL, '2022-08-04', 10, 2022, 13, 505.00, NULL, 505.00, 6, 19, 104, '', '', '', NULL, '851588285', NULL, 0, 104, 125, 4, 1, 1, NULL, '2022-10-17 12:28:28', '2022-10-17 14:32:15'),
(373, 'Valor enviado para a Sra. Ana Bela, com o custo de transação de 5,00MT.', NULL, '2022-08-04', 10, 2022, 13, 505.00, NULL, 505.00, 6, 19, 104, '', '', '', NULL, '851588285', NULL, 0, 104, 138, 4, 1, 1, NULL, '2022-10-17 12:30:04', '2022-10-17 14:33:07'),
(374, ' Valor enviado para o Rosário.', NULL, '2022-08-04', 10, 2022, 13, 400.00, NULL, 400.00, 6, 14, 5, '', '', '', NULL, '870502683', NULL, 0, 5, 168, 4, 1, 1, NULL, '2022-10-17 13:09:29', '2022-10-31 11:18:25'),
(375, 'Valor entregue ao Ambasse para compra de Plásticos (pequeno).', 1, '2022-08-04', 10, 2022, 13, 750.00, NULL, 750.00, 3, 3, 99, '', '', '', NULL, '', NULL, 1, 99, 99, 4, 3, 9, NULL, '2022-10-17 16:53:55', '2022-10-17 18:57:18'),
(376, 'Valor entregue ao Ambasse para compra de plásticos(grande).', 1, '2022-08-04', 10, 2022, 13, 600.00, NULL, 600.00, 3, 3, 99, '', '', '', NULL, '', NULL, 1, 99, 99, 4, 1, 9, NULL, '2022-10-17 16:59:04', '2022-10-17 18:59:52'),
(377, 'Valor solicitad0 pela Sra Isabel Carlos.', 1, '2022-08-04', 10, 2022, 15, 2560.50, 435.50, 2996.00, 3, 2, 105, '', '', '', NULL, '', NULL, 1, 105, 157, 4, 317, 1, NULL, '2022-10-17 17:29:58', '2022-10-17 19:31:08'),
(378, 'Valor entregue ao Milton para a correção do subsídio da Lúcia.', 1, '2022-08-04', 10, 2022, 13, 200.00, NULL, 200.00, 3, 3, 5, '', '', '', NULL, '', NULL, 1, 5, 78, 4, 1, 1, NULL, '2022-10-17 18:00:59', '2022-10-17 20:01:25'),
(379, 'Valor entregue ao Sr. Mariano.', 1, '2022-08-04', 10, 2022, 19, 854.70, 145.30, 1000.00, 3, 3, 3, '', '', '9578589', NULL, '', NULL, 1, 3, 53, 4, 1, 1, 1, '2022-10-17 18:05:27', '2022-10-17 20:06:27'),
(380, 'Valor entregue a Milton.', 1, '2022-08-04', 10, 2022, 22, 649.57, 110.43, 760.00, 3, 17, 102, '', '', 'A2218/34718', NULL, '', NULL, 1, 102, 145, 4, 3, 1, 2, '2022-10-17 18:11:40', '2022-10-17 20:12:05'),
(381, 'Valor enviado para o Francisco para compra de Envelopes, para os processos de Seguradoras.', NULL, '2022-08-04', 10, 2022, 15, 610.00, NULL, 610.00, 6, 10, 110, '', '', '00192', NULL, '842406938', NULL, 0, 106, 130, 4, 0, 1, 1, '2022-10-17 18:42:26', '2022-10-18 13:34:28'),
(382, 'Valor entregue ao Sr. Eurico.', 1, '2022-08-04', 10, 2022, 13, 200.00, NULL, 200.00, 3, 2, 5, '', '', '', NULL, '', NULL, 1, 5, 64, 4, 1, 1, NULL, '2022-10-17 22:03:27', '2022-10-18 00:04:34'),
(383, 'Valor entregue ao Sr. Jorge.', 1, '2022-08-04', 10, 2022, 13, 200.00, NULL, 200.00, 3, 2, 5, '', '', '', NULL, '', NULL, 1, 5, 66, 4, 1, 1, NULL, '2022-10-17 22:06:04', '2022-10-18 00:06:29'),
(384, 'Valor entregue ao Sr. Manuel.', 1, '2022-08-04', 10, 2022, 13, 200.00, NULL, 200.00, 3, 2, 5, '', '', '', NULL, '', NULL, 1, 5, 67, 4, 1, 1, NULL, '2022-10-18 00:57:47', '2022-10-18 02:58:20'),
(385, 'Valor entregue ao Sr. Raimundo.', 1, '2022-08-04', 10, 2022, 13, 200.00, NULL, 200.00, 3, 2, 5, '', '', '', NULL, '', NULL, 1, 5, 68, 4, 1, 1, NULL, '2022-10-18 00:59:41', '2022-10-18 03:00:15'),
(386, 'Valor enviado para o Rosário, com o custo de transação de 40,00MT.', NULL, '2022-08-04', 10, 2022, 23, 5040.00, NULL, 5040.00, 6, 10, 110, '', '', '00192', NULL, '870502683', NULL, 0, 106, 130, 4, 0, 1, 1, '2022-10-18 01:14:02', '2022-10-18 13:34:28'),
(387, 'Valor enviado para o Rosário.', NULL, '2022-08-04', 10, 2022, 24, 5000.00, NULL, 5000.00, 6, 10, 105, '', '', '', NULL, '875052683', NULL, 0, 105, 157, 4, 0, NULL, 1, '2022-10-18 01:26:51', '2022-10-18 03:27:34'),
(388, 'Valor entregue ao MIlton.', 1, '2022-08-04', 10, 2022, 21, 3000.00, 510.00, 3510.00, 3, 3, 9, '', '', '001963', NULL, '', NULL, 1, 9, 147, 4, 5, 1, 1, '2022-10-18 01:35:37', '2022-10-18 03:36:16'),
(389, 'Valor entregue ao Milton, para compra de Envelopes A4 ', 1, '2022-08-04', 10, 2022, 20, 400.00, NULL, 400.00, 3, 3, 110, '', '', '003676', NULL, '', NULL, 1, 106, 130, 4, 1, 1, 1, '2022-10-18 01:39:34', '2022-10-18 13:34:28'),
(390, 'Valor entregue a Milton para compra de envelopes A3 Branco.', 1, '2022-08-04', 10, 2022, 20, 200.00, NULL, 200.00, 3, 3, 110, '', '', '00376', NULL, '', NULL, 1, 106, 130, 4, 15, 9, 1, '2022-10-18 01:43:42', '2022-10-18 13:34:28'),
(391, 'Valor enviado para Rosário, com o custo de transação de 10,00MT.', NULL, '2022-08-04', 10, 2022, 11, 660.00, NULL, 660.00, 6, 1, 108, '', '', '', NULL, '870502683', NULL, 0, 108, 128, 4, 1, 1, NULL, '2022-10-18 01:50:37', '2022-10-18 03:51:15'),
(392, 'Valor enviado para o Rosário.', NULL, '2022-08-04', 10, 2022, 17, 848.57, 151.43, 1000.00, 6, 4, 7, '', '', '', NULL, '870502683', NULL, 0, 7, 15, 4, 64, 5, 1, '2022-10-18 02:01:09', '2022-10-18 04:01:54'),
(393, 'Valor entregue ao Nelson, para transporte de FME Kenneth Kaunda a FME Costa do Sol.', 1, '2022-08-04', 10, 2022, 15, 100.00, NULL, 100.00, 3, 1, 7, '', '', '', NULL, '', NULL, 1, 7, 11, 4, 1, 1, NULL, '2022-10-18 02:06:05', '2022-10-18 04:06:41'),
(394, 'Valor entregue ao Shamir.', 1, '2022-08-04', 10, 2022, 12, 500.00, NULL, 500.00, 3, 1, 100, '', '', '012790', NULL, '', NULL, 1, 100, 117, 4, 5, 3, 1, '2022-10-18 02:15:33', '2022-11-07 08:10:12'),
(395, 'Valor enviado para o Rosario.', NULL, '2022-08-05', 10, 2022, 27, 2400.00, NULL, 2400.00, 6, 12, 7, '', '', '', NULL, '870502683', NULL, 0, 7, 9, 4, 1, 9, 2, '2022-10-18 04:13:11', '2022-10-18 06:13:37'),
(396, 'Valor enviado para o Rosario.', 1, '2022-08-05', 10, 2022, 17, 848.57, 151.43, 1000.00, 6, 16, 7, '', '', '000000', NULL, '870502683', NULL, 1, 7, 15, 4, 111, 5, 1, '2022-10-18 04:35:05', '2022-10-18 10:51:15'),
(397, 'Valor enviado para o Rosario.', NULL, '2022-08-05', 10, 2022, 11, 660.00, NULL, 660.00, 6, 1, 108, '', '', '', NULL, '870502683', NULL, 0, 108, 128, 4, 1, 1, NULL, '2022-10-18 04:37:47', '2022-10-18 06:38:13'),
(398, 'Valor enviado para o Sr. Albino, com o custo de transação de 05,00MT.', NULL, '2022-08-22', 10, 2022, 13, 505.00, NULL, 505.00, 6, 19, 104, '', '', '', NULL, '845629961', NULL, 0, 104, 125, 4, 1, 1, NULL, '2022-10-18 05:13:55', '2022-10-18 07:14:21'),
(399, 'Valor enviado para o Sr. Albino.', NULL, '2022-08-22', 10, 2022, 13, 505.00, NULL, 505.00, 6, 19, 104, '', '', '', NULL, '845629961', NULL, 0, 104, 136, 4, 1, 1, NULL, '2022-10-18 05:15:37', '2022-10-18 07:16:03'),
(400, 'Valor envido para o Sr. Albino, com o custo de transação de 5,00MT.', NULL, '2022-08-22', 10, 2022, 13, 505.00, NULL, 505.00, 6, 19, 104, '', '', '', NULL, '845626691', NULL, 0, 104, 138, 4, 0, NULL, NULL, '2022-10-18 05:17:27', '2022-10-18 07:17:56'),
(401, 'Valor entregue ao Milton.', 1, '2022-08-12', 10, 2022, 13, 500.00, NULL, 500.00, 3, 3, 5, '', '', '', NULL, '', NULL, 1, 5, 73, 4, 1, 1, NULL, '2022-10-18 05:53:22', '2022-10-18 07:53:50'),
(402, 'Valor entregue ao Fenias.', 1, '2022-08-12', 10, 2022, 13, 500.00, NULL, 500.00, 3, 3, 5, '', '', '', NULL, '', NULL, 1, 5, 72, 4, 1, 1, NULL, '2022-10-18 05:55:00', '2022-10-18 07:55:25'),
(403, 'Valor entregue a Joana.', 1, '2022-08-12', 10, 2022, 13, 500.00, NULL, 500.00, 3, 3, 5, '', '', '', NULL, '', NULL, 1, 5, 74, 4, 1, 1, NULL, '2022-10-18 05:57:34', '2022-10-18 08:00:22'),
(404, 'Valor entregue ao Milton.', 1, '2022-08-12', 10, 2022, 13, 1000.00, NULL, 1000.00, 3, 3, 5, '', '', '', NULL, '', NULL, 1, 5, 76, 4, 1, 1, NULL, '2022-10-18 05:58:45', '2022-10-18 08:00:43'),
(405, 'Valor entregue ao Milton.', 1, '2022-08-12', 10, 2022, 13, 500.00, NULL, 500.00, 3, 3, 5, '', '', '', NULL, '', NULL, 1, 5, 78, 4, 1, 1, NULL, '2022-10-18 05:59:58', '2022-10-18 08:02:15'),
(406, 'Valor entregue ao Shamir', 1, '2022-08-12', 10, 2022, 13, 500.00, NULL, 500.00, 3, 3, 5, '', '', '', NULL, '', NULL, 1, 5, 79, 4, 1, 1, NULL, '2022-10-18 06:01:52', '2022-10-18 08:02:43'),
(407, 'valor entregue ao milton', 1, '2022-10-18', 10, 2022, 20, 6700.00, 120.00, 6820.00, 3, 2, NULL, '', '', 'y686667', '', '', NULL, 1, 7, 8, 4, 1, 1, 1, '2022-10-18 07:38:32', '0000-00-00 00:00:00'),
(408, 'Valor entregue ao Carpinteiro', 1, '2022-08-05', 10, 2022, 15, 520.00, NULL, 520.00, 3, 2, 13, '', '', '', NULL, '', NULL, 1, 13, 159, 4, 4, 1, 1, '2022-10-18 08:57:32', '2022-10-18 10:58:07'),
(409, 'Valor entregue ao Carpinteiro para deslocação, para compra de material.', 1, '2022-08-05', 10, 2022, 13, 20.00, NULL, 20.00, 3, 2, 7, '', '', '', NULL, '', NULL, 1, 7, 11, 4, 1, 1, NULL, '2022-10-18 09:00:10', '2022-10-18 11:00:41'),
(411, 'Valor entregue ao Milton.', 1, '2022-08-05', 10, 2022, 25, 506.05, 73.95, 580.00, 3, 3, 9, '', '', '589/2022', NULL, '', NULL, 1, 9, 161, 4, 2, 1, 1, '2022-10-18 09:16:20', '2022-11-04 08:56:40'),
(412, 'Valor entregue ao Milton.', 1, '2022-08-05', 10, 2022, 25, 290.00, NULL, 290.00, 3, 2, 9, '', '', '589/2022', NULL, '', NULL, 1, 9, 161, 4, 1, 1, 1, '2022-10-18 09:18:25', '2022-10-18 11:18:54'),
(413, 'Valor enviado para o Rosario.', NULL, '2022-08-04', 10, 2022, 13, 400.00, NULL, 400.00, 6, 4, 5, '', '', '', NULL, '870502683', NULL, 0, 5, 81, 4, 1, 1, NULL, '2022-10-18 12:19:33', '2022-10-18 14:20:06'),
(414, 'Valor enviado para o Rosario.', NULL, '2022-08-04', 10, 2022, 13, 100.00, NULL, 100.00, 6, 4, 5, '', '', '', NULL, '870502683', NULL, 0, 5, 82, 4, 1, 1, NULL, '2022-10-18 12:21:17', '2022-10-18 14:21:44'),
(415, 'Valor enviado para o Rosario.', NULL, '2022-08-04', 10, 2022, 13, 600.00, NULL, 600.00, 6, 4, 5, '', '', '', NULL, '870502683', NULL, 0, 5, 83, 4, 1, 1, NULL, '2022-10-18 12:22:52', '2022-10-18 14:24:49'),
(416, 'Valor enviado para o Rosario.', NULL, '2022-08-04', 10, 2022, 13, 400.00, NULL, 400.00, 6, 4, 5, '', '', '', NULL, '870502683', NULL, 0, 5, 85, 4, 1, 1, NULL, '2022-10-18 12:24:07', '2022-10-18 14:25:13'),
(417, 'Valor enviado para o Rosário.', NULL, '2022-08-04', 10, 2022, 13, 800.00, NULL, 800.00, 6, 10, 5, '', '', '', NULL, '870502683', NULL, 0, 5, 86, 4, 1, 1, NULL, '2022-10-18 12:34:46', '2022-10-18 14:43:23'),
(418, 'Valor enviado para o Rosario.', NULL, '2022-08-04', 10, 2022, 13, 400.00, NULL, 400.00, 6, 10, 5, '', '', '', NULL, '870502683', NULL, 0, 5, 87, 4, 1, 1, NULL, '2022-10-18 12:35:52', '2022-10-18 14:43:54'),
(419, 'Valor enviado para o Rosario.', NULL, '2022-08-04', 10, 2022, 13, 500.00, NULL, 500.00, 6, 10, 5, '', '', '', NULL, '8750502683', NULL, 0, 5, 88, 4, 1, 1, NULL, '2022-10-18 12:37:20', '2022-10-18 14:45:49'),
(420, 'Valor enviado para o Rosario.', NULL, '2022-08-04', 10, 2022, 13, 400.00, NULL, 400.00, 6, 10, 5, '', '', '', NULL, '870502683', NULL, 0, 5, 89, 4, 1, 1, NULL, '2022-10-18 12:38:38', '2022-10-18 14:45:27'),
(421, 'Valor enviado para o Rosario.', NULL, '2022-08-04', 10, 2022, 13, 100.00, NULL, 100.00, 6, 10, 5, '', '', '', NULL, '870502683', NULL, 0, 5, 90, 4, 1, 1, NULL, '2022-10-18 12:40:04', '2022-10-18 14:44:59'),
(422, 'Valor enviado pra o Rosario.', NULL, '2022-08-04', 10, 2022, 13, 400.00, NULL, 400.00, 6, 10, 5, '', '', '', NULL, '870502683', NULL, 0, 5, 91, 4, 1, 1, NULL, '2022-10-18 12:41:22', '2022-10-18 14:44:37'),
(423, 'Valor enviado para o Rosario.', NULL, '2022-08-04', 10, 2022, 13, 400.00, NULL, 400.00, 6, 10, 5, '', '', '', NULL, '870502683', NULL, 0, 5, 92, 4, 1, 1, NULL, '2022-10-18 12:42:54', '2022-10-18 14:44:15'),
(424, 'Valor enviado para o Rosario.', NULL, '2022-08-04', 10, 2022, 13, 400.00, NULL, 400.00, 6, 12, NULL, '', '', '', '', '870502683', NULL, NULL, 5, 94, 4, 1, 1, NULL, '2022-10-18 12:54:48', '0000-00-00 00:00:00'),
(425, 'Valor enviado para o Rosario.', NULL, '2022-08-04', 10, 2022, 13, 400.00, NULL, 400.00, 6, 12, NULL, '', '', '', '', '870502683', NULL, NULL, 5, 95, 4, 1, 1, NULL, '2022-10-18 12:56:02', '0000-00-00 00:00:00'),
(426, 'Valor enviado para o Rosario', NULL, '2022-08-04', 10, 2022, 13, 100.00, NULL, 100.00, 6, 12, NULL, '', '', '', '', '870502683', NULL, NULL, 5, 96, 4, 1, 1, NULL, '2022-10-18 12:57:10', '0000-00-00 00:00:00'),
(427, 'Valor enviado para o Rosário.', NULL, '2022-08-04', 10, 2022, 13, 400.00, NULL, 400.00, 6, 15, NULL, '', '', '', '', '870502683', NULL, NULL, 5, 98, 4, 1, 1, NULL, '2022-10-18 13:01:28', '0000-00-00 00:00:00'),
(428, 'Valor entregue ao Ambasse, para o envio de Mercadoria Maputo-Beira.', 1, '2022-08-08', 10, 2022, 15, 700.00, NULL, 700.00, 3, 1, NULL, '', '', '', '', '', NULL, 1, 7, 13, 4, NULL, NULL, NULL, '2022-10-18 13:22:17', '0000-00-00 00:00:00'),
(429, 'Valor enviado para o Rosário, com o custo de transação de 10,00MT.', NULL, '2022-08-29', 10, 2022, 13, 410.00, NULL, 410.00, 6, 4, NULL, '', '', '', '', '870502683', NULL, NULL, 5, 81, 4, 1, 1, NULL, '2022-10-18 13:36:11', '0000-00-00 00:00:00'),
(430, 'Valor enviado para o Rosario.', NULL, '2022-08-29', 10, 2022, 13, 100.00, NULL, 100.00, 6, 4, NULL, '', '', '', '', '870502683', NULL, NULL, 5, 82, 4, 1, 1, NULL, '2022-10-18 13:37:42', '0000-00-00 00:00:00'),
(431, 'Valor enviado para o Rosário.', NULL, '2022-08-29', 10, 2022, 13, 600.00, NULL, 600.00, 6, 4, NULL, '', '', '', '', '870502683', NULL, NULL, 5, 83, 4, 1, 1, NULL, '2022-10-18 13:39:01', '0000-00-00 00:00:00'),
(432, 'Valor enviado para o Rosario.', NULL, '2022-08-29', 10, 2022, 13, 400.00, NULL, 400.00, 6, 4, NULL, '', '', '', '', '870502683', NULL, NULL, 5, 85, 4, 1, 1, NULL, '2022-10-18 13:40:06', '0000-00-00 00:00:00'),
(433, 'Valor enviado para o Rosário.', NULL, '2022-08-16', 10, 2022, 13, 400.00, NULL, 400.00, 6, 4, NULL, '', '', '', '', '870502683', NULL, NULL, 5, 81, 4, 1, 1, NULL, '2022-10-19 07:53:42', '0000-00-00 00:00:00'),
(434, 'Valor enviado para o Rosario.', NULL, '2022-08-16', 10, 2022, 13, 100.00, NULL, 100.00, 6, 4, NULL, '', '', '', '', '870502683', NULL, NULL, 5, 82, 4, 1, 1, NULL, '2022-10-19 07:54:52', '0000-00-00 00:00:00');
INSERT INTO `execucaodespesas` (`idexecucaodespesas`, `descricaodespesa`, `idrequisicaodefundos`, `data`, `mes`, `ano`, `idempresa`, `valorinicial`, `iva`, `valortotal`, `idformapagamento`, `idcentrodecusto`, `idgrupodespesas`, `observacoes`, `anexo`, `recibo`, `numerocheque`, `transfcontadestino`, `transferenciabanco`, `caixa`, `grupo`, `subgrupo`, `banco`, `quantidade`, `unidade`, `tipo_documento_id`, `data_criacao`, `data_atualizacao`) VALUES
(435, 'Valor enviado para o Rosario.', NULL, '2022-08-16', 10, 2022, 13, 600.00, NULL, 600.00, 6, 4, NULL, '', '', '', '', '870502683', NULL, NULL, 5, 83, 4, 1, 1, NULL, '2022-10-19 07:55:55', '0000-00-00 00:00:00'),
(436, 'Valor enviado para o Rosário.\r\n', NULL, '2022-08-16', 10, 2022, 13, 400.00, NULL, 400.00, 6, 4, NULL, '', '', '', '', '870502683', NULL, NULL, 5, 85, 4, 1, 1, NULL, '2022-10-19 07:56:59', '0000-00-00 00:00:00'),
(437, 'Valor enviado para o Rosário.\r\n', NULL, '2022-08-16', 10, 2022, 13, 400.00, NULL, 400.00, 6, 12, NULL, '', '', '', '', '870502683', NULL, NULL, 5, 94, 4, 1, 1, NULL, '2022-10-19 08:00:57', '0000-00-00 00:00:00'),
(438, 'Valor enviado para o Rosário.\r\n', NULL, '2022-08-16', 10, 2022, 13, 400.00, NULL, 400.00, 6, 12, NULL, '', '', '', '', '870502683', NULL, NULL, 5, 95, 4, NULL, NULL, NULL, '2022-10-19 08:02:04', '0000-00-00 00:00:00'),
(439, 'Valor enviado para Rosário.\r\n', NULL, '2022-08-16', 10, 2022, 13, 100.00, NULL, 100.00, 6, 12, NULL, '', '', '', '', '870502683', NULL, NULL, 5, 96, 4, 1, 1, NULL, '2022-10-19 08:03:48', '0000-00-00 00:00:00'),
(440, 'Valor enviado para o Rosário, com o custo de transação de 5,00MT.', NULL, '2022-08-16', 10, 2022, 13, 405.00, NULL, 405.00, 6, 14, 5, '', '', '', NULL, '870502683', NULL, 0, 5, 168, 4, 1, 1, NULL, '2022-10-19 08:09:04', '2022-10-31 11:17:30'),
(441, 'Valor enviado para o Rosário, com o custo de transação de 05,00MT.', NULL, '2022-08-09', 10, 2022, 13, 805.00, NULL, 805.00, 6, 10, NULL, '', '', '', '', '870502683', NULL, NULL, 5, 86, 4, 1, 1, NULL, '2022-10-19 08:19:28', '0000-00-00 00:00:00'),
(442, 'Valor enviado para o Rosário com o custo de transação de 05,00MT.', NULL, '2022-08-09', 10, 2022, 13, 405.00, NULL, 405.00, 6, 10, NULL, '', '', '', '', '870502683', NULL, NULL, 5, 87, 4, 1, 1, NULL, '2022-10-19 08:22:26', '0000-00-00 00:00:00'),
(443, 'Valor enviado para o Rosário,', NULL, '2022-08-09', 10, 2022, 13, 500.00, NULL, 500.00, 6, 10, NULL, '', '', '', '', '870502683', NULL, NULL, 5, 88, 4, 1, 1, NULL, '2022-10-19 08:24:38', '0000-00-00 00:00:00'),
(444, 'Valor enviado para o Rosário, com o custo de transação de 05,00MT.', NULL, '2022-08-09', 10, 2022, 13, 405.00, NULL, 405.00, 6, 10, NULL, '', '', '', '', '870502683', NULL, NULL, 5, 89, 4, 1, 1, NULL, '2022-10-19 08:28:48', '0000-00-00 00:00:00'),
(445, 'Valor enviado para o Rosário.', NULL, '2022-08-09', 10, 2022, 13, 400.00, NULL, 400.00, 6, 10, NULL, '', '', '', '', '870502683', NULL, NULL, 5, 91, 4, 1, 1, NULL, '2022-10-19 08:30:42', '0000-00-00 00:00:00'),
(446, 'Valor enviado para o Rosário com o custo de transação de 05,00MT.', NULL, '2022-08-09', 10, 2022, 13, 105.00, NULL, 105.00, 6, 10, NULL, '', '', '', '', '870502683', NULL, NULL, 5, 90, 4, 1, 1, NULL, '2022-10-19 08:38:14', '0000-00-00 00:00:00'),
(447, 'Valor enviado para o Rosário.\r\n', NULL, '2022-08-09', 10, 2022, 13, 400.00, NULL, 400.00, 6, 10, NULL, '', '', '', '', '870502683', NULL, NULL, 5, 92, 4, 1, 1, NULL, '2022-10-19 08:40:15', '0000-00-00 00:00:00'),
(448, 'Valor enviado para o Rosario.', NULL, '2022-08-09', 10, 2022, 13, 400.00, NULL, 400.00, 6, 4, NULL, '', '', '', '', '870502683', NULL, NULL, 5, 81, 4, 1, 1, NULL, '2022-10-19 08:44:58', '0000-00-00 00:00:00'),
(449, 'Valor enviado para o Rosario.', NULL, '2022-08-09', 10, 2022, 13, 100.00, NULL, 100.00, 6, 4, NULL, '', '', '', '', '870502683', NULL, NULL, 5, 82, 4, 1, 1, NULL, '2022-10-19 08:46:03', '0000-00-00 00:00:00'),
(450, 'Valor enviado para o Rosario.', NULL, '2022-08-09', 10, 2022, 13, 600.00, NULL, 600.00, 6, 4, NULL, '', '', '', '', '870502683', NULL, NULL, 5, 83, 4, 1, 1, NULL, '2022-10-19 08:47:08', '0000-00-00 00:00:00'),
(451, 'Valor enviado para o Rosario.', NULL, '2022-08-09', 10, 2022, 13, 400.00, NULL, 400.00, 6, 4, NULL, '', '', '', '', '870502683', NULL, NULL, 5, 85, 4, 1, NULL, NULL, '2022-10-19 08:48:05', '0000-00-00 00:00:00'),
(452, 'Valor entregue ao Milton.\r\n', NULL, '2022-08-22', 10, 2022, 15, 5000.00, NULL, 5000.00, 3, 2, 7, '', '', '', NULL, '870502683', NULL, 1, 7, 8, 4, 1, 1, NULL, '2022-10-19 08:51:43', '2022-10-20 20:43:41'),
(453, 'Valor enviado para o Rosário, com o custo de transação de 05,00MT.', NULL, '2022-08-22', 10, 2022, 13, 805.00, NULL, 805.00, 6, 10, NULL, '', '', '', '', '870502683', NULL, NULL, 5, 86, 4, 1, 1, NULL, '2022-10-19 09:02:14', '0000-00-00 00:00:00'),
(454, 'Valor enviado para o Rosário, com o custo de transação de 05,00MT.', NULL, '2022-08-22', 10, 2022, 13, 405.00, NULL, 405.00, 6, 10, NULL, '', '', '', '', '870502683', NULL, NULL, 5, 87, 4, 1, 1, NULL, '2022-10-19 09:03:38', '0000-00-00 00:00:00'),
(455, 'Valor enviado para o Rosário com o custo de transação de 05,00MT.', NULL, '2022-08-22', 10, 2022, 13, 505.00, NULL, 505.00, 6, 10, NULL, '', '', '', '', '870502683', NULL, NULL, 5, 88, 4, 1, 1, NULL, '2022-10-19 09:05:57', '0000-00-00 00:00:00'),
(456, 'Valor enviado para o Rosário, com o custo de transação de 05,00MT.', NULL, '2022-08-22', 10, 2022, 13, 405.00, NULL, 405.00, 6, 10, NULL, '', '', '', '', '870502683', NULL, NULL, 5, 89, 4, 1, 1, NULL, '2022-10-19 09:08:17', '0000-00-00 00:00:00'),
(457, 'Valor enviado para o Rosário.\r\n', NULL, '2022-08-22', 10, 2022, 13, 100.00, NULL, 100.00, 6, 10, NULL, '', '', '', '', '870502683', NULL, NULL, 5, 90, 4, 1, 1, NULL, '2022-10-19 09:10:11', '0000-00-00 00:00:00'),
(458, 'Valor enviado para o Rosário, com o custo de transação de 05,00MT.', NULL, '2022-08-22', 10, 2022, 13, 405.00, NULL, 405.00, 6, 10, NULL, '', '', '', '', '870502683', NULL, NULL, 5, 91, 4, 1, 1, NULL, '2022-10-19 09:11:57', '0000-00-00 00:00:00'),
(459, 'Valor enviado para o Rosário.\r\n', NULL, '2022-08-22', 10, 2022, 13, 400.00, NULL, 400.00, 6, 10, NULL, '', '', '', '', '870502683', NULL, NULL, 5, 92, 4, 1, 1, NULL, '2022-10-19 09:13:11', '0000-00-00 00:00:00'),
(460, 'Valor enviado para o Rosário, com o Custo de transação de 05,00MT.', NULL, '2022-08-08', 10, 2022, 13, 405.00, NULL, 405.00, 6, 12, 5, '', '', '', NULL, '870502683', NULL, 0, 5, 94, 4, 1, 1, NULL, '2022-10-19 09:16:54', '2022-10-19 11:22:32'),
(461, 'Valor enviado para o Rosario.', NULL, '2022-08-08', 10, 2022, 13, 400.00, NULL, 400.00, 6, 12, 5, '', '', '', NULL, '870502683', NULL, 0, 5, 95, 4, 1, 1, NULL, '2022-10-19 09:18:18', '2022-10-19 11:23:13'),
(462, 'Valor enviado para o Rosario.', NULL, '2022-08-08', 10, 2022, 13, 100.00, NULL, 100.00, 6, 12, 5, '', '', '', NULL, '870502683', NULL, 0, 5, 96, 4, 1, 1, NULL, '2022-10-19 09:19:29', '2022-10-19 11:23:41'),
(463, 'Valor enviado para o Rosário, com o custo de transação de 05,00MT.', NULL, '2022-08-29', 10, 2022, 13, 405.00, NULL, 405.00, 6, 12, NULL, '', '', '', '', '870502683', NULL, NULL, 5, 94, 4, 1, 1, NULL, '2022-10-19 09:34:48', '0000-00-00 00:00:00'),
(464, 'Valor enviado para o Rosario.', NULL, '2022-08-29', 10, 2022, 13, 400.00, NULL, 400.00, 6, 12, NULL, '', '', '', '', '870502683', NULL, NULL, 5, 95, 4, 1, 1, NULL, '2022-10-19 09:36:44', '0000-00-00 00:00:00'),
(465, 'Valor enviado para o Rosario.', NULL, '2022-08-29', 10, 2022, 13, 100.00, NULL, 100.00, 6, 12, NULL, '', '', '', '', '870502683', NULL, NULL, 5, 96, 4, 1, 1, NULL, '2022-10-19 09:41:14', '0000-00-00 00:00:00'),
(466, 'Valor enviado para o Rosário, com o custo de transação de 05,00MT.', NULL, '2022-08-17', 10, 2022, 13, 805.00, NULL, 805.00, 6, 10, NULL, '', '', '', '', '870502683', NULL, NULL, 5, 86, 4, 1, 1, NULL, '2022-10-19 09:44:36', '0000-00-00 00:00:00'),
(467, 'Valor enviado para o Rosário, com custo de transação de 05,00MT.', NULL, '2022-08-17', 10, 2022, 13, 405.00, NULL, 405.00, 6, 10, NULL, '', '', '', '', '870502683', NULL, NULL, 5, 87, 4, 1, 1, NULL, '2022-10-19 09:46:14', '0000-00-00 00:00:00'),
(468, 'Valor enviado para o Rosário, com o custo de transação de 05,00MT.', NULL, '2022-08-17', 10, 2022, 13, 505.00, NULL, 505.00, 6, 10, NULL, '', '', '', '', '870502683', NULL, NULL, 5, 88, 4, 1, 1, NULL, '2022-10-19 09:48:00', '0000-00-00 00:00:00'),
(469, 'Valor enviado para o Rosário, com o custo de transação de 05,00MT.', NULL, '2022-08-17', 10, 2022, 13, 405.00, NULL, 405.00, 6, 10, NULL, '', '', '', '', '870502683', NULL, NULL, 5, 89, 4, 1, 1, NULL, '2022-10-19 09:49:47', '0000-00-00 00:00:00'),
(470, 'Valor enviado para o Rosário, com o custo de transação de 05,00MT.', NULL, '2022-08-17', 10, 2022, 13, 105.00, NULL, 105.00, 6, 10, NULL, '', '', '', '', '870502683', NULL, NULL, 5, 90, 4, 1, 1, NULL, '2022-10-19 09:51:22', '0000-00-00 00:00:00'),
(471, 'Valor enviado para o Rosario.', NULL, '2022-08-17', 10, 2022, 13, 400.00, NULL, 400.00, 6, 10, NULL, '', '', '', '', '870502683', NULL, NULL, 5, 91, 4, 1, 1, NULL, '2022-10-19 09:52:36', '0000-00-00 00:00:00'),
(472, 'Valor enviado para o Rosario.', NULL, '2022-08-17', 10, 2022, 13, 400.00, NULL, 400.00, 6, 10, NULL, '', '', '', '', '870502683', NULL, NULL, 5, 92, 4, 1, 1, NULL, '2022-10-19 09:53:44', '0000-00-00 00:00:00'),
(473, 'Valor enviado para o Rosario.', NULL, '2022-08-29', 10, 2022, 13, 800.00, NULL, 800.00, 6, 10, NULL, '', '', '', '', '870502683', NULL, NULL, 5, 86, 4, 1, 1, NULL, '2022-10-19 09:57:12', '0000-00-00 00:00:00'),
(474, 'Valor enviado para o Rosario.', NULL, '2022-08-29', 10, 2022, 13, 400.00, NULL, 400.00, 6, 10, NULL, '', '', '', '', '870502683', NULL, NULL, 5, 87, 4, 1, 1, NULL, '2022-10-19 09:58:17', '0000-00-00 00:00:00'),
(475, 'Valor enviado para o Rosario.', NULL, '2022-08-29', 10, 2022, 13, 500.00, NULL, 500.00, 6, 10, NULL, '', '', '', '', '870502683', NULL, NULL, 5, 88, 4, 1, 1, NULL, '2022-10-19 09:59:27', '0000-00-00 00:00:00'),
(476, 'Valor enviado para o Rosario.', NULL, '2022-08-29', 10, 2022, 13, 400.00, NULL, 400.00, 6, 10, NULL, '', '', '', '', '870502683', NULL, NULL, 5, 89, 4, 1, 1, NULL, '2022-10-19 10:00:58', '0000-00-00 00:00:00'),
(477, 'Valor enviado para o Rosário, com custo de transação de 05,00MT.', NULL, '2022-08-29', 10, 2022, 13, 105.00, NULL, 105.00, 6, 10, NULL, '', '', '', '', '', NULL, NULL, 5, 90, 4, 1, 1, NULL, '2022-10-19 10:03:11', '0000-00-00 00:00:00'),
(478, 'Valor enviado para o Rosário, com custo de transação de 10,00MT.', NULL, '2022-08-29', 10, 2022, 13, 410.00, NULL, 410.00, 6, 10, NULL, '', '', '', '', '870502683', NULL, NULL, 5, 91, 4, 1, 1, NULL, '2022-10-19 10:05:10', '0000-00-00 00:00:00'),
(479, 'Valor enviado para o Rosário, com custo de transação de 05,00MT.', NULL, '2022-08-29', 10, 2022, 13, 405.00, NULL, 405.00, 6, 10, NULL, '', '', '', '', '870502683', NULL, NULL, 5, 92, 4, 1, 1, NULL, '2022-10-19 10:06:48', '0000-00-00 00:00:00'),
(480, 'Valor enviado para o Rosário.\r\n', NULL, '2022-08-29', 10, 2022, 13, 400.00, NULL, 400.00, 6, 14, 5, '', '', '', NULL, '870502683', NULL, 0, 5, 168, 4, 1, 1, NULL, '2022-10-19 10:10:35', '2022-10-31 11:16:48'),
(481, 'Compra de recarga efactuada no Mpesa.', NULL, '2022-08-05', 10, 2022, 4, 1000.00, NULL, 1000.00, 6, 19, NULL, '', '', '', '', '844825301', NULL, NULL, 3, 160, 4, 1, 1, NULL, '2022-10-19 10:24:58', '0000-00-00 00:00:00'),
(482, 'Valor enviado para o Rosário, com o custo de transação de 10,00MT.', NULL, '2022-08-06', 10, 2022, 4, 1010.00, NULL, 1010.00, 6, 17, NULL, '', '', '', '', '870502683', NULL, NULL, 3, 56, 4, 1, 1, NULL, '2022-10-19 10:32:39', '0000-00-00 00:00:00'),
(483, 'Valor enviado para o Rosário, para a refeição de Domingo.', NULL, '2022-08-06', 10, 2022, 38, 500.00, NULL, 500.00, 6, 10, NULL, '', '', '00492', '', '870502683', NULL, NULL, 4, 61, 4, NULL, NULL, 1, '2022-10-19 10:40:04', '0000-00-00 00:00:00'),
(484, 'Compra efetuada pela Diana.', 1, '2022-08-06', 10, 2022, 17, 2320.00, 80.00, 2400.00, 3, 2, NULL, '', '', '', '', '', NULL, 1, 7, 15, 4, 157, 5, 1, '2022-10-19 10:46:48', '0000-00-00 00:00:00'),
(485, 'Valor entregue ao Ambasse para o envio de Medicamentos Maputo-Beira.', 1, '2022-08-06', 10, 2022, 15, 500.00, NULL, 500.00, 3, 1, NULL, '', '', '', '', '', NULL, 1, 7, 13, 4, NULL, NULL, NULL, '2022-10-19 10:49:07', '0000-00-00 00:00:00'),
(486, 'Electricista Dinho. Manutenção do Letreiro.', NULL, '2022-08-06', 10, 2022, 15, 1500.00, NULL, 1500.00, 6, 2, NULL, '', '', '', '', '872900046', NULL, NULL, 13, 102, 6, 1, 1, NULL, '2022-10-19 10:56:28', '0000-00-00 00:00:00'),
(487, 'Valor entregue ao Caixa de Serviço.\r\n', 1, '2022-08-06', 10, 2022, 30, 1000.00, NULL, 1000.00, 3, 2, 4, '', '', '00608', NULL, '', NULL, 1, 4, 61, 4, 1, 1, 1, '2022-10-19 11:07:55', '2022-10-19 13:13:21'),
(488, 'Valor entregue ao Caixa de Serviço.', 1, '2022-08-06', 10, 2022, 29, 1000.00, NULL, 1000.00, 3, 3, 4, '', '', '1084', NULL, '', NULL, 1, 4, 61, 4, 0, NULL, 1, '2022-10-19 11:16:02', '2022-10-19 13:21:53'),
(489, 'Compra efectuada por Diana.', 1, '2022-08-09', 10, 2022, 17, 1809.30, 190.70, 2000.00, 3, 2, NULL, '', '', '', '', '', NULL, 1, 7, 15, 4, 136, 5, 1, '2022-10-19 11:36:50', '0000-00-00 00:00:00'),
(490, 'Valor entregue ao Sr. Albino.', 1, '2022-08-09', 10, 2022, 12, 2000.00, NULL, 2000.00, 3, 1, NULL, '', '', '0130068', '', '', NULL, 1, 100, 115, 4, 23, 3, 1, '2022-10-19 11:38:23', '0000-00-00 00:00:00'),
(491, 'Valor enviado para o Rosario.', NULL, '2022-08-09', 10, 2022, 13, 400.00, NULL, 400.00, 6, 4, NULL, '', '', '', '', '870502683', NULL, NULL, 5, 80, 4, 1, 1, NULL, '2022-10-19 12:00:10', '0000-00-00 00:00:00'),
(492, 'Valor entregue ao Ambasse.', 1, '2022-08-11', 10, 2022, 10, 1500.00, NULL, 1500.00, 3, 1, NULL, '', '', '04048', '', '', NULL, 1, 7, 13, 4, NULL, NULL, 1, '2022-10-19 12:27:00', '0000-00-00 00:00:00'),
(493, 'Valor entregue ao Milton', 1, '2022-08-11', 10, 2022, 46, 1689.81, NULL, 1689.81, 3, 3, NULL, '', '', '', '', '', NULL, 1, 7, 16, 4, NULL, NULL, 2, '2022-10-19 12:37:49', '0000-00-00 00:00:00'),
(494, 'Valor entregue ao Nelson Joao.', 1, '2022-08-12', 10, 2022, 22, 547.01, 92.99, 640.00, 3, 17, NULL, '', '', 'A2218/36067', '', '', NULL, 1, 102, 145, 4, 6, 1, 2, '2022-10-20 06:19:58', '0000-00-00 00:00:00'),
(495, 'Valor entregue a Monica.', 1, '2022-08-12', 10, 2022, 22, 504.27, 85.73, 590.00, 3, 17, NULL, '', '', 'A2218/36056', '', '', NULL, 1, 102, 145, 4, 3, 1, 2, '2022-10-20 06:25:47', '0000-00-00 00:00:00'),
(496, 'Valor entregue ao Guarda de Serviço. ', 1, '2022-08-12', 10, 2022, 17, 732.31, 67.69, 800.00, 3, 2, NULL, '', '', '', '', '', NULL, 1, 7, 15, 4, 48, 5, 1, '2022-10-20 06:35:49', '0000-00-00 00:00:00'),
(497, 'Valor entregue ao Ambasse.', 1, '2022-08-12', 10, 2022, 12, 500.00, NULL, 500.00, 3, 1, NULL, '', '', '8431313', '', '', NULL, 1, 100, 116, 4, 6, 3, 1, '2022-10-20 06:38:00', '0000-00-00 00:00:00'),
(498, 'Valor entregue ao Milton.', 1, '2022-08-12', 10, 2022, 12, 3000.00, NULL, 3000.00, 3, 1, NULL, '', '', '0125855', '', '', NULL, 1, 100, 117, 4, 34, 3, 1, '2022-10-20 06:41:11', '0000-00-00 00:00:00'),
(499, 'Deslocação da Diana para o Banco BCI Exclusivo, para abertura de conta.', 1, '2022-08-12', 10, 2022, 33, 490.00, NULL, 490.00, 3, 1, NULL, '', '', '003202', '', '', NULL, 1, 7, 11, 4, 2, 1, 1, '2022-10-20 06:44:46', '0000-00-00 00:00:00'),
(500, 'Valor entregue ao Ambasse, para compra de Plásticos.', 1, '2022-08-12', 10, 2022, 13, 1500.00, NULL, 1500.00, 3, 1, NULL, '', '', '', '', '', NULL, 1, 99, 99, 4, NULL, NULL, NULL, '2022-10-20 06:53:45', '0000-00-00 00:00:00'),
(501, 'Valor entregue ao Ambasse, para compra de Bebida do Sr. Marco.', 1, '2022-08-12', 10, 2022, 15, 2000.00, NULL, 2000.00, 3, 19, NULL, '', '', '', '', '', NULL, 1, 99, 99, 4, 2, 9, NULL, '2022-10-20 06:55:26', '0000-00-00 00:00:00'),
(502, 'Valor entregue ao Milton para Autenticar Documentos.', 1, '2022-08-12', 10, 2022, 13, 200.00, NULL, 200.00, 3, 1, NULL, '', '', '', '', '', NULL, 1, 99, 99, 4, 1, 5, NULL, '2022-10-20 06:56:54', '0000-00-00 00:00:00'),
(503, 'Valor entregue aos guardas: Raimundo M.; Manuel Tinosse; Jorge Sitoe e Eurico.', 1, '2022-08-12', 10, 2022, 13, 3500.00, NULL, 3500.00, 3, 2, NULL, '', '', '', '', '', NULL, 1, 4, 62, 4, 4, 1, NULL, '2022-10-20 07:01:54', '0000-00-00 00:00:00'),
(504, 'Valor entregue ao Sr. Eurico.', 1, '2022-08-12', 10, 2022, 13, 200.00, NULL, 200.00, 3, 2, NULL, '', '', '', '', '', NULL, 1, 5, 64, 4, 1, 1, NULL, '2022-10-20 07:04:09', '0000-00-00 00:00:00'),
(505, 'Valor entregue ao Sr. Jorge.', 1, '2022-08-12', 10, 2022, 13, 200.00, NULL, 200.00, 3, 2, NULL, '', '', '', '', '', NULL, 1, 5, 66, 4, 1, 1, NULL, '2022-10-20 07:06:31', '0000-00-00 00:00:00'),
(506, 'Valor entregue ao Sr. Manuel.', 1, '2022-08-12', 10, 2022, 13, 200.00, NULL, 200.00, 3, 2, NULL, '', '', '', '', '', NULL, 1, 5, 67, 4, 1, 1, NULL, '2022-10-20 07:07:53', '0000-00-00 00:00:00'),
(507, 'Valor entregue ao Sr. Raimundo.', 1, '2022-08-12', 10, 2022, 13, 200.00, NULL, 200.00, 3, 2, NULL, '', '', '', '', '', NULL, 1, 5, 68, 4, 1, 1, NULL, '2022-10-20 07:09:06', '0000-00-00 00:00:00'),
(508, 'Valor entregue a Sra. Ana Dias.', 1, '2022-08-12', 10, 2022, 13, 500.00, NULL, 500.00, 3, 2, NULL, '', '', '', '', '', NULL, 1, 5, 63, 4, 1, 1, NULL, '2022-10-20 07:13:21', '0000-00-00 00:00:00'),
(509, 'Valor entregue a Sra. Isabel.\r\n', 1, '2022-08-12', 10, 2022, 13, 300.00, NULL, 300.00, 3, 2, NULL, '', '', '', '', '', NULL, 1, 5, 65, 4, 1, 1, NULL, '2022-10-20 07:18:03', '0000-00-00 00:00:00'),
(510, 'Valor entregue a Rufina.', 1, '2022-08-12', 10, 2022, 13, 500.00, NULL, 500.00, 3, 2, NULL, '', '', '', '', '', NULL, 1, 5, 123, 4, 1, 1, NULL, '2022-10-20 07:19:17', '0000-00-00 00:00:00'),
(511, 'Valor entregue a Monica.', 1, '2022-08-12', 10, 2022, 13, 300.00, NULL, 300.00, 3, 3, NULL, '', '', '', '', '', NULL, 1, 5, 77, 4, 1, 1, NULL, '2022-10-20 07:35:28', '0000-00-00 00:00:00'),
(512, 'Valor entregue ao Guarda de serviço.\r\n', 1, '2022-08-13', 10, 2022, 17, 1809.30, 190.70, 2000.00, 3, 2, NULL, '', '', '', '', '', NULL, 1, 7, 15, 4, 136, 5, 1, '2022-10-20 07:53:03', '0000-00-00 00:00:00'),
(513, 'Valor entregue ao Shamir.', 1, '2022-08-13', 10, 2022, 31, 200.00, NULL, 200.00, 3, 1, NULL, '', '', '000154', '', '', NULL, 1, 112, 133, 4, 1, 1, 1, '2022-10-20 07:57:00', '0000-00-00 00:00:00'),
(514, 'Valor entregue ao Caixa de serviço. ', 1, '2022-08-13', 10, 2022, 30, 1000.00, NULL, 1000.00, 3, 2, NULL, '', '', '00626', '', '', NULL, 1, 4, 61, 4, 1, 1, 1, '2022-10-20 08:00:05', '0000-00-00 00:00:00'),
(515, 'Valor entregue ao Milton.', 1, '2022-08-13', 10, 2022, 27, 4358.97, 741.03, 5100.00, 3, 3, NULL, '', '', '', '', '', NULL, 1, 7, 9, 4, 1, 9, 2, '2022-10-20 08:14:31', '0000-00-00 00:00:00'),
(516, 'Valor entregue ao caixa em serviço.\r\n', 1, '2022-08-14', 10, 2022, 30, 1000.00, NULL, 1000.00, 3, 3, NULL, '', '', '', '', '', NULL, 1, 4, 61, 4, 1, 9, 1, '2022-10-20 08:27:26', '0000-00-00 00:00:00'),
(517, 'Valor entregue ao Milton.', 1, '2022-08-20', 10, 2022, 27, 6830.00, NULL, 6830.00, 3, 19, NULL, '', '', '', '', '', NULL, 1, 7, 9, 4, 1, 9, NULL, '2022-10-20 08:31:35', '0000-00-00 00:00:00'),
(518, 'Valor entregue ao Milton.', 1, '2022-08-14', 10, 2022, 12, 3000.00, NULL, 3000.00, 3, 1, NULL, '', '', '0002343', '', '', NULL, 1, 100, 117, 4, NULL, NULL, 1, '2022-10-20 08:36:22', '0000-00-00 00:00:00'),
(519, 'Valor entregue ao Milton.', 1, '2022-08-14', 10, 2022, 27, 6830.00, NULL, 6830.00, 3, 19, NULL, '', '', '', '', '', NULL, 1, 7, 9, 4, 1, 9, NULL, '2022-10-20 08:38:12', '0000-00-00 00:00:00'),
(520, 'Pagamento de Impressoes A3, valor entregue ao Nelson Joao.', 1, '2022-08-15', 10, 2022, 36, 400.00, NULL, 400.00, 3, 2, NULL, '', '', '00034', '', '', NULL, 1, 99, 99, 4, 4, 1, 1, '2022-10-20 10:13:16', '0000-00-00 00:00:00'),
(521, 'Valor entregue a sra. Ana Dias. ', 1, '2022-08-15', 10, 2022, 13, 500.00, NULL, 500.00, 3, 2, NULL, '', '', '', '', '', NULL, 1, 5, 63, 4, 1, 1, NULL, '2022-10-20 10:15:45', '0000-00-00 00:00:00'),
(522, 'Valor entregue a Sra. Isabel.', 1, '2022-08-15', 10, 2022, 13, 300.00, NULL, 300.00, 3, 2, NULL, '', '', '', '', '', NULL, 1, 5, 65, 4, 1, 1, NULL, '2022-10-20 10:17:01', '0000-00-00 00:00:00'),
(523, 'valor entregue a Rufina.', 1, '2022-08-15', 10, 2022, 13, 500.00, NULL, 500.00, 3, 2, NULL, '', '', '', '', '', NULL, 1, 5, 123, 4, 1, 1, NULL, '2022-10-20 10:25:29', '0000-00-00 00:00:00'),
(524, 'Valor entregue ao Nelson Joao.', 1, '2022-08-15', 10, 2022, 13, 500.00, NULL, 500.00, 3, 2, NULL, '', '', '', '', '', NULL, 1, 5, 153, 4, 1, 1, NULL, '2022-10-20 10:27:20', '0000-00-00 00:00:00'),
(525, 'Valor entregue ao Ambasse.', 1, '2022-08-15', 10, 2022, 12, 3000.00, NULL, 3000.00, 3, 1, NULL, '', '', '0126067', '', '', NULL, 1, 100, 116, 4, 34, 3, 1, '2022-10-20 10:28:50', '0000-00-00 00:00:00'),
(526, 'Valor entregue ao Sr. Eurico.', 1, '2022-08-15', 10, 2022, 13, 200.00, NULL, 200.00, 3, 2, NULL, '', '', '', '', '', NULL, 1, 5, 64, 4, 1, 1, NULL, '2022-10-20 10:32:07', '0000-00-00 00:00:00'),
(527, '200', 1, '2022-08-15', 10, 2022, 13, 200.00, NULL, 200.00, 3, 2, NULL, '', '', '', '', '', NULL, 1, 5, 66, 4, 1, 1, NULL, '2022-10-20 10:33:37', '0000-00-00 00:00:00'),
(528, 'Valor entregue ao Sr. Manuel.', 1, '2022-08-15', 10, 2022, 13, 200.00, NULL, 200.00, 3, 2, NULL, '', '', '', '', '', NULL, 1, 5, 67, 4, 1, 1, NULL, '2022-10-20 10:35:39', '0000-00-00 00:00:00'),
(529, 'Valor entregue ao Sr. Raimundo.', 1, '2022-08-15', 10, 2022, 13, 200.00, NULL, 200.00, 3, 2, NULL, '', '', '', '', '', NULL, 1, 5, 68, 4, 1, 1, NULL, '2022-10-20 10:36:57', '0000-00-00 00:00:00'),
(530, 'Valor enviado pra o Sr. Tomas, pagamento de mão de obra.', 1, '2022-08-15', 10, 2022, 13, 2500.00, NULL, 2500.00, 6, 10, 13, '', '', '', NULL, '846476730', NULL, 1, 13, 102, 4, 1, 1, NULL, '2022-10-20 10:45:43', '2022-10-20 12:57:51'),
(531, 'Valor enviado para o Milton. ', NULL, '2022-08-16', 10, 2022, 13, 510.00, NULL, 510.00, 6, 3, 5, '', '', '', NULL, '845533809', NULL, 0, 5, 70, 4, 1, 1, NULL, '2022-10-20 11:21:37', '2022-10-20 14:55:37'),
(532, 'Valor enviado para ao Milton.', NULL, '2022-08-16', 10, 2022, 13, 500.00, NULL, 500.00, 6, 3, 5, '', '', '', NULL, '845533809', NULL, 0, 5, 72, 4, 1, 1, NULL, '2022-10-20 11:23:14', '2022-10-20 14:57:47'),
(533, 'Valor entregue ao Mohamad ', 1, '2022-08-15', 10, 2022, 13, 500.00, NULL, 500.00, 3, 2, NULL, '', '', '', '', '', NULL, 1, 5, 152, 4, NULL, NULL, NULL, '2022-10-20 11:34:44', '0000-00-00 00:00:00'),
(534, 'Valor Enviado ao Manuel Tinosse', 1, '2022-08-18', 10, 2022, 13, 200.00, NULL, 200.00, 3, 2, NULL, '', '', '', '', '', NULL, 1, 5, 67, 4, NULL, NULL, NULL, '2022-10-20 11:46:47', '0000-00-00 00:00:00'),
(535, 'Valor entregue ao Raimundo', 1, '2022-08-18', 10, 2022, 13, 200.00, NULL, 200.00, 3, 2, NULL, '', '', '', '', '', NULL, 1, 5, 68, 4, NULL, NULL, NULL, '2022-10-20 11:47:56', '0000-00-00 00:00:00'),
(536, 'Valor entregue ao Jorge', 1, '2022-08-18', 10, 2022, 13, 200.00, NULL, 200.00, 3, 2, NULL, '', '', '', '', '', NULL, 1, 5, 66, 4, NULL, NULL, NULL, '2022-10-20 11:49:07', '0000-00-00 00:00:00'),
(537, 'Valor entregue ao Milton.', 1, '2022-08-18', 10, 2022, 12, 3000.00, NULL, 3000.00, 3, 1, NULL, '', '', '0002364', '', '', NULL, 1, 100, 117, 4, NULL, NULL, 1, '2022-10-20 11:51:31', '0000-00-00 00:00:00'),
(538, 'Valor entregue ao Chamir. Compra de Tabuleiro.', 1, '2022-08-18', 10, 2022, 42, 2880.00, NULL, 2880.00, 3, 3, 106, '', '', '', NULL, '', NULL, 1, 106, 171, 4, 0, NULL, NULL, '2022-10-20 11:56:15', '2022-11-07 13:38:42'),
(539, 'Valor entre ao fenias', 1, '2022-08-19', 10, 2022, 13, 209.00, NULL, 209.00, 3, 1, NULL, '', '', '', '', '', NULL, 1, 7, 11, 4, NULL, NULL, NULL, '2022-10-20 12:03:21', '0000-00-00 00:00:00'),
(540, 'Valor enviado para o Milton.', NULL, '2022-08-16', 10, 2022, 13, 500.00, NULL, 500.00, 6, 3, NULL, '', '', '', '', '845533809', NULL, NULL, 5, 73, 4, 1, 1, NULL, '2022-10-20 12:58:56', '0000-00-00 00:00:00'),
(541, 'Valor enviado para o Milton. Subsídio relativo a duas semanas.', NULL, '2022-08-16', 10, 2022, 13, 1000.00, NULL, 1000.00, 6, 3, 5, '', '', '', NULL, '845533809', NULL, 0, 5, 74, 4, 1, 1, NULL, '2022-10-20 13:00:23', '2022-10-20 15:07:58'),
(542, 'Valor enviado para o Milton.', NULL, '2022-08-16', 10, 2022, 13, 300.00, NULL, 300.00, 6, 3, NULL, '', '', '', '', '855533809', NULL, NULL, 5, 75, 4, 1, 1, NULL, '2022-10-20 13:01:26', '0000-00-00 00:00:00'),
(543, 'Valor enviado para o Milton.', NULL, '2022-08-16', 10, 2022, 13, 500.00, NULL, 500.00, 6, 2, 5, '', '', '', NULL, '845533809', NULL, 0, 5, 69, 4, 1, 1, NULL, '2022-10-20 13:05:44', '2022-10-31 10:07:06'),
(544, 'Valor entregue ao Guarda em Serviço.\r\n', 1, '2022-08-21', 10, 2022, 17, 1809.30, 190.70, 2000.00, 3, 2, 7, '', '', '', NULL, '', NULL, 1, 7, 15, 4, 136, 5, 1, '2022-10-20 13:12:38', '2022-10-20 15:13:51'),
(545, 'Valor entregue ao Raimundo para compra de Credelec.', 1, '2022-08-21', 10, 2022, 13, 30.00, NULL, 30.00, 3, 2, NULL, '', '', '', '', '', NULL, 1, 7, 11, 4, 2, 9, NULL, '2022-10-20 13:14:53', '0000-00-00 00:00:00'),
(546, 'Valor entregue ao caixa em serviço. ', 1, '2022-08-21', 10, 2022, 29, 1000.00, NULL, 1000.00, 3, 3, NULL, '', '', '1157', '', '', NULL, 1, 4, 61, 4, 1, NULL, 1, '2022-10-20 13:16:19', '0000-00-00 00:00:00'),
(547, 'Valor entregue ao Sr. Albino para Transporte e pagamento de Portagem.', 1, '2022-08-21', 10, 2022, 13, 200.00, NULL, 200.00, 3, 1, 99, '', '', '', NULL, '', NULL, 1, 99, 99, 4, 1, 9, NULL, '2022-10-20 13:18:18', '2022-11-04 06:14:00'),
(548, 'Valor entregue ao Caixa.', 1, '2022-08-21', 10, 2022, 30, 1000.00, NULL, 1000.00, 3, 2, NULL, '', '', '', '', '', NULL, 1, 4, 61, 4, 1, 1, 1, '2022-10-20 13:19:28', '0000-00-00 00:00:00'),
(549, 'Valor entregue ao Sr. Mariano.', 1, '2022-08-21', 10, 2022, 12, 3000.00, NULL, 3000.00, 3, 1, NULL, '', '', '07791', '', '', NULL, 1, 100, 117, 4, NULL, NULL, 1, '2022-10-20 13:20:36', '0000-00-00 00:00:00'),
(550, 'Valor enviado para o Rosario.', NULL, '2022-08-22', 10, 2022, 13, 400.00, NULL, 400.00, 6, 4, NULL, '', '', '', '', '870502683', NULL, NULL, 5, 81, 4, 1, 1, NULL, '2022-10-20 13:36:19', '0000-00-00 00:00:00'),
(551, 'Valor enviado para o Rosario.', NULL, '2022-08-22', 10, 2022, 13, 100.00, NULL, 100.00, 6, 4, NULL, '', '', '', '', '870502683', NULL, NULL, 5, 82, 4, 1, 1, NULL, '2022-10-20 13:37:27', '0000-00-00 00:00:00'),
(552, 'Valor enviado para o Rosario', NULL, '2022-08-22', 10, 2022, 13, 600.00, NULL, 600.00, 6, 4, NULL, '', '', '', '', '870502683', NULL, NULL, 5, 83, 4, 1, 1, NULL, '2022-10-20 13:38:29', '0000-00-00 00:00:00'),
(553, 'Valor enviado para o Rosario.', NULL, '2022-08-22', 10, 2022, 13, 400.00, NULL, 400.00, 6, 4, NULL, '', '', '', '', '870502683', NULL, NULL, 5, 85, 4, 1, 1, NULL, '2022-10-20 13:39:40', '0000-00-00 00:00:00'),
(554, 'Valor enviado para o Rosario.', NULL, '2022-08-22', 10, 2022, 13, 400.00, NULL, 400.00, 6, 12, NULL, '', '', '', '', '870502683', NULL, NULL, 5, 93, 4, 1, 1, NULL, '2022-10-20 18:04:00', '0000-00-00 00:00:00'),
(555, 'Valor enviado para o Rosario.', NULL, '2022-08-22', 10, 2022, 13, 400.00, NULL, 400.00, 6, 12, NULL, '', '', '', '', '870502683', NULL, NULL, 5, 94, 4, 1, 1, NULL, '2022-10-20 18:05:51', '0000-00-00 00:00:00'),
(556, 'Valor enviado para o Rosário, com o custo de transação de 05,00MT.', NULL, '2022-08-22', 10, 2022, 13, 405.00, NULL, 405.00, 6, 12, NULL, '', '', '', '', '870502683', NULL, NULL, 5, 95, 4, 1, 1, NULL, '2022-10-20 18:07:27', '0000-00-00 00:00:00'),
(557, 'Valor enviado para o Rosário, com o custo de transação de 05,00MT.', NULL, '2022-08-22', 10, 2022, 13, 105.00, NULL, 105.00, 6, 12, NULL, '', '', '', '', '870502683', NULL, NULL, 5, 96, 4, 1, 1, NULL, '2022-10-20 18:10:03', '0000-00-00 00:00:00'),
(558, 'Vale, cujo valor será debitado nas semanas seguintes.', 1, '2022-08-22', 10, 2022, 13, 500.00, NULL, 500.00, 3, 3, 5, '', '', '', NULL, '', NULL, 1, 5, 75, 4, 1, 1, NULL, '2022-10-20 19:02:21', '2022-10-20 21:03:34'),
(559, 'Valor entregue ao Ambasse para envio de mercadoria Maputo-Beira.', 1, '2022-08-23', 10, 2022, 15, 1500.00, NULL, 1500.00, 3, 1, NULL, '', '', '', '', '', NULL, 1, 7, 13, 4, 1, 9, NULL, '2022-10-20 19:31:25', '0000-00-00 00:00:00'),
(560, 'Valor entregue ao canalizador Carlinhos.', 1, '2022-08-23', 10, 2022, 45, 320.00, NULL, 320.00, 3, 3, NULL, '', '', '001404', '', '', NULL, 1, 13, 104, 4, 1, 1, 1, '2022-10-20 19:41:23', '0000-00-00 00:00:00'),
(561, 'Pagamento do canalizador Carlinhos.', 1, '2022-08-23', 10, 2022, 13, 500.00, NULL, 500.00, 3, 3, NULL, '', '', '', '', '', NULL, 1, 13, 104, 4, 1, 9, NULL, '2022-10-20 19:43:58', '0000-00-00 00:00:00'),
(562, 'Compra de Fixa. Valor entregue ao canalizador.', 1, '2022-08-23', 10, 2022, 45, 25.00, NULL, 25.00, 3, 3, NULL, '', '', '001404', '', '', NULL, 1, 13, 104, 4, 1, 1, 1, '2022-10-20 19:50:35', '0000-00-00 00:00:00'),
(563, 'Valor solicitado pela Florinda.', 1, '2022-08-26', 10, 2022, 13, 4000.00, NULL, 4000.00, 3, 1, 99, '', '', '', NULL, '', NULL, 1, 99, 99, 4, 0, 9, NULL, '2022-10-20 21:02:15', '2022-10-20 23:16:08'),
(564, 'Valor enviado para o Rosario.', NULL, '2022-08-30', 10, 2022, 46, 3500.00, NULL, 3500.00, 6, 10, NULL, '', '', '', '', '870502683', NULL, NULL, 7, 16, 4, NULL, NULL, NULL, '2022-10-20 22:34:57', '0000-00-00 00:00:00'),
(565, 'Pagamento feito pela Diana.', 1, '2022-09-01', 10, 2022, 6, 8000.00, NULL, 8000.00, 3, 1, NULL, '', '', '10170', '', '', NULL, 1, 100, 122, 4, 92, 3, 1, '2022-10-20 22:40:48', '0000-00-00 00:00:00'),
(566, 'Valor entregue ao Shamir.', 1, '2022-09-01', 10, 2022, 12, 3000.00, NULL, 3000.00, 3, 1, NULL, '', '', '0127385', '', '', NULL, 1, 100, 117, 4, 34, 3, 1, '2022-10-20 22:43:00', '0000-00-00 00:00:00'),
(567, 'Compra efectuada pela Diana.', 1, '2022-09-01', 10, 2022, 17, 1818.08, 181.92, 2000.00, 3, 2, NULL, '', '', '', '', '', NULL, 1, 7, 15, 4, 129, 5, 1, '2022-10-20 22:46:44', '0000-00-00 00:00:00'),
(568, 'Valor entregue ao sr. Albino.', 1, '2022-09-01', 10, 2022, 12, 2000.00, NULL, 2000.00, 3, 1, NULL, '', '', '0127351', '', '', NULL, 1, 100, 115, 4, 23, 3, 1, '2022-10-20 22:48:27', '0000-00-00 00:00:00'),
(569, 'Compra de Rolos de POS. Valor enviado para o Rosario.', NULL, '2022-09-01', 10, 2022, 47, 3250.00, NULL, 3250.00, 6, 10, 106, '', '', '04505', NULL, '870502683', NULL, 0, 106, 130, 4, 0, NULL, 1, '2022-10-21 04:29:53', '2022-10-21 06:40:41'),
(570, 'Compra de Rolos de Sistema. Valor enviado para o Rosario.', NULL, '2022-09-01', 10, 2022, 47, 8750.00, NULL, 8750.00, 6, 10, 106, '', '', '04505', NULL, '870502683', NULL, 0, 106, 130, 4, 50, 1, 1, '2022-10-21 04:34:25', '2022-10-21 06:44:03'),
(571, 'Valor enviado para o Rosario.', NULL, '2022-09-01', 10, 2022, 11, 650.00, NULL, 650.00, 6, 1, NULL, '', '', '', '', '870502683', NULL, NULL, 108, 129, 4, 1, 9, NULL, '2022-10-21 04:46:08', '0000-00-00 00:00:00'),
(572, 'Deslocação de Rosário para Farmácia Mualy, para recolha de Facturas.', 1, '2022-09-01', 10, 2022, 13, 350.00, NULL, 350.00, 6, 1, NULL, '', '', '', '', '870502683', NULL, 1, 7, 11, 4, 1, 1, NULL, '2022-10-21 04:52:38', '0000-00-00 00:00:00'),
(573, 'Valor entregue ao Carlos Bento para deslocação de FME Beach Front- FME Kenneth Kaunda.', NULL, '2022-09-01', 10, 2022, 11, 610.00, NULL, 610.00, 6, 1, NULL, '', '', '', '', '843910518', NULL, NULL, 7, 11, 4, 2, 9, NULL, '2022-10-21 04:54:40', '0000-00-00 00:00:00'),
(574, 'Valor entregue ao Fenias para deslocação as Seguradoras.', 1, '2022-09-01', 10, 2022, 13, 200.00, NULL, 200.00, 3, 1, 7, '', '', '', NULL, '', NULL, 1, 7, 11, 4, 0, NULL, NULL, '2022-10-21 05:00:25', '2022-10-21 07:14:15'),
(575, 'Valor enviado para a sra. Florinda.', NULL, '2022-09-01', 10, 2022, 12, 3000.00, NULL, 3000.00, 6, 1, NULL, '', '', '', '', '842173242', NULL, NULL, 100, 119, 4, NULL, NULL, NULL, '2022-10-21 05:03:36', '0000-00-00 00:00:00'),
(576, 'Pagamento parcial da Escola de Condução do Sr. Marco. Valor entregue ao Milton.', 1, '2022-09-01', 10, 2022, 13, 6250.00, NULL, 6250.00, 3, 17, NULL, '', '', '', '', '', NULL, 1, 99, 99, 4, NULL, NULL, NULL, '2022-10-21 05:05:38', '0000-00-00 00:00:00'),
(577, 'Pagamento de Polícia de Transito. Ractis ALD 852 MC, valor entregue ao Shamir.', 1, '2022-09-01', 10, 2022, 13, 1000.00, NULL, 1000.00, 3, 1, NULL, '', '', '', '', '', NULL, 1, 99, 99, 4, NULL, NULL, NULL, '2022-10-21 05:09:06', '0000-00-00 00:00:00'),
(578, 'Valor enviado para o Sr. Albino.', NULL, '2022-08-11', 10, 2022, 12, 2000.00, NULL, 2000.00, 6, 1, NULL, '', '', '', '', '878500911', NULL, NULL, 100, 115, 7, NULL, NULL, NULL, '2022-10-21 07:45:32', '0000-00-00 00:00:00'),
(579, 'Valor entregue ao Milton para Compra de 6 caixas de Agua Namaacha Mineral Natural 1,5l e 04 caixas de 500ml.', 1, '2022-08-22', 10, 2022, 55, 2692.30, 457.70, 3150.00, 3, 2, 99, '', '', '5880/2022A', NULL, '', NULL, 1, 99, 99, 4, 0, 1, 2, '2022-10-25 07:54:42', '2022-10-25 09:56:21'),
(580, 'Manutenção da Viatura. Valor envia para a Florinda', 1, '2022-08-12', 10, 2022, 56, 32868.00, 6732.00, 39600.00, 3, 1, NULL, '', '', '', '', '', NULL, 1, 1, 46, 4, NULL, 1, NULL, '2022-10-25 10:15:43', '0000-00-00 00:00:00'),
(581, 'Valor entregue a Monica.', 1, '2022-08-06', 10, 2022, 13, 3000.00, NULL, 3000.00, 3, 3, NULL, '', '', '', '', '', NULL, 1, 105, 157, 4, NULL, NULL, NULL, '2022-10-25 11:31:17', '0000-00-00 00:00:00'),
(582, 'Valor entregue ao Ambasse.', 1, '2022-08-05', 10, 2022, 12, 500.00, NULL, 500.00, 3, 1, NULL, '', '', '012790', '', '', NULL, 1, 100, 116, 4, NULL, NULL, 1, '2022-10-28 02:19:20', '0000-00-00 00:00:00'),
(583, 'Valor enviado para a Sra Eufrasia.', NULL, '2022-08-10', 10, 2022, 26, 2700.00, NULL, 2700.00, 6, 1, NULL, '', '', '95154', '', '846538861', NULL, NULL, 100, 120, 4, NULL, 3, 1, '2022-10-28 04:12:25', '0000-00-00 00:00:00'),
(584, 'Valor enviado para a Sra. Eufrasia.', NULL, '2022-08-14', 10, 2022, 26, 2800.00, NULL, 2800.00, 6, 1, NULL, '', '', '95537', '', '846538861', NULL, NULL, 100, 120, 4, NULL, 3, 1, '2022-10-28 04:15:30', '0000-00-00 00:00:00'),
(585, 'Pagamento do Dr. Perreira. Prestação de Serviços. ', NULL, '2022-08-11', 10, 2022, 13, 17400.00, NULL, 17400.00, 6, 1, 99, '', '', '', NULL, '12947210910001', NULL, 0, 99, 99, 1, 0, NULL, NULL, '2022-10-28 11:38:02', '2022-10-31 13:32:28'),
(586, 'valor entregue ao Milton.', 1, '2022-08-12', 10, 2022, 13, 500.00, NULL, 500.00, 3, 3, NULL, '', '', '', '', '', NULL, 1, 5, 170, 4, NULL, 1, NULL, '2022-10-31 10:39:17', '0000-00-00 00:00:00'),
(587, 'Valor entregue ao Milton', 1, '2022-08-29', 10, 2022, 13, 500.00, NULL, 500.00, 3, 3, NULL, '', '', '', '', '', NULL, 1, 5, 170, 4, NULL, NULL, NULL, '2022-10-31 10:40:12', '0000-00-00 00:00:00'),
(588, 'Valor entregue ao Milton.', 1, '2022-08-23', 10, 2022, 13, 500.00, NULL, 500.00, 3, 3, NULL, '', '', '', '', '', NULL, 1, 5, 170, 4, NULL, 1, NULL, '2022-10-31 10:42:53', '0000-00-00 00:00:00'),
(589, 'Compra efectuada pelo Guarda em Servico.', 1, '2022-08-06', 11, 2022, 17, 542.79, 57.21, 600.00, 3, 2, NULL, '', '', '', '', '', NULL, 1, 7, 15, 4, NULL, 5, 1, '2022-11-01 11:35:55', '0000-00-00 00:00:00'),
(590, 'Valor entregue ao Sr. Albino.', 1, '2022-11-16', 11, 2022, 57, 1000.00, NULL, 1000.00, 3, 1, NULL, '', '', '274624', '', '', NULL, 1, 100, 115, 4, NULL, 3, 1, '2022-11-17 09:40:53', '0000-00-00 00:00:00'),
(591, 'Compra efectuada pela Manthima Assane.', 1, '2022-11-16', 11, 2022, 17, 452.33, 47.67, 500.00, 3, 2, 7, '', '', '103', NULL, '', NULL, 1, 7, 15, 4, 0, 5, 5, '2022-11-17 09:45:43', '2022-12-12 12:33:27'),
(592, 'Compra efectuada pela Diana.', 1, '2022-11-16', 11, 2022, 17, 904.65, 95.35, 1000.00, 3, 19, NULL, '', '', '', '', '', NULL, 1, 7, 15, 4, NULL, 5, 1, '2022-11-17 09:54:35', '0000-00-00 00:00:00'),
(593, 'Compra efectuada pelo Rosario. A despesa apresenta Guia.', 1, '2022-11-16', 11, 2022, 4, 1000.00, NULL, 1000.00, 3, 17, NULL, '', '', '', '', '', NULL, 1, 3, 56, 4, NULL, 1, NULL, '2022-11-17 09:59:30', '0000-00-00 00:00:00'),
(594, 'Valor entregue ao dr. Perreira. Despesa com o Tribunal, pagamento de impostos.', 1, '2022-11-16', 11, 2022, 13, 5000.00, NULL, 5000.00, 3, 1, NULL, '', '', '', '', '', NULL, 1, 99, 99, 4, NULL, 1, 2, '2022-11-17 10:13:00', '0000-00-00 00:00:00'),
(595, 'Compra efectuada pelo Rosario.', 1, '2022-12-05', 12, 2022, 17, 1364.61, 135.39, 1500.00, 3, 2, NULL, '', '', '', '', '', NULL, 1, 7, 15, 4, NULL, 5, 1, '2022-12-05 09:42:28', '0000-00-00 00:00:00'),
(596, 'Compra efectuada pelo Rosario. Compra de Cadeado DBB pl0902.', 1, '2022-11-02', 12, 2022, 58, 850.00, NULL, 850.00, 3, 12, NULL, '', '', '69371', '', '', NULL, 1, 99, 99, 4, NULL, 1, 1, '2022-12-06 06:18:17', '0000-00-00 00:00:00'),
(597, 'Compra efectuada pela Olga.', 1, '2022-11-01', 12, 2022, 17, 1821.14, 178.86, 2000.00, 3, 15, NULL, '', '', '', '', '', NULL, 1, 7, 15, 4, NULL, 5, 1, '2022-12-06 06:20:26', '0000-00-00 00:00:00'),
(598, 'Compra efectuada pelo Rosario.', 1, '2022-11-01', 12, 2022, 15, 550.00, NULL, 550.00, 3, 15, NULL, '', '', '18', '', '', NULL, 1, 7, 9, 4, NULL, 1, 5, '2022-12-06 06:28:48', '0000-00-00 00:00:00'),
(599, 'Valor entregue ao Txopela.', 1, '2022-11-01', 12, 2022, 11, 650.00, NULL, 650.00, 3, 1, NULL, '', '', '19', '', '', NULL, 1, 108, 128, 4, NULL, 1, 5, '2022-12-06 06:32:54', '0000-00-00 00:00:00'),
(600, 'Valor entregue a Sra. Florinda.', 1, '2022-11-02', 12, 2022, 12, 3000.00, NULL, 3000.00, 3, 1, NULL, '', '', '17877', '', '', NULL, 1, 100, 119, 4, NULL, 3, 1, '2022-12-06 06:42:30', '0000-00-00 00:00:00'),
(601, ' Valor solicitado pelo Fenias.', NULL, '2022-11-02', 12, 2022, 15, 200.00, NULL, 200.00, 6, 1, NULL, '', '', '20', '', '846852356', NULL, NULL, 7, 11, 4, NULL, 1, 5, '2022-12-06 06:52:29', '0000-00-00 00:00:00'),
(602, 'Valor entregue ao Sr. Albino.', NULL, '2022-11-02', 12, 2022, 57, 1000.00, NULL, 1000.00, 6, 1, 100, '', '', '272874', NULL, '845629961', NULL, 0, 100, 115, 2, 0, 3, 1, '2022-12-06 06:56:28', '2022-12-06 11:33:34'),
(603, 'Custo de transacao de 70,00MT. Valor enviado para o bombeiro.', NULL, '2022-11-03', 12, 2022, 12, 3170.00, NULL, 3170.00, 6, 1, NULL, '', '', '21', '', '845211323', NULL, NULL, 100, 122, 2, NULL, 3, 5, '2022-12-06 07:06:25', '0000-00-00 00:00:00'),
(604, 'Deslocacao de Patricio para FME Passagem de Nivel, para fazer inventario.', 1, '2022-11-03', 12, 2022, 11, 400.00, NULL, 400.00, 3, 1, NULL, '', '', '22', '', '', NULL, 1, 7, 11, 4, NULL, 1, 5, '2022-12-06 07:15:40', '0000-00-00 00:00:00'),
(605, 'Valor entregue ao Oscar.', 1, '2022-11-03', 12, 2022, 13, 400.00, NULL, 400.00, 3, 10, NULL, '', '', '', '', '', NULL, 1, 5, 84, 4, NULL, 1, NULL, '2022-12-06 07:17:41', '0000-00-00 00:00:00'),
(606, 'Valor entregue ao Rosario.', 1, '2022-11-03', 12, 2022, 13, 800.00, NULL, 800.00, 3, 10, NULL, '', '', '', '', '', NULL, 1, 5, 86, 4, NULL, 1, NULL, '2022-12-06 07:19:13', '0000-00-00 00:00:00'),
(607, 'Valoor entregue a Fatima.', 1, '2022-11-03', 12, 2022, 13, 400.00, NULL, 400.00, 3, 10, NULL, '', '', '', '', '', NULL, 1, 5, 87, 4, NULL, 1, NULL, '2022-12-06 07:20:07', '0000-00-00 00:00:00'),
(608, 'Valor entregue ao Francisco.', 1, '2022-11-03', 12, 2022, 13, 500.00, NULL, 500.00, 3, 10, NULL, '', '', '', '', '', NULL, 1, 5, 88, 4, NULL, 1, NULL, '2022-12-06 07:21:14', '0000-00-00 00:00:00'),
(609, 'Valor entregue ao Latifo.', 1, '2022-11-03', 12, 2022, 13, 400.00, NULL, 400.00, 3, 10, NULL, '', '', '', '', '', NULL, 1, 5, 89, 4, NULL, 1, NULL, '2022-12-06 07:22:57', '0000-00-00 00:00:00'),
(610, 'Valor entregue ao Agostinho.', 1, '2022-11-03', 12, 2022, 13, 100.00, NULL, 100.00, 3, 10, NULL, '', '', '', '', '', NULL, 1, 5, 90, 4, NULL, 1, NULL, '2022-12-06 07:24:06', '0000-00-00 00:00:00'),
(611, 'Valor entregue a Jacinta.', 1, '2022-11-03', 12, 2022, 13, 400.00, NULL, 400.00, 3, 10, NULL, '', '', '', '', '', NULL, 1, 5, 91, 4, NULL, 1, NULL, '2022-12-06 07:25:04', '0000-00-00 00:00:00'),
(612, 'valor entregue a Miquelina.', 1, '2022-11-03', 12, 2022, 13, 400.00, NULL, 400.00, 3, 10, NULL, '', '', '', '', '', NULL, 1, 5, 92, 4, NULL, 1, NULL, '2022-12-06 07:26:42', '0000-00-00 00:00:00'),
(613, 'Valor entregue ao Armando.', 1, '2022-11-03', 12, 2022, 13, 400.00, NULL, 400.00, 3, 4, NULL, '', '', '', '', '', NULL, 1, 5, 80, 4, NULL, 1, NULL, '2022-12-06 07:28:02', '0000-00-00 00:00:00'),
(614, 'Valor entregue a Leticia.', 1, '2022-11-03', 12, 2022, 13, 400.00, NULL, 400.00, 3, 4, NULL, '', '', '', '', '', NULL, 1, 5, 81, 4, NULL, 1, NULL, '2022-12-06 07:29:18', '0000-00-00 00:00:00'),
(615, 'Valor entregue a Manuela.', 1, '2022-11-03', 12, 2022, 13, 100.00, NULL, 100.00, 3, 4, NULL, '', '', '', '', '', NULL, 1, 5, 82, 4, NULL, 1, NULL, '2022-12-06 07:30:04', '0000-00-00 00:00:00'),
(616, 'Valor enviado para o Milton.', 1, '2022-11-28', 12, 2022, 13, 500.00, NULL, 500.00, 3, 3, 5, '', '', '', NULL, '', NULL, 1, 5, 170, 4, 1, 1, NULL, '2022-12-06 07:31:12', '2022-12-13 10:39:30'),
(617, 'Valor enviado para a Joana, com o custo de transação e levantamento de 20,00MT.', 1, '2022-11-28', 12, 2022, 13, 520.00, NULL, 520.00, 6, 3, 5, '', '', '', NULL, '844257344', NULL, 1, 5, 74, 4, 1, 1, NULL, '2022-12-06 07:32:46', '2022-12-12 14:29:15'),
(618, 'Valor entregue ao Oscar.', 1, '2022-11-03', 12, 2022, 11, 650.00, NULL, 650.00, 3, 1, NULL, '', '', '23', '', '', NULL, 1, 108, 128, 4, NULL, 1, 5, '2022-12-06 07:35:19', '0000-00-00 00:00:00'),
(619, 'valor entregue ao Sr. Albino. Custo de transacao e levantamento de 50,00MT.', NULL, '2022-11-03', 12, 2022, 12, 2050.00, NULL, 2050.00, 6, 1, 100, '', '', '273805', NULL, '845629961', NULL, 0, 100, 115, 2, 0, 3, 1, '2022-12-06 07:43:51', '2022-12-06 11:48:20'),
(620, 'Deslocacao de Anilza de FME kenneth Kaunda para FME Costa do Sol.', NULL, '2022-11-03', 12, 2022, 11, 400.00, NULL, 400.00, 6, 1, NULL, '', '', '24', '', '847185476', NULL, NULL, 7, 11, 2, NULL, 1, 5, '2022-12-06 07:55:43', '0000-00-00 00:00:00'),
(621, 'Compra de 27 Rolos de POS e 10 Rolos de Maquina. Valor enviado para o Milton.', NULL, '2022-11-03', 12, 2022, 20, 2700.00, NULL, 2700.00, 6, 3, NULL, '', '', '004320', '', '845533809', NULL, NULL, 106, 171, 2, NULL, 1, 1, '2022-12-06 08:04:22', '0000-00-00 00:00:00'),
(622, 'Deslocacao de Fenias para Importadora  TMI. Para fazer devolucao de medicamentos.', NULL, '2022-11-03', 12, 2022, 13, 200.00, NULL, 200.00, 6, 1, NULL, '', '', '25', '', '846852356', NULL, NULL, 7, 11, 4, NULL, 1, 5, '2022-12-06 08:28:01', '0000-00-00 00:00:00'),
(623, 'Vale do sr. Marco.', 1, '2022-11-03', 12, 2022, 13, 780.00, NULL, 780.00, 3, 1, NULL, '', '', '26', '', '', NULL, 1, 99, 99, 4, NULL, NULL, 5, '2022-12-06 08:31:00', '0000-00-00 00:00:00'),
(624, 'Envio de Documentos efectuado pelo Rosario.', 1, '2022-11-03', 12, 2022, 13, 1500.00, NULL, 1500.00, 3, 1, NULL, '', '', '26', '', '', NULL, 1, 7, 12, 4, NULL, 1, 5, '2022-12-06 08:36:55', '0000-00-00 00:00:00'),
(625, 'Valor enviado para a Sra. Vina(professora), solicitado pelo Sr. Marco com o custo de envio de 50,00MT.', NULL, '2022-11-03', 12, 2022, 13, 2550.00, NULL, 2550.00, 6, 1, 99, '', '', '27', NULL, '845588808', NULL, 0, 99, 99, 2, 0, 1, 5, '2022-12-06 08:45:51', '2022-12-06 10:47:40'),
(626, 'Recarga do Sr. Marco, compra efectuada pelo Rosario.', 1, '2022-11-03', 12, 2022, 4, 1000.00, NULL, 1000.00, 3, 1, NULL, '', '', '29', '', '', NULL, 1, 3, 56, 4, NULL, 1, 5, '2022-12-06 08:50:43', '0000-00-00 00:00:00'),
(627, 'Valor entregue ao guarda em Servico.', 1, '2022-11-03', 12, 2022, 12, 1700.00, NULL, 1700.00, 3, 10, NULL, '', '', '8283', '', '', NULL, 1, 8, 146, 4, NULL, 3, 1, '2022-12-06 08:59:24', '0000-00-00 00:00:00'),
(628, 'Deslocacao de Rosario para o Aeroporto, para envio de Mercadoria.', 1, '2022-11-03', 12, 2022, 13, 600.00, NULL, 600.00, 3, 1, NULL, '', '', '29', '', '', NULL, 1, 7, 11, 4, NULL, 1, 5, '2022-12-06 09:02:14', '0000-00-00 00:00:00'),
(629, 'Valo0pr entregue a Sra. Eufrasia.', 1, '2022-11-02', 12, 2022, 26, 2700.00, NULL, 2700.00, 3, 1, NULL, '', '', '97133', '', '', NULL, 1, 100, 120, 4, NULL, 3, 1, '2022-12-06 10:27:24', '0000-00-00 00:00:00'),
(630, 'Valor entregue a Aissa.', 1, '2022-11-04', 12, 2022, 13, 400.00, NULL, 400.00, 3, 12, NULL, '', '', '', '', '', NULL, 1, 5, 94, 4, NULL, NULL, NULL, '2022-12-06 10:30:01', '0000-00-00 00:00:00'),
(631, 'Valor entregue a Alcina.', 1, '2022-11-04', 12, 2022, 13, 400.00, NULL, 400.00, 3, 12, NULL, '', '', '', '', '', NULL, 1, 5, 95, 4, NULL, 1, NULL, '2022-12-06 10:30:56', '0000-00-00 00:00:00'),
(632, 'Valor entregue a Maria.', 1, '2022-11-04', 12, 2022, 13, 100.00, NULL, 100.00, 3, 12, NULL, '', '', '', '', '', NULL, 1, 5, 96, 4, NULL, 1, NULL, '2022-12-06 10:32:01', '0000-00-00 00:00:00'),
(633, 'Valor entregue ao Oscar.', 1, '2022-11-04', 12, 2022, 13, 650.00, NULL, 650.00, 3, 1, NULL, '', '', '30', '', '', NULL, 1, 108, 128, 4, NULL, NULL, 5, '2022-12-06 10:33:16', '0000-00-00 00:00:00'),
(634, 'A despesa nao apresenta VD. 500Vodacom e 500 Movitel.', 1, '2022-11-04', 12, 2022, 15, 1000.00, NULL, 1000.00, 3, 15, NULL, '', '', '31', '', '', NULL, 1, 7, 9, 4, NULL, 1, 5, '2022-12-06 10:36:10', '0000-00-00 00:00:00'),
(635, 'Pagamento parcial. Compra efectuada por Milton.', 1, '2022-11-04', 12, 2022, 25, 3500.00, NULL, 3500.00, 3, 3, NULL, '', '', '32', '', '', NULL, 1, 7, 8, 4, NULL, 1, 5, '2022-12-06 10:40:28', '0000-00-00 00:00:00'),
(636, 'Compra efectuada pelo Rosario.', 1, '2022-11-04', 12, 2022, 17, 909.15, 90.85, 1000.00, 3, 2, NULL, '', '', '', '', '', NULL, 1, 7, 15, 4, NULL, 5, 1, '2022-12-06 10:44:02', '0000-00-00 00:00:00'),
(637, 'Compra efectuada pelo Rosario.', 1, '2022-11-04', 12, 2022, 17, 1364.62, 135.38, 1500.00, 3, 2, NULL, '', '', '', '', '', NULL, 1, 7, 15, 4, NULL, NULL, 1, '2022-12-06 10:47:08', '0000-00-00 00:00:00'),
(638, 'Compra de Bebida para o Sr. Marco, valor enviado para o Ambasse com o custo de transacao e levantamento de 70,00MT.', NULL, '2022-11-04', 12, 2022, 15, 2745.00, NULL, 2745.00, 6, 17, NULL, '', '', '33', '', '844396996', NULL, NULL, 99, 99, 2, NULL, 1, 5, '2022-12-06 10:51:51', '0000-00-00 00:00:00'),
(639, 'Reparacao do Pneu. Valor enviado para o Milton.', NULL, '2022-11-04', 12, 2022, 15, 500.00, NULL, 500.00, 6, 1, NULL, '', '', '34', '', '845533809', NULL, NULL, 1, 101, 4, NULL, 1, 5, '2022-12-06 10:55:12', '0000-00-00 00:00:00'),
(640, 'Deslocacao de Fenias para fazer devolucao de Medicamentos.', NULL, '2022-11-04', 12, 2022, 13, 100.00, NULL, 100.00, 6, 1, NULL, '', '', '34', '', '846852356', NULL, NULL, 7, 11, 4, NULL, 1, 5, '2022-12-06 11:33:37', '0000-00-00 00:00:00'),
(641, 'Recarga telefonica para Mualide.', 1, '2022-11-04', 12, 2022, 4, 200.00, NULL, 200.00, 3, 1, NULL, '', '', '36', '', '', NULL, 1, 99, 99, 4, NULL, 1, 4, '2022-12-06 11:36:19', '0000-00-00 00:00:00'),
(642, 'Valor solicitado pela Mualide, com autorizacao do Sr. Marco. valor enviado para o Milton. Custo de transacao e levantamento de 50,00MT.', NULL, '2022-11-04', 12, 2022, 13, 1550.00, NULL, 1550.00, 6, 1, NULL, '', '', '37', '', '845533809', NULL, NULL, 99, 99, 2, NULL, 1, 5, '2022-12-06 11:39:16', '0000-00-00 00:00:00'),
(643, 'Compra efectuada pelo Rosario.', 1, '2022-11-05', 12, 2022, 59, 10470.09, 1779.91, 12250.00, 3, 4, NULL, '', '', '1638', '', '', NULL, 1, 7, 8, 4, NULL, 1, 1, '2022-12-06 11:44:16', '0000-00-00 00:00:00'),
(644, 'Valor entregue a Dolca.', 1, '2022-11-05', 12, 2022, 12, 2700.00, NULL, 2700.00, 3, 1, NULL, '', '', '13293', '', '', NULL, 1, 100, 139, 4, NULL, 3, 1, '2022-12-06 11:49:02', '0000-00-00 00:00:00'),
(645, 'Compra efectuada pelo Rosario.', 1, '2022-11-05', 12, 2022, 17, 2731.71, 268.29, 3000.00, 3, 10, NULL, '', '', '', '', '', NULL, 1, 7, 15, 4, NULL, 5, 1, '2022-12-06 11:51:29', '0000-00-00 00:00:00'),
(646, 'Compra efectuada pelo Rosario.', 1, '2022-11-05', 12, 2022, 17, 1821.14, 178.86, 2000.00, 3, 15, NULL, '', '', '', '', '', NULL, 1, 7, 15, 4, NULL, 5, 1, '2022-12-06 11:53:27', '0000-00-00 00:00:00'),
(647, 'Compra efectuada pelo Guarda em Servico.', 1, '2022-11-05', 12, 2022, 12, 1700.00, NULL, 1700.00, 3, 10, NULL, '', '', '008254', '', '', NULL, 1, 8, 146, 4, NULL, 3, 1, '2022-12-06 11:55:04', '0000-00-00 00:00:00'),
(648, 'Pagamento de captacao de dados Sr Marco, valor enviado para o Milton com o custo de transacao e levantamento de 70,00MT ', 1, '2022-11-06', 12, 2022, 15, 2070.00, NULL, 2070.00, 6, 17, 99, '', '', '37', NULL, '845533809', NULL, 1, 99, 99, 2, 0, 1, 5, '2022-12-06 12:12:26', '2022-12-06 14:15:49'),
(649, 'Pagamento Parcial, valor enviado para o Milton com o custo de transacao e levantamento de 70,00MT.', NULL, '2022-11-06', 12, 2022, 25, 3570.00, NULL, 3570.00, 6, 3, NULL, '', '', '37', '', '845533809', NULL, NULL, 7, 8, 2, NULL, 1, 5, '2022-12-06 12:20:10', '0000-00-00 00:00:00'),
(650, 'Valor entregue ao Staff em Servico.', 1, '2022-11-06', 12, 2022, 38, 500.00, NULL, 500.00, 3, 10, NULL, '', '', '00525', '', '', NULL, 1, 111, 156, 4, NULL, 1, 1, '2022-12-06 12:23:22', '0000-00-00 00:00:00'),
(651, 'Valor entregue ao Staff em Servico.', 1, '2022-11-06', 12, 2022, 49, 500.00, NULL, 500.00, 3, 4, NULL, '', '', '000084', '', '', NULL, 1, 111, 156, 4, NULL, 1, 1, '2022-12-06 12:25:30', '0000-00-00 00:00:00'),
(652, 'Deslocacao de tecnico e Caixa em servico.', 1, '2022-11-06', 12, 2022, 11, 300.00, NULL, 300.00, 3, 10, NULL, '', '', '38', '', '', NULL, 1, 7, 11, 4, NULL, 1, 5, '2022-12-06 12:29:42', '0000-00-00 00:00:00'),
(653, 'Valor entregue a Dona Decia, solicitado pelo Sr. Marco. Valor enviado para Lucia com o custo de transacao e levantamento de 30,00MT.', NULL, '2022-11-06', 12, 2022, 13, 1030.00, NULL, 1030.00, 6, 1, NULL, '', '', '39', '', '848383477', NULL, NULL, 99, 99, 2, NULL, 1, 5, '2022-12-06 12:45:38', '0000-00-00 00:00:00'),
(654, 'Deslocacao de Fenias a Importadora para fazer devolucao.', NULL, '2022-11-06', 12, 2022, 11, 300.00, NULL, 300.00, 6, 1, NULL, '', '', '40', '', '846852356', NULL, NULL, 7, 11, 4, NULL, 1, 5, '2022-12-06 13:03:56', '0000-00-00 00:00:00'),
(655, 'Deslocacao de Monica para casa do Sr. Marco(entregar folha de Salario) com custa de transicao e levantamento de 10,00MT.', NULL, '2022-11-06', 12, 2022, 11, 310.00, NULL, 310.00, 6, 1, NULL, '', '', '41', '', '876765939', NULL, NULL, 7, 11, 4, NULL, 1, 5, '2022-12-06 13:06:26', '0000-00-00 00:00:00'),
(656, 'Compra efectuada pelo Rosario.', 1, '2022-11-06', 12, 2022, 17, 907.52, 92.48, 1000.00, 3, 19, NULL, '', '', '', '', '', NULL, 1, 7, 15, 4, NULL, 5, 1, '2022-12-06 13:08:37', '0000-00-00 00:00:00');
INSERT INTO `execucaodespesas` (`idexecucaodespesas`, `descricaodespesa`, `idrequisicaodefundos`, `data`, `mes`, `ano`, `idempresa`, `valorinicial`, `iva`, `valortotal`, `idformapagamento`, `idcentrodecusto`, `idgrupodespesas`, `observacoes`, `anexo`, `recibo`, `numerocheque`, `transfcontadestino`, `transferenciabanco`, `caixa`, `grupo`, `subgrupo`, `banco`, `quantidade`, `unidade`, `tipo_documento_id`, `data_criacao`, `data_atualizacao`) VALUES
(657, 'Compra efectuada pelo Rosario.', 1, '2022-11-06', 12, 2022, 17, 904.65, 95.35, 1000.00, 3, 2, NULL, '', '', '', '', '', NULL, 1, 7, 15, 4, NULL, 5, 1, '2022-12-06 13:10:32', '0000-00-00 00:00:00'),
(658, 'Valor enviado para o Ambasse com o custo de transacao e levantamento de 40,00MT.', NULL, '2022-11-06', 12, 2022, 10, 1040.00, NULL, 1040.00, 6, 1, NULL, '', '', '', '', '844396996', NULL, NULL, 7, 13, 2, NULL, 1, 5, '2022-12-06 13:13:18', '0000-00-00 00:00:00'),
(659, 'Valor entregue aos Guardas.', 1, '2022-11-06', 12, 2022, 13, 3000.00, NULL, 3000.00, 3, 10, NULL, '', '', '', '', '', NULL, 1, 4, 62, 4, NULL, 1, NULL, '2022-12-06 13:14:32', '0000-00-00 00:00:00'),
(660, 'Compra efectua pelo Caixa em servico.', 1, '2022-11-06', 12, 2022, 17, 904.65, 95.35, 1000.00, 3, 4, NULL, '', '', '', '', '', NULL, 1, 7, 15, 4, NULL, 5, 1, '2022-12-06 13:17:54', '0000-00-00 00:00:00'),
(661, 'Deslocacao do guarda em servico de FME Macuti para FME Ponta gea para entrega de medicamentos.', 1, '2022-11-06', 12, 2022, 11, 200.00, NULL, 200.00, 3, 1, NULL, '', '', '41', '', '', NULL, 1, 7, 11, 4, NULL, 1, 5, '2022-12-06 13:20:01', '0000-00-00 00:00:00'),
(662, 'Abastecimento feito pelo Rosario.', 1, '2022-11-06', 12, 2022, 12, 2880.00, NULL, 2880.00, 3, 1, NULL, '', '', '8285', '', '', NULL, 1, 100, 119, 4, NULL, 3, 1, '2022-12-06 13:24:36', '0000-00-00 00:00:00'),
(663, 'Valor enviado para o Mpesa da FME Kenneth kaunda, com o custo de transacdao de 7,50MT', 1, '2022-11-06', 12, 2022, 13, 507.50, NULL, 507.50, 6, 2, NULL, '', '', '', '', '855154553', NULL, 1, 5, 63, 2, NULL, 1, NULL, '2022-12-06 13:31:06', '0000-00-00 00:00:00'),
(664, 'valor enviado para a FME Kenneth Kaunda, com o custo de transacdao de 7,50MT', NULL, '2022-11-06', 12, 2022, 13, 307.50, NULL, 307.50, 6, 2, NULL, '', '', '', '', '855154553', NULL, NULL, 5, 65, 2, NULL, 1, NULL, '2022-12-06 13:32:47', '0000-00-00 00:00:00'),
(665, 'Valor enviado para o mpesa da FME Kenneth Kaunda com o Custo de transicao de 7.50MT', NULL, '2022-11-06', 12, 2022, 13, 507.50, NULL, 507.50, 6, 2, NULL, '', '', '', '', '8551545553', NULL, NULL, 5, 123, 2, NULL, 1, NULL, '2022-12-06 13:36:04', '0000-00-00 00:00:00'),
(666, 'Valor enviado para o mpesa de FME Kenneth Kau7ndea com o custo9 de transacao de 7.50MT', NULL, '2022-11-06', 12, 2022, 13, 507.50, NULL, 507.50, 6, 2, NULL, '', '', '', '', '855154553', NULL, NULL, 5, 153, 2, NULL, 1, NULL, '2022-12-06 13:40:56', '0000-00-00 00:00:00'),
(667, 'Valor enviado para o Milton.', NULL, '2022-11-06', 12, 2022, 13, 500.00, NULL, 500.00, 6, 3, NULL, '', '', '', '', '845533809', NULL, NULL, 5, 70, 2, NULL, 1, NULL, '2022-12-07 06:58:37', '0000-00-00 00:00:00'),
(668, 'Valor enviado para o Milton.', NULL, '2022-11-06', 12, 2022, 13, 500.00, NULL, 500.00, 6, 3, NULL, '', '', '', '', '845533809', NULL, NULL, 5, 72, 2, NULL, 1, NULL, '2022-12-07 07:03:50', '0000-00-00 00:00:00'),
(669, 'Valor enviado para o Milton.', NULL, '2022-11-06', 12, 2022, 13, 500.00, NULL, 500.00, 6, 3, NULL, '', '', '', '', '84553809', NULL, NULL, 5, 73, 2, NULL, 1, NULL, '2022-12-07 07:06:27', '0000-00-00 00:00:00'),
(670, 'Valor enviado para o Milton.', NULL, '2022-11-06', 12, 2022, 13, 500.00, NULL, 500.00, 6, 3, NULL, '', '', '', '', '845533809', NULL, NULL, 5, 74, 2, NULL, 1, NULL, '2022-12-07 07:09:28', '0000-00-00 00:00:00'),
(671, 'Valor enviado para o Milton, com o custo de transação.', NULL, '2022-11-06', 12, 2022, 13, 1010.00, NULL, 1010.00, 6, 3, NULL, '', '', '', '', '845533809', NULL, NULL, 5, 76, 2, NULL, 1, NULL, '2022-12-07 07:12:53', '0000-00-00 00:00:00'),
(672, 'Valor enviado para o Milton, custo de transação de 05,00MT.', NULL, '2022-11-06', 12, 2022, 13, 305.00, NULL, 305.00, 6, 3, NULL, '', '', '', '', '845533809', NULL, NULL, 5, 77, 2, NULL, 1, NULL, '2022-12-07 07:16:52', '0000-00-00 00:00:00'),
(673, 'Valor enviado para o Milton, com o custo de transação de 05,00MT.', NULL, '2022-11-06', 12, 2022, 13, 505.00, NULL, 505.00, 6, 3, NULL, '', '', '', '', '845533809', NULL, NULL, 5, 78, 2, NULL, 1, NULL, '2022-12-07 07:24:42', '0000-00-00 00:00:00'),
(674, 'Valor enviado para o Milton, com o custo de transação de 05,00MT', NULL, '2022-11-06', 12, 2022, 13, 505.00, NULL, 505.00, 6, 3, 5, '', '', '', NULL, '845533809', NULL, 0, 5, 170, 2, 1, 1, NULL, '2022-12-07 07:32:02', '2022-12-07 09:39:19'),
(675, 'Valor enviado para o Milton, com o custo de transação de 05,00MT.', NULL, '2022-11-06', 12, 2022, 13, 505.00, NULL, 505.00, 6, 3, NULL, '', '', '', '', '845533809', NULL, NULL, 5, 172, 2, NULL, 1, NULL, '2022-12-07 07:44:02', '0000-00-00 00:00:00'),
(676, 'Valor enviado para a Sra. Ana Bela, com custo de transação de 10,00MT.', NULL, '2022-11-06', 12, 2022, 13, 520.00, NULL, 520.00, 6, 19, 104, '', '', '', NULL, '851588285', NULL, 0, 104, 124, 2, 0, 1, NULL, '2022-12-07 07:47:28', '2022-12-07 10:17:52'),
(677, 'Valor enviado para a Sra. Ana Bela, com o custo de transação de 15,00MT. ', NULL, '2022-11-06', 12, 2022, 13, 515.00, NULL, 515.00, 6, 19, NULL, '', '', '', '', '851588285', NULL, NULL, 104, 125, 2, NULL, 1, NULL, '2022-12-07 08:08:14', '0000-00-00 00:00:00'),
(678, 'Valor enviado para o Sra Ana Bela, com o custo de transação de 15,00MT.', NULL, '2022-11-06', 12, 2022, 13, 515.00, NULL, 515.00, 6, 19, 104, '', '', '', NULL, '851588285', NULL, 0, 104, 136, 2, 0, 1, NULL, '2022-12-07 08:14:29', '2022-12-07 10:16:37'),
(679, 'Valor enviado para o Carlos.\r\n', NULL, '2022-11-07', 12, 2022, 4, 1000.00, NULL, 1000.00, 6, 1, NULL, '', '', '42', '', '843910518', NULL, NULL, 3, 137, 2, NULL, 1, 5, '2022-12-07 08:30:25', '0000-00-00 00:00:00'),
(680, 'Compra efectuada pelo Rosario.', 1, '2022-11-07', 12, 2022, 17, 1809.30, 190.70, 2000.00, 3, 4, NULL, '', '', '', '', '', NULL, 1, 7, 15, 4, NULL, 1, 1, '2022-12-07 08:36:10', '0000-00-00 00:00:00'),
(681, 'Valor entregue ao Oscar.', 1, '2022-11-07', 12, 2022, 13, 650.00, NULL, 650.00, 3, 1, NULL, '', '', '43', '', '', NULL, 1, 108, 128, 4, NULL, 1, 5, '2022-12-07 08:37:32', '0000-00-00 00:00:00'),
(682, 'Valor enviado para Manthima.\r\n', NULL, '2022-11-07', 12, 2022, 17, 500.00, NULL, 500.00, 6, 2, NULL, '', '', '44', '', '850557874', NULL, NULL, 7, 15, 2, NULL, 1, 5, '2022-12-07 08:40:55', '0000-00-00 00:00:00'),
(683, 'Compra efetuada pelo Rosario.', 1, '2022-11-07', 12, 2022, 17, 1817.90, 182.10, 2000.00, 3, 2, NULL, '', '', '', '', '', NULL, 1, 7, 15, 4, NULL, 5, 1, '2022-12-07 08:43:24', '0000-00-00 00:00:00'),
(684, 'Valor enviado para Lúcia.\r\n', NULL, '2022-11-07', 12, 2022, 22, 310.00, NULL, 310.00, 6, 17, NULL, '', '', '45', '', '848383477', NULL, NULL, 102, 145, 2, NULL, 1, 5, '2022-12-07 08:54:41', '0000-00-00 00:00:00'),
(685, 'Compra eefctuada pelo Rosario.', 1, '2022-11-07', 12, 2022, 17, 2731.71, 268.29, 3000.00, 3, 10, NULL, '', '', '', '', '', NULL, 1, 7, 15, 4, NULL, 5, 1, '2022-12-07 08:56:44', '0000-00-00 00:00:00'),
(686, 'Valor enviado para o Sr. Bernardo, com o custo de transação e levantamento de 60,00MT.', NULL, '2022-11-07', 12, 2022, 13, 3060.00, NULL, 3060.00, 6, 1, NULL, '', '', '45', '', '848767881', NULL, NULL, 112, 135, 2, NULL, 1, 5, '2022-12-07 08:59:04', '0000-00-00 00:00:00'),
(687, 'Deslocação de Rosário para Ímpar Seguradora, deixar documentos.', 1, '2022-11-07', 12, 2022, 13, 240.00, NULL, 240.00, 3, 1, NULL, '', '', '46', '', '', NULL, 1, 7, 11, 4, NULL, 1, 5, '2022-12-07 09:02:07', '0000-00-00 00:00:00'),
(688, 'Valor enviado para o Sr. Manuel, com o custo de transação e levantamento de 60,00MT.', NULL, '2022-11-08', 12, 2022, 13, 3560.00, NULL, 3560.00, 6, 2, NULL, '', '', '', '', '841158201', NULL, NULL, 4, 62, 2, NULL, 1, NULL, '2022-12-07 09:31:13', '0000-00-00 00:00:00'),
(689, 'Envio de documentos de Livrança e Facturas Seguradora de Beira para Maputo através de Voo. A despesa não apresenta VD.', 1, '2022-11-08', 12, 2022, 15, 700.00, NULL, 700.00, 3, 1, NULL, '', '', '46', '', '', NULL, 1, 7, 12, 4, NULL, 1, 5, '2022-12-07 09:35:27', '0000-00-00 00:00:00'),
(690, 'Deslocação de Rosário para o Aeroporto, para envio de Documentos.', 1, '2022-11-07', 12, 2022, 13, 300.00, NULL, 300.00, 3, 1, NULL, '', '', '47', '', '', NULL, 1, 7, 11, 4, NULL, 1, 5, '2022-12-07 09:37:36', '0000-00-00 00:00:00'),
(691, 'Compra de 27 Rolos de POS e 10 Rolos de Sistema. Valor enviado para o Miiton, com o custo de transação e levantamento de 60,00MT.', NULL, '2022-11-08', 12, 2022, 20, 2760.00, NULL, 2760.00, 6, 2, NULL, '', '', '004082', '', '845533809', NULL, NULL, 106, 171, 2, NULL, 1, 1, '2022-12-07 09:41:01', '0000-00-00 00:00:00'),
(692, 'Deslocação de Monica para o Banco.', NULL, '2022-11-08', 12, 2022, 11, 220.00, NULL, 220.00, 6, 1, 7, '', '', '47', NULL, '876765939', NULL, 0, 7, 11, 4, 0, 1, 5, '2022-12-07 09:43:02', '2022-12-07 12:08:12'),
(693, 'Deslocação para o Aeroporto, para envio de Documentos', NULL, '2022-11-08', 12, 2022, 11, 300.00, NULL, 300.00, 3, 1, 7, '', '', '47', NULL, '876765939', NULL, 1, 7, 11, 2, 0, 1, 5, '2022-12-07 09:43:02', '2022-12-07 12:09:41'),
(694, 'Deslocação do eletricista Tomas para manutenção de lâmpadas. ', NULL, '2022-11-08', 12, 2022, 11, 420.00, NULL, 420.00, 6, 1, NULL, '', '', '48', '', '846476730', NULL, NULL, 7, 11, 4, NULL, 1, 5, '2022-12-07 09:46:31', '0000-00-00 00:00:00'),
(695, 'Valor entregue ao Oscar.', 1, '2022-11-08', 12, 2022, 11, 650.00, NULL, 650.00, 3, 1, NULL, '', '', '49', '', '', NULL, 1, 108, 128, 4, NULL, 1, 5, '2022-12-07 09:50:04', '0000-00-00 00:00:00'),
(696, 'Abastecimento efectuado pelo Rosario.', 1, '2022-11-08', 12, 2022, 12, 3000.00, NULL, 3000.00, 3, 1, NULL, '', '', '8255', '', '', NULL, 1, 100, 119, 4, NULL, 3, 1, '2022-12-07 09:55:13', '0000-00-00 00:00:00'),
(697, 'Deslocação de Rosário para Importadora EquiFarma, para levar recibos de pagamentos.', 1, '2022-11-08', 12, 2022, 11, 450.00, NULL, 450.00, 3, 1, NULL, '', '', '49', '', '', NULL, 1, 7, 11, 4, NULL, 1, 5, '2022-12-07 09:57:45', '0000-00-00 00:00:00'),
(698, 'Compra efectuada pelo Rosário.', 1, '2022-11-09', 12, 2022, 17, 2731.71, 268.29, 3000.00, 3, 10, NULL, '', '', '', '', '', NULL, 1, 7, 15, 4, NULL, 5, 2, '2022-12-08 06:40:13', '0000-00-00 00:00:00'),
(699, 'Requisição feita pelo Rosário.', 1, '2022-11-09', 12, 2022, 24, 12006.52, 2459.16, 14465.68, 3, 10, NULL, '', '', '1853', '', '', NULL, 1, 105, 157, 4, NULL, 1, 1, '2022-12-08 06:44:00', '0000-00-00 00:00:00'),
(700, 'Valor entregue ao Oscar.', 1, '2022-11-09', 12, 2022, 13, 650.00, NULL, 650.00, 3, 1, NULL, '', '', '', '', '', NULL, 1, 108, 128, 4, NULL, 1, NULL, '2022-12-08 06:46:25', '0000-00-00 00:00:00'),
(701, 'Deslocação de Abiba para o Recheio.', NULL, '2022-11-09', 12, 2022, 13, 520.00, NULL, 520.00, 6, 1, NULL, '', '', '51', '', '852890120', NULL, NULL, 7, 11, 2, NULL, 1, 5, '2022-12-08 06:54:41', '0000-00-00 00:00:00'),
(702, 'Pagamento de Manutenção Geral e uma Carga de Gás. Valor entregue ao Mestre.', 1, '2022-11-09', 12, 2022, 15, 1750.00, NULL, 1750.00, 3, 4, NULL, '', '', '52', '', '', NULL, 1, 2, 30, 4, NULL, 1, 5, '2022-12-08 07:01:30', '0000-00-00 00:00:00'),
(703, 'Pagamento de Manutenção Geral e uma Carga de Gás. Valor entregue ao Mestre.', 1, '2022-11-09', 12, 2022, 15, 1750.00, NULL, 1750.00, 3, 4, NULL, '', '', '52', '', '', NULL, 1, 2, 32, 4, NULL, 1, 5, '2022-12-08 07:55:05', '0000-00-00 00:00:00'),
(704, 'Valor enviado para o Sr. Albino com o custo de transação e levantamento de 50,00MT.', NULL, '2022-11-07', 12, 2022, 12, 2050.00, NULL, 2050.00, 6, 1, NULL, '', '', '0133264', '', '845629961', NULL, NULL, 100, 115, NULL, NULL, 3, 1, '2022-12-08 08:00:22', '0000-00-00 00:00:00'),
(705, 'Valor enviado para o Milton, para compra de Embalagem de Envelope A4, Envelope A3 e Guia de Remessa.', NULL, '2022-11-07', 12, 2022, 20, 1700.00, NULL, 1700.00, 6, 3, 106, '', '', '004078', NULL, '845533809', NULL, 0, 106, 171, 4, 0, 1, 1, '2022-12-08 08:05:11', '2022-12-08 10:07:18'),
(706, 'Pagamento efectuado pela Florinda', 1, '2022-11-10', 12, 2022, 27, 2905.98, 494.02, 3400.00, 3, 10, NULL, '', '', '606036', '', '', NULL, 1, 7, 9, 4, NULL, 1, 1, '2022-12-08 08:10:40', '0000-00-00 00:00:00'),
(707, 'Valor entregue ao guarda em serviço, ', 1, '2022-11-10', 12, 2022, 13, 800.00, NULL, 800.00, 3, 15, NULL, '', '', '', '', '', NULL, 1, 5, 97, 4, NULL, 1, NULL, '2022-12-08 08:14:26', '0000-00-00 00:00:00'),
(708, 'Valor entregue ao guarda em serviço', 1, '2022-11-09', 12, 2022, 13, 2000.00, NULL, 2000.00, 3, 4, NULL, '', '', '', '', '', NULL, 1, 4, 62, 4, NULL, 1, NULL, '2022-12-08 08:18:34', '0000-00-00 00:00:00'),
(709, 'Valor enviado para o Mpesa da FME Kenneth K. Com o custo de transação de 10,00MT.', NULL, '2022-11-10', 12, 2022, 13, 510.00, NULL, 510.00, 6, 2, NULL, '', '', '', '', '855154553', NULL, NULL, 5, 63, 2, NULL, 1, NULL, '2022-12-08 08:50:42', '0000-00-00 00:00:00'),
(710, 'Valor enviado para mpsa da farmacia com o custo de transacção 10,00MT', NULL, '2022-11-10', 12, 2022, 13, 510.00, NULL, 510.00, 6, 2, NULL, '', '', '', '', '855154553', NULL, NULL, 5, 123, 2, NULL, 1, NULL, '2022-12-08 08:59:17', '0000-00-00 00:00:00'),
(711, 'Valor transferido para o Mpsa da farmácia com o custo da transação 10,00MT ', NULL, '2022-11-10', 12, 2022, 13, 510.00, NULL, 510.00, 6, 2, NULL, '', '', '', '', '867816502', NULL, NULL, 5, 153, 2, NULL, 1, NULL, '2022-12-08 09:07:36', '0000-00-00 00:00:00'),
(712, 'Valor enviado para mpsa da farmácia com o custo de transação de 10,00MT', NULL, '2022-11-10', 12, 2022, 13, 310.00, NULL, 310.00, 6, 2, 5, '', '', '', NULL, '855154553', NULL, 0, 5, 65, 2, 1, 1, NULL, '2022-12-08 09:13:16', '2022-12-12 14:36:02'),
(713, 'Valor enviado para o Milton com o Custo da transação 100,00mt e o custo de levantamento no valor de 50,00mt', NULL, '2022-11-11', 12, 2022, 27, 5837.90, 992.10, 6830.00, 6, 19, NULL, '', '', '344789', '', '845533809', NULL, NULL, 7, 9, 2, NULL, 1, 2, '2022-12-08 09:28:25', '0000-00-00 00:00:00'),
(714, 'Compra efetuada pelo Rosário ', 1, '2022-11-10', 12, 2022, 17, 904.65, 95.35, 1000.00, 3, 2, NULL, '', '', '', '', '', NULL, 1, 7, 15, 4, NULL, 5, 1, '2022-12-08 09:34:36', '0000-00-00 00:00:00'),
(715, 'Compra efetuado pelo João Marroa  ', 1, '2022-11-10', 12, 2022, 17, 904.65, 95.35, 1000.00, 3, 15, NULL, '', '', '', '', '', NULL, 1, 7, 15, 4, NULL, 5, 1, '2022-12-08 09:40:39', '0000-00-00 00:00:00'),
(716, 'Compra efetuada pelo Rosário ', 1, '2022-11-10', 12, 2022, 17, 2731.71, 268.29, 3000.00, 3, 10, NULL, '', '', '', '', '', NULL, 1, 7, 15, 4, NULL, 5, 1, '2022-12-08 09:44:12', '0000-00-00 00:00:00'),
(717, 'Valor entregue ao txopelista ', 1, '2022-11-10', 12, 2022, 11, 650.00, NULL, 650.00, 3, 1, NULL, '', '', '', '', '', NULL, 1, 108, 128, 4, NULL, 1, NULL, '2022-12-08 09:46:56', '0000-00-00 00:00:00'),
(718, 'Compra efetuada pelo Armando ', 1, '2022-11-11', 12, 2022, 17, 1809.30, 190.70, 2000.00, 3, 4, NULL, '', '', '', '', '', NULL, 1, 7, 15, 4, NULL, 5, 1, '2022-12-08 09:50:26', '0000-00-00 00:00:00'),
(719, 'Compra efetuada pelo Rosário ', 1, '2022-11-11', 12, 2022, 17, 1812.65, 187.35, 2000.00, 3, 19, NULL, '', '', '', '', '', NULL, 1, 7, 15, 4, NULL, 5, 1, '2022-12-08 09:53:33', '0000-00-00 00:00:00'),
(720, 'Valor enviado para Lucia Com o custo da transação de 10,00MT ', NULL, '2022-11-11', 12, 2022, 13, 510.00, NULL, 510.00, 6, 3, NULL, '', '', '', '', '848383477', NULL, NULL, 5, 74, 2, NULL, 1, NULL, '2022-12-08 09:59:37', '0000-00-00 00:00:00'),
(721, 'Valor enviado para Lúcia com o custo da transação de 10,00MT', NULL, '2022-11-11', 12, 2022, 13, 510.00, NULL, 510.00, 6, 3, NULL, '', '', '', '', '848383477', NULL, NULL, 5, 73, 2, NULL, 1, NULL, '2022-12-08 10:03:53', '0000-00-00 00:00:00'),
(722, 'Valor enviado para Lúcia com o custo da transação de 10,00MT', NULL, '2022-11-11', 12, 2022, 13, 510.00, NULL, 510.00, 6, 3, NULL, '', '', '', '', '848383477', NULL, NULL, 5, 72, 2, NULL, 1, NULL, '2022-12-08 10:09:54', '0000-00-00 00:00:00'),
(723, 'Valor enviado para Lúcia com o custo da transação de 10,00MT', NULL, '2022-11-11', 12, 2022, 13, 310.00, NULL, 310.00, 6, 3, NULL, '', '', '', '', '848383477', NULL, NULL, 5, 77, 2, NULL, 1, NULL, '2022-12-08 10:15:15', '0000-00-00 00:00:00'),
(724, 'Valor enviado para Lúcia ', NULL, '2022-11-11', 12, 2022, 13, 1000.00, NULL, 1000.00, 6, 3, NULL, '', '', '', '', '848383477', NULL, NULL, 5, 76, 2, NULL, 1, NULL, '2022-12-08 10:17:50', '0000-00-00 00:00:00'),
(725, 'Valor enviado para Lúcia ', NULL, '2022-11-11', 12, 2022, 13, 500.00, NULL, 500.00, 6, 3, NULL, '', '', '', '', '848383477', NULL, NULL, 5, 70, 2, NULL, 1, NULL, '2022-12-08 10:20:40', '0000-00-00 00:00:00'),
(726, 'Valor enviado para Lúcia ', NULL, '2022-11-11', 12, 2022, 13, 500.00, NULL, 500.00, 6, 3, NULL, '', '', '', '', '848383477', NULL, NULL, 5, 170, 2, NULL, 1, NULL, '2022-12-08 10:23:52', '0000-00-00 00:00:00'),
(727, 'Valor enviado para Lúcia ', NULL, '2022-11-11', 12, 2022, 13, 500.00, NULL, 500.00, 6, 3, NULL, '', '', '', '', '848383477', NULL, NULL, 5, 78, 2, NULL, 1, NULL, '2022-12-08 10:29:26', '0000-00-00 00:00:00'),
(728, 'Valor enviado para Lúcia ', NULL, '2022-11-11', 12, 2022, 13, 500.00, NULL, 500.00, 6, 3, NULL, '', '', '', '', '848383477', NULL, NULL, 5, 172, 2, NULL, 1, NULL, '2022-12-08 10:31:41', '0000-00-00 00:00:00'),
(729, 'Valor enviado para o Fenias, foi fazer devolução na Importadora ', NULL, '2022-11-11', 12, 2022, 13, 200.00, NULL, 200.00, 6, 1, NULL, '', '', '53', '', '846852356', NULL, NULL, 7, 11, 4, NULL, 1, 5, '2022-12-08 10:38:09', '0000-00-00 00:00:00'),
(730, 'Envio de Material elétrico da Beira para Maputo, envio feito pelo Rosário. ', 1, '2022-11-12', 12, 2022, 10, 4500.00, NULL, 4500.00, 3, 1, NULL, '', '', '08583', '', '', NULL, 1, 7, 13, 4, NULL, 1, 1, '2022-12-08 10:44:01', '0000-00-00 00:00:00'),
(731, 'Deslocação do eletricista Tomás, foi fazer reparação de Luzes na casa do Sr. Marco.', NULL, '2022-11-12', 12, 2022, 13, 610.00, NULL, 610.00, 6, 1, NULL, '', '', '', '', '846476730', NULL, NULL, 7, 11, 4, NULL, 1, NULL, '2022-12-08 10:52:07', '0000-00-00 00:00:00'),
(732, 'Valor enviado para o mpsa da F.M.E Kenneth Kaunda, com o custo da transação de 40,00MT', NULL, '2022-11-12', 12, 2022, 60, 2965.00, NULL, 2965.00, 6, 2, NULL, '', '', '078643', '', '855154553', NULL, NULL, 105, 157, 2, NULL, 1, 1, '2022-12-08 11:34:42', '0000-00-00 00:00:00'),
(733, 'Valor entregue ao txopelista ', 1, '2022-11-12', 12, 2022, 13, 650.00, NULL, 650.00, 3, 1, NULL, '', '', '', '', '', NULL, 1, 108, 128, 4, NULL, 1, 5, '2022-12-08 12:01:42', '0000-00-00 00:00:00'),
(734, 'Valor entregue ao staff em serviço ', 1, '2022-11-13', 12, 2022, 61, 500.00, NULL, 500.00, 3, 10, NULL, '', '', '000812', '', '', NULL, 1, 111, 156, 4, NULL, 1, 1, '2022-12-08 12:13:19', '0000-00-00 00:00:00'),
(735, 'Valor entregue ao staff em serviço ', 1, '2022-11-13', 12, 2022, 49, 500.00, NULL, 500.00, 3, 4, NULL, '', '', '000086', '', '', NULL, 1, 111, 156, 4, NULL, 1, 1, '2022-12-08 12:15:52', '0000-00-00 00:00:00'),
(736, 'Compra de plástico no mercado informal, compra efetuada pelo Sr. Mariano.', NULL, '2022-11-13', 12, 2022, 15, 1530.00, NULL, 1530.00, 6, 3, NULL, '', '', '55', '', '847876424', NULL, NULL, 99, 99, 2, NULL, 1, 5, '2022-12-08 12:22:21', '0000-00-00 00:00:00'),
(737, 'Transporte Rosário foi para aeroporto enviar documentos para Maputo.', 1, '2022-11-13', 12, 2022, 13, 500.00, NULL, 500.00, 3, 1, NULL, '', '', '55', '', '', NULL, 1, 7, 11, 4, NULL, 1, 5, '2022-12-08 12:25:34', '0000-00-00 00:00:00'),
(738, 'Envio de Medicamento para Maputo de Voo, medicamento enviado pelo Rosário. ', 1, '2022-11-13', 12, 2022, 13, 800.00, NULL, 800.00, 3, 1, NULL, '', '', '56', '', '', NULL, 1, 7, 13, 4, NULL, NULL, 5, '2022-12-08 12:29:32', '0000-00-00 00:00:00'),
(739, 'Valor enviado para o Manuel com o custo da transação de 10,00MT', NULL, '2022-11-13', 12, 2022, 13, 210.00, NULL, 210.00, 6, 2, NULL, '', '', '', '', '841158201', NULL, NULL, 5, 64, 2, NULL, 1, NULL, '2022-12-08 12:34:04', '0000-00-00 00:00:00'),
(740, 'Valor enviado para o Manuel, com o custo da transação de 10,00MT ', NULL, '2022-11-13', 12, 2022, 13, 210.00, NULL, 210.00, 6, 2, NULL, '', '', '', '', '841158201', NULL, NULL, 5, 66, 2, NULL, 1, NULL, '2022-12-08 12:38:03', '0000-00-00 00:00:00'),
(741, 'Valor transferido para o Sr. Manuel, com o custo de transação de 10,00MT.', NULL, '2022-11-13', 12, 2022, 13, 210.00, NULL, 210.00, 6, 2, NULL, '', '', '', '', '841158201', NULL, NULL, 5, 67, 2, NULL, 1, NULL, '2022-12-08 12:48:56', '0000-00-00 00:00:00'),
(742, 'Valor enviado para o Sr. Manuel, com o custo de transação de 10,00MT', NULL, '2022-11-13', 12, 2022, 13, 210.00, NULL, 210.00, 6, 2, NULL, '', '', '', '', '841158201', NULL, NULL, 5, 68, 2, NULL, 1, NULL, '2022-12-08 12:50:57', '0000-00-00 00:00:00'),
(743, 'Valor entregue a Dolca.', 1, '2022-11-14', 12, 2022, 12, 2700.00, NULL, 2700.00, 3, 1, NULL, '', '', '096820', '', '', NULL, 1, 100, 139, 4, NULL, 3, 1, '2022-12-08 12:53:24', '0000-00-00 00:00:00'),
(744, 'Valor entregue ao guarda em Serviço. ', 1, '2022-11-14', 12, 2022, 12, 1700.00, NULL, 1700.00, 3, 4, NULL, '', '', '096802', '', '', NULL, 1, 8, 146, 4, NULL, 3, 1, '2022-12-08 12:56:19', '0000-00-00 00:00:00'),
(745, 'Valor entregue ao Rosario.', 1, '2022-11-14', 12, 2022, 13, 800.00, NULL, 800.00, 3, 10, NULL, '', '', '', '', '', NULL, 1, 5, 86, 4, NULL, 1, NULL, '2022-12-08 13:16:54', '0000-00-00 00:00:00'),
(746, 'Valor entregue ao Oscar.', 1, '2022-11-13', 12, 2022, 13, 400.00, NULL, 400.00, 3, 10, NULL, '', '', '', '', '', NULL, 1, 5, 84, 4, NULL, 1, NULL, '2022-12-08 13:23:54', '0000-00-00 00:00:00'),
(747, 'Valor entregue a Fatima.', 1, '2022-11-13', 12, 2022, 13, 400.00, NULL, 400.00, 3, 10, NULL, '', '', '', '', '', NULL, 1, 5, 87, 4, NULL, 1, NULL, '2022-12-08 13:25:22', '0000-00-00 00:00:00'),
(748, 'Valor entregue a Miquelina.', 1, '2022-11-13', 12, 2022, 13, 400.00, NULL, 400.00, 3, 10, NULL, '', '', '', '', '', NULL, 1, 5, 92, 4, NULL, 1, NULL, '2022-12-08 13:26:38', '0000-00-00 00:00:00'),
(749, 'Valor entregue a Jacinta.', 1, '2022-11-13', 12, 2022, 13, 400.00, NULL, 400.00, 3, 10, NULL, '', '', '', '', '', NULL, 1, 5, 91, 4, NULL, 1, NULL, '2022-12-08 13:27:36', '0000-00-00 00:00:00'),
(750, 'Valor entregue ao Francisco.', 1, '2022-11-13', 12, 2022, 13, 500.00, NULL, 500.00, 3, 10, NULL, '', '', '', '', '', NULL, 1, 5, 88, 4, NULL, 1, NULL, '2022-12-08 13:28:39', '0000-00-00 00:00:00'),
(751, 'Valor entregue ao Latifo.', 1, '2022-11-13', 12, 2022, 13, 400.00, NULL, 400.00, 3, 10, NULL, '', '', '', '', '', NULL, 1, 5, 89, 4, NULL, 1, NULL, '2022-12-08 13:29:38', '0000-00-00 00:00:00'),
(752, 'Valor entregue ao Agostinho.', 1, '2022-11-13', 12, 2022, 13, 100.00, NULL, 100.00, 3, 10, NULL, '', '', '', '', '', NULL, 1, 5, 90, 4, NULL, 1, NULL, '2022-12-08 13:30:46', '0000-00-00 00:00:00'),
(753, 'Valor enviuado para o Mpesa da FME Kenneth kaunda, com o custo de transação de 10,00MT.', 1, '2022-11-13', 12, 2022, 13, 510.00, NULL, 510.00, 6, 2, 5, '', '', '', NULL, '855154553', NULL, 1, 5, 123, 2, 1, 1, NULL, '2022-12-08 13:33:42', '2022-12-12 14:17:57'),
(754, 'Valor entregue ao Oscar.', 1, '2022-11-04', 12, 2022, 13, 400.00, NULL, 400.00, 3, 10, NULL, '', '', '', '', '', NULL, 1, 5, 84, 4, NULL, 1, NULL, '2022-12-08 13:37:51', '0000-00-00 00:00:00'),
(755, 'Valor enviado para o Mpesa da FME Kenneth Kaunda, com o custo de transação de 10,00mt.', 1, '2022-10-28', 12, 2022, 13, 510.00, NULL, 510.00, 6, 2, 5, '', '', '', NULL, '851514553', NULL, 1, 5, 153, 2, 1, 1, NULL, '2022-12-08 13:38:41', '2022-12-12 14:20:39'),
(756, 'Valor entregue a Fatima.', 1, '2022-11-04', 12, 2022, 13, 400.00, NULL, 400.00, 3, 10, NULL, '', '', '', '', '', NULL, 1, 5, 87, 4, NULL, 1, NULL, '2022-12-08 13:42:42', '0000-00-00 00:00:00'),
(757, 'Valor entregue ao Francisco.', 1, '2022-11-04', 12, 2022, 13, 500.00, NULL, 500.00, 3, 10, NULL, '', '', '', '', '', NULL, 1, 5, 88, 4, NULL, 1, NULL, '2022-12-08 13:43:46', '0000-00-00 00:00:00'),
(758, 'Valor entregue ao Latifo.', 1, '2022-11-04', 12, 2022, 13, 400.00, NULL, 400.00, 3, 10, NULL, '', '', '', '', '', NULL, 1, 5, 89, 4, NULL, 1, NULL, '2022-12-08 13:44:44', '0000-00-00 00:00:00'),
(759, 'Valor entregue ao Agostinho.', 1, '2022-11-04', 12, 2022, 13, 100.00, NULL, 100.00, 3, 10, NULL, '', '', '', '', '', NULL, 1, 5, 90, 4, NULL, 1, NULL, '2022-12-08 13:45:49', '0000-00-00 00:00:00'),
(760, 'Valor entregue ao Armando.', 1, '2022-11-04', 12, 2022, 13, 400.00, NULL, 400.00, 3, 4, 5, '', '', '', NULL, '', NULL, 1, 5, 80, 4, 1, 1, NULL, '2022-12-08 13:46:45', '2022-12-08 15:52:11'),
(761, 'Valor entregue a Jacinta.', 1, '2022-11-04', 12, 2022, 13, 400.00, NULL, 400.00, 3, 10, NULL, '', '', '', '', '', NULL, 1, 5, 91, 4, NULL, 1, NULL, '2022-12-08 13:48:01', '0000-00-00 00:00:00'),
(762, 'Valor entregue a Miquelina.', 1, '2022-11-04', 12, 2022, 13, 400.00, NULL, 400.00, 3, 10, NULL, '', '', '', '', '', NULL, 1, 5, 92, 4, NULL, 1, NULL, '2022-12-08 13:48:55', '0000-00-00 00:00:00'),
(763, 'Valor entregue a Leticia.', 1, '2022-11-04', 12, 2022, 13, 400.00, NULL, 400.00, 3, 4, NULL, '', '', '', '', '', NULL, 1, 5, 81, 4, NULL, 1, NULL, '2022-12-08 13:59:28', '0000-00-00 00:00:00'),
(764, 'Valor entregue a Manuela.', 1, '2022-11-04', 12, 2022, 13, 100.00, NULL, 100.00, 3, 4, NULL, '', '', '', '', '', NULL, 1, 5, 82, 4, NULL, 1, NULL, '2022-12-08 14:00:50', '0000-00-00 00:00:00'),
(765, 'Valor entregue ao Osvaldo.', 1, '2022-11-04', 12, 2022, 13, 600.00, NULL, 600.00, 3, 4, NULL, '', '', '', '', '', NULL, 1, 5, 83, 4, NULL, 1, NULL, '2022-12-08 14:02:27', '0000-00-00 00:00:00'),
(766, 'Valor entregue ao Silvestre.', 1, '2022-11-04', 12, 2022, 13, 400.00, NULL, 400.00, 3, 4, 5, '', '', '', NULL, '', NULL, 1, 5, 85, 4, 1, 1, NULL, '2022-12-08 14:03:32', '2022-12-12 14:26:34'),
(767, 'Valor entregue ao Sr. Manuel.', NULL, '2022-11-04', 12, 2022, 13, 210.00, NULL, 210.00, 6, 2, NULL, '', '', '', '', '841158201', NULL, NULL, 5, 64, 2, NULL, 1, NULL, '2022-12-08 14:07:01', '0000-00-00 00:00:00'),
(768, 'Valor entregue ao Sr. Manuel.', NULL, '2022-11-04', 12, 2022, 13, 200.00, NULL, 200.00, 6, 2, NULL, '', '', '', '', '841158201', NULL, NULL, 5, 66, 2, NULL, 1, NULL, '2022-12-08 14:08:11', '0000-00-00 00:00:00'),
(769, 'Valor entregue ao Sr. Manuel.', NULL, '2022-11-04', 12, 2022, 13, 200.00, NULL, 200.00, 6, 2, NULL, '', '', '', '', '841158201', NULL, NULL, 5, 67, 2, NULL, 1, NULL, '2022-12-08 14:09:22', '0000-00-00 00:00:00'),
(770, 'Valor entregue ao Sr. Manuel.', NULL, '2022-11-04', 12, 2022, 13, 200.00, NULL, 200.00, 6, 2, NULL, '', '', '', '', '841158201', NULL, NULL, 5, 68, 2, NULL, 1, NULL, '2022-12-08 14:10:54', '0000-00-00 00:00:00'),
(771, 'Valor entregue ao Armando.', 1, '2022-11-13', 12, 2022, 13, 400.00, NULL, 400.00, 3, 4, NULL, '', '', '', '', '', NULL, 1, 5, 80, 4, NULL, 1, NULL, '2022-12-08 14:13:07', '0000-00-00 00:00:00'),
(772, 'Valor entregue a Leticia.', 1, '2022-11-13', 12, 2022, 13, 400.00, NULL, 400.00, 3, 4, NULL, '', '', '', '', '', NULL, 1, 5, 81, 4, NULL, 1, NULL, '2022-12-08 14:14:04', '0000-00-00 00:00:00'),
(773, 'Valor entrigue ao Osvaldo.', 1, '2022-11-13', 12, 2022, 13, 600.00, NULL, 600.00, 3, 4, NULL, '', '', '', '', '', NULL, 1, 5, 83, 4, NULL, 1, NULL, '2022-12-08 17:25:02', '0000-00-00 00:00:00'),
(774, 'Valor entregue ao Silvestre.', 1, '2022-11-13', 12, 2022, 13, 400.00, NULL, 400.00, 3, 4, NULL, '', '', '', '', '', NULL, 1, 5, 85, 4, NULL, 1, NULL, '2022-12-08 17:38:06', '0000-00-00 00:00:00'),
(775, 'Valor entregue a Manuela.', 1, '2022-11-13', 12, 2022, 13, 100.00, NULL, 100.00, 3, 4, NULL, '', '', '', '', '', NULL, 1, 5, 82, 4, NULL, 1, NULL, '2022-12-08 17:41:02', '0000-00-00 00:00:00'),
(776, 'Valor entregue ao Txopelista ', 1, '2022-11-14', 12, 2022, 13, 650.00, NULL, 650.00, 3, 1, NULL, '', '', '57', '', '', NULL, 1, 108, 128, 4, NULL, 1, 5, '2022-12-08 18:12:33', '0000-00-00 00:00:00'),
(777, 'Compra efetuada pelo Milton.', NULL, '2022-11-14', 12, 2022, 21, 3750.00, NULL, 3750.00, 6, 3, NULL, '', '', '002087', '', '845533809', NULL, NULL, 9, 147, 2, NULL, 1, 1, '2022-12-08 18:24:59', '0000-00-00 00:00:00'),
(778, 'Compra efectuada pelo Rosario.', 1, '2022-11-14', 12, 2022, 17, 904.65, 95.35, 1000.00, 3, 2, NULL, '', '', '', '', '', NULL, 1, 7, 15, 4, NULL, 5, 1, '2022-12-08 18:38:14', '0000-00-00 00:00:00'),
(779, 'Compra efetuada pelo Rosario.', 1, '2022-11-14', 12, 2022, 13, 1812.65, 187.35, 2000.00, 3, 17, NULL, '', '', '', '', '', NULL, 1, 7, 15, 4, NULL, 5, 1, '2022-12-08 18:52:22', '0000-00-00 00:00:00'),
(780, 'Trasporte Rosario foi para barceltecnica buscar material electrico. ', 1, '2022-11-14', 12, 2022, 13, 500.00, NULL, 500.00, 3, 1, NULL, '', '', '57', '', '', NULL, 1, 7, 11, 4, NULL, 1, 5, '2022-12-08 19:06:49', '0000-00-00 00:00:00'),
(781, 'Transporte Fenias foi para importadoras fazer devolucao de medicamento.   ', NULL, '2022-11-14', 12, 2022, 13, 210.00, NULL, 210.00, 6, 1, NULL, '', '', '58', '', '846852356', NULL, NULL, 7, 11, 4, NULL, 1, 5, '2022-12-08 20:46:54', '0000-00-00 00:00:00'),
(782, 'Compra efetuada pelo Rosario.', 1, '2022-11-14', 12, 2022, 5, 854.70, 145.30, 1000.00, 3, 1, NULL, '', '', '20221114', '', '', NULL, 1, 3, 54, 4, NULL, 1, 1, '2022-12-08 20:53:32', '0000-00-00 00:00:00'),
(783, 'Compra efetuada com o Guarda em serviço.', 1, '2022-11-14', 12, 2022, 12, 1900.00, NULL, 1900.00, 3, 10, NULL, '', '', '8286', '', '', NULL, 1, 8, 146, 4, NULL, 3, 1, '2022-12-08 21:03:15', '0000-00-00 00:00:00'),
(784, 'Pagamento efetuado pelo Rosario.', 1, '2022-11-15', 12, 2022, 62, 6500.00, NULL, 6500.00, 3, 10, NULL, '', '', '001017', '', '', NULL, 1, 7, 8, 4, NULL, 1, 1, '2022-12-08 21:10:47', '0000-00-00 00:00:00'),
(785, 'Envio de medicamento para Maputo de voo, envio feito pelo Rosario.', 1, '2022-11-15', 12, 2022, 13, 600.00, NULL, 600.00, 3, 1, NULL, '', '', '58', '', '', NULL, 1, 7, 13, 4, NULL, 1, 5, '2022-12-08 21:16:13', '0000-00-00 00:00:00'),
(786, 'Valor entregue ao txopelista.', 1, '2022-11-15', 12, 2022, 13, 650.00, NULL, 650.00, 3, 1, NULL, '', '', '59', '', '', NULL, 1, 108, 128, 4, NULL, 1, 5, '2022-12-08 21:18:55', '0000-00-00 00:00:00'),
(787, 'Transporte Rosario foi para aeroporto enviar Medicamento para Maputo.', 1, '2022-11-15', 12, 2022, 13, 500.00, NULL, 500.00, 3, 1, NULL, '', '', '60', '', '', NULL, 1, 7, 11, 4, NULL, 1, 5, '2022-12-08 21:22:13', '0000-00-00 00:00:00'),
(788, 'Compra efetuada pelo Rosario.', 1, '2022-11-15', 12, 2022, 4, 1000.00, NULL, 1000.00, 3, 17, NULL, '', '', '61', '', '', NULL, 1, 3, 56, 4, NULL, 1, 5, '2022-12-08 21:28:35', '0000-00-00 00:00:00'),
(789, 'Compra efetuada pelo Rosario.', 1, '2022-11-15', 12, 2022, 17, 2731.71, 268.29, 3000.00, 3, 10, NULL, '', '', '', '', '', NULL, 1, 7, 15, 4, NULL, 5, 1, '2022-12-08 21:33:01', '0000-00-00 00:00:00'),
(790, 'Compra efetuada pelo Patricio.', 1, '2022-11-15', 12, 2022, 17, 1809.30, 190.70, 2000.00, 3, 4, NULL, '', '', '', '', '', NULL, 1, 7, 15, 4, NULL, 5, 1, '2022-12-08 21:36:30', '0000-00-00 00:00:00'),
(791, 'Valor enviado para Taverna.', NULL, '2022-11-15', 12, 2022, 13, 680.00, NULL, 680.00, 6, 17, NULL, '', '', '62', '', '850685164', NULL, NULL, 102, 145, NULL, NULL, 1, 5, '2022-12-08 21:42:39', '0000-00-00 00:00:00'),
(792, 'Compra efetuada pelo Rosario.', 1, '2022-11-16', 12, 2022, 17, 904.65, 95.35, 1000.00, 3, 15, NULL, '', '', '', '', '', NULL, 1, 7, 15, 4, NULL, 5, 1, '2022-12-08 21:47:17', '0000-00-00 00:00:00'),
(793, 'Pagamento efetuado pelo Rosario.', 1, '2022-11-16', 12, 2022, 63, 700.00, NULL, 700.00, 3, 4, NULL, '', '', '00013', '', '', NULL, 1, 2, 32, 4, NULL, 1, 2, '2022-12-08 21:56:58', '0000-00-00 00:00:00'),
(794, 'Valor entregue ao Txopelista.', 1, '2022-11-16', 12, 2022, 13, 650.00, NULL, 650.00, 3, 1, NULL, '', '', '63', '', '', NULL, 1, 108, 129, 4, NULL, 1, 5, '2022-12-08 22:00:21', '0000-00-00 00:00:00'),
(795, 'Compra efetuada pelo Milton com o custo da transaçao de 30,00MT', NULL, '2022-11-16', 12, 2022, 12, 1530.00, NULL, 1530.00, 6, 1, NULL, '', '', '016461', '', '', NULL, NULL, 100, 117, 2, NULL, 3, 1, '2022-12-09 05:37:35', '0000-00-00 00:00:00'),
(796, 'Despesa de Refeiçao Casimiro,Rosario e Agostinho. estavam a trabalhar na Cristal-Box.', 1, '2022-11-16', 12, 2022, 13, 750.00, NULL, 750.00, 3, 1, NULL, '', '', '62', '', '', NULL, 1, 99, 99, 4, NULL, 1, 5, '2022-12-09 06:01:15', '0000-00-00 00:00:00'),
(797, 'Compra efetuada pelo Milton, com o custo da transaçao de 30,00MT', NULL, '2022-11-17', 12, 2022, 12, 1030.00, NULL, 1030.00, 6, 1, NULL, '', '', '21022', '', '845533809', NULL, NULL, 100, 117, 2, NULL, 3, 1, '2022-12-09 06:10:17', '0000-00-00 00:00:00'),
(798, 'Compra de Rolos de Sistema e de POS,compra efetuada pelo Milto, com o custo da transaçao de 30,00MT', NULL, '2022-11-17', 12, 2022, 20, 1530.00, NULL, 1530.00, 6, 2, NULL, '', '', '0043012', '', '845533809', NULL, NULL, 106, 171, 2, NULL, 1, 1, '2022-12-09 06:20:04', '0000-00-00 00:00:00'),
(799, 'Transporte Fenias foi fazer devoluçao. ', NULL, '2022-11-17', 12, 2022, 13, 210.00, NULL, 210.00, 6, 1, NULL, '', '', '63', '', '846852356', NULL, NULL, 7, 11, 4, NULL, 1, 5, '2022-12-09 06:32:39', '0000-00-00 00:00:00'),
(800, 'Pagamento efetuado pelo Rosario.', 1, '2022-11-17', 12, 2022, 13, 1300.00, NULL, 1300.00, 3, 1, NULL, '', '', '66', '', '', NULL, 1, 99, 99, 4, NULL, 1, 5, '2022-12-09 06:36:49', '0000-00-00 00:00:00'),
(801, 'valor entregue ao Txopelista. ', 1, '2022-11-17', 12, 2022, 13, 650.00, NULL, 650.00, 3, 1, NULL, '', '', '67', '', '', NULL, 1, 108, 129, 4, NULL, 1, 5, '2022-12-09 06:40:00', '0000-00-00 00:00:00'),
(802, 'Compra efetuada pelo Rosario.', 1, '2022-11-18', 12, 2022, 17, 1858.46, 141.54, 2000.00, 3, 4, NULL, '', '', '', '', '', NULL, 1, 7, 15, 4, NULL, 5, 1, '2022-12-09 07:02:13', '0000-00-00 00:00:00'),
(803, 'Compra efetuada pelo Rosario.', 1, '2022-11-18', 12, 2022, 17, 904.65, 95.35, 1000.00, 3, 2, NULL, '', '', '', '', '', NULL, 1, 7, 15, 4, NULL, 5, 1, '2022-12-09 07:31:27', '0000-00-00 00:00:00'),
(804, 'Compra efetuada pelo Albino, com o custo da Transaçao de 30,00MT', NULL, '2022-11-18', 12, 2022, 57, 1030.00, NULL, 1030.00, 6, 1, NULL, '', '', '274792', '', '878500911', NULL, NULL, 100, 115, 2, NULL, 3, 1, '2022-12-09 07:36:02', '0000-00-00 00:00:00'),
(805, 'Compra efetuada pelo Rosario.', 1, '2022-11-18', 12, 2022, 17, 2731.71, 268.29, 3000.00, 3, 10, NULL, '', '', '', '', '', NULL, 1, 7, 15, 4, NULL, 5, 1, '2022-12-09 07:39:31', '0000-00-00 00:00:00'),
(806, 'transporte Joana foi para o banco fazer deposito.', NULL, '2022-11-18', 12, 2022, 13, 400.00, NULL, 400.00, 6, 1, NULL, '', '', '67', '', '844257344', NULL, NULL, 7, 11, 4, NULL, 1, 5, '2022-12-09 07:46:10', '0000-00-00 00:00:00'),
(807, 'Pagamento PRM da  6ºEsquadra Macuti.  ', 1, '2022-11-17', 12, 2022, 13, 15000.00, NULL, 15000.00, 3, 4, NULL, '', '', '68', '', '', NULL, 1, 99, 99, 4, NULL, 1, 5, '2022-12-09 07:57:21', '0000-00-00 00:00:00'),
(808, 'Transporte Rosario foi para 6ºEsquadra fazer o pagamento.', 1, '2022-11-17', 12, 2022, 11, 200.00, NULL, 200.00, 3, 1, NULL, '', '', '69', '', '', NULL, 1, 7, 11, 4, NULL, 1, 5, '2022-12-09 08:04:35', '0000-00-00 00:00:00'),
(809, 'Valor entregue ao Txopelista.', 1, '2022-11-18', 12, 2022, 13, 650.00, NULL, 650.00, 3, 1, NULL, '', '', '70', '', '', NULL, 1, 108, 129, 4, NULL, 1, 5, '2022-12-09 08:09:45', '0000-00-00 00:00:00'),
(810, 'Aluguel de carro caixa aberta para carregar prateleiras e balcao da Cristal- Box para passagem de nivel.', 1, '2022-11-18', 12, 2022, 13, 2500.00, NULL, 2500.00, 3, 1, NULL, '', '', '71', '', '', NULL, 1, 99, 99, 4, NULL, 1, 5, '2022-12-09 08:15:09', '0000-00-00 00:00:00'),
(811, 'Compra efetuada pelo Rosario.', 1, '2022-11-19', 12, 2022, 17, 904.65, 95.35, 1000.00, 3, 15, NULL, '', '', '', '', '', NULL, 1, 7, 15, 4, NULL, 5, 1, '2022-12-09 08:23:29', '0000-00-00 00:00:00'),
(812, 'Compra efetuada pelo Rosario.', 1, '2022-11-19', 12, 2022, 17, 904.65, 95.35, 1000.00, 3, 4, NULL, '', '', '', '', '', NULL, 1, 7, 15, 4, NULL, 5, 1, '2022-12-09 08:28:03', '0000-00-00 00:00:00'),
(813, 'Compra efetuada pelo Rosario.', 1, '2022-11-19', 12, 2022, 17, 906.35, 93.65, 1000.00, 3, 19, NULL, '', '', '', '', '', NULL, 1, 7, 15, 4, NULL, 5, 1, '2022-12-09 08:32:05', '0000-00-00 00:00:00'),
(814, 'Valor entregue ao Armando.', 1, '2022-11-21', 12, 2022, 13, 400.00, NULL, 400.00, 3, 4, NULL, '', '', '', '', '', NULL, 1, 5, 80, 4, NULL, 1, NULL, '2022-12-09 08:33:44', '0000-00-00 00:00:00'),
(815, 'Valor entregue ao Tivane.', 1, '2022-11-19', 12, 2022, 13, 6000.00, NULL, 6000.00, 3, 1, NULL, '', '', '72', '', '', NULL, 1, 6, 3, 4, NULL, 1, 5, '2022-12-09 08:35:23', '0000-00-00 00:00:00'),
(816, 'Valor entregue a Leticia.', 1, '2022-11-21', 12, 2022, 13, 400.00, NULL, 400.00, 3, 4, NULL, '', '', '', '', '', NULL, 1, 5, 81, 4, NULL, 1, NULL, '2022-12-09 08:38:52', '0000-00-00 00:00:00'),
(817, 'Valor entregue a Manuela.', 1, '2022-11-21', 12, 2022, 13, 100.00, NULL, 100.00, 3, 4, NULL, '', '', '', '', '', NULL, 1, 5, 82, 4, NULL, 1, NULL, '2022-12-09 08:41:26', '0000-00-00 00:00:00'),
(818, 'Compra de Vassoura de palha.\r\nCompra efetuada pelo Guarda em serviço no mercado informal.', 1, '2022-11-19', 12, 2022, 13, 300.00, NULL, 300.00, 3, 10, 105, '', '', '73', NULL, '', NULL, 1, 105, 157, 4, 1, 1, 5, '2022-12-09 08:42:13', '2022-12-12 10:16:36'),
(819, 'Compra efetuada pelo Staff em serviço.', 1, '2022-11-20', 12, 2022, 49, 500.00, NULL, 500.00, 3, 4, NULL, '', '', '000089', '', '', NULL, 1, 111, 156, 4, NULL, 1, 1, '2022-12-09 08:46:28', '0000-00-00 00:00:00'),
(820, 'Compra efetuada pelo Staff em serviço, nao teve recibo. ', 1, '2022-11-20', 12, 2022, 13, 500.00, NULL, 500.00, 3, 10, NULL, '', '', '74', '', '', NULL, 1, 111, 156, 4, NULL, 1, 5, '2022-12-09 08:50:48', '0000-00-00 00:00:00'),
(821, 'Valor entregue ao Osvaldo.', 1, '2022-11-21', 12, 2022, 13, 600.00, NULL, 600.00, 3, 4, NULL, '', '', '', '', '', NULL, 1, 5, 83, 4, NULL, 1, NULL, '2022-12-09 08:51:12', '0000-00-00 00:00:00'),
(822, 'Valor entregue ao Silvestre.', 1, '2022-11-21', 12, 2022, 13, 400.00, NULL, 400.00, 3, 4, NULL, '', '', '', '', '', NULL, 1, 5, 85, 4, NULL, 1, NULL, '2022-12-09 08:53:20', '0000-00-00 00:00:00'),
(823, 'Transporte Tecnico e Caixa da Farmacia Ponta-Gea no Domingo.', 1, '2022-11-20', 12, 2022, 13, 300.00, NULL, 300.00, 3, 10, 7, '', '', '74', NULL, '', NULL, 1, 7, 11, 4, 1, 1, 5, '2022-12-09 08:56:10', '2022-12-10 12:45:42'),
(824, 'Valor entregue ao Oscar.', 1, '2022-11-21', 12, 2022, 13, 400.00, NULL, 400.00, 3, 10, NULL, '', '', '', '', '', NULL, 1, 5, 84, 4, NULL, 1, NULL, '2022-12-09 08:57:28', '0000-00-00 00:00:00'),
(825, 'Valor entregue ao Rosario.', 1, '2022-11-21', 12, 2022, 13, 800.00, NULL, 800.00, 3, 10, NULL, '', '', '', '', '', NULL, 1, 5, 86, 4, NULL, 1, NULL, '2022-12-09 09:11:25', '0000-00-00 00:00:00'),
(826, 'Valor entregue a Fatima.', 1, '2022-11-21', 12, 2022, 13, 400.00, NULL, 400.00, 3, 10, NULL, '', '', '', '', '', NULL, 1, 5, 87, 4, NULL, 1, NULL, '2022-12-09 09:14:17', '0000-00-00 00:00:00'),
(827, 'Compra efetuada pelo Mariano  no mercado informal , com o custo da transaçao de 30,00MT', NULL, '2022-11-20', 12, 2022, 13, 1830.00, NULL, 1830.00, 6, 3, NULL, '', '', '74', '', '847876424', NULL, NULL, 99, 99, 2, NULL, 1, 5, '2022-12-09 09:14:25', '0000-00-00 00:00:00'),
(828, 'Valor entregue ao Francisco.', 1, '2022-11-21', 12, 2022, 13, 500.00, NULL, 500.00, 3, 10, NULL, '', '', '', '', '', NULL, 1, 5, 88, 4, NULL, 1, NULL, '2022-12-09 09:16:57', '0000-00-00 00:00:00'),
(829, 'Valor entregue ao Latifo.', 1, '2022-11-21', 12, 2022, 13, 400.00, NULL, 400.00, 3, 10, NULL, '', '', '', '', '', NULL, 1, 5, 89, 4, NULL, 1, NULL, '2022-12-09 09:31:03', '0000-00-00 00:00:00'),
(830, 'Valor entregue ao Agostinho.', 1, '2022-11-21', 12, 2022, 13, 100.00, NULL, 100.00, 3, 10, NULL, '', '', '', '', '', NULL, 1, 5, 90, 4, NULL, 1, NULL, '2022-12-09 09:34:42', '0000-00-00 00:00:00'),
(831, 'Valor entregue a Jacinta.', 1, '2022-11-21', 12, 2022, 13, 400.00, NULL, 400.00, 3, 10, NULL, '', '', '', '', '', NULL, 1, 5, 91, 4, NULL, 1, NULL, '2022-12-09 09:46:18', '0000-00-00 00:00:00'),
(832, 'Valor entregue a Miquelina.', 1, '2022-11-21', 12, 2022, 13, 400.00, NULL, 400.00, 3, 10, NULL, '', '', '', '', '', NULL, 1, 5, 92, 4, NULL, 1, NULL, '2022-12-09 09:48:10', '0000-00-00 00:00:00'),
(833, 'Pagamento do aluguel da casa para o Osvaldo, 3 meses  ', 1, '2022-11-20', 12, 2022, 13, 21000.00, NULL, 21000.00, 3, 1, NULL, '', '', '75', '', '', NULL, 1, 99, 99, 4, NULL, 1, 5, '2022-12-09 09:56:11', '0000-00-00 00:00:00'),
(834, 'Valor enviado para o mpesa da FME Kenneth Kaunda, com o custo de transação de 10,00MT.', NULL, '2022-11-21', 12, 2022, 13, 510.00, NULL, 510.00, 6, 2, NULL, '', '', '', '', '855154553', NULL, NULL, 5, 63, 2, NULL, 1, NULL, '2022-12-09 09:58:11', '0000-00-00 00:00:00'),
(835, 'Valor enviado para o Mpesa da FME K.k, com o custo de transação de 10,00MT>', NULL, '2022-11-21', 12, 2022, 13, 310.00, NULL, 310.00, 6, 2, NULL, '', '', '', '', '851514553', NULL, NULL, 5, 65, 2, NULL, 1, NULL, '2022-12-09 10:04:49', '0000-00-00 00:00:00'),
(836, 'Valor enviado para o Mpesa da FME Kenneth Kaunda, com o custo de transação de 10,00MT.', NULL, '2022-11-21', 12, 2022, 13, 510.00, NULL, 510.00, 6, 2, NULL, '', '', '', '', '855154553', NULL, NULL, 5, 123, 2, NULL, 1, NULL, '2022-12-09 10:19:20', '0000-00-00 00:00:00'),
(837, 'Valor enviado para o mpesa da FME Kenneth Kaunda.', NULL, '2022-11-21', 12, 2022, 13, 500.00, NULL, 500.00, 6, 2, NULL, '', '', '', '', '855154553', NULL, NULL, 5, 153, 2, NULL, 1, NULL, '2022-12-09 10:33:06', '0000-00-00 00:00:00'),
(838, 'Valor enviado para o Milton, com o custo de transação de 10,00MT.', NULL, '2022-11-21', 12, 2022, 13, 510.00, NULL, 510.00, 6, 3, NULL, '', '', '', '', '845533809', NULL, NULL, 5, 70, 2, NULL, 1, NULL, '2022-12-09 10:35:08', '0000-00-00 00:00:00'),
(839, 'Valor enviado para o Milton, com o custo de transação de 10,00MT.', 1, '2022-11-21', 12, 2022, 13, 510.00, NULL, 510.00, 3, 3, NULL, '', '', '', '', '', NULL, 1, 5, 72, 4, NULL, 1, NULL, '2022-12-09 10:37:32', '0000-00-00 00:00:00'),
(840, 'Valor enviado para o Milton, com o custo de transação de 10,00MT.', NULL, '2022-11-21', 12, 2022, 13, 510.00, NULL, 510.00, 6, 3, NULL, '', '', '', '', '845533809', NULL, NULL, 5, 73, 2, NULL, 1, NULL, '2022-12-09 10:38:36', '0000-00-00 00:00:00'),
(841, 'Valor enviado para o Milton, com o custo de transação de 10,00MT.', NULL, '2022-11-21', 12, 2022, 13, 510.00, NULL, 510.00, 6, 3, NULL, '', '', '', '', '845533809', NULL, NULL, 5, 74, 2, NULL, 1, NULL, '2022-12-09 10:39:39', '0000-00-00 00:00:00'),
(842, 'Valor enviado para o Milton, com o custo de transação de 10,00MT.', NULL, '2022-11-21', 12, 2022, 13, 1010.00, NULL, 1010.00, 6, 3, 5, '', '', '', NULL, '845533809', NULL, 0, 5, 76, 2, 0, 1, NULL, '2022-12-09 11:26:11', '2022-12-12 14:30:59'),
(843, 'Valor enviado para o Milton.', NULL, '2022-11-21', 12, 2022, 13, 300.00, NULL, 300.00, 6, 3, NULL, '', '', '', '', '845533809', NULL, NULL, 5, 77, 2, NULL, 1, NULL, '2022-12-09 11:27:56', '0000-00-00 00:00:00'),
(844, 'Transporte Fenias foi fazer devoluçao de Medicamento.', NULL, '2022-11-20', 12, 2022, 13, 310.00, NULL, 310.00, 6, 1, NULL, '', '', '76', '', '846852356', NULL, NULL, 7, 11, 4, NULL, 1, 5, '2022-12-09 11:33:08', '0000-00-00 00:00:00'),
(845, 'Valor entregue ao Txopelista ', 1, '2022-11-20', 12, 2022, 13, 650.00, NULL, 650.00, 3, 1, NULL, '', '', '77', '', '', NULL, 1, 108, 129, 4, NULL, 1, 5, '2022-12-09 11:36:43', '0000-00-00 00:00:00'),
(846, 'Valor enviado para o Milton.', NULL, '2022-11-21', 12, 2022, 13, 500.00, NULL, 500.00, 6, 3, NULL, '', '', '', '', '845533809', NULL, NULL, 5, 78, 2, NULL, 1, NULL, '2022-12-09 11:43:12', '0000-00-00 00:00:00'),
(847, 'Valor enviado para o Milton.', NULL, '2022-11-21', 12, 2022, 13, 500.00, NULL, 500.00, 6, 3, NULL, '', '', '', '', '845533809', NULL, NULL, 5, 170, 2, NULL, 1, NULL, '2022-12-09 11:44:48', '0000-00-00 00:00:00'),
(848, 'Valor enviado para o Milton\r\n', NULL, '2022-11-21', 12, 2022, 13, 500.00, NULL, 500.00, 6, 3, NULL, '', '', '', '', '845533809', NULL, NULL, 5, 172, 2, NULL, 1, NULL, '2022-12-09 11:46:13', '0000-00-00 00:00:00'),
(849, 'Valor entregue ao Dr. Pereira para pagar o Inposto de constituinte em assistencia contra o Stephano.', 1, '2022-11-15', 12, 2022, 13, 5000.00, NULL, 5000.00, 3, 1, NULL, '', '', '78', '', '', NULL, 1, 99, 99, 4, NULL, 1, 5, '2022-12-09 11:59:22', '0000-00-00 00:00:00'),
(850, 'Valor entregue ao Dr.Pereira para tratar parecer no sindicato, processo contra Miquelina.', 1, '2022-11-21', 12, 2022, 13, 5000.00, NULL, 5000.00, 3, 1, NULL, '', '', '79', '', '', NULL, 1, 99, 99, 4, NULL, 1, 5, '2022-12-09 12:21:00', '0000-00-00 00:00:00'),
(851, 'Compra efetuada pelo Patricio.', 1, '2022-11-21', 12, 2022, 17, 904.65, 95.35, 1000.00, 3, 4, NULL, '', '', '', '', '', NULL, 1, 7, 15, 4, NULL, 5, 1, '2022-12-09 12:27:02', '0000-00-00 00:00:00'),
(852, 'Valor enviado para o Manuel, com o custo de transação de 10,00MT.', NULL, '2022-11-21', 12, 2022, 13, 210.00, NULL, 210.00, 6, 2, NULL, '', '', '', '', '841158201', NULL, NULL, 5, 64, NULL, NULL, 1, NULL, '2022-12-09 12:29:54', '0000-00-00 00:00:00'),
(853, 'Valor enviado para o Sr. Manuel, com custo de transação de 10,00MT.', NULL, '2022-11-21', 12, 2022, 13, 210.00, NULL, 210.00, 6, 2, NULL, '', '', '', '', '841158201', NULL, NULL, 5, 66, 2, NULL, 1, NULL, '2022-12-09 12:31:47', '0000-00-00 00:00:00'),
(854, 'Valor entregue ao Albino com o custo da transaçao de 30,00MT', NULL, '2022-11-21', 12, 2022, 57, 2030.00, NULL, 2030.00, 6, 1, NULL, '', '', '275041', '', '845629961', NULL, NULL, 100, 115, 2, NULL, 3, 1, '2022-12-09 12:32:46', '0000-00-00 00:00:00'),
(855, 'Valor enviado para o Sr. Manuel, com custo de transação de 10,00MT.', NULL, '2022-11-21', 12, 2022, 13, 210.00, NULL, 210.00, 6, 2, NULL, '', '', '', '', '841158201', NULL, NULL, 5, 67, 2, NULL, 1, NULL, '2022-12-09 12:37:02', '0000-00-00 00:00:00'),
(856, 'Valor enviado para o Sr. Manuel.', NULL, '2022-11-21', 12, 2022, 13, 200.00, NULL, 200.00, 6, 2, NULL, '', '', '', '', '841158201', NULL, NULL, 5, 68, 2, NULL, 1, NULL, '2022-12-09 12:39:37', '0000-00-00 00:00:00'),
(857, 'Compra efetuada pelo Rosario.', 1, '2022-11-21', 12, 2022, 62, 6500.00, NULL, 6500.00, 3, 10, NULL, '', '', '001021', '', '', NULL, 1, 7, 8, 4, NULL, 1, 1, '2022-12-09 13:03:01', '0000-00-00 00:00:00'),
(858, 'Pagamento Barbeiro do Sr.Marco.', NULL, '2022-11-21', 12, 2022, 13, 800.00, NULL, 800.00, 6, 17, NULL, '', '', '76', '', '846158970', NULL, NULL, 99, 99, 4, NULL, 1, 5, '2022-12-09 13:18:16', '0000-00-00 00:00:00'),
(859, 'Valor enviado para Amelia Nhama.', NULL, '2022-11-21', 12, 2022, 13, 1030.00, NULL, 1030.00, 6, 17, NULL, '', '', '77', '', '843687889', NULL, NULL, 99, 99, 2, NULL, 1, 5, '2022-12-09 13:24:23', '0000-00-00 00:00:00'),
(860, 'Valor entregue ao Txopelista.', 1, '2022-11-21', 12, 2022, 13, 650.00, NULL, 650.00, 3, 1, NULL, '', '', '78', '', '', NULL, 1, 108, 129, 4, NULL, 1, 5, '2022-12-09 13:30:22', '0000-00-00 00:00:00'),
(861, 'Transporte Monica foi para kenneth kaunda a mando do Sr.Marco ', NULL, '2022-11-21', 12, 2022, 13, 310.00, NULL, 310.00, 6, 1, NULL, '', '', '78', '', '876765939', NULL, NULL, 7, 11, 4, NULL, 1, 5, '2022-12-09 13:34:47', '0000-00-00 00:00:00'),
(862, 'Manutençao Geral do Ar condicionado do armazem.', 1, '2022-11-21', 12, 2022, 13, 700.00, NULL, 700.00, 3, 10, NULL, '', '', '79', '', '', NULL, 1, 2, 28, 4, NULL, 1, 5, '2022-12-09 13:41:25', '0000-00-00 00:00:00'),
(863, 'Manutençao Geral  do Ar condicionado e troca de capacitor.', 1, '2022-11-21', 12, 2022, 13, 3200.00, NULL, 3200.00, 3, 10, NULL, '', '', '79', '', '', NULL, 1, 2, 26, 4, NULL, 1, 5, '2022-12-09 13:45:04', '0000-00-00 00:00:00'),
(864, 'Manutençao Geral do Ar condicionado e uma carga de gas.', 1, '2022-11-21', 12, 2022, 13, 2100.00, NULL, 2100.00, 3, 10, NULL, '', '', '79', '', '', NULL, 1, 2, 29, 4, NULL, 1, 5, '2022-12-09 13:48:15', '0000-00-00 00:00:00'),
(865, 'Manutençao Geral do Ar condicionado.', 1, '2022-11-21', 12, 2022, 13, 700.00, NULL, 700.00, 3, 10, NULL, '', '', '79', '', '', NULL, 1, 2, 33, 4, NULL, 1, 5, '2022-12-09 13:55:01', '0000-00-00 00:00:00'),
(866, 'Valor entregue ao Txopelista', 1, '2022-11-22', 12, 2022, 13, 650.00, NULL, 650.00, 3, 1, NULL, '', '', '79', '', '', NULL, 1, 108, 129, 4, NULL, 1, 5, '2022-12-09 17:33:34', '0000-00-00 00:00:00'),
(867, 'Compra de 27 Rolos de Pos e 10 Rolos de Sistema, compra efetuada pelo Milton. ', NULL, '2022-11-22', 12, 2022, 20, 2700.00, NULL, 2700.00, 6, 3, NULL, '', '', '004304', '', '845533809', NULL, NULL, 106, 171, 2, NULL, 1, 1, '2022-12-09 17:45:36', '0000-00-00 00:00:00'),
(868, 'Compra efetuada pelo Rosario.', 1, '2022-11-22', 12, 2022, 17, 1809.30, 190.70, 2000.00, 3, 4, NULL, '', '', '', '', '', NULL, 1, 7, 15, 4, NULL, 5, 1, '2022-12-09 17:52:22', '0000-00-00 00:00:00'),
(869, 'Transporte Fenias foi buscar nota de credito na inportadora.', NULL, '2022-11-22', 12, 2022, 13, 350.00, NULL, 350.00, 6, 1, NULL, '', '', '80', '', '846852356', NULL, NULL, 7, 11, 4, NULL, 1, 5, '2022-12-09 17:59:12', '0000-00-00 00:00:00'),
(870, 'Compra efetuada pelo Rosario.', 1, '2022-11-22', 12, 2022, 17, 904.65, 95.35, 1000.00, 3, 2, NULL, '', '', '', '', '', NULL, 1, 7, 15, 4, NULL, 5, 1, '2022-12-09 18:04:28', '0000-00-00 00:00:00'),
(871, 'Compra de plasticos para farmacias Ponta-Gea,Macuti,P.nivel, Compra efetuada pelo Rosario.', 1, '2022-11-22', 12, 2022, 34, 5850.00, NULL, 5850.00, 3, 10, NULL, '', '', '62092', '', '', NULL, 1, 99, 99, 4, NULL, 1, 1, '2022-12-09 18:15:23', '0000-00-00 00:00:00'),
(872, 'Compra de uma Caixa de Resma A4, compra efectuada pelo Rosario.', 1, '2022-11-22', 12, 2022, 47, 2100.00, NULL, 2100.00, 3, 10, NULL, '', '', '04713', '', '', NULL, 1, 106, 130, 4, NULL, 1, 1, '2022-12-09 18:24:23', '0000-00-00 00:00:00');
INSERT INTO `execucaodespesas` (`idexecucaodespesas`, `descricaodespesa`, `idrequisicaodefundos`, `data`, `mes`, `ano`, `idempresa`, `valorinicial`, `iva`, `valortotal`, `idformapagamento`, `idcentrodecusto`, `idgrupodespesas`, `observacoes`, `anexo`, `recibo`, `numerocheque`, `transfcontadestino`, `transferenciabanco`, `caixa`, `grupo`, `subgrupo`, `banco`, `quantidade`, `unidade`, `tipo_documento_id`, `data_criacao`, `data_atualizacao`) VALUES
(873, 'Compra efetuada pelo Rosario.', 1, '2022-11-23', 12, 2022, 17, 904.65, 95.35, 1000.00, 3, 19, NULL, '', '', '', '', '', NULL, 1, 7, 15, 4, NULL, 5, 1, '2022-12-09 18:33:52', '0000-00-00 00:00:00'),
(874, 'Transporte Sara foi para Inportadoras.', NULL, '2022-11-23', 12, 2022, 13, 610.00, NULL, 610.00, 6, 1, NULL, '', '', '81', '', '844333222', NULL, NULL, 7, 11, 4, NULL, 1, 5, '2022-12-09 18:39:13', '0000-00-00 00:00:00'),
(875, 'Combustivel Dolca, compra efetuada pela Dolca.', 1, '2022-11-23', 12, 2022, 12, 2700.00, NULL, 2700.00, 3, 1, NULL, '', '', '004170', '', '', NULL, 1, 100, 139, 4, NULL, 3, 1, '2022-12-09 18:45:55', '0000-00-00 00:00:00'),
(876, 'Valor levado pelo Sr.Marco na farmacia costa do sol, no turno do Nelson.', 1, '2022-11-23', 12, 2022, 13, 400.00, NULL, 400.00, 3, 17, NULL, '', '', '82', '', '', NULL, 1, 99, 99, 4, NULL, 1, 5, '2022-12-09 18:51:43', '0000-00-00 00:00:00'),
(877, 'Transporte Rosario foi fazer recolha de valores nas farmacias, valor entregue ao Txopelista.', 1, '2022-11-23', 12, 2022, 13, 650.00, NULL, 650.00, 3, 1, NULL, '', '', '83', '', '', NULL, 1, 108, 129, 4, NULL, 1, 5, '2022-12-09 18:56:46', '0000-00-00 00:00:00'),
(878, 'Pagamento pequeno almoço Sr.Marco', NULL, '2022-11-23', 12, 2022, 22, 2290.00, NULL, 2290.00, 6, 17, NULL, '', '', '83', '', '850685164', NULL, NULL, 102, 145, 4, NULL, 1, 5, '2022-12-09 19:01:00', '0000-00-00 00:00:00'),
(879, 'Valor enviado para o Albino com o custo da transaçao de 30,00MT', NULL, '2022-11-23', 12, 2022, 12, 1030.00, NULL, 1030.00, 6, 1, NULL, '', '', '045383', '', '845629961', NULL, NULL, 100, 115, 2, NULL, 3, 1, '2022-12-09 19:11:44', '0000-00-00 00:00:00'),
(880, 'Compra efetuada pelo Albino com o Custo da transaçao de 30,00MT', NULL, '2022-11-24', 12, 2022, 12, 2030.00, NULL, 2030.00, 6, 1, NULL, '', '', '045392', '', '845629961', NULL, NULL, 100, 115, NULL, NULL, 3, 1, '2022-12-09 19:28:11', '0000-00-00 00:00:00'),
(881, 'Rancho Carlos MPT.', 1, '2022-11-24', 12, 2022, 13, 5000.00, NULL, 5000.00, 3, 3, 4, '', '', '83', NULL, '', NULL, 1, 4, 166, 4, 1, 1, 5, '2022-12-09 19:32:00', '2022-12-12 08:55:44'),
(882, 'Compra efetuada pelo Rosario.', 1, '2022-11-24', 12, 2022, 12, 3000.00, NULL, 3000.00, 3, 1, NULL, '', '', '8303', '', '', NULL, 1, 100, 119, 4, NULL, 3, 1, '2022-12-09 19:39:41', '0000-00-00 00:00:00'),
(883, 'Compra efetuada pelo Guarda em Serviço.', 1, '2022-11-24', 12, 2022, 12, 1700.00, NULL, 1700.00, 3, 10, NULL, '', '', '8293', '', '', NULL, 1, 8, 146, 4, NULL, 3, 1, '2022-12-09 19:46:47', '0000-00-00 00:00:00'),
(884, 'Compra efetuada pelo Rosario.', 1, '2022-11-24', 12, 2022, 17, 1858.46, 141.54, 2000.00, 3, 2, NULL, '', '', '', '', '', NULL, 1, 7, 15, 4, NULL, 5, 1, '2022-12-09 19:51:43', '0000-00-00 00:00:00'),
(885, 'Valor entregue ao Guarda em serviço.', 1, '2022-11-24', 12, 2022, 13, 2000.00, NULL, 2000.00, 3, 14, NULL, '', '', '84', '', '', NULL, 1, 4, 62, 4, NULL, 1, 5, '2022-12-09 19:56:46', '0000-00-00 00:00:00'),
(886, 'Valor enviado para o Mpesa da FME Kenneth Kaunda, com o custo de transação de 10,00MT.', 1, '2022-11-28', 12, 2022, 13, 510.00, NULL, 510.00, 6, 2, 5, '', '', '', NULL, '855154553', NULL, 1, 5, 123, 2, 1, 1, NULL, '2022-12-09 19:56:48', '2022-12-12 08:53:04'),
(887, 'Valor enviado para o Mpesa da FME Kenneth Kaunda, com o custo de transação de 10,00MT.', 1, '2022-11-28', 12, 2022, 13, 510.00, NULL, 510.00, 6, 2, 5, '', '', '', NULL, '855154553', NULL, 1, 5, 153, 2, 1, 1, NULL, '2022-12-09 19:58:30', '2022-12-13 10:21:29'),
(888, 'valor enviado para Abiba para entregar a tia do Sr.Marco, Clara Monteiro.', NULL, '2022-11-24', 12, 2022, 13, 1030.00, NULL, 1030.00, 6, 1, NULL, '', '', '85', '', '852890120', NULL, NULL, 99, 99, 4, NULL, 1, 5, '2022-12-09 20:09:55', '0000-00-00 00:00:00'),
(889, 'Transporte Jeny foi para casa do Sr. Marco levar as chaves.', NULL, '2022-11-24', 12, 2022, 13, 500.00, NULL, 500.00, 6, 1, NULL, '', '', '85', '', '878383477', NULL, NULL, 7, 11, 4, NULL, 1, 5, '2022-12-09 20:18:02', '0000-00-00 00:00:00'),
(890, 'Compra efetuada pelo Rosario.', 1, '2022-11-25', 12, 2022, 17, 1809.30, 190.70, 2000.00, 3, 4, NULL, '', '', '', '', '', NULL, 1, 7, 15, 4, NULL, 5, 1, '2022-12-09 20:22:13', '0000-00-00 00:00:00'),
(891, 'Compra efetuada pelo Rosario.', 1, '2022-11-25', 12, 2022, 17, 904.65, 95.35, 1000.00, 3, 2, NULL, '', '', '', '', '', NULL, 1, 7, 15, 4, NULL, 5, 1, '2022-12-09 20:26:20', '0000-00-00 00:00:00'),
(892, 'Compra efetuada pelo Rosario.', 1, '2022-11-25', 12, 2022, 17, 904.65, 95.35, 1000.00, 3, 19, NULL, '', '', '', '', '', NULL, 1, 7, 15, 4, NULL, 5, 1, '2022-12-09 20:29:41', '0000-00-00 00:00:00'),
(893, 'valor enviado para o Milton.', NULL, '2022-11-25', 12, 2022, 25, 6100.00, 1037.00, 7137.00, 6, 3, NULL, '', '', '618', '', '845533809', NULL, NULL, 7, 8, 4, NULL, 1, 1, '2022-12-09 20:36:28', '0000-00-00 00:00:00'),
(894, 'Compra de Credito para Rosario Via Mpsa nao tem Recibo.', 1, '2022-11-25', 12, 2022, 4, 1000.00, NULL, 1000.00, 3, 1, NULL, '', '', '86', '', '', NULL, 1, 3, 58, 4, NULL, 1, 5, '2022-12-09 20:39:37', '0000-00-00 00:00:00'),
(895, 'Compra efectuada pelo Rosario', 1, '2022-11-25', 12, 2022, 13, 1000.00, NULL, 1000.00, 3, 17, NULL, '', '', '86', '', '', NULL, 1, 3, 56, 4, NULL, 1, 5, '2022-12-09 20:45:07', '0000-00-00 00:00:00'),
(896, 'Compra efetuada pelo Mariano.', NULL, '2022-11-25', 12, 2022, 12, 1000.00, NULL, 1000.00, 6, 1, NULL, '', '', '0047217', '', '847876424', NULL, NULL, 100, 117, 4, NULL, 3, 1, '2022-12-09 20:50:15', '0000-00-00 00:00:00'),
(897, 'transporte Rosario foi fazer recolha nas farmacias, valor entregue ao Txopelista.', 1, '2022-11-25', 12, 2022, 13, 650.00, NULL, 650.00, 3, 1, NULL, '', '', '87', '', '', NULL, 1, 108, 129, 4, NULL, 1, 5, '2022-12-09 20:53:36', '0000-00-00 00:00:00'),
(898, 'Compra efetuada pelo Rosario.', 1, '2022-11-25', 12, 2022, 17, 904.65, 95.35, 1000.00, 3, 15, NULL, '', '', '', '', '', NULL, 1, 7, 15, 4, NULL, 5, 1, '2022-12-09 20:59:41', '0000-00-00 00:00:00'),
(899, 'Compra de Internete efetuada pelo Milton.', NULL, '2022-11-26', 12, 2022, 27, 6100.00, NULL, 6100.00, 6, 2, NULL, '', '', '1106598', '', '845533809', NULL, NULL, 7, 9, 4, NULL, 1, 2, '2022-12-09 21:07:45', '0000-00-00 00:00:00'),
(900, 'Pagamento Pequeno almoço para Sr.Marco.', NULL, '2022-11-26', 12, 2022, 22, 1580.00, NULL, 1580.00, 6, 17, NULL, '', '', '87', '', '850685164', NULL, NULL, 102, 145, 2, NULL, 1, 5, '2022-12-09 21:12:31', '0000-00-00 00:00:00'),
(901, 'Pagamento Casimiro fes trabalho na Cristal-Box, desmontagem de Prateleiras e Balcao.', 1, '2022-11-26', 12, 2022, 13, 4000.00, NULL, 4000.00, 3, 1, NULL, '', '', '87', '', '', NULL, 1, 99, 99, 4, NULL, 1, 5, '2022-12-09 21:16:44', '0000-00-00 00:00:00'),
(902, 'Envio de material eletrico para Maputo. envio feito pelo Rosario.', 1, '2022-11-26', 12, 2022, 10, 3000.00, NULL, 3000.00, 3, 1, NULL, '', '', '08549', '', '', NULL, 1, 7, 13, 4, NULL, 1, 1, '2022-12-09 21:20:49', '0000-00-00 00:00:00'),
(903, 'Compra feita pelo Rosario.', 1, '2022-11-27', 12, 2022, 17, 904.65, 95.35, 1000.00, 3, 4, NULL, '', '', '', '', '', NULL, 1, 7, 15, 4, NULL, 5, 1, '2022-12-09 21:25:02', '0000-00-00 00:00:00'),
(904, 'Compra efetuada pelo Rosario.', 1, '2022-11-27', 12, 2022, 17, 1858.46, 141.54, 2000.00, 3, 2, NULL, '', '', '', '', '', NULL, 1, 7, 15, 4, NULL, 5, 1, '2022-12-09 21:29:02', '0000-00-00 00:00:00'),
(905, 'Compra efetuada pelo Rosario.', 1, '2022-11-27', 12, 2022, 17, 1812.97, 187.03, 2000.00, 3, 19, NULL, '', '', '', '', '', NULL, 1, 7, 15, 4, NULL, 5, 1, '2022-12-09 21:33:32', '0000-00-00 00:00:00'),
(906, 'Transporte Rosario foi faser recolha de valores nas farmacias,', 1, '2022-11-27', 12, 2022, 13, 650.00, NULL, 650.00, 3, 1, NULL, '', '', '88', '', '', NULL, 1, 108, 129, 4, NULL, 1, 5, '2022-12-09 21:36:58', '0000-00-00 00:00:00'),
(907, 'valor enviado para o Albino com o custo da transaçao de 30,00MT', 1, '2022-11-27', 12, 2022, 12, 2030.00, NULL, 2030.00, 6, 1, NULL, '', '', '0047375', '', '845629961', NULL, 1, 100, 115, 2, NULL, 3, 1, '2022-12-09 21:42:08', '0000-00-00 00:00:00'),
(908, 'Compra efetuada pelo Staff em Serviço.', 1, '2022-11-27', 12, 2022, 61, 500.00, NULL, 500.00, 3, 10, NULL, '', '', '000831', '', '', NULL, 1, 111, 156, 4, NULL, 1, 1, '2022-12-09 21:45:46', '0000-00-00 00:00:00'),
(909, 'Compra efetuada pelo Staff em serviço.', 1, '2022-11-27', 12, 2022, 49, 500.00, NULL, 500.00, 3, 4, NULL, '', '', '000094', '', '', NULL, 1, 111, 156, 4, NULL, 1, 1, '2022-12-09 21:50:59', '0000-00-00 00:00:00'),
(910, 'Transporte tecnico e caixa Farmacia Ponta-Gea no domingo.', 1, '2022-11-27', 12, 2022, 13, 300.00, NULL, 300.00, 3, 1, NULL, '', '', '89', '', '', NULL, 1, 7, 11, 4, NULL, 1, 5, '2022-12-09 21:54:37', '0000-00-00 00:00:00'),
(911, 'Valor enviado para o Marcolino com o custo da transaçao de 30,00MT', NULL, '2022-11-28', 12, 2022, 12, 2030.00, NULL, 2030.00, 6, 1, 100, '', '', '002068', NULL, '849626590', NULL, 0, 100, 117, 2, 0, 3, 1, '2022-12-10 04:49:08', '2022-12-10 06:53:17'),
(912, 'Transporte Rosario foi para o aeroporto enviar cartao do Bci para Maputo.', 1, '2022-11-28', 12, 2022, 13, 300.00, NULL, 300.00, 3, 1, NULL, '', '', '88', '', '', NULL, 1, 7, 11, 4, NULL, 1, 5, '2022-12-10 05:09:22', '0000-00-00 00:00:00'),
(913, 'Envio de cartao do Bci do Sr.Marco para Maputo de Voo.', 1, '2022-11-28', 12, 2022, 13, 600.00, NULL, 600.00, 3, 1, NULL, '', '', '89', '', '', NULL, 1, 7, 13, 4, NULL, 1, 5, '2022-12-10 05:16:11', '0000-00-00 00:00:00'),
(914, 'Valor entregue ao Oscar.', 1, '2022-11-28', 12, 2022, 13, 400.00, NULL, 400.00, 3, 10, NULL, '', '', '', '', '', NULL, 1, 5, 84, 4, NULL, 1, NULL, '2022-12-10 05:19:51', '0000-00-00 00:00:00'),
(915, 'Compra efetuada pelo Rosario.', 1, '2022-11-28', 12, 2022, 17, 929.23, 70.77, 1000.00, 3, 2, NULL, '', '', '', '', '', NULL, 1, 7, 15, 4, NULL, 5, 1, '2022-12-10 05:20:01', '0000-00-00 00:00:00'),
(916, 'Valor entregue ao Rosario.', 1, '2022-11-28', 12, 2022, 13, 800.00, NULL, 800.00, 3, 10, NULL, '', '', '', '', '', NULL, 1, 5, 86, 4, NULL, 1, NULL, '2022-12-10 05:21:06', '0000-00-00 00:00:00'),
(917, 'Valor entregue a Fatima.', 1, '2022-11-28', 12, 2022, 13, 400.00, NULL, 400.00, 3, 10, NULL, '', '', '', '', '', NULL, 1, 5, 87, 4, NULL, 1, NULL, '2022-12-10 05:22:16', '0000-00-00 00:00:00'),
(918, 'Transporte Rosario foi fazer recolhas de valores nas farmacias. ', 1, '2022-11-28', 12, 2022, 13, 650.00, NULL, 650.00, 3, 1, NULL, '', '', '90', '', '', NULL, 1, 108, 129, 4, NULL, 1, 5, '2022-12-10 05:22:58', '0000-00-00 00:00:00'),
(919, 'Alugar Carro para transportar cadeiras e Cacifos para Ponta-Gea Macuti e P.nivel.', 1, '2022-11-28', 12, 2022, 13, 2500.00, NULL, 2500.00, 3, 1, NULL, '', '', '91', '', '', NULL, 1, 99, 99, 4, NULL, 1, 5, '2022-12-10 05:32:59', '0000-00-00 00:00:00'),
(920, 'Valor entregue ao Francisco.', 1, '2022-11-28', 12, 2022, 13, 500.00, NULL, 500.00, 3, 10, NULL, '', '', '', '', '', NULL, 1, 5, 88, 4, NULL, 1, NULL, '2022-12-10 05:54:16', '0000-00-00 00:00:00'),
(921, 'Valor entregue Latifo.', 1, '2022-11-28', 12, 2022, 13, 400.00, NULL, 400.00, 3, 10, NULL, '', '', '', '', '', NULL, 1, 5, 89, 4, NULL, 1, NULL, '2022-12-10 06:01:44', '0000-00-00 00:00:00'),
(922, 'Valor entregue ao Agostinho.', 1, '2022-11-28', 12, 2022, 13, 100.00, NULL, 100.00, 3, 10, NULL, '', '', '', '', '', NULL, 1, 5, 90, 4, NULL, 1, NULL, '2022-12-10 06:04:13', '0000-00-00 00:00:00'),
(923, 'Valor entregue a Jacinta.', 1, '2022-11-28', 12, 2022, 13, 400.00, NULL, 400.00, 3, 10, NULL, '', '', '', '', '', NULL, 1, 5, 91, 4, NULL, 1, NULL, '2022-12-10 06:05:19', '0000-00-00 00:00:00'),
(924, 'Valor entregue a Miquelina.', 1, '2022-11-28', 12, 2022, 13, 400.00, NULL, 400.00, 3, 10, NULL, '', '', '', '', '', NULL, 1, 5, 92, 4, NULL, 1, NULL, '2022-12-10 06:12:06', '0000-00-00 00:00:00'),
(925, 'Valor entregue ao Oscar.', 1, '2022-12-02', 12, 2022, 13, 400.00, NULL, 400.00, 3, 10, NULL, '', '', '', '', '', NULL, 1, 5, 84, 4, NULL, 1, NULL, '2022-12-10 06:24:52', '0000-00-00 00:00:00'),
(926, 'Valor entregue ao Rosario.', 1, '2022-12-02', 12, 2022, 13, 800.00, NULL, 800.00, 3, 10, NULL, '', '', '', '', '', NULL, 1, 5, 86, 4, NULL, 1, NULL, '2022-12-10 06:28:04', '0000-00-00 00:00:00'),
(927, 'Valor entregue a Fátima.', 1, '2022-12-02', 12, 2022, 13, 400.00, NULL, 400.00, 3, 10, NULL, '', '', '', '', '', NULL, 1, 5, 87, 4, NULL, 1, NULL, '2022-12-10 06:32:35', '0000-00-00 00:00:00'),
(928, 'Valor entregue ao Francisco.', 1, '2022-12-02', 12, 2022, 13, 500.00, NULL, 500.00, 3, 10, NULL, '', '', '', '', '', NULL, 1, 5, 88, 4, NULL, 1, NULL, '2022-12-10 06:40:11', '0000-00-00 00:00:00'),
(929, 'Valor entregue ao Latifo.', 1, '2022-12-02', 12, 2022, 13, 400.00, NULL, 400.00, 3, 10, NULL, '', '', '', '', '', NULL, 1, 5, 89, 4, NULL, 1, NULL, '2022-12-10 06:43:34', '0000-00-00 00:00:00'),
(930, 'Valor entregue ao Agostinho.\r\n', 1, '2022-12-02', 12, 2022, 13, 100.00, NULL, 100.00, 3, 10, NULL, '', '', '', '', '', NULL, 1, 5, 90, 4, NULL, 1, NULL, '2022-12-10 06:45:25', '0000-00-00 00:00:00'),
(931, 'Valor entregue Jacinta.', 1, '2022-12-02', 12, 2022, 13, 400.00, NULL, 400.00, 3, 10, NULL, '', '', '', '', '', NULL, 1, 5, 91, 4, NULL, 1, NULL, '2022-12-10 06:51:04', '0000-00-00 00:00:00'),
(932, 'Valor entregue ao Miquelina.', 1, '2022-12-02', 12, 2022, 13, 400.00, NULL, 400.00, 3, 10, NULL, '', '', '', '', '', NULL, 1, 5, 92, 4, NULL, 1, NULL, '2022-12-10 06:52:08', '0000-00-00 00:00:00'),
(933, 'Valor entregue ao Armando', 1, '2022-12-02', 12, 2022, 13, 400.00, NULL, 400.00, 3, 4, NULL, '', '', '', '', '', NULL, 1, 5, 80, 4, NULL, 1, NULL, '2022-12-10 06:53:24', '0000-00-00 00:00:00'),
(934, 'Valor entregue a Leticia.', 1, '2022-12-02', 12, 2022, 13, 400.00, NULL, 400.00, 3, 4, NULL, '', '', '', '', '', NULL, 1, 5, 81, 4, NULL, 1, NULL, '2022-12-10 06:54:41', '0000-00-00 00:00:00'),
(935, 'Valor entregue a Manuela.', 1, '2022-12-02', 12, 2022, 13, 100.00, NULL, 100.00, 3, 4, NULL, '', '', '', '', '', NULL, 1, 5, 82, 4, NULL, 1, NULL, '2022-12-10 06:56:07', '0000-00-00 00:00:00'),
(936, 'Valor entregue ao Osvaldo.', 1, '2022-12-02', 12, 2022, 13, 600.00, NULL, 600.00, 3, 4, NULL, '', '', '', '', '', NULL, 1, 5, 83, 4, NULL, 1, NULL, '2022-12-10 06:57:22', '0000-00-00 00:00:00'),
(937, 'Compra efetuada pelo Rosario.', 1, '2022-11-29', 12, 2022, 17, 1809.30, 190.70, 2000.00, 3, 4, NULL, '', '', '', '', '', NULL, 1, 7, 15, 4, NULL, 5, 1, '2022-12-10 07:00:59', '0000-00-00 00:00:00'),
(938, 'Transporte Rosario foi fazer recolha de valores nas farmacias.', 1, '2022-11-29', 12, 2022, 13, 650.00, NULL, 650.00, 3, 1, NULL, '', '', '93', '', '', NULL, 1, 108, 129, 4, NULL, 1, NULL, '2022-12-10 07:06:21', '0000-00-00 00:00:00'),
(939, 'Compra efetuada pelo Rosario.', 1, '2022-11-29', 12, 2022, 17, 1929.23, 70.77, 2000.00, 3, 2, NULL, '', '', '', '', '', NULL, 1, 7, 15, 4, NULL, 5, 1, '2022-12-10 07:10:01', '0000-00-00 00:00:00'),
(940, 'Pagamento de tinta  PVA light Grey 4L na OKAPI,Lda, pagamento efetuado pela Dolca.', 1, '2022-11-29', 12, 2022, 13, 940.17, 159.83, 1100.00, 3, 1, NULL, '', '', '1479 733', '', '', NULL, 1, 99, 99, 4, NULL, 3, 2, '2022-12-10 07:20:48', '0000-00-00 00:00:00'),
(941, 'Valor entregue ao Silvestre.', 1, '2022-12-02', 12, 2022, 13, 400.00, NULL, 400.00, 3, 4, NULL, '', '', '', '', '', NULL, 1, 5, 85, 4, NULL, 1, NULL, '2022-12-10 07:23:00', '0000-00-00 00:00:00'),
(942, 'Despesa de Refeiçao e transporte para o pintor que estava pintar a farmacia Academica.', 1, '2022-11-29', 12, 2022, 13, 550.00, NULL, 550.00, 3, 1, NULL, '', '', '95', '', '', NULL, 1, 99, 99, 4, NULL, 1, 5, '2022-12-10 07:24:42', '0000-00-00 00:00:00'),
(943, 'Valor entregue ao Armando.', 1, '2022-11-28', 12, 2022, 13, 400.00, NULL, 400.00, 3, 4, NULL, '', '', '', '', '', NULL, 1, 5, 80, 4, NULL, 1, NULL, '2022-12-10 07:25:56', '0000-00-00 00:00:00'),
(944, 'Valor entregue a Leticia.', 1, '2022-11-28', 12, 2022, 13, 400.00, NULL, 400.00, 3, 4, NULL, '', '', '', '', '', NULL, 1, 5, 81, 4, NULL, 1, NULL, '2022-12-10 07:28:51', '0000-00-00 00:00:00'),
(945, 'valor enviado para o Milton fazer o pagamento da reparaçao da janela da residencia costa do sol.', NULL, '2022-11-29', 12, 2022, 13, 500.00, NULL, 500.00, 6, 3, NULL, '', '', '96', '', '845553809', NULL, NULL, 99, 99, 4, NULL, 1, 5, '2022-12-10 07:29:26', '0000-00-00 00:00:00'),
(946, 'Valor enviado para o Milton fazer o pagamento dos livros de VD para farmacia Kenneth Kaunda.', NULL, '2022-11-29', 12, 2022, 21, 3750.00, NULL, 3750.00, 6, 2, NULL, '', '', '002099', '', '845533809', NULL, NULL, 9, 147, NULL, NULL, 1, 1, '2022-12-10 07:35:00', '0000-00-00 00:00:00'),
(947, 'Compra efetuada pela Dolca.', 1, '2022-11-29', 12, 2022, 9, 2700.00, NULL, 2700.00, 3, 1, NULL, '', '', '0106862', '', '', NULL, 1, 100, 139, 4, NULL, 3, 1, '2022-12-10 07:38:29', '0000-00-00 00:00:00'),
(948, 'Compra efetuada pela Dolca.', 1, '2022-11-29', 12, 2022, 9, 2700.00, NULL, 2700.00, 3, 1, NULL, '', '', '0106862', '', '', NULL, 1, 100, 139, 4, NULL, 3, 1, '2022-12-10 07:41:15', '0000-00-00 00:00:00'),
(949, 'Compra de Bilhete para Rosario, Beira-Maputo.', 1, '2022-11-29', 12, 2022, 28, 2500.00, NULL, 2500.00, 3, 1, NULL, '', '', '021172', '', '', NULL, 1, 7, 11, 4, NULL, 1, 1, '2022-12-10 07:45:42', '0000-00-00 00:00:00'),
(950, 'Despesa de Refeiçao Rosario Almoço e Jantar.', 1, '2022-11-29', 12, 2022, 13, 1000.00, NULL, 1000.00, 3, 1, NULL, '', '', '98', '', '', NULL, 1, 101, 165, 4, NULL, 1, 5, '2022-12-10 07:48:33', '0000-00-00 00:00:00'),
(951, 'Compra efetuada pelo Rosario.', 1, '2022-11-29', 12, 2022, 59, 10470.09, 1779.91, 12250.00, 3, 4, NULL, '', '', '2316', '', '', NULL, 1, 7, 8, 4, NULL, 1, 1, '2022-12-10 07:57:07', '0000-00-00 00:00:00'),
(952, 'Transporte Rosario foi para aeroporto levantar medicamento que vieram do Maputo de voo, as 22hor', 1, '2022-11-29', 12, 2022, 13, 600.00, NULL, 600.00, 3, 1, NULL, '', '', '99', '', '', NULL, 1, 7, 11, 4, NULL, 1, 5, '2022-12-10 08:01:26', '0000-00-00 00:00:00'),
(953, 'Valor entregue a Manuela.', 1, '2022-11-28', 12, 2022, 13, 100.00, NULL, 100.00, 3, 4, NULL, '', '', '', '', '', NULL, 1, 5, 82, 4, NULL, 1, NULL, '2022-12-10 08:07:50', '0000-00-00 00:00:00'),
(954, 'Valor entregue ao Osvaldo.', 1, '2022-11-28', 12, 2022, 13, 600.00, NULL, 600.00, 3, 4, NULL, '', '', '', '', '', NULL, 1, 5, 83, 4, NULL, 1, NULL, '2022-12-10 08:11:49', '0000-00-00 00:00:00'),
(955, 'Compra efetuada pelo Rosario.', 1, '2022-11-30', 12, 2022, 17, 913.43, 86.57, 1000.00, 3, 2, NULL, '', '', '', '', '', NULL, 1, 7, 15, 4, NULL, 5, 1, '2022-12-10 08:12:47', '0000-00-00 00:00:00'),
(956, 'Valor entregue ao Silvestre.', 1, '2022-11-28', 12, 2022, 13, 400.00, NULL, 400.00, 3, 4, NULL, '', '', '', '', '', NULL, 1, 5, 85, 4, NULL, 1, NULL, '2022-12-10 08:14:40', '0000-00-00 00:00:00'),
(957, 'Compra efetuada pelo Rosario.', 1, '2022-11-30', 12, 2022, 17, 910.09, 89.91, 1000.00, 3, 19, NULL, '', '', '', '', '', NULL, 1, 7, 15, 4, NULL, 5, 1, '2022-12-10 08:20:53', '0000-00-00 00:00:00'),
(958, 'Transporte Rosario foi fazer recolha de valores nas farmacias.', 1, '2022-11-30', 12, 2022, 13, 650.00, NULL, 650.00, 3, 1, NULL, '', '', '99', '', '', NULL, 1, 108, 129, 4, NULL, 1, 5, '2022-12-10 08:30:04', '0000-00-00 00:00:00'),
(959, 'Valor enviado para o m-pesa da FME Kenneth Kaunda, com o custo de transação de 10,00MT.\r\n', NULL, '2022-11-28', 12, 2022, 13, 510.00, NULL, 510.00, 6, 2, NULL, '', '', '', '', '855154553', NULL, NULL, 5, 63, 2, NULL, 1, NULL, '2022-12-10 09:05:27', '0000-00-00 00:00:00'),
(960, 'Valor enviado para o Ambasse, com o custo de transação de 100,00MT.', NULL, '2022-11-01', 12, 2022, 10, 8100.00, NULL, 8100.00, 6, 1, NULL, '', '', '06435', '', '869478628', NULL, NULL, 7, 13, 2, NULL, 1, 1, '2022-12-10 09:09:48', '0000-00-00 00:00:00'),
(961, 'Compra efetuada pelo Rosario.', 1, '2022-11-01', 12, 2022, 17, 1821.14, 178.86, 2000.00, 3, 19, 7, '', '', '', NULL, '', NULL, 1, 7, 15, 4, 0, 5, 1, '2022-12-10 09:14:12', '2022-12-10 11:17:34'),
(962, 'Valor enviado para Diana.', NULL, '2022-11-01', 12, 2022, 12, 3000.00, NULL, 3000.00, 6, 1, NULL, '', '', '000954', '', '849551617', NULL, NULL, 100, 117, 2, NULL, 3, 1, '2022-12-10 09:15:55', '0000-00-00 00:00:00'),
(963, 'Compra efectuada pelo Rosario.', 1, '2022-11-02', 12, 2022, 17, 2713.95, 286.05, 3000.00, 3, 10, NULL, '', '', '', '', '', NULL, 1, 7, 15, 4, NULL, 5, 1, '2022-12-10 09:21:10', '0000-00-00 00:00:00'),
(964, 'Valor enviado para o Ambasse com o custo de transação e levantamento de 50,00MT.', NULL, '2022-11-02', 12, 2022, 12, 3050.00, NULL, 3050.00, 6, 1, NULL, '', '', '000987', '', '869478628', NULL, NULL, 100, 116, 2, NULL, 3, 1, '2022-12-10 09:23:55', '0000-00-00 00:00:00'),
(965, 'Valor enviado para o Milton, com custo de transação e levantamento de 50,00MT.', NULL, '2022-11-02', 12, 2022, 12, 3050.00, NULL, 3050.00, 6, 1, NULL, '', '', '016448', '', '845533809', NULL, NULL, 100, 117, 2, NULL, 3, 1, '2022-12-10 09:27:06', '0000-00-00 00:00:00'),
(966, 'Valor enviado para o Milton, com o custo de transação de 10,00MT.', NULL, '2022-11-02', 12, 2022, 5, 1010.00, NULL, 1010.00, 6, 1, NULL, '', '', '09549567', '', '845533809', NULL, NULL, 3, 51, 2, NULL, 1, 1, '2022-12-10 09:29:25', '0000-00-00 00:00:00'),
(967, 'Compra efectuada pelo Rosario', 1, '2022-11-04', 12, 2022, 17, 910.57, 89.43, 1000.00, 3, 4, NULL, '', '', '', '', '', NULL, 1, 7, 15, 4, NULL, 5, 1, '2022-12-10 09:33:04', '0000-00-00 00:00:00'),
(968, 'Valor enviado para o Milton, com o custo de transação de 10,00MT', NULL, '2022-11-07', 12, 2022, 5, 1010.00, NULL, 1010.00, 6, 1, NULL, '', '', '100', '', '845533809', NULL, NULL, 3, 49, 2, NULL, 1, 5, '2022-12-10 09:39:50', '0000-00-00 00:00:00'),
(969, 'Valor enviado para o Milton, com o custo de transação de 15,00MT.', NULL, '2022-11-03', 12, 2022, 5, 1015.00, NULL, 1015.00, 6, 2, NULL, '', '', '095495795', '', '845533809', NULL, NULL, 3, 100, 2, NULL, 1, 1, '2022-12-10 09:42:37', '0000-00-00 00:00:00'),
(970, 'Valor enviado para o Milton, com o custo de transação de 15,00MT.', NULL, '2022-11-03', 12, 2022, 5, 1015.00, NULL, 1015.00, 6, 3, NULL, '', '', '095495795', '', '845533809', NULL, NULL, 3, 100, 2, NULL, 1, 1, '2022-12-10 09:44:52', '0000-00-00 00:00:00'),
(971, 'Valor enviado para o Milton.', NULL, '2022-11-08', 12, 2022, 12, 1750.00, NULL, 1750.00, 6, 3, NULL, '', '', '016473', '', '845533809', NULL, NULL, 8, 146, 2, NULL, 3, 1, '2022-12-10 09:53:45', '0000-00-00 00:00:00'),
(972, 'Valor enviado para o Ambasse, com custo de transação e levantamento de 50,00MT.', NULL, '2022-11-10', 12, 2022, 12, 3050.00, NULL, 3050.00, 6, 1, NULL, '', '', '000212', '', '844396996', NULL, NULL, 100, 116, 2, NULL, 3, 1, '2022-12-10 10:02:13', '0000-00-00 00:00:00'),
(973, 'Valor enviado para Monica, com o custo de transação de 30,00MT.', NULL, '2022-11-12', 12, 2022, 15, 2030.00, NULL, 2030.00, 6, 3, NULL, '', '', '101', '', '876765939', NULL, NULL, 105, 157, 2, NULL, 1, 5, '2022-12-10 10:06:27', '0000-00-00 00:00:00'),
(974, 'Despesa de refeição da Dolca e do Patrício( trabalho na FME Cristal Box).', 1, '2022-11-12', 12, 2022, 40, 680.00, NULL, 680.00, 3, 1, NULL, '', '', '002720', '', '', NULL, 1, 99, 99, 4, NULL, 1, 1, '2022-12-10 10:18:01', '0000-00-00 00:00:00'),
(975, 'Valor enivado para o Milton.', NULL, '2022-11-13', 12, 2022, 27, 4358.97, 741.03, 5100.00, 6, 3, NULL, '', '', '1111661', '', '845533809', NULL, NULL, 7, 9, 4, NULL, 1, 2, '2022-12-10 19:28:28', '0000-00-00 00:00:00'),
(976, 'Valor tranferido para o Albino.', NULL, '2022-11-15', 12, 2022, 44, 1000.00, NULL, 1000.00, 6, 1, NULL, '', '', '000902', '', '845629961', NULL, NULL, 100, 115, NULL, NULL, 3, 1, '2022-12-10 21:01:55', '0000-00-00 00:00:00'),
(977, 'Envio de Medicamento Maputo-Beira, valor enviado para o Ambasse.', NULL, '2022-11-16', 12, 2022, 10, 7000.00, NULL, 7000.00, 6, 1, NULL, '', '', '06436', '', '869478628', NULL, NULL, 7, 13, 4, NULL, 1, 1, '2022-12-10 21:09:11', '0000-00-00 00:00:00'),
(978, 'Valor transferido para o Albino com o custo da transaçao de 30,00MT', NULL, '2022-11-17', 12, 2022, 12, 1030.00, NULL, 1030.00, 6, 1, NULL, '', '', '0003251', '', '855629961', NULL, NULL, 100, 115, 2, NULL, 3, 1, '2022-12-10 21:24:23', '0000-00-00 00:00:00'),
(979, 'Valor enviado para o Milton com o custo da transaçao de 30,00MT.', NULL, '2022-11-18', 12, 2022, 57, 2000.00, NULL, 2000.00, 6, 1, NULL, '', '', '274351', '', '845533809', NULL, NULL, 100, 117, 2, NULL, 3, 1, '2022-12-10 21:30:21', '0000-00-00 00:00:00'),
(980, 'Compra efetuada pelo Milton.', NULL, '2022-11-19', 12, 2022, 19, 1000.00, NULL, 1000.00, 6, 1, NULL, '', '', '095495816', '', '845533809', NULL, NULL, 3, 50, 4, NULL, 1, 2, '2022-12-10 21:38:56', '0000-00-00 00:00:00'),
(981, 'Compra efetuada pelo Milton.', NULL, '2022-11-22', 12, 2022, 42, 4855.50, NULL, 4855.50, 6, 3, NULL, '', '', '187692', '', '845533809', NULL, NULL, 106, 171, 4, NULL, 1, 1, '2022-12-11 05:19:24', '0000-00-00 00:00:00'),
(982, 'Valor enviado para o Milton com o custo da transaçao de 30,00MT', NULL, '2022-11-25', 12, 2022, 20, 530.00, NULL, 530.00, 6, 3, NULL, '', '', '004331', '', '845533809', NULL, NULL, 106, 171, 2, NULL, 1, 1, '2022-12-11 05:38:34', '0000-00-00 00:00:00'),
(983, 'Valor enviado para o Mariano com o custo da transaçao de 30,00MT', NULL, '2022-11-27', 12, 2022, 12, 2030.00, NULL, 2030.00, 6, 1, NULL, '', '', '005831', '', '847876424', NULL, NULL, 100, 117, 2, NULL, 3, 1, '2022-12-11 05:59:29', '0000-00-00 00:00:00'),
(984, 'Pagamento efetuado pelo Milton.', NULL, '2022-11-07', 12, 2022, 13, 2543.38, NULL, 2543.38, 6, 1, NULL, '', '', '02271350034', '', '845533809', NULL, NULL, 103, 132, 4, NULL, 1, 2, '2022-12-11 06:25:05', '0000-00-00 00:00:00'),
(985, 'Compra de Rolos de sistema e de Pos, compra efetuada pelo Milton', NULL, '2022-11-23', 12, 2022, 20, 2700.00, NULL, 2700.00, 6, 3, NULL, '', '', '004304', '', '845533809', NULL, NULL, 106, 171, 4, NULL, 1, 1, '2022-12-11 06:30:25', '0000-00-00 00:00:00'),
(986, 'Valor enviado para o Albino com o custo da transaçao de 30,00MT.', NULL, '2022-11-15', 12, 2022, 12, 1030.00, NULL, 1030.00, 6, 1, NULL, '', '', '0003217', '', '845629961', NULL, NULL, 100, 115, 2, NULL, 3, 1, '2022-12-11 06:34:50', '0000-00-00 00:00:00'),
(987, 'Envio de Medicamento Maputo-Beira. Valor enviado para o Milton Com o custo da Transaçao de 150,00MT', NULL, '2022-11-22', 12, 2022, 10, 7150.00, NULL, 7150.00, 6, 1, NULL, '', '', '0006432', '', '845533806', NULL, NULL, 7, 13, 2, NULL, 1, 1, '2022-12-11 06:40:03', '0000-00-00 00:00:00'),
(988, 'Envio de Medicamento Maputo-Beira,valor enviado para o Ambasse com o custo da transaçao de 50,00MT.', NULL, '2022-11-04', 12, 2022, 10, 3050.00, NULL, 3050.00, 6, 1, NULL, '', '', '0006434', '', '855154553', NULL, NULL, 7, 13, 2, NULL, 1, 1, '2022-12-11 06:47:01', '0000-00-00 00:00:00'),
(989, 'Valor enviado para o Milton com o custo da transaçao de 30,00MT', NULL, '2022-11-28', 12, 2022, 12, 2030.00, NULL, 2030.00, 6, 1, NULL, '', '', '0001217', '', '', NULL, NULL, 100, 116, 2, NULL, 3, 1, '2022-12-11 06:51:14', '0000-00-00 00:00:00'),
(990, 'Valor enviado para o Milton com o custo da Transaçao de 30,00MT', NULL, '2022-11-29', 12, 2022, 57, 1530.00, NULL, 1530.00, 6, 1, NULL, '', '', '272567', '', '845533809', NULL, NULL, 100, 117, 2, NULL, 3, 1, '2022-12-11 06:54:38', '0000-00-00 00:00:00'),
(991, 'Valor enviado para o Milton com o custo da transaçao de 30,00MT', NULL, '2022-11-21', 12, 2022, 12, 1530.00, NULL, 1530.00, 6, 1, NULL, '', '', '016438', '', '845533809', NULL, NULL, 100, 117, 2, NULL, 3, 1, '2022-12-11 06:59:42', '0000-00-00 00:00:00'),
(992, 'Valor enviado para o Albino com o custo da transaçao de 30,00MT.', NULL, '2022-11-19', 12, 2022, 12, 1030.00, NULL, 1030.00, 6, 1, NULL, '', '', '0134608', '', '845629961', NULL, NULL, 100, 115, 2, NULL, 3, 1, '2022-12-11 07:05:02', '0000-00-00 00:00:00'),
(993, 'Valor enviado para o Milton com o custo da transaçao de 10,00MT', NULL, '2022-11-20', 12, 2022, 12, 510.00, NULL, 510.00, 6, 1, NULL, '', '', '0134437', '', '845533809', NULL, NULL, 100, 117, 4, NULL, 3, 1, '2022-12-11 07:09:08', '0000-00-00 00:00:00'),
(994, 'Valor enviado para o Ambasse com o custo da transaçao de 50,00MT', NULL, '2022-11-01', 12, 2022, 12, 2050.00, NULL, 2050.00, 6, 1, NULL, '', '', '043136', '', '869478628', NULL, NULL, 100, 116, 2, NULL, 3, 1, '2022-12-11 07:28:14', '0000-00-00 00:00:00'),
(995, 'Compra efetuada pelo Milton.', NULL, '2022-11-01', 12, 2022, 42, 2300.00, NULL, 2300.00, 6, 3, NULL, '', '', '187692', '', '845533809', NULL, NULL, 106, 171, 4, NULL, 1, 1, '2022-12-11 07:37:47', '0000-00-00 00:00:00'),
(996, 'Compra efetuada pelo Milton.', NULL, '2022-11-01', 12, 2022, 42, 2300.00, NULL, 2300.00, 6, 3, 106, '', '', '187692', NULL, '845533809', NULL, 0, 106, 171, 4, 0, 1, 1, '2022-12-11 07:41:25', '2022-12-13 13:39:56'),
(997, 'compra efetuada pelo Rosario.', 1, '2022-11-04', 12, 2022, 59, 28427.05, 5822.05, 34249.10, 3, 10, NULL, '', '', '1937', '', '', NULL, 1, 106, 130, 4, NULL, 1, 1, '2022-12-11 08:16:00', '0000-00-00 00:00:00'),
(998, 'Compra efetuada pela Diana, Recarga da Movitel.', NULL, '2022-11-07', 12, 2022, 64, 1000.00, NULL, 1000.00, 6, 1, NULL, '', '', '004492', '', '849551617', NULL, NULL, 3, 57, 4, NULL, 1, 1, '2022-12-11 09:18:21', '0000-00-00 00:00:00'),
(999, 'Pagamento estadia do Osvaldo na Dona Lucia, 30 dias.', 1, '2022-11-30', 12, 2022, 13, 25500.00, NULL, 25500.00, 3, 1, NULL, '', '', '102', '', '', NULL, 1, 99, 99, 4, NULL, 1, 5, '2022-12-11 10:02:57', '0000-00-00 00:00:00'),
(1000, 'Valor enviado para o Mpesa da FME Kenneth Kaunda, com o custo de transação de 10,00MT.', NULL, '2022-11-28', 12, 2022, 13, 310.00, NULL, 310.00, 6, 2, NULL, '', '', '', '', '855154553', NULL, NULL, 5, 65, 2, NULL, 1, NULL, '2022-12-11 10:52:31', '0000-00-00 00:00:00'),
(1001, 'Valor entregue a Sra Eufrasia.', 1, '2022-11-24', 12, 2022, 26, 2700.00, NULL, 2700.00, 3, 1, NULL, '', '', '92599', '', '', NULL, 1, 100, 120, 4, NULL, 3, 1, '2022-12-12 08:45:32', '0000-00-00 00:00:00'),
(1002, 'Valor enviado para o Mpesa do Taverna. Pagamento de refeição de dois dias consecutivos. ', NULL, '2022-11-30', 12, 2022, 22, 1440.00, NULL, 1440.00, 6, 17, NULL, '', '', '', '', '850685164', NULL, NULL, 102, 145, 2, NULL, 1, 1, '2022-12-12 09:31:15', '0000-00-00 00:00:00'),
(1003, 'Compra efetuada pelo Rosário.', 1, '2022-11-16', 12, 2022, 17, 1821.14, 178.86, 2000.00, 3, 2, NULL, '', '', '', '', '', NULL, 1, 7, 15, 4, NULL, 5, 1, '2022-12-12 10:45:26', '0000-00-00 00:00:00'),
(1004, 'Compra de Vasos e Flores para a Sra. Marcela.', 1, '2022-11-30', 12, 2022, 13, 3050.00, NULL, 3050.00, 3, 1, NULL, '', '', '', '', '', NULL, 1, 99, 99, 4, NULL, 1, NULL, '2022-12-12 10:55:17', '0000-00-00 00:00:00'),
(1005, 'Valor enviado para a Sra Ana Bela, com o custo de transação e Levantamento de 15,00MT', NULL, '2022-11-14', 12, 2022, 13, 515.00, NULL, 515.00, 6, 19, NULL, '', '', '', '', '851588285', NULL, NULL, 104, 124, 2, NULL, 1, NULL, '2022-12-12 11:15:34', '0000-00-00 00:00:00'),
(1006, 'Valor enviado para a Sra. Ana bela, com o custo de transação de e levantamento de 15,00MT.', NULL, '2022-11-14', 12, 2022, 13, 515.00, NULL, 515.00, 6, 19, NULL, '', '', '', '', '851588285', NULL, NULL, 104, 125, 2, NULL, 1, NULL, '2022-12-12 11:17:42', '0000-00-00 00:00:00'),
(1007, 'Valor enviado para a Sra Ana Bela, com o custo de transação e levantamento de 20,00MT.', NULL, '2022-11-14', 12, 2022, 13, 520.00, NULL, 520.00, 6, 19, NULL, '', '', '', '', '851588285', NULL, NULL, 104, 136, 2, NULL, 1, NULL, '2022-12-12 11:19:58', '0000-00-00 00:00:00'),
(1008, 'Valor enviado para a Sra. Ana Bela, com o custo de transação e levantamento de 15,00MT.', NULL, '2022-11-21', 12, 2022, 13, 515.00, NULL, 515.00, 6, 19, NULL, '', '', '', '', '851588285', NULL, NULL, 104, 124, 2, NULL, 1, NULL, '2022-12-12 11:23:41', '0000-00-00 00:00:00'),
(1009, 'Valor enviado para a Sra Ana Bela, com o custo de transação e levantamento de 15,00MT.', NULL, '2022-11-21', 12, 2022, 13, 515.00, NULL, 515.00, 6, 19, NULL, '', '', '', '', '851588285', NULL, NULL, 104, 125, 2, NULL, 1, NULL, '2022-12-12 11:37:11', '0000-00-00 00:00:00'),
(1010, 'Valor enviado para a Sra Ana Bela, com o custo de transação e levantamento de 20,00MT.', NULL, '2022-11-21', 12, 2022, 13, 520.00, NULL, 520.00, 6, 19, NULL, '', '', '', '', '851588285', NULL, NULL, 104, 136, 2, NULL, 1, NULL, '2022-12-12 11:39:53', '0000-00-00 00:00:00'),
(1011, 'Valor enviado para a Sra Ana Bela com o custo de transação de 15,00MT.', NULL, '2022-11-28', 12, 2022, 13, 515.00, NULL, 515.00, 6, 19, NULL, '', '', '', '', '851588285', NULL, NULL, 104, 124, 2, NULL, 1, NULL, '2022-12-12 11:41:27', '0000-00-00 00:00:00'),
(1012, 'Valor enviado para a Sra Ana Bela, com o custo de transação e levantamento de 15,00MT.', NULL, '2022-11-28', 12, 2022, 13, 515.00, NULL, 515.00, 6, 19, NULL, '', '', '', '', '851588285', NULL, NULL, 104, 125, 2, NULL, 1, NULL, '2022-12-12 11:44:16', '0000-00-00 00:00:00'),
(1013, 'Valor enviado para a Sra Ana Bela, com o custo de transação e levantamento de 20,00MT.', NULL, '2022-11-28', 12, 2022, 13, 520.00, NULL, 520.00, 6, 19, NULL, '', '', '', '', '851588285', NULL, NULL, 104, 136, 2, NULL, 1, NULL, '2022-12-12 11:47:15', '0000-00-00 00:00:00'),
(1014, 'Valor enviado para o Sr. Manuel, com o custo de transação e levantamento de 10,00MT.', NULL, '2022-11-28', 12, 2022, 13, 210.00, NULL, 210.00, 6, 2, NULL, '', '', '', '', '841158201', NULL, NULL, 5, 64, 2, NULL, 1, NULL, '2022-12-12 12:04:24', '0000-00-00 00:00:00'),
(1015, 'Valor enviado para o Sr. Manuel, com o custo de transação e levantamento de 10,00MT.', NULL, '2022-11-28', 12, 2022, 13, 210.00, NULL, 210.00, 6, 2, NULL, '', '', '', '', '841158201', NULL, NULL, 5, 66, 2, NULL, 1, NULL, '2022-12-12 12:05:38', '0000-00-00 00:00:00'),
(1016, 'Valor enviado para o Sr. Manuel, com o custo de transação e levantamento de 10,00MT.', NULL, '2022-11-28', 12, 2022, 13, 210.00, NULL, 210.00, 6, 2, NULL, '', '', '', '', '841158201', NULL, NULL, 5, 67, 2, NULL, 1, NULL, '2022-12-12 12:06:47', '0000-00-00 00:00:00'),
(1017, 'Valor enviado para o Sr. Manuel.', NULL, '2022-11-28', 12, 2022, 13, 200.00, NULL, 200.00, 6, 2, NULL, '', '', '', '', '841158201', NULL, NULL, 5, 68, 2, NULL, 1, NULL, '2022-12-12 12:14:56', '0000-00-00 00:00:00'),
(1018, 'Valor enviado para Lúcia, com o custo de transação de 10,00MT.', NULL, '2022-11-28', 12, 2022, 13, 510.00, NULL, 510.00, 6, 3, NULL, '', '', '', '', '848383477', NULL, NULL, 5, 78, 2, NULL, 1, NULL, '2022-12-12 12:25:08', '0000-00-00 00:00:00'),
(1019, 'Valor enviado para o Milton com o custo de transação de 10,00MT.', NULL, '2022-11-28', 12, 2022, 13, 1010.00, NULL, 1010.00, 6, 3, NULL, '', '', '', '', '845533809', NULL, NULL, 5, 76, 2, NULL, 1, NULL, '2022-12-12 12:33:04', '0000-00-00 00:00:00'),
(1020, 'Compra de Oleo do motor, compra efetuada pelo Milton.', NULL, '2022-11-23', 12, 2022, 65, 2645.00, NULL, 2645.00, 6, 1, NULL, '', '', '004658', '', '845533809', NULL, NULL, 1, 44, 4, NULL, 3, 1, '2022-12-12 17:45:06', '0000-00-00 00:00:00'),
(1021, 'Pagamento efetuado pelo Agostinho.', 1, '2022-11-03', 12, 2022, 46, 3000.00, NULL, 3000.00, 3, 10, NULL, '', '', '80274', '', '', NULL, 1, 7, 16, 4, NULL, 1, 2, '2022-12-12 17:52:00', '0000-00-00 00:00:00'),
(1022, 'Pagamento efetuado pelo Milton.', NULL, '2022-11-09', 12, 2022, 46, 1347.71, NULL, 1347.71, 6, 3, NULL, '', '', '1226', '', '845533809', NULL, NULL, 7, 16, 4, NULL, 1, 2, '2022-12-12 17:58:19', '0000-00-00 00:00:00'),
(1023, 'Compra efetuada pelo Milton.', NULL, '2022-11-04', 12, 2022, 25, 6100.00, 1037.00, 7137.00, 6, 3, NULL, '', '', '517', '', '845533809', NULL, NULL, 7, 8, 4, NULL, 1, 1, '2022-12-12 18:37:18', '0000-00-00 00:00:00'),
(1024, 'Compra efetuada pelo Milton.', NULL, '2022-11-29', 12, 2022, 25, 8365.00, 1422.05, 9787.05, 6, 2, 7, '', '', '537', NULL, '845533809', NULL, 0, 7, 8, 4, 1, 1, 1, '2022-12-12 18:42:15', '2022-12-12 20:54:22'),
(1025, 'Compra efetuada pelo Milton.', NULL, '2022-11-13', 12, 2022, 25, 8365.00, 1422.05, 9787.05, 6, 2, NULL, '', '', '526', '', '845533809', NULL, NULL, 7, 8, 4, NULL, 1, 2, '2022-12-12 18:45:47', '0000-00-00 00:00:00'),
(1026, 'Compra de Toner efetuado pelo Milton.', NULL, '2022-11-21', 12, 2022, 25, 5675.00, 964.75, 6639.75, 6, 3, NULL, '', '', '529', '', '845533806', NULL, NULL, 7, 8, 4, NULL, 1, 1, '2022-12-12 19:07:26', '0000-00-00 00:00:00'),
(1027, 'Pagamento efetuado pelo Rosario.', 1, '2022-11-07', 12, 2022, 13, 29754.02, NULL, 29754.02, 3, 1, NULL, '', '', '76126978', '', '', NULL, 1, 10, 163, 4, NULL, 1, 2, '2022-12-12 19:45:05', '0000-00-00 00:00:00'),
(1028, 'Pagamento efetuado pelo Milton, fez o pagamento online.', NULL, '2022-11-09', 12, 2022, 46, 2006.00, NULL, 2006.00, 6, 3, NULL, '', '', '31005248055', '', '845533809', NULL, NULL, 7, 16, 4, NULL, 1, 2, '2022-12-12 20:19:06', '0000-00-00 00:00:00'),
(1029, 'Valor enviado para o Mpesa da Farmácia.', NULL, '2022-11-06', 12, 2022, 15, 1000.00, NULL, 1000.00, 6, 2, NULL, '', '', '', '', '855154553', NULL, NULL, 111, 156, 2, NULL, 1, 5, '2022-12-13 06:14:52', '0000-00-00 00:00:00'),
(1030, 'Valor enviado para o Mpesa da Farmácia.', NULL, '2022-11-13', 12, 2022, 15, 1000.00, NULL, 1000.00, 6, 2, NULL, '', '', '', '', '855154553', NULL, NULL, 111, 156, 2, NULL, 1, 5, '2022-12-13 06:50:19', '0000-00-00 00:00:00'),
(1031, 'Valor enviado para o Mpesa da FME Kenneth Kaunda.', NULL, '2022-11-20', 12, 2022, 15, 1000.00, NULL, 1000.00, 6, 2, NULL, '', '', '', '', '855154553', NULL, NULL, 111, 156, 2, NULL, 1, 5, '2022-12-13 06:57:34', '0000-00-00 00:00:00'),
(1032, 'Valor enviado para o mpesa da FME Kenneth Kaunda.', NULL, '2022-11-27', 12, 2022, 15, 1000.00, NULL, 1000.00, 6, 2, NULL, '', '', '', '', '855154553', NULL, NULL, 111, 156, 2, NULL, 1, 5, '2022-12-13 07:02:33', '0000-00-00 00:00:00'),
(1033, 'Valor enviado para Geni Mafassitela.', NULL, '2022-11-06', 12, 2022, 15, 1000.00, NULL, 1000.00, 6, 3, NULL, '', '', '', '', '8436314556', NULL, NULL, 111, 156, 2, NULL, 1, 5, '2022-12-13 07:10:24', '0000-00-00 00:00:00'),
(1034, 'Valor enviado para Geni Mafassitela.', NULL, '2022-11-13', 12, 2022, 15, 1000.00, NULL, 1000.00, 6, 3, NULL, '', '', '', '', '843631456', NULL, NULL, 111, 156, 2, NULL, 1, 5, '2022-12-13 07:16:50', '0000-00-00 00:00:00'),
(1035, 'Valor enviado para o Milton.', NULL, '2022-11-20', 12, 2022, 13, 1000.00, NULL, 1000.00, 6, 3, NULL, '', '', '', '', '845533809', NULL, NULL, 111, 156, 2, NULL, 1, 5, '2022-12-13 07:20:09', '0000-00-00 00:00:00'),
(1036, 'Valor enviado para Milton.', NULL, '2022-11-27', 12, 2022, 15, 1000.00, NULL, 1000.00, 6, 3, NULL, '', '', '', '', '845533809', NULL, NULL, 111, 156, 2, NULL, 1, 5, '2022-12-13 07:21:49', '0000-00-00 00:00:00'),
(1037, 'Valor enviado para Milton.', NULL, '2022-11-28', 12, 2022, 13, 500.00, NULL, 500.00, 6, 3, NULL, '', '', '', '', '845533809', NULL, NULL, 5, 73, 2, NULL, 1, NULL, '2022-12-13 08:33:06', '0000-00-00 00:00:00'),
(1038, 'primeiro  inserte', 1, '2023-01-17', 1, 2023, 2, 1000.00, NULL, 1000.00, 3, 1, 105, '', '', '', NULL, '', NULL, 1, 105, 157, 4, 1, NULL, NULL, '2023-01-17 08:11:19', '2023-01-17 11:16:32'),
(1039, 'cfd', NULL, '2023-01-02', 1, 2023, 5, 1000.00, NULL, 1000.00, 2, 12, NULL, '', '', '232', '', '', NULL, NULL, 6, 6, 2, 1, NULL, 2, '2023-01-23 08:40:08', '0000-00-00 00:00:00'),
(1040, 'Teste', 1, '2023-01-23', 1, 2023, 42, 100.00, NULL, 100.00, 3, 3, NULL, '', '', '123', '', '', NULL, 1, 5, 70, 4, 1, NULL, 2, '2023-01-23 08:41:25', '0000-00-00 00:00:00'),
(1041, 'dwew', NULL, '2023-01-23', 1, 2023, 5, 100.00, NULL, 100.00, 6, 10, NULL, '', '', '123', '', '12312', NULL, NULL, 6, 6, 2, 1, NULL, 2, '2023-01-23 08:43:57', '0000-00-00 00:00:00'),
(1042, 'wqew', 1, '2023-01-23', 1, 2023, 1, 1000.00, NULL, 1000.00, 3, 12, NULL, '', '', '123', '', '', NULL, 1, 6, 6, 4, 1, NULL, 2, '2023-01-23 08:48:47', '0000-00-00 00:00:00'),
(1043, 'weew', 1, '2023-01-02', 1, 2023, 5, 100.00, NULL, 100.00, 3, 12, NULL, '', '', '123', '', '', NULL, 1, 6, 6, 4, 1, NULL, 2, '2023-01-23 08:52:03', '0000-00-00 00:00:00'),
(1044, 'we', 1, '2023-01-15', 1, 2023, 3, 1000.00, NULL, 1000.00, 3, 2, NULL, '', '', '12343', '', '', NULL, 1, 1, 42, 4, 1, NULL, 2, '2023-01-23 08:58:49', '0000-00-00 00:00:00'),
(1045, 'qweqw', 1, '2023-01-02', 1, 2023, 1, 1000.00, NULL, 1000.00, 3, 13, NULL, '', '', '1221', '', '', NULL, 1, 7, 14, 4, 1, NULL, 2, '2023-01-23 09:01:45', '0000-00-00 00:00:00'),
(1046, 'dewe', 1, '2023-01-16', 1, 2023, 4, 1000.00, NULL, 1000.00, 3, 2, NULL, '', '', '1212', '', '', NULL, 1, 2, 18, 4, 1, NULL, 2, '2023-01-23 09:02:57', '0000-00-00 00:00:00'),
(1047, 'wq', 1, '2023-01-02', 1, 2023, 3, 100.00, NULL, 100.00, 3, 2, NULL, '', '', '121', '', '', NULL, 1, 2, 18, 4, 1, NULL, NULL, '2023-01-23 09:04:37', '0000-00-00 00:00:00'),
(1048, 'ewwe', 1, '2023-01-16', 1, 2023, 5, 100.00, NULL, 100.00, 3, 3, NULL, '', '', '121', '', '', NULL, 1, 2, 21, 4, 1, NULL, 2, '2023-01-23 09:06:04', '0000-00-00 00:00:00'),
(1049, 'wqqw', 1, '2023-01-23', 1, 2023, 4, 100.00, NULL, 100.00, 3, 2, NULL, '', '', '1313', '', '', NULL, 1, 2, 18, 4, 1, NULL, NULL, '2023-01-23 09:07:05', '0000-00-00 00:00:00'),
(1050, 'wqew', 1, '2023-01-02', 1, 2023, 3, 1000.00, NULL, 1000.00, 3, 2, NULL, '', '', '1213', '', '', NULL, 1, 2, 18, 4, 1, NULL, NULL, '2023-01-23 09:16:09', '0000-00-00 00:00:00'),
(1051, 'wq', 1, '2023-01-23', 1, 2023, 5, 1000.00, NULL, 1000.00, 3, 1, NULL, '', '', '123', '', '', NULL, 1, 1, 42, 4, 1, NULL, NULL, '2023-01-23 09:18:17', '0000-00-00 00:00:00'),
(1052, 'wedw', 1, '2023-01-23', 1, 2023, 5, 100.00, NULL, 100.00, 3, 1, NULL, '', '', '1230', '', '', NULL, 1, 1, 42, 4, 1, NULL, NULL, '2023-01-23 09:21:09', '0000-00-00 00:00:00'),
(1053, 'Tetes 22', 1, '2023-01-23', 1, 2023, 4, 50.00, NULL, 50.00, 3, 1, NULL, '', '', '123', '', '', NULL, 1, 3, 50, 4, 1, NULL, NULL, '2023-01-23 09:27:50', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `factura`
--

CREATE TABLE `factura` (
  `id` int(11) NOT NULL,
  `fornecedor_id` int(11) NOT NULL,
  `nr_factura` varchar(20) NOT NULL,
  `valor` double(12,2) NOT NULL,
  `data_factura` date NOT NULL,
  `centro_custo_id` int(11) NOT NULL,
  `tipo_factura_id` int(11) NOT NULL,
  `saldo` double(12,2) NOT NULL,
  `estado` enum('1','2') NOT NULL DEFAULT '1' COMMENT '1 => aberto, 2 => fechado',
  `desconto` double(12,2) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `update_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `factura`
--

INSERT INTO `factura` (`id`, `fornecedor_id`, `nr_factura`, `valor`, `data_factura`, `centro_custo_id`, `tipo_factura_id`, `saldo`, `estado`, `desconto`, `created_at`, `update_at`) VALUES
(1, 2, 'F.M.E-20/2023', 50000.00, '2023-01-31', 10, 2, 49300.00, '1', 10.00, '2023-02-01 13:26:46', '2023-03-03 16:39:10'),
(2, 2, 'F.M.E-01/2023', 200000.00, '2023-02-01', 3, 1, 199780.00, '1', 20.00, '2023-02-01 13:27:49', '2023-03-08 08:51:04'),
(3, 4, 'F.M.E-02/2022', 2000.00, '2023-02-01', 3, 2, 1850.00, '1', 0.00, '2023-02-02 09:31:21', '2023-02-02 09:38:31'),
(4, 4, '02/2022', 1500.00, '2023-02-01', 3, 1, 1450.00, '1', 30.00, '2023-02-02 09:35:21', '2023-03-08 09:26:24'),
(5, 1, 'F.M.E-13/2023', 5000.00, '2023-02-13', 1, 1, 2980.00, '1', 20.00, '2023-02-13 15:11:35', '2023-02-13 16:13:22'),
(6, 3, 'F.M.E-13', 20000.00, '2023-02-13', 1, 1, 19690.00, '1', 10.00, '2023-02-13 15:12:08', '2023-02-21 10:04:28'),
(7, 5, 'F.M.E-13-2023', 10000.00, '2023-02-13', 1, 1, 10000.00, '1', 10.00, '2023-02-13 15:12:38', '0000-00-00 00:00:00'),
(8, 1, '09876', 20.00, '2023-03-08', 2, 2, 20.00, '1', 20.00, '2023-03-08 08:46:37', '2023-03-08 08:47:00'),
(9, 4, 'F.M.E-08/2023', 1500.00, '2023-03-01', 4, 1, 1300.00, '1', 0.00, '2023-03-08 09:24:46', '2023-03-08 09:26:24');

-- --------------------------------------------------------

--
-- Table structure for table `factura_cliente`
--

CREATE TABLE `factura_cliente` (
  `id` int(11) NOT NULL,
  `cliente_id` int(11) NOT NULL,
  `nr_factura` varchar(50) NOT NULL,
  `valor` double(12,2) NOT NULL,
  `data` date NOT NULL,
  `tipo_factura_id` int(11) NOT NULL,
  `estado_id` int(11) NOT NULL,
  `codigo_autorizacao` varchar(50) NOT NULL,
  `nr_membro` varchar(50) NOT NULL,
  `valor_remanescente` double(12,2) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `factura_cliente`
--

INSERT INTO `factura_cliente` (`id`, `cliente_id`, `nr_factura`, `valor`, `data`, `tipo_factura_id`, `estado_id`, `codigo_autorizacao`, `nr_membro`, `valor_remanescente`, `created_at`, `updated_at`) VALUES
(1, 7, 'F.M.E-20/2023', 690.00, '2023-02-28', 2, 1, '67', '90', 690.00, '2023-03-13 09:00:30', '0000-00-00 00:00:00'),
(2, 10, '100912', 700.00, '2023-02-28', 1, 1, '87', '87', 600.00, '2023-03-13 09:01:04', '2023-03-13 09:03:11'),
(3, 2, '78', 80.00, '2023-03-02', 2, 1, '89', '76', 80.00, '2023-03-13 09:01:35', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `formadepagamento`
--

CREATE TABLE `formadepagamento` (
  `idformapagamento` int(2) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `descricao` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `formadepagamento`
--

INSERT INTO `formadepagamento` (`idformapagamento`, `titulo`, `descricao`) VALUES
(1, 'Cheque', 'Cheque'),
(2, 'Cartão', 'Cartão'),
(3, 'Fundo de Maneio', 'Fundo de Maneio'),
(6, 'Transferencia', 'Transferencia');

-- --------------------------------------------------------

--
-- Table structure for table `formas_pagamento`
--

CREATE TABLE `formas_pagamento` (
  `id` int(11) NOT NULL,
  `pagamento_id` int(11) NOT NULL,
  `tipo_pagamento_id` int(11) NOT NULL,
  `valor` double(12,2) NOT NULL,
  `referencia` int(10) DEFAULT NULL,
  `data` date NOT NULL,
  `idformapagamento` int(11) NOT NULL,
  `numerocheque` varchar(70) DEFAULT NULL,
  `caixa` int(11) DEFAULT NULL,
  `status` int(2) NOT NULL DEFAULT 1 COMMENT '1 => activo, 0 => removido',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `formas_pagamento`
--

INSERT INTO `formas_pagamento` (`id`, `pagamento_id`, `tipo_pagamento_id`, `valor`, `referencia`, `data`, `idformapagamento`, `numerocheque`, `caixa`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 3, 40.00, 87, '2023-03-13', 6, 'NULL', NULL, 1, '2023-03-13 07:03:11', '0000-00-00 00:00:00'),
(2, 1, 5, 60.00, NULL, '2023-03-13', 2, 'NULL', NULL, 1, '2023-03-13 07:03:11', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `grupo`
--

CREATE TABLE `grupo` (
  `id` int(3) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `visibilidade` int(1) NOT NULL DEFAULT 1,
  `validacao_factura` enum('sim','não') NOT NULL,
  `data_atualizacao` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `grupo`
--

INSERT INTO `grupo` (`id`, `nome`, `visibilidade`, `validacao_factura`, `data_atualizacao`) VALUES
(1, 'Manutenção e Reparação de Viaturas', 1, 'não', '0000-00-00 00:00:00'),
(2, 'Manutenção e Reparação de AC', 1, 'sim', '0000-00-00 00:00:00'),
(3, 'Recargas de Telemóvel', 1, 'sim', '0000-00-00 00:00:00'),
(4, 'Subsídio Alimentação', 1, 'sim', '0000-00-00 00:00:00'),
(5, 'Subsídio de Transporte', 1, 'sim', '0000-00-00 00:00:00'),
(6, 'Avenças e Prestação de Serviços', 1, 'sim', '0000-00-00 00:00:00'),
(7, 'Despesas Fixas', 1, 'sim', '0000-00-00 00:00:00'),
(8, 'Combustível de Gerador', 1, 'sim', '0000-00-00 00:00:00'),
(9, 'Livros de VD. Facturas, Recibos e Facturas Pró Formas', 1, 'sim', '0000-00-00 00:00:00'),
(10, 'INSS', 1, 'sim', '0000-00-00 00:00:00'),
(11, 'IRPS', 1, 'sim', '0000-00-00 00:00:00'),
(12, 'IVA', 1, 'sim', '0000-00-00 00:00:00'),
(13, 'Manutenções Gerais', 1, 'sim', '0000-00-00 00:00:00'),
(14, 'Taxas Municipais', 1, 'sim', '0000-00-00 00:00:00'),
(15, 'Impostos com viaturas', 1, 'sim', '0000-00-00 00:00:00'),
(99, 'Outras despesas', 1, 'sim', '0000-00-00 00:00:00'),
(100, 'Combustível', 1, 'sim', '0000-00-00 00:00:00'),
(101, 'Refeição para Viagem', 1, 'sim', '0000-00-00 00:00:00'),
(102, 'Refeição do Sr. Marco', 1, 'sim', '0000-00-00 00:00:00'),
(103, 'Ímpar Seguros', 1, 'não', '2023-01-25 19:55:12'),
(104, 'Subsídio de Transporte', 1, 'não', '2022-10-26 07:55:10'),
(105, 'Material de Higiene', 1, 'sim', '0000-00-00 00:00:00'),
(106, 'Material de Escritório', 1, 'sim', '0000-00-00 00:00:00'),
(107, 'Credelec', 1, 'sim', '0000-00-00 00:00:00'),
(108, 'Recolha para Fundo de Maneio', 1, 'sim', '0000-00-00 00:00:00'),
(109, 'Toners', 1, 'não', '0000-00-00 00:00:00'),
(111, 'Refeição de Domingos', 1, 'não', '0000-00-00 00:00:00'),
(112, 'Car Wash 1', 1, 'sim', '2023-01-25 19:55:23'),
(113, 'Rafik', 1, 'sim', '2023-01-17 10:00:25');

-- --------------------------------------------------------

--
-- Table structure for table `grupodespesas`
--

CREATE TABLE `grupodespesas` (
  `idgrupodespesas` int(3) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `descricao` varchar(255) DEFAULT NULL,
  `centrocusto` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `grupodespesas`
--

INSERT INTO `grupodespesas` (`idgrupodespesas`, `titulo`, `descricao`, `centrocusto`) VALUES
(10, 'Viatura AFC-777MC', 'Viatura AFC-777MC', 1),
(11, 'Viatura ABY-499MC', 'Viatura ABY-499MC', 1),
(12, 'Viatura AEX-773MP', 'Viatura AEX-773MP', 2),
(13, 'Viatura ADA-103MP', 'Viatura ADA-103MP', 2),
(14, 'despesas fixas', 'despesas fixas', 1),
(15, 'despesas material didactico CIU', 'despesas material didactico CIU', 1),
(16, 'despesas material didactico CUN', 'despesas material didactico CUN', 2),
(17, 'Despesas alimenticias', 'Despesas alimenticias', 1),
(18, 'despesas material de limpesa', 'despesas material de limpesa', 1),
(19, 'despesas produ??o e aquisi??o de uniforme CIU', 'despesas produ??o e aquisi??o de uniforme CIU', 1),
(20, 'despesas produ??o e aquisi??o de uniforme CUN', 'despesas produ??o e aquisi??o de uniforme CUN', 2),
(21, 'Outras saidas CIU', 'Outras saidas CIU', 1),
(22, 'despesa salario CIU', 'despesa salario CIU', 1),
(23, 'despesa salario CUN', 'despesa salario CUN', 2),
(25, 'Material de escritorio', 'Material de escritorio', 1),
(26, 'Equipamento de funcionamento CUN', 'Equipamento de funcionamento CUN', 2),
(27, 'Equipamento de funcionamento CIU', 'Equipamento de funcionamento CIU', 1),
(28, 'Despesas de Investimento CIU', 'Despesas de Investimento CIU', 1),
(29, 'Despesas de Investimento CUN', 'Despesas de Investimento CUN', 2),
(30, 'Outras saidas CUN\r\n', 'Outras saidas CUN', 2),
(31, 'Despesas de reparacoes e manutencao CIU', 'Despesas de reparacoes e manutencao CIU', 1),
(32, 'Despesas de reparacoes e manutencao CUN', 'Despesas de reparacoes e manutencao CUN', 2),
(33, 'Financiamento', 'Financiamento BancÃ¡rio', 1),
(34, 'Taxas de Juros', 'Taxas de Juros', 1),
(35, 'Taxas BancÃ¡rias', 'Taxas BancÃ¡rias', 1);

-- --------------------------------------------------------

--
-- Table structure for table `nivel_acesso`
--

CREATE TABLE `nivel_acesso` (
  `nivel_acesso_id` int(1) UNSIGNED NOT NULL,
  `nivel_acesso_nome` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nivel_acesso`
--

INSERT INTO `nivel_acesso` (`nivel_acesso_id`, `nivel_acesso_nome`) VALUES
(0, 'Administrador'),
(1, 'Gestão Despesas'),
(2, 'Gestão Facturação');

-- --------------------------------------------------------

--
-- Table structure for table `numeracao`
--

CREATE TABLE `numeracao` (
  `id` int(11) NOT NULL,
  `numero` int(11) NOT NULL,
  `ano` year(4) NOT NULL,
  `tipo` enum('venda_dinheiro','venda_credito','cotacao','fornecedor','nota_credito_fornecedor','inventario','devolucao','nota_credito','abate') NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `numeracao`
--

INSERT INTO `numeracao` (`id`, `numero`, `ano`, `tipo`, `created_at`, `updated_at`) VALUES
(1, 16, 2023, 'nota_credito', '2023-02-21 10:04:28', '2023-03-08 09:26:24'),
(2, 6, 2023, 'venda_credito', '2023-03-03 17:28:34', '2023-03-13 10:01:02');

-- --------------------------------------------------------

--
-- Table structure for table `pagamento`
--

CREATE TABLE `pagamento` (
  `id` int(11) NOT NULL,
  `fornecedor_id` int(11) DEFAULT NULL,
  `nr_recibo` varchar(20) NOT NULL,
  `data` date NOT NULL,
  `data_recibo` date NOT NULL,
  `nr_pagamento` varchar(50) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `update_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pagamento`
--

INSERT INTO `pagamento` (`id`, `fornecedor_id`, `nr_recibo`, `data`, `data_recibo`, `nr_pagamento`, `created_at`, `update_at`) VALUES
(1, 3, 'R21PMACOU', '2023-02-21', '2023-02-01', 'NC 1/2023', '2023-02-21 08:04:28', '0000-00-00 00:00:00'),
(7, 1, '22', '2022-01-21', '2022-01-20', 'NC 2/2023', '2023-02-21 08:23:23', '0000-00-00 00:00:00'),
(15, 2, 'R120PM', '2023-02-21', '2023-02-01', 'NC 3/2023', '2023-02-21 08:37:13', '2023-02-21 08:38:27'),
(23, 1, '2wq', '2022-01-21', '2022-01-20', 'NC 3/2023', '2023-02-21 08:50:25', '0000-00-00 00:00:00'),
(24, 1, '2w', '2022-01-21', '2022-01-20', 'NC 4/2023', '2023-02-21 08:50:46', '0000-00-00 00:00:00'),
(25, 1, '2qwa', '2022-01-21', '2022-01-20', 'NC 5/2023', '2023-02-21 08:52:52', '0000-00-00 00:00:00'),
(26, 1, '2qw', '2022-01-21', '2022-01-20', 'NC 6/2023', '2023-02-21 08:53:26', '0000-00-00 00:00:00'),
(27, 1, 'wawew', '2022-01-21', '2022-01-20', 'NC 7/2023', '2023-02-21 08:54:08', '0000-00-00 00:00:00'),
(28, 1, 'wawe', '2022-01-21', '2022-01-20', 'NC 7/2023', '2023-02-21 08:55:48', '0000-00-00 00:00:00'),
(29, 1, 'wwe', '2022-01-21', '2022-01-20', 'NC 8/2023', '2023-02-21 08:56:36', '0000-00-00 00:00:00'),
(30, 1, 'wwe21', '2022-01-21', '2022-01-20', 'NC 9/2023', '2023-02-21 08:57:25', '0000-00-00 00:00:00'),
(31, 1, 'wwe2', '2022-01-21', '2022-01-20', 'NC 10/2023', '2023-02-21 08:58:00', '0000-00-00 00:00:00'),
(32, 1, 'ww76', '2022-01-21', '2022-01-20', 'NC 11/2023', '2023-02-21 09:01:13', '0000-00-00 00:00:00'),
(33, 1, 'ww7', '2022-01-21', '2022-01-20', 'NC 12/2023', '2023-02-21 09:01:29', '0000-00-00 00:00:00'),
(37, 1, 'ww99', '2022-01-21', '2022-01-20', 'NC 12/2023', '2023-02-21 09:08:56', '0000-00-00 00:00:00'),
(38, 1, 'ww9', '2022-01-21', '2022-01-20', 'NC 12/2023', '2023-02-21 09:12:27', '0000-00-00 00:00:00'),
(39, 2, '12', '2023-02-21', '2023-02-01', 'NC 13/2023', '2023-02-21 09:15:28', '0000-00-00 00:00:00'),
(40, 2, '456', '2023-03-02', '2023-03-01', 'NC 14/2023', '2023-03-03 14:39:10', '0000-00-00 00:00:00'),
(44, 2, 'R1U01', '2023-03-08', '2023-03-01', 'NC 15/2023', '2023-03-08 06:51:04', '0000-00-00 00:00:00'),
(45, 4, '1221', '2023-03-07', '2023-03-01', 'NC 16/2023', '2023-03-08 07:26:24', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `pagamento_facturas`
--

CREATE TABLE `pagamento_facturas` (
  `id` int(11) NOT NULL,
  `pagamento_id` int(11) DEFAULT NULL,
  `factura_id` int(11) DEFAULT NULL,
  `valor` double(12,2) NOT NULL,
  `remanescente` double(12,2) NOT NULL,
  `saldo` double(12,2) NOT NULL,
  `desconto` double(12,2) DEFAULT NULL,
  `total` double(12,2) NOT NULL,
  `status` int(2) NOT NULL DEFAULT 1 COMMENT '1 => activo, 0 => removido',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `update_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pagamento_facturas`
--

INSERT INTO `pagamento_facturas` (`id`, `pagamento_id`, `factura_id`, `valor`, `remanescente`, `saldo`, `desconto`, `total`, `status`, `created_at`, `update_at`) VALUES
(1, 1, 6, 20000.00, 300.00, 19690.00, 10.00, 290.00, 1, '2023-02-21 08:04:28', '0000-00-00 00:00:00'),
(22, 39, 2, 200000.00, 200.00, 199780.00, 20.00, 180.00, 1, '2023-02-21 09:15:28', '0000-00-00 00:00:00'),
(23, 40, 2, 200000.00, 70.00, 199910.00, 20.00, 50.00, 1, '2023-03-03 14:39:10', '0000-00-00 00:00:00'),
(24, 40, 1, 50000.00, 690.00, 49300.00, 10.00, 680.00, 1, '2023-03-03 14:39:10', '0000-00-00 00:00:00'),
(28, 44, 2, 200000.00, 200.00, 199780.00, 20.00, 180.00, 1, '2023-03-08 06:51:04', '0000-00-00 00:00:00'),
(29, 45, 9, 1500.00, 200.00, 1300.00, 0.00, 200.00, 1, '2023-03-08 07:26:24', '0000-00-00 00:00:00'),
(30, 45, 4, 1500.00, 20.00, 1450.00, 30.00, -10.00, 1, '2023-03-08 07:26:24', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `pagamento_factura_cliente`
--

CREATE TABLE `pagamento_factura_cliente` (
  `id` int(11) NOT NULL,
  `cliente_id` int(11) NOT NULL,
  `data_pagamento` date NOT NULL,
  `nr_pagamento` varchar(50) NOT NULL,
  `nr_nota_pagamento` varchar(50) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pagamento_factura_cliente`
--

INSERT INTO `pagamento_factura_cliente` (`id`, `cliente_id`, `data_pagamento`, `nr_pagamento`, `nr_nota_pagamento`, `created_at`, `updated_at`) VALUES
(1, 10, '2023-03-01', '1243', 'NC 5/2023', '2023-03-13 09:03:11', '0000-00-00 00:00:00'),
(2, 7, '2023-03-01', '76', 'NC 6/2023', '2023-03-13 10:01:02', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `pagamento_factura_cliente_item`
--

CREATE TABLE `pagamento_factura_cliente_item` (
  `id` int(11) NOT NULL,
  `factura_cliente_id` int(11) NOT NULL,
  `pagamento_factura_id` int(11) NOT NULL,
  `valor_pago` double(12,2) NOT NULL,
  `data` date NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pagamento_factura_cliente_item`
--

INSERT INTO `pagamento_factura_cliente_item` (`id`, `factura_cliente_id`, `pagamento_factura_id`, `valor_pago`, `data`, `created_at`, `updated_at`) VALUES
(1, 2, 1, 100.00, '2023-03-13', '2023-03-13 09:03:11', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `paramempresa`
--

CREATE TABLE `paramempresa` (
  `empresaId` bigint(20) UNSIGNED NOT NULL,
  `empresaNome` varchar(250) NOT NULL,
  `empresaEndereco` varchar(250) NOT NULL,
  `empresaTelefone` int(11) NOT NULL,
  `empresaEmail` varchar(250) NOT NULL,
  `empresaLogo` varchar(250) NOT NULL,
  `sigla` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `paramempresa`
--

INSERT INTO `paramempresa` (`empresaId`, `empresaNome`, `empresaEndereco`, `empresaTelefone`, `empresaEmail`, `empresaLogo`, `sigla`) VALUES
(1, 'Centro Infantil Universo & Colégio Universo', 'Avenida de Moçambique, Marracuene, Facim', 844370150, 'secretaria@universo-mz.com', 'imagens/txovaERP.png', '');

-- --------------------------------------------------------

--
-- Table structure for table `pc_classe`
--

CREATE TABLE `pc_classe` (
  `id` int(2) NOT NULL,
  `codigo` int(2) NOT NULL,
  `nome` varchar(70) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `pc_grupo`
--

CREATE TABLE `pc_grupo` (
  `id` int(2) NOT NULL,
  `codigo` int(2) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `classe_id` int(2) NOT NULL,
  `sub_classe_id` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `pc_sub_classe`
--

CREATE TABLE `pc_sub_classe` (
  `id` int(2) NOT NULL,
  `codigo` int(2) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `classe_id` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `pc_sub_grupo`
--

CREATE TABLE `pc_sub_grupo` (
  `id` int(2) NOT NULL,
  `codigo` int(2) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `classe_id` int(2) NOT NULL,
  `sub_classe_id` int(2) NOT NULL,
  `grupo_id` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `pc_sub_grupo1`
--

CREATE TABLE `pc_sub_grupo1` (
  `id` int(2) NOT NULL,
  `codigo` int(2) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `classe_id` int(2) NOT NULL,
  `sub_classe_id` int(2) NOT NULL,
  `grupo_id` int(2) NOT NULL,
  `sub_grupo_id` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `pc_sub_grupo2`
--

CREATE TABLE `pc_sub_grupo2` (
  `id` int(2) NOT NULL,
  `codigo` int(2) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `classe_id` int(2) NOT NULL,
  `sub_classe_id` int(2) NOT NULL,
  `grupo_id` int(2) NOT NULL,
  `sub_grupo_id` int(2) NOT NULL,
  `sub_grupo1_id` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `pc_sub_grupo3`
--

CREATE TABLE `pc_sub_grupo3` (
  `id` int(2) NOT NULL,
  `codigo` int(2) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `classe_id` int(2) NOT NULL,
  `sub_classe_id` int(2) NOT NULL,
  `grupo_id` int(2) NOT NULL,
  `sub_grupo_id` int(2) NOT NULL,
  `sub_grupo1_id` int(2) NOT NULL,
  `sub_grupo2_id` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `pc_sub_grupo4`
--

CREATE TABLE `pc_sub_grupo4` (
  `id` int(2) NOT NULL,
  `codigo` int(2) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `classe_id` int(2) NOT NULL,
  `sub_classe_id` int(2) NOT NULL,
  `grupo_id` int(2) NOT NULL,
  `sub_grupo_id` int(2) NOT NULL,
  `sub_grupo1_id` int(2) NOT NULL,
  `sub_grupo2_id` int(2) NOT NULL,
  `sub_grupo3_id` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Stand-in structure for view `requicoesexecutadas`
-- (See below for the actual view)
--
CREATE TABLE `requicoesexecutadas` (
`idrequisicaodefundos` int(8)
,`descricaodespesa` varchar(255)
,`data` date
,`valortotal` double(10,2)
,`datarequisiaao` date
,`descrequisicao` varchar(255)
,`valorreq` double(10,2)
);

-- --------------------------------------------------------

--
-- Table structure for table `requisicaofundos`
--

CREATE TABLE `requisicaofundos` (
  `idrequisicaodefundos` int(8) NOT NULL,
  `datarequisicao` date DEFAULT NULL,
  `descricao` varchar(255) NOT NULL,
  `valorestimado` double(10,2) NOT NULL,
  `observacoes` varchar(255) DEFAULT NULL,
  `anexo` varchar(255) DEFAULT NULL,
  `estado` int(1) NOT NULL DEFAULT 0,
  `iduser` int(5) DEFAULT NULL,
  `mes` int(2) NOT NULL,
  `ano` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `requisicaofundos`
--

INSERT INTO `requisicaofundos` (`idrequisicaodefundos`, `datarequisicao`, `descricao`, `valorestimado`, `observacoes`, `anexo`, `estado`, `iduser`, `mes`, `ano`) VALUES
(1, '2022-07-15', 'Requisição genérica', 10000.00, '', '', 1, NULL, 0, 0),
(2, '2022-07-14', 'Troca de Amortecedor de frente e direito', 20000.00, '', '', 1, NULL, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `subgrupo`
--

CREATE TABLE `subgrupo` (
  `id` int(5) NOT NULL,
  `nome` varchar(200) NOT NULL,
  `grupo` int(4) NOT NULL,
  `centro_custo_id` int(11) NOT NULL DEFAULT 1,
  `data_atualizacao` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subgrupo`
--

INSERT INTO `subgrupo` (`id`, `nome`, `grupo`, `centro_custo_id`, `data_atualizacao`) VALUES
(1, 'Dr. Pereira(Advogado)', 6, 1, '0000-00-00 00:00:00'),
(2, 'Leonel(Contabilista)', 6, 1, '0000-00-00 00:00:00'),
(3, 'Tivane(Tecnico informatico)', 6, 1, '0000-00-00 00:00:00'),
(4, 'Acsun(Software)', 6, 1, '0000-00-00 00:00:00'),
(5, 'Primavera(Software)', 6, 1, '0000-00-00 00:00:00'),
(6, 'PIE(Software)', 6, 1, '0000-00-00 00:00:00'),
(7, 'Seguranca Privada', 6, 1, '0000-00-00 00:00:00'),
(8, 'Toner', 7, 1, '0000-00-00 00:00:00'),
(9, 'Internet', 7, 1, '0000-00-00 00:00:00'),
(11, 'Deslocacao', 7, 1, '0000-00-00 00:00:00'),
(12, 'Despacho e envio de documentos', 7, 1, '0000-00-00 00:00:00'),
(13, 'Despacho e envio de mercadoria', 7, 1, '0000-00-00 00:00:00'),
(14, 'Material de higiene', 7, 1, '0000-00-00 00:00:00'),
(15, 'Energia', 7, 1, '0000-00-00 00:00:00'),
(16, 'Água', 7, 1, '0000-00-00 00:00:00'),
(17, 'AIRCON AC Gab. Nelson', 2, 2, '0000-00-00 00:00:00'),
(18, 'AIRCON AC1 Central', 2, 2, '0000-00-00 00:00:00'),
(19, 'AIRCON AC 2 Lado direito', 2, 2, '0000-00-00 00:00:00'),
(20, 'AIRCON AC  Lado Esquerdo(optica)', 2, 3, '0000-00-00 00:00:00'),
(21, 'AIRCON AC Central', 2, 3, '0000-00-00 00:00:00'),
(22, 'AIRCON AC Lado Dto.(perfumaria)', 2, 3, '0000-00-00 00:00:00'),
(23, 'AIRCON AC Escritorio', 2, 3, '0000-00-00 00:00:00'),
(24, 'AIRCON AC Sala(apartamento)', 2, 3, '0000-00-00 00:00:00'),
(25, 'AIRCON AC Quarto(apartamento)', 2, 3, '0000-00-00 00:00:00'),
(26, 'AIRCON AC lado esquerdo(Gab. Gestor)', 2, 10, '0000-00-00 00:00:00'),
(27, 'AIRCON AC2 lado esq.(Gab. Gestor)', 2, 10, '0000-00-00 00:00:00'),
(28, 'AIRCON AC Armazem', 2, 10, '0000-00-00 00:00:00'),
(29, 'AIRCON AC escritoruio principal', 2, 10, '0000-00-00 00:00:00'),
(30, 'AIRCON AC Porta principal(Vitrine)', 2, 4, '0000-00-00 00:00:00'),
(31, 'AIRCON AC Porta principal', 2, 4, '0000-00-00 00:00:00'),
(32, 'AIRCON AC Armazem', 2, 4, '0000-00-00 00:00:00'),
(33, 'AIRCON AC Sala', 2, 10, '0000-00-00 00:00:00'),
(34, 'AIRCON AC Quarto principal', 2, 10, '0000-00-00 00:00:00'),
(35, 'AIRCON AC2 Quarto', 2, 10, '0000-00-00 00:00:00'),
(36, 'AIRCON AC Unico', 2, 12, '0000-00-00 00:00:00'),
(37, 'AIRCON AC Unico', 2, 13, '0000-00-00 00:00:00'),
(38, 'AIRCON AC Unico', 2, 14, '0000-00-00 00:00:00'),
(39, 'AIRCON AC Unico', 2, 14, '0000-00-00 00:00:00'),
(40, 'AIRCON AC Unico', 2, 15, '0000-00-00 00:00:00'),
(41, 'AIRCON AC Unico', 2, 16, '0000-00-00 00:00:00'),
(42, 'KIA - AID 621 MC (Maputo)', 1, 1, '0000-00-00 00:00:00'),
(43, 'RACTIS CINZA - AEN 261 MC (Maputo)', 1, 1, '0000-00-00 00:00:00'),
(44, 'RACTIS AZUL ALD 852 MC (Maputo)', 1, 1, '0000-00-00 00:00:00'),
(45, 'RACTIS CINZA - AGM 067 MP (Beira)', 1, 1, '0000-00-00 00:00:00'),
(46, 'RACTIS PRETO - AGL 231 MP (Beira)', 1, 1, '0000-00-00 00:00:00'),
(47, 'RACTIS BRANCO - AJD 238 MC (Beira)', 1, 1, '0000-00-00 00:00:00'),
(48, 'RANGE ROVER - ABU 495 MP (Beira)', 1, 1, '0000-00-00 00:00:00'),
(49, 'Joana', 3, 1, '0000-00-00 00:00:00'),
(50, 'Milton', 3, 1, '0000-00-00 00:00:00'),
(51, 'Lucia Moamba', 3, 1, '0000-00-00 00:00:00'),
(52, 'Ambasse', 3, 1, '0000-00-00 00:00:00'),
(53, 'Mariano Nobre', 3, 1, '0000-00-00 00:00:00'),
(54, 'Anilza Mazoio', 3, 1, '0000-00-00 00:00:00'),
(55, 'Shamir Premoji', 3, 1, '0000-00-00 00:00:00'),
(56, 'Marco Aurelio', 3, 1, '0000-00-00 00:00:00'),
(57, 'Diana Timana', 3, 1, '0000-00-00 00:00:00'),
(58, 'Rosario', 3, 1, '0000-00-00 00:00:00'),
(59, 'Florinda ', 3, 1, '0000-00-00 00:00:00'),
(60, 'Eufrasia', 3, 1, '0000-00-00 00:00:00'),
(61, 'STAFF', 4, 1, '0000-00-00 00:00:00'),
(62, 'Guardas', 4, 1, '0000-00-00 00:00:00'),
(63, 'Ana Dias', 5, 2, '0000-00-00 00:00:00'),
(64, 'Eurico Alexandre', 5, 2, '0000-00-00 00:00:00'),
(65, 'Isabel Nhatumbo', 5, 2, '0000-00-00 00:00:00'),
(66, 'Jorge Sitoe', 5, 2, '0000-00-00 00:00:00'),
(67, 'Manuel Tinosse', 5, 2, '0000-00-00 00:00:00'),
(68, 'Raimundo Mandlate', 5, 2, '0000-00-00 00:00:00'),
(69, 'Leandro', 5, 2, '0000-00-00 00:00:00'),
(70, 'Ambasse', 5, 3, '0000-00-00 00:00:00'),
(71, 'Albino', 5, 3, '0000-00-00 00:00:00'),
(72, 'Fenias Maro', 5, 3, '0000-00-00 00:00:00'),
(73, 'Geni Natacha', 5, 3, '0000-00-00 00:00:00'),
(74, 'Joana  Tarquino ', 5, 3, '0000-00-00 00:00:00'),
(75, 'Isac ', 5, 3, '0000-00-00 00:00:00'),
(76, 'Milton Muianga', 5, 3, '0000-00-00 00:00:00'),
(77, 'Monica', 5, 3, '0000-00-00 00:00:00'),
(78, 'Lucia Moamba', 5, 3, '0000-00-00 00:00:00'),
(79, 'Shamir Premoji', 5, 3, '0000-00-00 00:00:00'),
(80, 'Armando', 5, 4, '0000-00-00 00:00:00'),
(81, 'Leticia', 5, 4, '0000-00-00 00:00:00'),
(82, 'Manuela', 5, 4, '0000-00-00 00:00:00'),
(83, 'Osvaldo', 5, 4, '0000-00-00 00:00:00'),
(84, 'Oscar', 5, 10, '0000-00-00 00:00:00'),
(85, 'Silvestre', 5, 4, '0000-00-00 00:00:00'),
(86, 'Rosario', 5, 10, '0000-00-00 00:00:00'),
(87, 'Fatima', 5, 10, '0000-00-00 00:00:00'),
(88, 'Francisco', 5, 10, '0000-00-00 00:00:00'),
(89, 'Latifo', 5, 10, '0000-00-00 00:00:00'),
(90, 'Agostinho', 5, 10, '0000-00-00 00:00:00'),
(91, 'Jacinta', 5, 10, '0000-00-00 00:00:00'),
(92, 'Miquelina', 5, 10, '0000-00-00 00:00:00'),
(93, 'Yniss', 5, 12, '0000-00-00 00:00:00'),
(94, 'Aissa', 5, 12, '0000-00-00 00:00:00'),
(95, 'Alcina', 5, 12, '0000-00-00 00:00:00'),
(96, 'Maria', 5, 12, '0000-00-00 00:00:00'),
(97, 'Joao Yoca', 5, 15, '0000-00-00 00:00:00'),
(98, 'Olga', 5, 15, '0000-00-00 00:00:00'),
(99, 'Outras Despesas', 99, 1, '0000-00-00 00:00:00'),
(100, 'Geral', 3, 1, '0000-00-00 00:00:00'),
(101, 'RANGE ROVER - AII 249 MP (Maputo)', 1, 1, '0000-00-00 00:00:00'),
(102, 'Eletricista', 13, 1, '0000-00-00 00:00:00'),
(103, 'Pedreiro', 13, 1, '0000-00-00 00:00:00'),
(104, 'Canalizador', 13, 1, '0000-00-00 00:00:00'),
(105, 'Pintor', 13, 1, '0000-00-00 00:00:00'),
(106, 'Taxas Municipais', 14, 1, '0000-00-00 00:00:00'),
(107, 'KIA - AID 621 MC (Maputo)', 15, 1, '0000-00-00 00:00:00'),
(108, 'RACTIS CINZA - AEN 261 MC (Maputo)', 15, 1, '0000-00-00 00:00:00'),
(109, 'RACTIS AZUL - ALD 852 MC (Maputo)', 15, 1, '0000-00-00 00:00:00'),
(110, 'RACTIS CINZA - AGM 067 MP (Beira)', 15, 1, '0000-00-00 00:00:00'),
(111, 'RACTIS PRETO - AGL 231 MP (Beira)', 15, 1, '0000-00-00 00:00:00'),
(112, 'RACTIS BRANCO - AJD 238 MC (Beira)', 15, 1, '0000-00-00 00:00:00'),
(113, 'RANGE ROVER - ABU 495 MP (Beira)', 15, 1, '0000-00-00 00:00:00'),
(114, 'RANGE ROVER - AII 249 MP (Maputo)', 15, 1, '0000-00-00 00:00:00'),
(115, 'KIA - AID 621 MC (Maputo)', 100, 1, '0000-00-00 00:00:00'),
(116, 'RACTIS CINZA - AEN 261 MC (Maputo)', 100, 1, '0000-00-00 00:00:00'),
(117, 'RACTIS AZUL - ALD 852 MC (Maputo)', 100, 1, '0000-00-00 00:00:00'),
(118, 'RACTIS CINZA - AGM 067 MP (Beira)', 100, 1, '0000-00-00 00:00:00'),
(119, 'RACTIS PRETO - AGL 231 MP (Beira)', 100, 1, '0000-00-00 00:00:00'),
(120, 'RACTIS BRANCO - AJD 238 MC (Beira)', 100, 1, '0000-00-00 00:00:00'),
(121, 'RANGE ROVER - ABU 495 MP (Beira)', 100, 1, '0000-00-00 00:00:00'),
(122, 'RANGE ROVER - AII 249 MP (Maputo)', 100, 1, '0000-00-00 00:00:00'),
(123, 'Rufina Simbine', 5, 2, '0000-00-00 00:00:00'),
(124, 'Ana Bela', 104, 19, '0000-00-00 00:00:00'),
(125, 'Analila Chauque', 104, 19, '0000-00-00 00:00:00'),
(128, 'Oscar', 108, 1, '0000-00-00 00:00:00'),
(129, 'Rosario Dias', 108, 1, '0000-00-00 00:00:00'),
(130, 'Material de Escritório', 106, 15, '2023-01-25 19:55:54'),
(131, 'Marcia Matlhava', 3, 19, '0000-00-00 00:00:00'),
(132, 'RACTIS AZUL - ALD 852 MC (Maputo)', 103, 1, '0000-00-00 00:00:00'),
(133, 'RACTIS AZUL - ALD 852 MC (Maputo)', 112, 1, '0000-00-00 00:00:00'),
(134, 'KIA - AID 621 Rafik', 112, 1, '2022-10-26 07:53:05'),
(135, 'RANGE ROVER - AII 249 MP 100', 112, 15, '2022-12-21 08:38:40'),
(136, 'Albino', 104, 1, '0000-00-00 00:00:00'),
(137, 'Carlos Bento', 3, 1, '0000-00-00 00:00:00'),
(138, 'Marcia Matlhava', 104, 19, '0000-00-00 00:00:00'),
(139, 'VITZ AZUL - AGH 886 MP', 100, 1, '0000-00-00 00:00:00'),
(140, 'Nelson Joao', 104, 2, '0000-00-00 00:00:00'),
(141, 'Nelson João', 104, 2, '0000-00-00 00:00:00'),
(142, 'Leandro', 104, 3, '0000-00-00 00:00:00'),
(143, 'RACTIS BRANCO( Anilsa Mazoio) - AKW 699 MC', 100, 1, '0000-00-00 00:00:00'),
(144, 'Informatica', 13, 1, '0000-00-00 00:00:00'),
(145, 'Sr. Marco Aurelio', 102, 15, '2022-10-26 07:55:27'),
(146, 'Combustivel Gerador', 8, 1, '0000-00-00 00:00:00'),
(147, 'Livros VD', 9, 1, '0000-00-00 00:00:00'),
(148, 'Facturas', 9, 1, '0000-00-00 00:00:00'),
(149, 'Recibos', 9, 1, '0000-00-00 00:00:00'),
(150, 'Facturas Proformas', 9, 1, '0000-00-00 00:00:00'),
(151, 'Mohamad Vala', 104, 2, '0000-00-00 00:00:00'),
(152, 'Mohamad Vala', 5, 2, '0000-00-00 00:00:00'),
(153, 'Nelson João ', 5, 2, '0000-00-00 00:00:00'),
(154, 'Isabel Macubele', 104, 14, '0000-00-00 00:00:00'),
(155, 'Clara Timoteo', 104, 14, '0000-00-00 00:00:00'),
(156, 'RafiG', 111, 15, '2023-01-17 10:02:20'),
(157, 'Material de Higiene', 105, 1, '0000-00-00 00:00:00'),
(159, 'Carpinteiro', 13, 1, '0000-00-00 00:00:00'),
(160, 'Marcia Mathava', 3, 19, '0000-00-00 00:00:00'),
(161, 'Livro de Reclamações', 9, 1, '0000-00-00 00:00:00'),
(162, 'Marco Aurelio Pinto Monteiro', 103, 1, '0000-00-00 00:00:00'),
(163, 'INSS', 10, 1, '0000-00-00 00:00:00'),
(164, 'Guia de Remessa', 9, 1, '0000-00-00 00:00:00'),
(165, 'Refeição para Viagem', 101, 1, '0000-00-00 00:00:00'),
(166, 'Carlos Bento', 4, 3, '0000-00-00 00:00:00'),
(167, 'Elias Boneta', 5, 16, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tblusuario`
--

CREATE TABLE `tblusuario` (
  `usuarioId` int(10) UNSIGNED NOT NULL,
  `usuarioNome` varchar(250) CHARACTER SET latin1 NOT NULL,
  `usuarioSenha` varchar(250) NOT NULL,
  `usuarioAvatar` varchar(250) CHARACTER SET latin1 NOT NULL,
  `usuNivelAcesso` int(1) UNSIGNED NOT NULL,
  `nome` varchar(255) NOT NULL,
  `UsuarioApelido` varchar(255) NOT NULL,
  `UsuarioSexo` varchar(255) NOT NULL,
  `UsuarioEmail` varchar(255) NOT NULL,
  `UsuarioEstado` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tblusuario`
--

INSERT INTO `tblusuario` (`usuarioId`, `usuarioNome`, `usuarioSenha`, `usuarioAvatar`, `usuNivelAcesso`, `nome`, `UsuarioApelido`, `UsuarioSexo`, `UsuarioEmail`, `UsuarioEstado`) VALUES
(1, 'diana', '9e1e06ec8e02f0a0074f2fcc6b26303b', '', 1, 'Diana ', 'Tivane', 'Femenino', '', ''),
(3, 'Rafik', '4122cb13c7a474c1976c9706ae36521d', '0', 0, 'Jennifer', 'Maibasso', 'Femenino', '', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tipo_documento`
--

CREATE TABLE `tipo_documento` (
  `id` int(11) NOT NULL,
  `designacao` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tipo_documento`
--

INSERT INTO `tipo_documento` (`id`, `designacao`) VALUES
(1, 'VD'),
(2, 'Recibo/Factura'),
(3, 'Factura'),
(4, 'Factura-Proforma');

-- --------------------------------------------------------

--
-- Table structure for table `tipo_factura`
--

CREATE TABLE `tipo_factura` (
  `id` int(11) NOT NULL,
  `descricao` varchar(50) NOT NULL,
  `tipo_operacao` enum('1','2') NOT NULL COMMENT '1 => credito, 2 => debito'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tipo_factura`
--

INSERT INTO `tipo_factura` (`id`, `descricao`, `tipo_operacao`) VALUES
(1, 'VFA', '1'),
(2, 'VNC', '2');

-- --------------------------------------------------------

--
-- Table structure for table `unidade`
--

CREATE TABLE `unidade` (
  `id` int(3) NOT NULL,
  `nome` varchar(10) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `unidade`
--

INSERT INTO `unidade` (`id`, `nome`) VALUES
(1, 'Unidade'),
(2, 'Kg'),
(3, 'L'),
(4, 'm'),
(5, 'KW/h'),
(6, 'ml'),
(7, 'm3'),
(8, 'mm'),
(9, 'Outra');

-- --------------------------------------------------------

--
-- Stand-in structure for view `viewdespesastransporte`
-- (See below for the actual view)
--
CREATE TABLE `viewdespesastransporte` (
`descricaodespesa` varchar(255)
,`data` date
,`ano` int(11)
,`valortotal` double(10,2)
);

-- --------------------------------------------------------

--
-- Structure for view `requicoesexecutadas`
--
DROP TABLE IF EXISTS `requicoesexecutadas`;

CREATE ALGORITHM=UNDEFINED DEFINER=`u204463154_siged_felisa`@`127.0.0.1` SQL SECURITY DEFINER VIEW `requicoesexecutadas`  AS SELECT `execucaodespesas`.`idrequisicaodefundos` AS `idrequisicaodefundos`, `execucaodespesas`.`descricaodespesa` AS `descricaodespesa`, `execucaodespesas`.`data` AS `data`, `execucaodespesas`.`valortotal` AS `valortotal`, `requisicaofundos`.`datarequisicao` AS `datarequisiaao`, `requisicaofundos`.`descricao` AS `descrequisicao`, `requisicaofundos`.`valorestimado` AS `valorreq` FROM (`execucaodespesas` join `requisicaofundos` on(`requisicaofundos`.`idrequisicaodefundos` = `execucaodespesas`.`idrequisicaodefundos`))  ;

-- --------------------------------------------------------

--
-- Structure for view `viewdespesastransporte`
--
DROP TABLE IF EXISTS `viewdespesastransporte`;

CREATE ALGORITHM=UNDEFINED DEFINER=`u204463154_siged_felisa`@`127.0.0.1` SQL SECURITY DEFINER VIEW `viewdespesastransporte`  AS SELECT `execucaodespesas`.`descricaodespesa` AS `descricaodespesa`, `execucaodespesas`.`data` AS `data`, `execucaodespesas`.`ano` AS `ano`, `execucaodespesas`.`valortotal` AS `valortotal` FROM `execucaodespesas` WHERE `execucaodespesas`.`grupo` = 1 ORDER BY `execucaodespesas`.`idexecucaodespesas` ASC  ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `banco`
--
ALTER TABLE `banco`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `caixa`
--
ALTER TABLE `caixa`
  ADD PRIMARY KEY (`id`),
  ADD KEY `banco` (`banco`);

--
-- Indexes for table `centro_de_custo`
--
ALTER TABLE `centro_de_custo`
  ADD PRIMARY KEY (`idcentrodecusto`);

--
-- Indexes for table `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `empresas`
--
ALTER TABLE `empresas`
  ADD UNIQUE KEY `idempresa` (`idempresa`);

--
-- Indexes for table `execucaodespesas`
--
ALTER TABLE `execucaodespesas`
  ADD PRIMARY KEY (`idexecucaodespesas`),
  ADD UNIQUE KEY `idexecucaodespesas` (`idexecucaodespesas`),
  ADD KEY `grupo` (`grupo`),
  ADD KEY `subgrupo` (`subgrupo`),
  ADD KEY `unidade` (`unidade`);

--
-- Indexes for table `factura`
--
ALTER TABLE `factura`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fornecedor_id` (`fornecedor_id`),
  ADD KEY `centro_custo_id` (`centro_custo_id`),
  ADD KEY `tipo_factura_id` (`tipo_factura_id`);

--
-- Indexes for table `factura_cliente`
--
ALTER TABLE `factura_cliente`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `formadepagamento`
--
ALTER TABLE `formadepagamento`
  ADD PRIMARY KEY (`idformapagamento`);

--
-- Indexes for table `formas_pagamento`
--
ALTER TABLE `formas_pagamento`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pagamento_id` (`pagamento_id`),
  ADD KEY `tipo_pagamento_id` (`tipo_pagamento_id`),
  ADD KEY `idformapagamento` (`idformapagamento`),
  ADD KEY `caixa` (`caixa`);

--
-- Indexes for table `grupo`
--
ALTER TABLE `grupo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `grupodespesas`
--
ALTER TABLE `grupodespesas`
  ADD PRIMARY KEY (`idgrupodespesas`);

--
-- Indexes for table `nivel_acesso`
--
ALTER TABLE `nivel_acesso`
  ADD PRIMARY KEY (`nivel_acesso_id`);

--
-- Indexes for table `numeracao`
--
ALTER TABLE `numeracao`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tipo` (`tipo`),
  ADD KEY `ano` (`ano`);

--
-- Indexes for table `pagamento`
--
ALTER TABLE `pagamento`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fornecedor_id` (`fornecedor_id`);

--
-- Indexes for table `pagamento_facturas`
--
ALTER TABLE `pagamento_facturas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `factura_id` (`factura_id`),
  ADD KEY `pagamento_id` (`pagamento_id`);

--
-- Indexes for table `pagamento_factura_cliente`
--
ALTER TABLE `pagamento_factura_cliente`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pagamento_factura_cliente_item`
--
ALTER TABLE `pagamento_factura_cliente_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `paramempresa`
--
ALTER TABLE `paramempresa`
  ADD PRIMARY KEY (`empresaId`),
  ADD UNIQUE KEY `empresaId` (`empresaId`);

--
-- Indexes for table `pc_classe`
--
ALTER TABLE `pc_classe`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pc_grupo`
--
ALTER TABLE `pc_grupo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `classe_id` (`classe_id`),
  ADD KEY `sub_classe` (`sub_classe_id`);

--
-- Indexes for table `pc_sub_classe`
--
ALTER TABLE `pc_sub_classe`
  ADD PRIMARY KEY (`id`),
  ADD KEY `classe_id` (`classe_id`);

--
-- Indexes for table `pc_sub_grupo`
--
ALTER TABLE `pc_sub_grupo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `classe_id` (`classe_id`),
  ADD KEY `sub_classe` (`sub_classe_id`),
  ADD KEY `grupo` (`grupo_id`);

--
-- Indexes for table `pc_sub_grupo1`
--
ALTER TABLE `pc_sub_grupo1`
  ADD PRIMARY KEY (`id`),
  ADD KEY `classe_id` (`classe_id`),
  ADD KEY `sub_classe` (`sub_classe_id`),
  ADD KEY `grupo` (`grupo_id`),
  ADD KEY `sub_grupo_id` (`sub_grupo_id`);

--
-- Indexes for table `pc_sub_grupo2`
--
ALTER TABLE `pc_sub_grupo2`
  ADD PRIMARY KEY (`id`),
  ADD KEY `classe_id` (`classe_id`),
  ADD KEY `sub_classe` (`sub_classe_id`),
  ADD KEY `grupo` (`grupo_id`),
  ADD KEY `sub_grupo_id` (`sub_grupo_id`),
  ADD KEY `sub_grupo1_id` (`sub_grupo1_id`);

--
-- Indexes for table `pc_sub_grupo3`
--
ALTER TABLE `pc_sub_grupo3`
  ADD PRIMARY KEY (`id`),
  ADD KEY `classe_id` (`classe_id`),
  ADD KEY `sub_classe` (`sub_classe_id`),
  ADD KEY `grupo` (`grupo_id`),
  ADD KEY `sub_grupo_id` (`sub_grupo_id`),
  ADD KEY `sub_grupo1_id` (`sub_grupo1_id`),
  ADD KEY `sub_grupo2_id` (`sub_grupo2_id`);

--
-- Indexes for table `pc_sub_grupo4`
--
ALTER TABLE `pc_sub_grupo4`
  ADD PRIMARY KEY (`id`),
  ADD KEY `classe_id` (`classe_id`),
  ADD KEY `sub_classe` (`sub_classe_id`),
  ADD KEY `grupo` (`grupo_id`),
  ADD KEY `sub_grupo_id` (`sub_grupo_id`),
  ADD KEY `sub_grupo1_id` (`sub_grupo1_id`),
  ADD KEY `sub_grupo2_id` (`sub_grupo2_id`);

--
-- Indexes for table `requisicaofundos`
--
ALTER TABLE `requisicaofundos`
  ADD UNIQUE KEY `idrequisicaodefundos` (`idrequisicaodefundos`);

--
-- Indexes for table `subgrupo`
--
ALTER TABLE `subgrupo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `grupo` (`grupo`),
  ADD KEY `centro_custo_id` (`centro_custo_id`);

--
-- Indexes for table `tblusuario`
--
ALTER TABLE `tblusuario`
  ADD PRIMARY KEY (`usuarioId`),
  ADD UNIQUE KEY `usuarioId` (`usuarioId`),
  ADD KEY `usuNivelAcesso` (`usuNivelAcesso`);

--
-- Indexes for table `tipo_documento`
--
ALTER TABLE `tipo_documento`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tipo_factura`
--
ALTER TABLE `tipo_factura`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `unidade`
--
ALTER TABLE `unidade`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `banco`
--
ALTER TABLE `banco`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `caixa`
--
ALTER TABLE `caixa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `centro_de_custo`
--
ALTER TABLE `centro_de_custo`
  MODIFY `idcentrodecusto` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `cliente`
--
ALTER TABLE `cliente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `empresas`
--
ALTER TABLE `empresas`
  MODIFY `idempresa` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `execucaodespesas`
--
ALTER TABLE `execucaodespesas`
  MODIFY `idexecucaodespesas` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1054;

--
-- AUTO_INCREMENT for table `factura`
--
ALTER TABLE `factura`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `factura_cliente`
--
ALTER TABLE `factura_cliente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `formadepagamento`
--
ALTER TABLE `formadepagamento`
  MODIFY `idformapagamento` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `formas_pagamento`
--
ALTER TABLE `formas_pagamento`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `grupo`
--
ALTER TABLE `grupo`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=114;

--
-- AUTO_INCREMENT for table `grupodespesas`
--
ALTER TABLE `grupodespesas`
  MODIFY `idgrupodespesas` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `numeracao`
--
ALTER TABLE `numeracao`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `pagamento`
--
ALTER TABLE `pagamento`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `pagamento_facturas`
--
ALTER TABLE `pagamento_facturas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `pagamento_factura_cliente`
--
ALTER TABLE `pagamento_factura_cliente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `pagamento_factura_cliente_item`
--
ALTER TABLE `pagamento_factura_cliente_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `paramempresa`
--
ALTER TABLE `paramempresa`
  MODIFY `empresaId` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `pc_classe`
--
ALTER TABLE `pc_classe`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pc_grupo`
--
ALTER TABLE `pc_grupo`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pc_sub_classe`
--
ALTER TABLE `pc_sub_classe`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pc_sub_grupo`
--
ALTER TABLE `pc_sub_grupo`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pc_sub_grupo1`
--
ALTER TABLE `pc_sub_grupo1`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pc_sub_grupo2`
--
ALTER TABLE `pc_sub_grupo2`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pc_sub_grupo3`
--
ALTER TABLE `pc_sub_grupo3`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pc_sub_grupo4`
--
ALTER TABLE `pc_sub_grupo4`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `requisicaofundos`
--
ALTER TABLE `requisicaofundos`
  MODIFY `idrequisicaodefundos` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `subgrupo`
--
ALTER TABLE `subgrupo`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=168;

--
-- AUTO_INCREMENT for table `tblusuario`
--
ALTER TABLE `tblusuario`
  MODIFY `usuarioId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tipo_documento`
--
ALTER TABLE `tipo_documento`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tipo_factura`
--
ALTER TABLE `tipo_factura`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `unidade`
--
ALTER TABLE `unidade`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `caixa`
--
ALTER TABLE `caixa`
  ADD CONSTRAINT `caixa_ibfk_1` FOREIGN KEY (`banco`) REFERENCES `banco` (`id`);

--
-- Constraints for table `execucaodespesas`
--
ALTER TABLE `execucaodespesas`
  ADD CONSTRAINT `execucaodespesas_ibfk_1` FOREIGN KEY (`unidade`) REFERENCES `unidade` (`id`);

--
-- Constraints for table `factura`
--
ALTER TABLE `factura`
  ADD CONSTRAINT `factura_ibfk_1` FOREIGN KEY (`fornecedor_id`) REFERENCES `empresas` (`idempresa`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `factura_ibfk_2` FOREIGN KEY (`centro_custo_id`) REFERENCES `centro_de_custo` (`idcentrodecusto`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `factura_ibfk_3` FOREIGN KEY (`tipo_factura_id`) REFERENCES `tipo_factura` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `formas_pagamento`
--
ALTER TABLE `formas_pagamento`
  ADD CONSTRAINT `formas_pagamento_ibfk_2` FOREIGN KEY (`tipo_pagamento_id`) REFERENCES `banco` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `formas_pagamento_ibfk_3` FOREIGN KEY (`idformapagamento`) REFERENCES `formadepagamento` (`idformapagamento`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `formas_pagamento_ibfk_4` FOREIGN KEY (`caixa`) REFERENCES `caixa` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pagamento`
--
ALTER TABLE `pagamento`
  ADD CONSTRAINT `pagamento_ibfk_1` FOREIGN KEY (`fornecedor_id`) REFERENCES `empresas` (`idempresa`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pagamento_facturas`
--
ALTER TABLE `pagamento_facturas`
  ADD CONSTRAINT `pagamento_facturas_ibfk_1` FOREIGN KEY (`factura_id`) REFERENCES `factura` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pagamento_facturas_ibfk_2` FOREIGN KEY (`pagamento_id`) REFERENCES `pagamento` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `subgrupo`
--
ALTER TABLE `subgrupo`
  ADD CONSTRAINT `subgrupo_ibfk_1` FOREIGN KEY (`grupo`) REFERENCES `grupo` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `subgrupo_ibfk_2` FOREIGN KEY (`centro_custo_id`) REFERENCES `centro_de_custo` (`idcentrodecusto`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tblusuario`
--
ALTER TABLE `tblusuario`
  ADD CONSTRAINT `tblusuario_ibfk_1` FOREIGN KEY (`usuNivelAcesso`) REFERENCES `nivel_acesso` (`nivel_acesso_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
