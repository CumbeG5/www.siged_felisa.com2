<?php
@session_start();
$_SESSION['html'] = null;
$_SESSION['title'] = null;
$content = null;
include_once "../API/include.php";
include_once '../API/callAPI.php';
include_once './controller.php';
$controller = new Controller();
$seguradora_id = $_GET["seguradora_id"];
$data_inicio = $_GET["data_inicio"];
$data_fim = $_GET["data_fim"];
$estado = $_GET["estado"];
$numero = $_GET["numero"];
$limit = (int)$_GET['limit'];
$page = empty($_GET['page']) ? 1 : (int)$_GET['page'];
// print_r($page);
// print_r($limit);
$response = CallApi($URL . "api/factura_global/select_all.php?seguradora=$seguradora_id&data_inicio=$data_inicio&data_fim=$data_fim&estado=$estado&numero=$numero&page=$page&limit=$limit");
// print_r($URL . "api/factura_global/select_all.php?fornecedor=$fornecedor_id&data_inicio=$data_inicio&data_fim=$data_fim&estado=$estado&numero=$numero&page=$page&limit=$limit");
// exit;
$sqlAdd = " AND fg.estado IN('1','2')  ";
if (!empty($_GET['seguradora'])) {
    $seguradora = $_GET['seguradora'];
    $sqlAdd .= " AND fg.seguradora_id = {$seguradora}";
}

// if(!empty($_GET['cliente_id'])){
//     $seguradora_id = $_GET['cliente_id'];
//     $sqlAdd .= " AND fg.seguradora_id = {$seguradora_id}";
// }

if (!empty($_GET['estado'])) {
    $estado = $_GET['estado'];
    $sqlAdd .= " AND fg.estado = '{$estado}'";
}

if (!empty($_GET['numero'])) {
    $numero = $_GET['numero'];
    $sqlAdd .= " AND fg.numero LIKE '%{$numero}%'";
}

if (!empty($_GET['data_inicio']) && !empty($_GET['data_fim'])) {
    $data_inicio = $_GET['data_inicio'];
    $data_fim = $_GET['data_fim'];
    $sqlAdd .= " AND fg.data BETWEEN '{$data_inicio}' AND '{$data_fim}' ";
}

if (isset($response->data) && count($response->data) > 0) :
?>

    <div class="mb-2 text-right">
        <button class="btn btn-success" id="print"><b>PDF</b></button>
    </div>
    <?php ob_start(); ?>
    <table id="listarfacturas" class="table table-bordered table-striped">
        <thead>
            <tr>
                <td>
                    <div align="center"><strong>Id</strong></div>
                </td>
                <td>
                    <div align="center"><strong>Seguradora</strong></div>
                </td>
                <td>
                    <div align="center"><strong>Data</strong></div>
                </td>
                <td>
                    <div align="center"><strong>Número</strong></div>
                </td>
                <td>
                    <div align="center"><strong>Valor</strong></div>
                </td>
                <td>
                    <div align="center"><strong>Remanescente</strong></div>
                </td>
                <td>
                    <div align="center"><strong>Pago</strong></div>
                </td>
                <td>
                    <div align="center"><strong>Saldo</strong></div>
                </td>
                <td>
                    <div align="center"><strong>Estado F/P</strong></div>
                </td>
                <?php $content .= ob_get_contents(); ?>
                <td>
                    <div align="center"><strong>Acções</strong></div>
                </td>
                <?php ob_start(); ?>
            </tr>
        </thead>
        <tbody>
            <?php
            $total1 = 0;
            $total2 = 0;
            $total3 = 0;
            $total4 = 0;
            foreach ($response->data as $key => $row) :
                $total1 = $total1 + $row->valor;
                $total2 = $total2 + $row->remanescente;
                $total3 = $total3 + $row->pago;
                $total4 = $total4 + $row->saldo;
            ?>
                <tr>
                    <td><?= $row->id ?></td>
                    <td><?= $row->seguradora ?></td>
                    <td><?= $row->data ?></td>
                    <td><?= $row->numero ?></td>
                    <td><?= number_format($row->valor, 2, ',', '.') ?></td>
                    <td><?= number_format($row->remanescente, 2, ',', '.') ?></td>
                    <td><?= number_format($row->pago, 2, ',', '.')  ?></td>
                    <td class="text-right"><?= number_format($row->saldo, 2, ',', '.') ?></td>
                    <td>
                        <?php
                        if ($row->estado == 1) {
                            if ($row->valor == $row->rem) {
                        ?>
                                <span class='badge badge-success mb-2'>Aberto</span>
                                <span class='badge badge-warning'>não pago</span>

                            <?php } else if ($row->rem > 0) {   ?>
                                <span class='badge badge-success mb-2'>Aberto</span>
                                <span class='badge badge-info'>parcial</span>
                            <?php } else if ($row->valor == $row->pago) {   ?>
                                <span class='badge badge-success mb-2'>Aberto</span>
                                <span class='badge badge-success'>pago</span>

                            <?php }
                        } else if ($row->estado == 2) {
                            if ($row->valor == $row->rem) { ?>
                                <span class='badge badge-danger mb-2'>Fechado</span>
                                <span class='badge badge-warning'>não pago</span>
                            <?php } else if ($row->rem > 0) {    ?>
                                <span class='badge badge-danger mb-2'>Fechado</span>
                                <span class='badge badge-info'>parcial</span>
                            <?php } else if ($row->valor == $row->pago) {    ?>
                                <span class='badge badge-danger mb-2'>Fechado</span>
                                <span class='badge badge-success'>pago</span>
                            <?php  }
                        } else if ($row->estado == 3) {   ?>
                            <span class='badge badge-danger mb-2'>Eliminado</span>
                        <?php }
                        ?>
                    </td>
                    <td>


                        <a href="./listGuiasSeguradora.php?id=<?= $row->id ?>" class="btn btn-default" data-tooltip="Pagamentos Pendentes"><i class="fa-regular fa-clock fa-lg" style="color: #000000;"></i></a>
                        <a href="./class/pdf/documentos/reportFacturaGlobal.php?id=<?= $row->id ?> " value="<?= $row->id ?> " target="_blank" class="btn btn-default" data-tooltip="Visualizar Nota Pagamento"><i class="fa-solid fa-print fa-lg" style="color: #000000;"></i></a>
                        <?php if ($row->estado == 1) { ?>
                            <a class="btn btn-default" href="./frmFacturaSeguradoraUpdate.php?id=<?= $row->id ?>" data-tooltip="Editar Factura"> <i class="fa-regular fa-pen-to-square fa-lg" style="color: #000000;"></i> </a>
                        <?php   }
                        if ($row->pago == 0 && $row->estado != 3) { ?>
                            <a id="deleteFacturaGlobal" value="<?= $row->id ?> " class="deleteFacturaGlobal btn btn-default" data-tooltip="Eliminar Factura"><i class="fa-solid fa-trash fa-lg" style="color: #000000;"></i></a>
                        <?php    }
                        if ($row->pago == 0 && $row->estado == 2) { ?>
                            <a id="reabrirFactura" value="<?= $row->id ?> " class="reabrirFactura btn btn-default" data-tooltip="Reabrir Factura"><i class="fa-solid fa-lock-open fa-lg" style="color: #000000;"></i></a>
                        <?php  }
                        ?>
                    </td>
                </tr>

                <?php
                ?>
            <?php
            endforeach;
            ?>
        </tbody>
        <tfoot style="background-color: white;">
            <tr>

                <td></td>
                <td></td>
                <td></td>
                <td style="font-weight: bold; font-size: 15px;" class="text-center">Total:</td>
                <td style="font-weight: bold; font-size: 15px;" class="text-right"><?= number_format($total1, 2, ',', '.') ?></td>
                <td style="font-weight: bold; font-size: 15px;" class="text-right"><?= number_format($total2, 2, ',', '.') ?></td>
                <td style="font-weight: bold; font-size: 15px;" class="text-right"><?= number_format($total3, 2, ',', '.') ?></td>
                <td style="font-weight: bold; font-size: 15px;" class="text-right"><?= number_format($total4, 2, ',', '.') ?></td>
                <td style="font-weight: bold; font-size: 15px;"></td>
            </tr>
        </tfoot>
    </table>
    <div style="text-align: right; float:right;">
        <?php
        $controller->pagination($page, $limit, "", $controller->allRecord("factura_global fg ", $sqlAdd, ""));
        ?>
    </div>
<?php
else :
?>
    <div class="col-md-12 alert alert-info text-center">
        Nenhum registo encontrado!
    </div>
<?php
endif;
?>
<!-- pdf js -->
<!-- <script src="./class/pdf/js/VentanaCentrada.js"></script> -->
<!-- pdf js -->
<!-- <script src="./class/pdf/js/pdf.js"></script> -->

<?php
$content .= ob_get_contents();
$_SESSION['html'] = $content;
$_SESSION['title'] = "Listagem de facturas";
$_SESSION['dataInicio'] = "";
$_SESSION['dataFim'] = "";
$_SESSION['parametro1'] = "";
$_SESSION['parametro2'] = "";
$_SESSION['parametro3'] = "";
$_SESSION['parametro3'] = "";
$_SESSION['parametro4'] = "";
$_SESSION['user'] = $_SESSION['MM_nomeSgr'];
?>