<?php
@session_start();
$_SESSION['html'] = null;
$_SESSION['title'] = null;
$content = null;
include_once "../API/include.php";
include_once '../API/callAPI.php';
include_once './controller.php';
$controller = new Controller();
$data_inicio = isset($_GET["data_inicio"]) && !empty($_GET["data_inicio"]) ? $_GET["data_inicio"] : "";
$data_fim = isset($_GET["data_fim"]) && !empty($_GET["data_fim"]) ? $_GET["data_fim"] : "";
$centro_custo_id = isset($_GET["centro_custo"]) && !empty($_GET["centro_custo"]) ? $_GET["centro_custo"] : "";
$grupo_despesas = isset($_GET["grupo_despesas"]) && !empty($_GET["grupo_despesas"]) ? $_GET["grupo_despesas"] : "";
$subgrupo = isset($_GET["subgrupo"]) && !empty($_GET["subgrupo"]) ? $_GET["subgrupo"] : "";
$fornecedor_id = isset($_GET["fornecedor"]) && !empty($_GET["fornecedor"]) ? $_GET["fornecedor"] : "";
$limit = isset($_GET["limit"]) && !empty($_GET["limit"]) ? (int)$_GET['limit'] : 10;
$page = !isset($_GET["page"]) || empty($_GET['page']) ? 1 : (int)$_GET['page'];
$response = CallApi($URL . "api/despesas/select.php?data_inicio=$data_inicio&data_fim=$data_fim&centro_custo_id=$centro_custo_id&grupo_despesas=$grupo_despesas&subgrupo=$subgrupo&fornecedor_id=$fornecedor_id&page=$page&limit=$limit");
// print_r($response);
$sqlAdd = "";
$i = 1;
if (!empty($_GET['data_inicio']) or !empty($_GET['data_fim'])) {
    $data_inicio = $_GET['data_inicio'];
    $data_fim = $_GET['data_fim'];
    $sqlAdd .= " AND e.data BETWEEN '{$data_inicio}' AND '{$data_fim}' ";
}
if (!empty($centro_custo_id)) {
    // $centro_custo_id = $_GET['centro_custo_id'];
    $sqlAdd .= " AND e.idcentrodecusto = '{$centro_custo_id}' ";
}
if (!empty($_GET['grupo_despesas'])) {
    $grupo_despesas = $_GET['grupo_despesas'];
    $sqlAdd .= " AND e.grupo = '{$grupo_despesas}' ";
}
if (!empty($_GET['subgrupo'])) {
    $subgrupo = $_GET['subgrupo'];
    $sqlAdd .= " AND e.subgrupo = '{$subgrupo}' ";
}
if (!empty($fornecedor_id)) {
    // $fornecedor_id = $_GET['fornecedor_id'];
    $sqlAdd .= "AND e.idempresa = '{$fornecedor_id}'";
}

if (isset($response->data) && count($response->data) > 0) :
?>


    <div class="mb-2 text-right">
        <button class="btn btn-success" id="print"><b>PDF</b></button>
    </div>
    <?php ob_start(); ?>
    <table id="listarfacturas" class="table table-striped table-hover table-bordered">
        <thead>
            <tr>
                <th> Ord</th>
                <!-- <td><div align="center"><strong>ID</strong></div></td> -->
                <td>
                    <div align="center"><strong>Centro de Custo</strong></div>
                </td>
                <td>
                    <div align="center"><strong>Grupo de Despesa</strong></div>
                </td>
                <td>
                    <div align="center"><strong>Beneficiário</strong></div>
                </td>

                <td>
                    <div align="center"><strong>Descrição Despesa</strong></div>
                </td>
                <!-- <td><div align="center"><strong>Req.</strong></div></td> -->
                <td>
                    <div align="center"><strong>Data Exec.</strong></div>
                </td>
                <td>
                    <div align="center"><strong>Valor Total</strong></div>
                </td>
                <?php $content .= ob_get_contents();
                ?>
                <td style="font-weight: bold;">
                    <div align="center"><strong>Acções</strong></div>
                </td>
                <?php ob_start();
                ?>
            </tr>
        </thead>
        <tbody>
            <?php

            $total = 0;
            foreach ($response->data as $key => $row) :
                $total = $total + $row->val_total;
            ?>
                <tr>
                    <td><?= $row->id  ?></td>
                    <td><?= $row->centro_custo ?></td>
                    <td><?= $row->grupo . " - " . $row->sub_grupo ?></td>
                    <td><?= $row->beneficiario ?></td>
                    <td><?= $row->descr_despesa ?></td>
                    <td><?= $row->data_exec ?></td>
                    <td><?= $row->val_total ?></td>
                    <?php $content .= ob_get_contents();
                    ?>
                    <td>
                        <a id="viewDespesa" value="<?php echo $row->id ?>" class="viewDespesa btn btn-default" data-tooltip="visualizar despesa"><i class="fa fa-magnifying-glass-plus"></i></a>
                        <a class="btn btn-default" href="sigde.php?ord=<?= $row->id; ?>&go=3" data-tooltip="Editar despesa"><i class="fa-regular fa-pen-to-square" style="color: #000000;"></i> </a>
                    </td>
                    <?php ob_start(); ?>
                </tr>
            <?php
            endforeach;
            ?>
            <tr>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th style="font-weight: bold; font-size: 15px;" class="text-"><strong>Total:</strong></th>
                <th style="font-weight: bold; font-size: 15px;" class="text-right">
                    <?= number_format($total, 2, ',', '.') ?>
                    <?php $content .= ob_get_contents(); ?>
                    <input hidden id="valor_total_despesas" value="<?php echo $total ?>" type="text">
                    <?php ob_start(); ?>
                </th>
            </tr>
        </tbody>
    </table>
    <?php $content .= ob_get_contents();
    ?>

    <div style="text-align: right; float:right;">
        <?php
        $controller->pagination($page, $limit, "", $controller->allRecord2("execucaodespesas e", $sqlAdd, ""));
        ?>
    </div>
<?php
    ob_start();
else :
?>
    <div class="col-md-12 alert alert-info text-center">
        Nenhum registo encontrado!
    </div>
<?php
endif;
?>
<!-- <script src="./class/pdf/js/VentanaCentrada.js"></script>
<script src="./class/pdf/js/pdf.js"></script> -->

<?php
$content .= ob_get_contents();
$_SESSION['html'] = $content;
$_SESSION['title'] = "Listagem de Despesas";
$_SESSION['dataInicio'] = "";
$_SESSION['dataFim'] = "";
$_SESSION['parametro1'] = "";
$_SESSION['parametro2'] = "";
$_SESSION['parametro3'] = "";
$_SESSION['parametro3'] = "";
$_SESSION['parametro4'] = "";
$_SESSION['user'] = $_SESSION['MM_nomeSgr'];
?>