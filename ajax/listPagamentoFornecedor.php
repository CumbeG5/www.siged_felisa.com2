<?php
@session_start();
$_SESSION['html'] = null;
$_SESSION['title'] = null;
$content = null;
include_once "../API/include.php";
include_once '../API/callAPI.php';
include_once './controller.php';
$controller = new Controller();
$data_inicio = $_GET["data_inicio"];
$data_fim = $_GET["data_fim"];
$numero = $_GET["nr_factura"];
$limit = (int)$_GET['limit'];
$page = empty($_GET['page']) ? 1 : (int)$_GET['page'];
// print_r($page);
// print_r($limit);
$response = CallApi($URL . "api/pagamento/select.php?data_inicio=$data_inicio&data_fim=$data_fim&nr_recibo=$numero&page=$page&limit=$limit");
// print_r($URL . "api/factura_global/select_all.php?fornecedor=$fornecedor_id&data_inicio=$data_inicio&data_fim=$data_fim&estado=$estado&numero=$numero&page=$page&limit=$limit");
// exit;
$sqlAdd = " AND estado != '0' ";
if (!empty($_GET['data_inicio']) or !empty($_GET['data_fim'])) {
    $data_inicio = $_GET['data_inicio'];
    $data_fim = $_GET['data_fim'];
    $sqlAdd .= " AND p.data BETWEEN '{$data_inicio}' AND '{$data_fim}' ";
}

if (!empty($_GET['nr_factura'])) {
    $nr_recibo = $_GET['nr_factura'];
    $sqlAdd .= "AND p.nr_recibo LIKE '%{$nr_recibo}%'";
}

if (isset($response->data) && count($response->data) > 0) :
?>

   
    <?php ob_start(); ?>
    <table id="listarfacturas" class="table table-bordered table-striped">
        <thead>
            <tr>
                <td>
                    <div align="center"><strong>Id</strong></div>
                </td>
                <td>
                    <div align="center"><strong>Fornecedor</strong></div>
                </td>
                <td>
                    <div align="center"><strong>Número do Recibo</strong></div>
                </td>
                <td>
                    <div align="center"><strong>Data</strong></div>
                </td>
                <td>
                    <div align="center"><strong>Data do Recibo</strong></div>
                </td>
                <td>
                    <div align="center"><strong>Total</strong></div>
                </td>
                <?php $content .= ob_get_contents(); ?>
                <td>
                    <div align="center"><strong>Acções</strong></div>
                </td>
                <?php ob_start(); ?>
            </tr>
        </thead>
        <tbody>
            <?php
            $total1 = 0;
            foreach ($response->data as $key => $row) :
                $total1 = $total1 + (float)$row->totalPago;
            ?>
                <tr>
                    <td><?= $row->id ?></td>
                    <td><?= $row->fornecedor ?></td>
                    <td><?= $row->nr_recibo ?></td>
                    <td><?= $row->data ?></td>
                    <td><?= $row->data_recibo ?></td>
                    <td><?= number_format((float)$row->totalPago, 2, ',', '.') ?></td>
                    <td>
                        <a id="viewPagamento" value="<?= $row->id ?>" class="viewPagamento btn btn-default" data-bs-toggle="tooltip" data-bs-title="Visualizar Pagamento"><i class="fa-solid fa-magnifying-glass-plus" style="color: #000000;"></i></a>
                        <a href="./class/pdf/documentos/report.php?id=<?= $row->id ?>" value="<?= $row->id ?>" target="_blank" class="btn btn-default" data-tooltip="Visualizar Nota Pagamento"><i class="fa-solid fa-print" style="color: #000000;"></i></a>
                        <a href="sigde.php?id=<?= $row->id ?>&amp;go=131" value="<?= $row->id ?>" class="btn btn-default" data-tooltip="Editar Nota Pagamento"><i class="fa-solid fa-pen-to-square" style="color: #161717;"></i></a>
                        <?php if (intval($row->totalPago) === 0) { ?>
                            <button value="<?= $row->id ?>" class="eliminarPagamento btn btn-default" data-tooltip="Eliminar Pagamento"><i class="fa-solid fa-trash" style="color: #161717;"></i></button>
                        <?php  } ?>
                    </td>
                </tr>

                <?php
                ?>
            <?php
            endforeach;
            ?>
        </tbody>
        <tfoot style="background-color: white;">
            <tr>

                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td style="font-weight: bold; font-size: 15px;" class="text-center">Total:</td>
                <td style="font-weight: bold; font-size: 15px;" class="text-right"><?= number_format($total1, 2, ',', '.') ?></td>
                <!-- <td style="font-weight: bold; font-size: 15px;" class="text-right"><?= number_format($total2, 2, ',', '.') ?></td> -->
                <td style="font-weight: bold; font-size: 15px;"></td>
            </tr>
        </tfoot>
    </table>
    <div style="text-align: right; float:right;">
        <?php
        $controller->pagination($page, $limit, "", $controller->allRecord("pagamento p ", $sqlAdd, ""));
        ?>
    </div>
<?php
else :
?>
    <div class="col-md-12 alert alert-info text-center">
        Nenhum registo encontrado!
    </div>
<?php
endif;
?>
<script>
    const tooltipTriggerList = document.querySelectorAll('[data-bs-toggle="tooltip"]')
    const tooltipList = [...tooltipTriggerList].map(tooltipTriggerEl => new bootstrap.Tooltip(tooltipTriggerEl))
</script>
<!-- pdf js -->
<!-- <script src="./class/pdf/js/VentanaCentrada.js"></script> -->
<!-- pdf js -->
<!-- <script src="./class/pdf/js/pdf.js"></script> -->

<?php
$content .= ob_get_contents();
$_SESSION['html'] = $content;
$_SESSION['title'] = "Listagem de facturas";
$_SESSION['dataInicio'] = "";
$_SESSION['dataFim'] = "";
$_SESSION['parametro1'] = "";
$_SESSION['parametro2'] = "";
$_SESSION['parametro3'] = "";
$_SESSION['parametro3'] = "";
$_SESSION['parametro4'] = "";
$_SESSION['user'] = $_SESSION['MM_nomeSgr'];
?>