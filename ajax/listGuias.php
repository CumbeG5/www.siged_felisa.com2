<?php
@session_start();
$_SESSION['html'] = null;
$_SESSION['title'] = null;
$content = null;
include_once "./../API/callAPI.php";
include_once "./../API/include.php";
$id =  isset($_GET['id']) ? $_GET['id'] : 0;
$data_inicio = $_GET["data_inicio"];
$data_fim = $_GET["data_fim"];
$valor = $_GET["valor"];
$numero = $_GET["numero"];

$response = CallApi($URL . "api/factura_global/read_one.php?id=$id&data_inicio=$data_inicio&data_fim=$data_fim&valor=$valor&numero=$numero", $METHOD = "GET");
// print_r($response->fetchGuias);
if (!empty($response->fetchGuias)) :
?>

    <body>
        <div class="mb-2 text-right">
            <button class="btn btn-secondary btn-sm m-1" id="print"><i class="fa-solid fa-print fa-beat fa-lg" style="color: #000000;"></i></button>
        </div>
        <?php ob_start(); ?>
        <h1>Factura Global Nº: <?= $response->numero ?></h1>
        <br>
        <table border="1" style="width: 100%;">
            <tr>
                <th>Seguradora</th>
                <th>Data</th>
                <th>Valor</th>
            </tr>
            <tr>
                <td><?= $response->seguradora ?></td>
                <td><?= $response->data ?></td>
                <?php $valor_total = 0;
                foreach ($response->fetchGuias->data as $value) :
                    if ($value->valor > $value->dadosFacturaGlobal) {
                        $valor_total += $value->saldo;
                    };
                endforeach;
                ?>
                <td style="text-align:right;"><?= number_format($valor_total, 2, ",", ".") ?></td>
            </tr>
            <tr>
                <td colspan="3"></td>
            </tr>
            <tr>
                <th colspan="3">Guias</th>
            </tr>
        </table>
        <table style="width: 100%;" border="1">
            <tr>
                <th>#</th>
                <th>Número da Guia</th>
                <th>Nome do Segurado</th>
                <th>Número de Membro</th>
                <th>Codigo de Autorização</th>
                <th>Nome do Técnico</th>
                <th>Data</th>
                <th>Valor</th>
                <th>Valor Pago</th>
                <th>Nota de Crédito</th>
                <th>Remanescente</th>

            </tr>
            <tbody>
                <?php $count = 1;
                $total = 0;
                $saldo = 0;
                $nota = 0;
                $pago = 0;
                if (count($response->fetchGuias->data) > 0) :
                    foreach ($response->fetchGuias->data as $value) :
                        if ($value->valor > $value->dadosFacturaGlobal) {
                ?>
                            <tr>
                                <td><?= $count++ ?></td>
                                <td><?= $value->nr_factura ?></td>
                                <td><?= $value->nome_segurado ?></td>
                                <td><?= $value->nr_membro ?></td>
                                <td><?= $value->codigo_autorizacao ?></td>
                                <td><?= $value->nome_do_tecnico ?></td>
                                <td><?= $value->data ?></td>
                                <td style="text-align:right;"><?= number_format($value->valor, 2, ",", ".") ?></td>
                                <td style="text-align:right;"><?= number_format($value->dadosFacturaGlobal, 2, ",", ".") ?></td>
                                <td style="text-align:right;"><?= number_format($value->nota_credito, 2, ",", ".") ?></td>
                                <td style="text-align:right;"><?= number_format($value->saldo - $value->nota_credito, 2, ",", ".") ?></td>

                            </tr>
                <?php
                            $total += $value->valor;
                            $saldo += $value->saldo;
                            $nota += $value->nota_credito;
                            $pago += $value->dadosFacturaGlobal;
                        }
                    endforeach;

                endif;
                ?>
            </tbody>
            <tfoot style="background-color: white;">
                <tr>
                    <td colspan="7" style="text-align:right; font-weight: bold; font-size: 15px;">Total:</td>
                    <td style="font-weight: bold; font-size: 15px; text-align:right;"><?= number_format($total, 2, ",", ".") ?></td>
                    <td style="font-weight: bold; font-size: 15px; text-align:right;"><?= number_format($pago, 2, ",", ".") ?></td>
                    <td style="font-weight: bold; font-size: 15px; text-align:right;"><?= number_format($nota, 2, ",", ".") ?></td>
                    <td style="font-weight: bold; font-size: 15px; text-align:right;"><?= number_format($saldo - $nota, 2, ",", ".") ?></td>

                </tr>
            </tfoot>
        </table>
    </body>
<?php else : ?>
    <div class="alert alert-info" role="alert"><strong class="text-capitalize">Alerta!</strong> Sem guias pendentes.
        <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    </div>
<?php
endif;


$content .= ob_get_contents();
$_SESSION['html'] = $content;
$_SESSION['title'] = "Listagem de Guias Pendentes";
$_SESSION['dataInicio'] = "";
$_SESSION['dataFim'] = "";
$_SESSION['parametro1'] = "";
$_SESSION['parametro2'] = "";
$_SESSION['parametro3'] = "";
$_SESSION['parametro3'] = "";
$_SESSION['parametro4'] = "";
$_SESSION['user'] = $_SESSION['MM_nomeSgr'];
?>