<?php 
include_once '../API/config/database.php';

class Controller{
    // Connection
    private $conn;
    public function __construct()
    {
        $database = new Database();
        $db = $database->getConnection();
        $this->conn = $db;
    }
    function pagination($actual_page, $total_rows, $this_page, $total){
        # Original PHP code by Chirp Internet: www.chirp.com.au
        # Please acknowledge use of this code by including this header.
        $num_results = $total;
        $response = '';

        if(!isset($actual_page) || !$page = intval($actual_page)) $page = 1;

        // extra variables to append to navigation links (optional)
        $linkextra = [];
        if(isset($_GET['var1']) && $var1 = $_GET['var1']) { // repeat as needed for each extra variable
            $linkextra[] = "var1=" . urlencode($var1);
        }
        $linkextra = implode("&amp;", $linkextra);
        if($linkextra) $linkextra .= "&amp;";

        // build array containing links to all pages
        $tmp = [];
        for($p=1, $i=0; $i < $num_results; $p++, $i += $total_rows) {
            if($page == $p) {
                // current page shown as bold, no link
                $tmp[] = "<li class='page-item active'><button type='button' class='page-link changePage' value='{$p}'>{$p}</button></li>";
            } else {
                $tmp[] = "<li class='page-item'><button type='button' class='page-link changePage' value='{$p}'>{$p}</button></li>";
            }
        }

        // thin out the links (optional)
        for($i = count($tmp) - 3; $i > 1; $i--) {
            if(abs($page - $i - 1) > 2) {
                unset($tmp[$i]);
            }
        }

        // display page navigation iff data covers more than one page
        if(count($tmp) > 1) {
            $response .= '<ul class="pagination" style="text-align:right"';
            if($page > 1) {
                // display 'Prev' link
                $response .= "<li class='page-item'><button type='button' class='page-link changePage' value='".($page - 1)."'>&laquo; Prev</button></li>";
            } else {
                $response .= "<li class='page-item disabled'><button type='button' class='btn btn-sm btn-default' value='' disabled>&laquo; Prev</button></li>";
            }

            $lastlink = 0;
            foreach($tmp as $i => $link) {
                if($i > $lastlink + 1) {
                    $response .= "<li class='page-item disabled'><button type='button' class='page-link' value=''> ... </button></li>";
                }
                $response .= $link;
                $lastlink = $i;
            }

            if($page <= $lastlink) {
                // display 'Next' link
                $response .= "<li class='page-item'><button type='button' class='page-link changePage' value='".($page + 1)."'>Next &raquo;</button></li>";
            }
            $response .= "</ul>";
            $response .= "\n";

            echo $response;
        }
    }
    // function limitSelecBox($limit){
    //     echo '
    //     <div class="input-group" style="margin-bottom: 8px;">
    //         <select class="form-control col-1 pesquisar" id="limit" title="Limit">
    //             <option '.($limit == 10 ? "selected" : "").' value="10">10</option>
    //             <option '.($limit == 50 ? "selected" : "").' value="50">50</option>
    //             <option '.($limit == 100 ? "selected" : "").' value="100">100</option>
    //             <option '.($limit == 250 ? "selected" : "").' value="250">250</option>
    //             <option '.($limit == 500 ? "selected" : "").' value="500">500</option>
    //         </select>
    //     </div>
    //     ';
    // }

    function allRecord($table, $sqlAdd, $columns=null)
    {
		if(!empty($columns))
			$columns = ",".$columns;
        $sql = "SELECT Count(id) as total {$columns} FROM {$table} WHERE 1=1 " .$sqlAdd." ";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        extract($stmt->fetch(PDO::FETCH_ASSOC));
        
        // $row = $stmt->rowCount();
        return $total;
    }
    function allRecord2($table, $sqlAdd, $columns=null)
    {
		if(!empty($columns))
			$columns = ",".$columns;
        $sql = "SELECT Count(idexecucaodespesas) as total {$columns} FROM {$table} WHERE 1=1 ".$sqlAdd." ";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        extract($stmt->fetch(PDO::FETCH_ASSOC));
        
        // $row = $stmt->rowCount();
        return $total;
    }
}
?>