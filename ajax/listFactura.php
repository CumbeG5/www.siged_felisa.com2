<?php
@session_start();
$_SESSION['html'] = null;
$_SESSION['title'] = null;
$content = null;
include_once "../API/include.php";
include_once '../API/callAPI.php';
include_once './controller.php';
$controller = new Controller();
$fornecedor_id = $_GET["fornecedor_id"];
$centro_custo_id = $_GET["centro_custo_id"];
$data_inicio = $_GET["data_inicio"];
$data_fim = $_GET["data_fim"];
$estado = $_GET["estado"];
$mes_inicial = $_GET["mes_inicial"];
$mes_final = $_GET["mes_final"];
$nr_factura = $_GET["nr_factura"];
$limit = (int)$_GET['limit'];
$page = empty($_GET['page']) ? 1 : (int)$_GET['page'];
// print_r($page);
$response = CallApi($URL . "api/factura/select.php?fornecedor_id=$fornecedor_id&data_inicio=$data_inicio&data_fim=$data_fim&c_custo_id=$centro_custo_id&estado=$estado&mes_inicial=$mes_inicial&mes_final=$mes_final&nr_factura=$nr_factura&page=$page&limit=$limit");

$sqlAdd = "";
if (!empty($data_inicio) or !empty($data_fim)) {
    $sqlAdd .= " AND f.data_factura BETWEEN '{$data_inicio}' AND '{$data_fim}' ";
}

if (!empty($fornecedor_id)) {
    $sqlAdd .= "AND f.fornecedor_id = '{$fornecedor_id}'";
}
if (!empty($c_custo_id)) {
    $sqlAdd .= "AND f.centro_custo_id = '{$centro_custo_id}'";
}
if (!empty($estado)) {
    $sqlAdd .= "AND f.estado = '{$estado}'";
} else {
    $sqlAdd .= "AND f.estado != '0'";
}
if (!empty($mes)) {
    $sqlAdd .= "AND f.mes = '{$mes}'";
}
if (!empty($mes_inicial) or !empty($mes_final)) {
    $sqlAdd .= " AND f.mes BETWEEN '{$mes_inicial}' AND '{$mes_final}' ";
}
if (!empty($nr_factura)) {
    $sqlAdd .= "AND f.nr_factura like '%$nr_factura%'";
}

if (isset($response->data) && count($response->data) > 0) :
?>

    <?php ob_start(); ?>
    <div class="mb-2 text-right">
        <button class="btn btn-success" id="print"><b>PDF</b></button>
    </div>
    <table id="listarfacturas" class="table table-bordered table-striped">
        <thead>
            <tr>
                <td style="font-weight: bold;">
                    <div align="center"><strong>Id</strong></div>
                </td>
                <td style="font-weight: bold;">
                    <div align="center"><strong>Fornecedor</strong></div>
                </td>
                <td style="font-weight: bold;">
                    <div align="center"><strong>Numero da factura</strong></div>
                </td>
                <td style="font-weight: bold;">
                    <div align="center"><strong>Data da Factura</strong></div>
                </td>
                <td style="font-weight: bold;">
                    <div align="center"><strong>Farmacia</strong></div>
                </td>
                <td style="font-weight: bold;">
                    <div align="center"><strong>Tipo de Factura</strong></div>
                </td>
                <td style="font-weight: bold;">
                    <div align="center"><strong>Mês</strong></div>
                </td>
                <td style="font-weight: bold;">
                    <div align="center"><strong>Valor</strong></div>
                </td>
                <td style="font-weight: bold;">
                    <div align="center"><strong>Saldo</strong></div>
                </td>
                <td>
                    <div align="center"><strong>Estado Pagamento</strong></div>
                </td>
                <?php $content .= ob_get_contents();
                ?>
                <td style="font-weight: bold;">
                    <div align="center"><strong>Acções</strong></div>
                </td>
                <?php ob_start();
                ?>
            </tr>
        </thead>
        <tbody>
            <?php $total_valor = 0;
            $count_vfa = 0;
            $total_vfa = 0;
            $count_vnc = 0;
            $total_vnc = 0;
            foreach ($response->data as $key => $row) :
                $total_valor += $row->valor;
                $row->tipo_factura == 'VFA' ? $total_vfa += $row->valor : $total_vnc += $row->valor;
                if ($row->tipo_factura == 'VFA') {
                    $count_vfa++;
            ?>
                    <tr>
                        <td><?= $row->id ?></td>
                        <td><?= $row->fornecedor ?></td>
                        <td><?= $row->nr_factura ?></td>
                        <td><?= $row->data_factura ?></td>
                        <td><?= $row->farmacia ?></td>
                        <td><?= $row->tipo_factura ?></td>
                        <td><?= $row->mes_correspondente ?></td>
                        <td class="text-right"><?= number_format($row->valor, 2, ',', '.') ?></td>
                        <td class="text-right"><?= number_format($row->saldo, 2, ',', '.') ?></td>
                        <td>
                            <?= $row->saldo <= 0 ? '<span class="badge badge-pill badge-success">Pago</span>' : (($row->saldo < $row->valor) && $row->saldo > 0 ? '<span class="badge badge-pill badge-warning">Parcial</span>' : ($row->saldo == $row->valor ? '<span class="badge badge-pill badge-danger">Pendente</span>' : "")) ?></td>
                        <td class="text-center">
                            <a id="viewFactura" value="<?= $row->id ?>" class="viewFactura btn btn-default" data-tooltip="Visualizar Factura"><i class="fa-solid fa-magnifying-glass-plus" style="color: black;"></i></a>
                            <?php if ($row->saldo == $row->valor) { ?>
                                <a class="btn btn-default" href="./frmFacturaFornecedorUpdate.php?id=<?= $row->id ?>" data-tooltip="Editar Factura"> <i class="fa-solid fa-pen-to-square" style="color: #000000;"></i></a>
                                <a id="deleteFactura" value="<?= $row->id ?>" class="deleteFactura btn btn-default" data-tooltip="Eliminar Factura"><i class="fa-solid fa-trash"></i></a>
                            <?php } ?>
                        </td>
                    </tr>

                <?php
                }
                ?>
            <?php
            endforeach;
            ?>
            <tr>
                <td style="font-weight: bold; font-size: 15px;" class="text-center">Total VFA:</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td style="font-weight: bold; font-size: 15px;" class="text-right"><?= number_format($total_vfa, 2, ',', '.') ?></td>
            </tr>
            <?php
            foreach ($response->data as $key => $row) :
                if ($row->tipo_factura == 'VNC') {
                    $count_vnc++;
            ?>
                    <tr>
                        <td><?= $row->id ?></td>
                        <td><?= $row->fornecedor ?></td>
                        <td><?= $row->nr_factura ?></td>
                        <td><?= $row->data_factura ?></td>
                        <td><?= $row->farmacia ?></td>
                        <td><?= $row->tipo_factura ?></td>
                        <td><?= $row->mes_correspondente ?></td>
                        <td class="text-right"><?= number_format($row->valor, 2, ',', '.') ?></td>
                        <td class="text-right"><?= number_format($row->saldo, 2, ',', '.') ?></td>
                        <td><?= $row->estado == "4" ? '<span class="badge badge-pill badge-success">Pago</span>' : ($row->estado == "3" ? '<span class="badge badge-pill badge-warning">Parcial</span>' : ($row->estado == "2" || $row->estado == "1" ? '<span class="badge badge-pill badge-danger">Pendente</span>' : "")) ?></td>
                        <td class="text-center">
                            <a id="viewFactura" value="<?= $row->id ?>" class="viewFactura btn btn-default" data-tooltip="Visualizar Factura"><i class="ui zoom icon"></i></a>
                            <?php if ($row->estado == "1" || $row->estado == "2") { ?>
                                <a class="btn btn-default" href="sigde.php?id=<?= $row->id ?>&amp;go=125" data-tooltip="Editar Factura"> <i class="ui edit icon"></i> </a>
                                <a id="deleteFactura" value="<?= $row->id ?>" class="deleteFactura btn btn-default" data-tooltip="Eliminar Factura"><i class="ui trash icon"></i></a>
                            <?php } ?>
                        </td>
                    </tr>
                <?php
                }
                ?>
            <?php
            endforeach;
            ?>
            <tr>
                <td style="font-weight: bold; font-size: 15px;" class="text-center">Total VNC:</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td style="font-weight: bold; font-size: 15px;" class="text-right"><?= number_format($total_vnc, 2, ',', '.') ?></td>
            </tr>
        </tbody>
        <tfoot style="background-color: white;">
            <tr>
                <td style="font-weight: bold; font-size: 15px;" class="text-center">Total Geral:</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td style="font-weight: bold; font-size: 15px;" class="text-right"><?= number_format($total_vfa - $total_vnc, 2, ',', '.') ?></td>
                <td style="font-weight: bold; font-size: 15px;"></td>
            </tr>
        </tfoot>
    </table>
    <div style="text-align: right; float:right;">
        <?php
        $controller->pagination($page, $limit, "", $controller->allRecord("factura f", $sqlAdd, ""));
        ?>
    </div>
<?php
else :
?>
    <div class="col-md-12 alert alert-info text-center">
        Nenhum registo encontrado!
    </div>
<?php
endif;
?>
<!-- pdf js -->
<!-- <script src="./class/pdf/js/VentanaCentrada.js"></script> -->
<!-- pdf js -->
<!-- <script src="./class/pdf/js/pdf.js"></script> -->

<?php
$content .= ob_get_contents();
$_SESSION['html'] = $content;
$_SESSION['title'] = "Listagem de facturas";
$_SESSION['dataInicio'] = "";
$_SESSION['dataFim'] = "";
$_SESSION['parametro1'] = "";
$_SESSION['parametro2'] = "";
$_SESSION['parametro3'] = "";
$_SESSION['parametro3'] = "";
$_SESSION['parametro4'] = "";
$_SESSION['user'] = $_SESSION['MM_nomeSgr'];
?>