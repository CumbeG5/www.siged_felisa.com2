<?php
@session_start();
$_SESSION['html'] = null;
$_SESSION['title'] = null;
$content = null;
include_once "../API/include.php";
include_once '../API/callAPI.php';
include_once './controller.php';
$controller = new Controller();
$data_inicio = $_GET["data_inicio"];
$data_fim = $_GET["data_fim"];
$numero = $_GET["nr_factura"];
$limit = (int)$_GET['limit'];
$page = empty($_GET['page']) ? 1 : (int)$_GET['page'];
// print_r($page);
// print_r($limit);
$response = CallApi($URL . "api/pagamento_factura_cliente/select.php?data_inicio=$data_inicio&data_fim=$data_fim&nr_pagamento=$numero&page=$page&limit=$limit");
// print_r($URL . "api/factura_global/select_all.php?fornecedor=$fornecedor_id&data_inicio=$data_inicio&data_fim=$data_fim&estado=$estado&numero=$numero&page=$page&limit=$limit");
// exit;
$sqlAdd = " AND estado != '0' ";
if(!empty($_GET['data_inicio']) OR !empty($_GET['data_fim'])){
    $data_inicio = $_GET['data_inicio'];
    $data_fim = $_GET['data_fim'];
    $sqlAdd .= " AND pf.data_pagamento BETWEEN '{$data_inicio}' AND '{$data_fim}' ";
}

if(!empty($_GET['nr_factura'])){
    $nr_pagamento = $_GET['nr_factura'];
    $sqlAdd .= "AND pf.nr_pagamento LIKE '%{$nr_pagamento}%'";
}

if(!empty($_GET['seguradora_id'])){
    $seguradora_id = $_GET['seguradora_id'];
    $sqlAdd .= "AND pf.seguradora_id = {$seguradora_id}";
}

if (isset($response->data) && count($response->data) > 0) :
?>


    <?php ob_start(); ?>
    <table id="listarfacturas" class="table table-bordered table-striped">
        <thead>
            <tr>
                <td>
                    <div align="center"><strong>Id</strong></div>
                </td>
                <td>
                    <div align="center"><strong>Seguradora</strong></div>
                </td>
                <td>
                    <div align="center"><strong>Número pagamento</strong></div>
                </td>
                <td>
                    <div align="center"><strong>Número</strong></div>
                </td>
                <td>
                    <div align="center"><strong>Data</strong></div>
                </td>
                <td>
                    <div align="center"><strong>Valor Pago</strong></div>
                </td>
                <td>
                    <div align="left"><strong>Estado</strong></div>
                </td>
                <?php $content .= ob_get_contents(); ?>
                <td>
                    <div align="center"><strong>Acções</strong></div>
                </td>
                <?php ob_start(); ?>
            </tr>
        </thead>
        <tbody>
            <?php
            $total1 = 0;
            foreach ($response->data as $key => $row) :
                $total1 = $total1 + (float)$row->totalPago;
            ?>
                <tr>
                    <td><?= $row->id ?></td>
                    <td><?= $row->cliente ?></td>
                    <td><?= $row->nr_nota_pagamento ?></td>
                    <td><?= $row->nr_pagamento ?></td>
                    <td><?= $row->data_pagamento ?></td>
                    <td><?= number_format((float)$row->totalPago, 2, ',', '.') ?></td>
                    <td>
                        <?php if ($row->estado === "2") { ?>
                            <span style="text-align: center;" class="badge badge-pill badge-success text-center">Terminado</span>
                        <?php } else if ($row->estado === "1") { ?>
                            <span style="text-align: center;" class="badge badge-pill badge-warning text-center">Pendente</span>
                        <?php } else if ($row->estado === "0") { ?>
                            <span style="text-align: center;" class="badge badge-pill badge-danger text-center">Eliminado</span>
                        <?php } ?>
                    </td>
                    <td>
                        <a id="viewPagamentoCliente" value="<?= $row->id ?>" class="viewPagamentoCliente btn btn-default" data-tooltip="Visualizar Pagamento"><i class="fa-solid fa-magnifying-glass-plus" style="color: #000000;"></i></a>
                        <a href="./class/pdf/documentos/reportCliente.php?id=<?= $row->id ?>" value="<?= $row->id ?>" target="_blank" class="btn btn-default" data-tooltip="Visualizar Nota Pagamento"><i class="fa-solid fa-print" style="color: #000000;"></i></a>
                        <?php if ($row->estado === "1") { ?>
                            <a class="btn btn-default" href="sigde.php?id=<?= $row->id ?>&amp;go=126" data-tooltip="Editar Pagamento"> <i class="fa-solid fa-pen-to-square" style="color: #000000;"></i> </a>

                            <?php if (intval($row->totalPago) === 0) { ?>
                                <button class="btn btn-default eliminarPagamentoCliente" value="<?= $row->id ?>" data-tooltip="Eliminar Pagamento"> <i class="fa-solid fa-trash" style="color: #000000;"></i> </button>
                        <?php }
                        } ?>
                    </td>
                </tr>

                <?php
                ?>
            <?php
            endforeach;
            ?>
        </tbody>
        <tfoot style="background-color: white;">
            <tr>

                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td style="font-weight: bold; font-size: 15px;" class="text-center">Total:</td>
                <td style="font-weight: bold; font-size: 15px;" class="text-right"><?= number_format($total1, 2, ',', '.') ?></td>
                <!-- <td style="font-weight: bold; font-size: 15px;" class="text-right"><?= number_format($total2, 2, ',', '.') ?></td> -->
                <td style="font-weight: bold; font-size: 15px;"></td>
            </tr>
        </tfoot>
    </table>
    <div style="text-align: right; float:right;">
        <?php
        $controller->pagination($page, $limit, "", $controller->allRecord("pagamento_factura_cliente pf ", $sqlAdd, ""));
        ?>
    </div>
<?php
else :
?>
    <div class="col-md-12 alert alert-info text-center">
        Nenhum registo encontrado!
    </div>
<?php
endif;
?>
<script>
    const tooltipTriggerList = document.querySelectorAll('[data-bs-toggle="tooltip"]')
    const tooltipList = [...tooltipTriggerList].map(tooltipTriggerEl => new bootstrap.Tooltip(tooltipTriggerEl))
</script>
<!-- pdf js -->
<!-- <script src="./class/pdf/js/VentanaCentrada.js"></script> -->
<!-- pdf js -->
<!-- <script src="./class/pdf/js/pdf.js"></script> -->

<?php
$content .= ob_get_contents();
$_SESSION['html'] = $content;
$_SESSION['title'] = "Farmácia Maria Elisa";
$_SESSION['dataInicio'] = "";
$_SESSION['dataFim'] = "";
$_SESSION['parametro1'] = "";
$_SESSION['parametro2'] = "";
$_SESSION['parametro3'] = "";
$_SESSION['parametro3'] = "";
$_SESSION['parametro4'] = "";
$_SESSION['user'] = $_SESSION['MM_nomeSgr'];
?>