<?php
@session_start();
include "./classes/classes.php";
include('Connections/conexaosiged.php');
require_once("classes/Geral.php");

$query_comboempresass = "SELECT * FROM fornecedors";
$comboempresass = $conexaosiged->prepare($query_comboempresass);
$comboempresass->execute();

$query_combocentrodecusto = "SELECT * FROM centro_de_custo";
$combocentrodecusto = $conexaosiged->prepare($query_combocentrodecusto);
$combocentrodecusto->execute();

$meses = array(1 => "Janeiro", 2 => "Fevereiro", 3 => "Março", 4 => "Abril", 5 => "Maio", 6 => "Junho", 7 => "Julho", 8 => "Agosto", 9 => "Setembro", 10 => "Outubro", 11 => "Novembro", 12 => "Dezembro");

// New
$dataMes = date('m');
$dataAno = date('Y');

$query_grupo = "SELECT * FROM grupo WHERE visibilidade=1";
$grupo = $conexaosiged->prepare($query_grupo);
$grupo->execute();

$sqlGrupo_Depesas = "SELECT * FROM `grupodespesas`";
$queryDespesas = $conexaosiged->prepare($sqlGrupo_Depesas);
$queryDespesas->execute();


$sql = "SELECT * FROM `execucaodespesas` where MONTH(execucaodespesas.data)=$dataMes  AND YEAR(execucaodespesas.data) = $dataAno";
$stmt = $conexaosiged->prepare($sql);
$stmt->execute();

$query_comboempresass = "SELECT * FROM empresas";
$comboempresass = $conexaosiged->prepare($query_comboempresass);
$comboempresass->execute();

$query_combocentrodecusto = "SELECT * FROM centro_de_custo";
$combocentrodecusto = $conexaosiged->prepare($query_combocentrodecusto);
$combocentrodecusto->execute();


$i = 0;
?>
<!DOCTYPE html>
<html lang="en" dir="">
<meta http-equiv="content-type" content="text/html;charset=utf-8" />

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width,initial-scale=1" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>SIGED | Listar Despesas</title>
    <link href="./fonts.googleapis.com/csse27b.css?family=Nunito:300,400,400i,600,700,800,900" rel="stylesheet" />
    <link href="./dist-assets/css/themes/lite-purple.min.css" rel="stylesheet" />
    <link href="./dist-assets/css/plugins/perfect-scrollbar.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="./dist-assets/fonts/fontawesome/css/all.css" />
    <link href="./dist-assets/css/plugins/metisMenu.min.css" rel="stylesheet" />
    <link href="./dist-assets/flatpickr/dist/flatpickr.min.css" rel="stylesheet" />
    <link href="./dist-assets/fonts/fontawesome/css/fontawesome.min.css" rel="stylesheet" />
</head>

<body class="text-left">
    <div class="app-admin-wrap layout-horizontal-bar">
        <?php include_once './topbar.php' ?>
        <!-- =============== Horizontal bar End ================-->
        <div class="main-content-wrap d-flex flex-column">
            <!-- ============ Body content start ============= -->
            <div class="main-content">
                <div class="breadcrumb">
                    <h1>Listagem De Despesas</h1>
                </div>
                <div class="separator-breadcrumb border-top"></div>
                <div class="card">

                    <div class=" card-body ">
                        <div style="text-align: end;">
                            <a href="frmDespesa.php">
                                <button class="btn btn-success ripple m-1" type="button">Registar</button>
                            </a>
                        </div>
                        <div class="row">
                            <div class="col-md-3 form-group mb-3">
                                <label>Data Início:</label>
                                <input placeholder="yyyy-mm-dd" style="background: white !important;" name="data_inicio" value="" type="datetime" id="data_inicio" class="data1 search form-control" />
                            </div>
                            <div class="col-md-3 form-group mb-3">
                                <label>Data Fim:</label>
                                <input placeholder="yyyy-mm-dd" style="background: white !important;" name="data_fim" value="" type="datetime" id="data_fim" class="data1 search form-control" />
                            </div>

                            <div class="col-md-3 form-group mb-3">
                                <label for="picker1">Centro de custos:</label>
                                <select name="idcentrodecustos" id="idcentrodecustos" class="form-control ui fluid search selection dropdown resumoDispesas">
                                    <option value="">Todas Opções...</option>
                                    <?php
                                    foreach ($combocentrodecusto as $row_combocentrodecusto) {
                                    ?>
                                        <option value="<?php echo $row_combocentrodecusto['idcentrodecusto'] ?>"><?php echo $row_combocentrodecusto['titulo'] ?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-md-3 form-group mb-3">
                                <label>Grupo Despesas:</label>
                                <select name="grupos" id="grupos" class="form-control ui  fluid search selection dropdown resumoDispesas">
                                    <option value="">Todas Opções...</option>
                                    <?php foreach ($grupo as $item) : ?>
                                        <option value="<?= $item['id'] ?>"><?= $item['nome'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="col-md-3 form-group mb-3">
                                <label>Sub-grupo:</label>
                                <select name="subgrupo" id="subgrupo" class="ui form-control subgrupo_listagem fluid search selection dropdown resumoDispesas">
                                    <!-- <option value="">Todas Opções...</option> -->
                                </select>
                            </div>
                            <div class="col-md-3 form-group mb-3">
                                <label>Fornecedor:</label>
                                <select name="fornecedorResumo" id="fornecedorResumo_d" class="form-control ui fluid search selection dropdown resumoDispesas">
                                    <option value="">Todas Opções...</option>
                                    <?php
                                    foreach ($comboempresass as $row_comboempresass) {
                                    ?>
                                        <option value="<?= $row_comboempresass['idempresa'] ?>"><?= $row_comboempresass['descricaoempresa'] ?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-md-3 form-group mb-3">
                                <label>Total:</label>
                                <input style="font-weight: bolder; background: white; text-align: right;" value="" type="text" id="totalView" class="form-control" readonly />
                            </div>
                            <div class="col-md-3 form-group mb-3">
                                <label class="">Limite:<span style="color:red;"> </span></label>
                                <select name="limit" id="limit" title="Limit" class="ui fluid search selection dropdown form-control">
                                    <!-- <option value="2">2</option> -->
                                    <option value="10">10</option>
                                    <option value="25">25</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                    <option value="250">250</option>
                                    <option value="500">500</option>
                                    <option value="1000">1000</option>
                                    <option value="2000">2000</option>
                                </select>
                            </div>
                        </div>
                        <br>
                        <br>
                        <!-- Tabela -->
                        <div id="tabela_despesas_gerais" class="table-factura-fornecedor table-responsive">
                        </div>
                        <!-- </div> -->
                    </div>
                </div>
                <!-- end of main-content -->
            </div>
            <!-- Footer Start -->
            <div class="flex-grow-1"></div>
        </div>
    </div>

    <!-- ============ Search UI End ============= -->
    <script src="./dist-assets/js/plugins/jquery-3.3.1.min.js"></script>
    <script src="./dist-assets/js/plugins/bootstrap.bundle.min.js"></script>
    <script src="./dist-assets/js/plugins/perfect-scrollbar.min.js"></script>
    <script src="./dist-assets/js/scripts/script.min.js"></script>
    <script src="./dist-assets/js/scripts/sidebar-horizontal.script.js"></script>
    <script src="./dist-assets/flatpickr/dist/flatpickr.min.js"></script>
    <script src="./js/dateformat.js"></script>
    <script src="./dist-assets/fonts/fontawesome/js/all.js"></script>
    <script src="./dist-assets/numeral/min/numeral.min.js"></script>
    <!-- Despesa -->
    <script src="./js/listDespesas.js"></script>
    <!-- pdf js -->
    <script src="./class/pdf/js/VentanaCentrada.js"></script>
    <!-- pdf js -->
    <script src="./class/pdf/js/pdf.js"></script>
</body>


<!-- Mirrored from demos.ui-lib.com/gull/html/layout3/accordion.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 19 Aug 2021 10:04:12 GMT -->

</html>