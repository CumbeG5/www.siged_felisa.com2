<?php
@session_start();
include "./classes/classes.php";
include('Connections/conexaosiged.php');
require_once("classes/Geral.php");

$query_comboempresass = "SELECT * FROM empresas";
$comboempresass = $conexaosiged->prepare($query_comboempresass);
$comboempresass->execute();

$query_comboclientes = "SELECT * FROM `clientes` WHERE tipo_cliente_id IN (2,3,4) AND activo='1'";
$comboclientes = $conexaosiged->prepare($query_comboclientes);
$comboclientes->execute();

$tipo_factura = "SELECT * FROM tipo_factura";
$factura = $conexaosiged->prepare($tipo_factura);
$factura->execute();

$query_combocentrodecusto = "SELECT * FROM centro_de_custo";
$combocentrodecusto = $conexaosiged->prepare($query_combocentrodecusto);
$combocentrodecusto->execute();

$query_tecnico = "SELECT * FROM tecnico WHERE activo= '1'";
$tecnico = $conexaosiged->prepare($query_tecnico);
$tecnico->execute();

$result = $tecnico->fetchAll();
?>
<!DOCTYPE html>
<html lang="en" dir="">
<meta http-equiv="content-type" content="text/html;charset=utf-8" />

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width,initial-scale=1" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>SIGED | Registar Facturas Seguradora</title>
    <!-- <link href="./fonts.googleapis.com/csse27b.css?family=Nunito:300,400,400i,600,700,800,900" rel="stylesheet" /> -->
    <link href="./dist-assets/css/themes/lite-purple.css" rel="stylesheet" />
    <link href="./dist-assets/css/plugins/perfect-scrollbar.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="./dist-assets/fonts/fontawesome/css/all.css" />
    <link href="./dist-assets/css/plugins/metisMenu.min.css" rel="stylesheet" />
    <link href="./dist-assets/flatpickr/dist/flatpickr.min.css" rel="stylesheet" />
    <link href="./dist-assets/fonts/fontawesome/css/fontawesome.min.css" rel="stylesheet" />
    <link href="./dist-assets/toastr/build/toastr.css" rel="stylesheet" />
    <link rel="stylesheet" href="./dist-assets/sweetalert2/dist/sweetalert2.css" />
</head>

<body class="text-left">
    <div class="app-admin-wrap layout-horizontal-bar">
        <?php include_once './topbar.php' ?>
        <!-- =============== Horizontal bar End ================-->
        <div class="main-content-wrap d-flex flex-column">
            <!-- ============ Body content start ============= -->
            <div class="main-content">
                <div class="breadcrumb">
                    <h1>Registar Factura</h1>
                </div>
                <div class="separator-breadcrumb border-top"></div>
                <div class="card">

                    <div class=" card-body ">
                        <div class="uk-form-blank">
                            <div>
                                <label for="" style="font-weight: bold;">Dados da Factura Global</label>
                            </div>
                            <form class="ui form" method="post" enctype="multipart/form-data" name="formFacturaGlobal" id="formFacturaGlobal">
                                <input type="hidden" id="user_id" name="user_id" value="<?= $_SESSION['MM_idUserSgr']; ?>">
                                <input type="hidden" id="totalGuiasCarrinho" name="totalGuiasCarrinho" value="">
                                <div class="row">
                                    <div class="col-md-4 form-group mb-3">
                                        <label class="">Cliente:<span style="color:red;"> *</span></label>
                                        <select style="background: white !important;" name="factura_global_id" class="form-control" id="factura_global_id">
                                            <option value="">--selecione um fornecedor--</option>
                                            <?php
                                            foreach ($comboclientes as $value) {
                                            ?>
                                                <option value="<?= $value['id'] ?>" tipo_cliente_id="<?= $value['tipo_cliente_id'] ?>"><?= $value['nome'] ?></option>
                                            <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-md-4 form-group mb-3">
                                        <label class="">Data da Factura:<span style="color:red;"> *</span></label>
                                        <input style="background: white !important;" class="form-control data1" name="data_factura_global" type="datetime" id="data_factura_global" placeholder="Data da Factura Global" />
                                    </div>
                                    <div class="col-md-4 form-group mb-3">
                                        <label class="">Número da Factura:<span style="color:red;"> *</span></label>
                                        <input style="background: white !important;" class="form-control" name="nr_factura_global" type="text" id="nr_factura_global" placeholder="Numero do Recibo/Factura" />
                                    </div>

                                </div>
                                <div style="text-align: right;">
                                    <button id="rgFacturaGlobal" form="formFacturaGlobal" type="submit" class="btn btn-success pull-right">REGISTAR</button>
                                </div><br><br>
                            </form>
                            <form class="" method="post" enctype="multipart/form-data" name="formFacturaGlobalCliente" id="formFacturaGlobalCliente">
                                <input type="hidden" id="last_factura_global_id" name="last_factura_global_id" value="">
                                <input type="hidden" id="user_id" name="user_id" value="<?= $_SESSION['MM_idUserSgr']; ?>">

                                <div hidden id="formSegurado">
                                    <label for="" style="font-weight: bold;">Dados do Segurado</label>
                                    <div class="row">
                                        <div class="col-md-4 form-group mb-3">
                                            <label class="">Nome do Segurado:<span style="color:red;"> *</span></label>
                                            <input style="background: white !important;" class="form-control" style="color: black; font-weight: bold;" name="nome_segurado" type="text" id="nome_segurado" />
                                        </div>
                                        <div class="col-md-4 form-group mb-3">
                                            <label class="">Número de Membro:<span style="color:red;"> *</span></label>
                                            <input style="background: white !important;" class="form-control" style="color: black; font-weight: bold;" name="nr_membro" type="text" id="nr_membro" />
                                        </div>
                                        <div class="col-md-4 form-group mb-3">
                                            <label class="">Codigo da Autorização:<span style="color:red;"> *</span></label>
                                            <input style="background: white !important;" class="form-control" style="color: black; font-weight: bold;" name="codigo_autorizacao" type="text" id="codigo_autorizacao" />
                                        </div>

                                        <div class="col-md-4 form-group mb-3">
                                            <label class="">Número da Guia:<span style="color:red;"> *</span></label>
                                            <input style="background: white !important;" class="form-control" style="color: black; font-weight: bold;" name="nr_factura" type="text" id="nr_factura" />
                                        </div>
                                        <div class="col-md-4 form-group mb-3">
                                            <label class="">Valor Líquido:<span style="color:red;"> *</span></label>
                                            <input style="background: white !important;" class="form-control" step="0.01" name="valor" type="number" id="valor" />
                                        </div>
                                        <div class="col-md-4 form-group mb-3">
                                            <label class="">Data da Guia:<span style="color:red;"> *</span></label>
                                            <input style="background: white !important;" class="form-control data1" name="data" value="" type="datetime" id="data" />
                                        </div>
                                        <div class="col-md-4 form-group mb-3">
                                            <label class="">Nome do Técnico:</label>
                                            <select style="background: white !important;" name="nome_tecnico" class="form-control" id="nome_tecnico">
                                                <option value="">--selecione um tecnico--</option>
                                                <?php foreach ($result as $row) { ?>
                                                    <option value="<?= $row["id"] ?>"><?= $row["nome"] ?></option>
                                                <?php  } ?>
                                            </select>
                                        </div>
                                        <div class="col-md-4 form-group mb-3">
                                            <label class="">Anexo:</label>
                                            <div class="">
                                                <input style="background: white !important;" class="form-control" type="file" name="anexo_factura" id="anexo_factura" data-show-caption="true" data-msg-placeholder="Selecione um arquivo para carregar..." />
                                            </div>
                                        </div>
                                    </div>
                                    <br><br>
                                    <div style="text-align:right;">
                                        <div class="btn-group">
                                            <button id="verFacturas" hidden type="button" class="btn btn-danger pull-right pointer ml-1">Sair</button>
                                            <button style="background-color: #12abde !important; border-color: #12abde;" id="rgFacturaCliente" hidden form="formFacturaGlobalCliente" type="submit" class="btn btn-primary pull-right pointer ml-1">Adicionar</button>
                                        </div>
                                    </div>
                                </div>
                                <br><br>
                                <div id="listSegurado" hidden class="">
                                </div>
                                <br>
                                <div style="text-align:right;">
                                    <button style="border-color: #37a0d1cb !important;" id="verFacturaGlogal" hidden type="button" class="btn btn-info btn-lg pull-right pointer ml-1">Terminar</button>
                                </div>
                                <br>
                                <br>
                                <div class="ui error message"></div>
                            </form>
                        </div>


                        <br>
                        <br>

                        <!-- </div> -->
                    </div>
                </div>
                <!-- end of main-content -->
            </div>
            <!-- Footer Start -->
            <div class="flex-grow-1"></div>
        </div>
    </div>

    <!-- ============ Search UI End ============= -->
    <script src="./dist-assets/js/plugins/jquery-3.3.1.min.js"></script>
    <script src="./dist-assets/js/plugins/bootstrap.bundle.min.js"></script>
    <script src="./dist-assets/js/plugins/perfect-scrollbar.min.js"></script>
    <script src="./dist-assets/js/scripts/script.min.js"></script>
    <script src="./dist-assets/js/scripts/sidebar-horizontal.script.js"></script>
    <script src="./dist-assets/flatpickr/dist/flatpickr.min.js"></script>
    <script src="./js/dateformat.js"></script>
    <script src="./dist-assets/fonts/fontawesome/js/all.js"></script>
    <script src="./dist-assets/toastr/build/toastr.min.js"></script>
    <script src="./dist-assets/sweetalert2/dist/sweetalert2.all.js"></script>
    <script src="./js/rgFacturaSeguradora.js"></script>

    <!-- Factura -->
</body>

</html>

<script>
    $(document).ready(function() {

        $(document).on("dblclick", ".td_nr_membro", function() {

            var numerador = $(this).attr("numerador");
            // alert(numerador)
            $(".input_nr_membro" + numerador).attr("hidden", false);
            $(".input_nr_membro" + numerador).focus();
            $(".span_nr_membro" + numerador).attr("hidden", true);

            var factura_id = $(this).attr("factura_id");
            var id_guia = $(this).attr("id_guia");
            // alert(id_guia)

            $('.input_nr_membro').focusout(function() {
                var val = $(".input_nr_membro" + numerador).val();
                $(".span_nr_membro" + numerador).text(val);
                $(".input_nr_membro" + numerador).attr("hidden", true);
                $(".span_nr_membro" + numerador).attr("hidden", false);

                var nome_segurado = $(".input_nome_segurado" + numerador).val();
                var nr_membro = $(".input_nr_membro" + numerador).val();
                var nr_factura = $(".input_nr_factura" + numerador).val();
                var codigo_autorizacao = $(".input_codigo_autorizacao" + numerador).val();
                var valor = $(".input_valor" + numerador).val();
                var data = $(".input_data" + numerador).val();
                // var nr_membro_anterior = $(".input_nr_membro"+numerador).attr('nr_membro_anterior');

                if (nr_membro != "") {
                    $.ajax({
                        url: './API/api/factura_cliente/replace.php?id_guia=' + id_guia,
                        method: 'POST',
                        data: {
                            id_guia: id_guia,
                            nome_segurado: nome_segurado,
                            nr_membro: nr_membro,
                            nr_factura: nr_factura,
                            codigo_autorizacao: codigo_autorizacao,
                            valor: valor,
                            data: data
                        },
                        dataType: 'json',
                        success: function(response) {
                            if (response.status_text == 'error') {
                                Swal.fire({
                                    icon: 'error',
                                    title: response.message,
                                    showConfirmButton: true,
                                })
                            }

                            getItemFacturaGlobal('./sessao_facturas/fetch_item_factura_global.php?factura_global_id=' + factura_id ? './sessao_facturas/fetch_item_factura_global.php?factura_global_id=' + factura_id : 0);
                        },
                        error: function(err) {
                            alert("Deu erro");
                            console.log(err)
                        }
                    });
                } else {
                    getItemFacturaGlobal('./sessao_facturas/fetch_item_factura_global.php?factura_global_id=' + factura_id ? './sessao_facturas/fetch_item_factura_global.php?factura_global_id=' + factura_id : 0);
                    Swal.fire({
                        icon: 'error',
                        title: 'O campo Nr de membro não pode estar vazio!',
                        showConfirmButton: true,
                    })
                }
            })
        });

        $(document).on("dblclick", ".td_nome_segurado", function() {

            var numerador = $(this).attr("numerador");
            // alert(numerador)
            $(".input_nome_segurado" + numerador).attr("hidden", false);
            $(".input_nome_segurado" + numerador).focus();
            $(".span_nome_segurado" + numerador).attr("hidden", true);

            var factura_id = $(this).attr("factura_id");
            var id_guia = $(this).attr("id_guia");
            // alert(id_guia)

            $('.input_nome_segurado').focusout(function() {
                var val = $(".input_nome_segurado" + numerador).val();
                $(".span_nome_segurado" + numerador).text(val);
                $(".input_nome_segurado" + numerador).attr("hidden", true);
                $(".span_nome_segurado" + numerador).attr("hidden", false);

                var nome_segurado = $(".input_nome_segurado" + numerador).val();
                var nr_membro = $(".input_nr_membro" + numerador).val();
                var nr_factura = $(".input_nr_factura" + numerador).val();
                var codigo_autorizacao = $(".input_codigo_autorizacao" + numerador).val();
                var valor = $(".input_valor" + numerador).val();
                var data = $(".input_data" + numerador).val();
                // var nr_membro_anterior = $(".input_nr_membro"+numerador).attr('nr_membro_anterior');

                if (nome_segurado != "") {
                    $.ajax({
                        url: './API/api/factura_cliente/replace.php?id_guia=' + id_guia,
                        method: 'POST',
                        data: {
                            id_guia: id_guia,
                            nome_segurado: nome_segurado,
                            nr_membro: nr_membro,
                            nr_factura: nr_factura,
                            codigo_autorizacao: codigo_autorizacao,
                            valor: valor,
                            data: data
                        },
                        dataType: 'json',
                        success: function(response) {
                            if (response.status_text == 'error') {
                                Swal.fire({
                                    icon: 'error',
                                    title: response.message,
                                    showConfirmButton: true,
                                })
                            }

                            getItemFacturaGlobal('./sessao_facturas/fetch_item_factura_global.php?factura_global_id=' + factura_id ? './sessao_facturas/fetch_item_factura_global.php?factura_global_id=' + factura_id : 0);
                        },
                        error: function(err) {
                            alert("Deu erro");
                            console.log(err)
                        }
                    });
                } else {
                    getItemFacturaGlobal('./sessao_facturas/fetch_item_factura_global.php?factura_global_id=' + factura_id ? './sessao_facturas/fetch_item_factura_global.php?factura_global_id=' + factura_id : 0);
                    Swal.fire({
                        icon: 'error',
                        title: 'O campo nome do segurado não pode estar vazio!',
                        showConfirmButton: true,
                    })
                }
            })
        });

        $(document).on("dblclick", ".td_nr_factura", function() {

            var numerador = $(this).attr("numerador");
            // alert(numerador)
            $(".input_nr_factura" + numerador).attr("hidden", false);
            $(".input_nr_factura" + numerador).focus();
            $(".span_nr_factura" + numerador).attr("hidden", true);

            var factura_id = $(this).attr("factura_id");
            var id_guia = $(this).attr("id_guia");
            // alert(id_guia)

            $('.input_nr_factura').focusout(function() {
                var val = $(".input_nr_factura" + numerador).val();
                $(".span_nr_factura" + numerador).text(val);
                $(".input_nr_factura" + numerador).attr("hidden", true);
                $(".span_nr_factura" + numerador).attr("hidden", false);

                var nome_segurado = $(".input_nome_segurado" + numerador).val();
                var nr_membro = $(".input_nr_membro" + numerador).val();
                var nr_factura = $(".input_nr_factura" + numerador).val();
                var codigo_autorizacao = $(".input_codigo_autorizacao" + numerador).val();
                var valor = $(".input_valor" + numerador).val();
                var data = $(".input_data" + numerador).val();
                // var nr_membro_anterior = $(".input_nr_membro"+numerador).attr('nr_membro_anterior');

                if (nr_factura != "") {
                    $.ajax({
                        url: './API/api/factura_cliente/replace.php?id_guia=' + id_guia,
                        method: 'POST',
                        data: {
                            id_guia: id_guia,
                            nome_segurado: nome_segurado,
                            nr_membro: nr_membro,
                            nr_factura: nr_factura,
                            codigo_autorizacao: codigo_autorizacao,
                            valor: valor,
                            data: data
                        },
                        dataType: 'json',
                        success: function(response) {
                            if (response.status_text == 'error') {
                                Swal.fire({
                                    icon: 'error',
                                    title: response.message,
                                    showConfirmButton: true,
                                })
                            }

                            getItemFacturaGlobal('./sessao_facturas/fetch_item_factura_global.php?factura_global_id=' + factura_id ? './sessao_facturas/fetch_item_factura_global.php?factura_global_id=' + factura_id : 0);
                        },
                        error: function(err) {
                            alert("Deu erro");
                            console.log(err)
                        }
                    });
                } else {
                    getItemFacturaGlobal('./sessao_facturas/fetch_item_factura_global.php?factura_global_id=' + factura_id ? './sessao_facturas/fetch_item_factura_global.php?factura_global_id=' + factura_id : 0);
                    Swal.fire({
                        icon: 'error',
                        title: 'O campo nome do segurado não pode estar vazio!',
                        showConfirmButton: true,
                    })
                }
            })
        });

        $(document).on("dblclick", ".td_codigo_autorizacao", function() {

            var numerador = $(this).attr("numerador");
            // alert(numerador)
            $(".input_codigo_autorizacao" + numerador).attr("hidden", false);
            $(".input_codigo_autorizacao" + numerador).focus();
            $(".span_codigo_autorizacao" + numerador).attr("hidden", true);

            var factura_id = $(this).attr("factura_id");
            var id_guia = $(this).attr("id_guia");
            // alert(id_guia)

            $('.input_codigo_autorizacao').focusout(function() {
                var val = $(".input_codigo_autorizacao" + numerador).val();
                $(".span_codigo_autorizacao" + numerador).text(val);
                $(".input_codigo_autorizacao" + numerador).attr("hidden", true);
                $(".span_codigo_autorizacao" + numerador).attr("hidden", false);

                var nome_segurado = $(".input_nome_segurado" + numerador).val();
                var nr_membro = $(".input_nr_membro" + numerador).val();
                var nr_factura = $(".input_nr_factura" + numerador).val();
                var codigo_autorizacao = $(".input_codigo_autorizacao" + numerador).val();
                var valor = $(".input_valor" + numerador).val();
                var data = $(".input_data" + numerador).val();
                // var nr_membro_anterior = $(".input_nr_membro"+numerador).attr('nr_membro_anterior');

                if (codigo_autorizacao != "") {
                    $.ajax({
                        url: './API/api/factura_cliente/replace.php?id_guia=' + id_guia,
                        method: 'POST',
                        data: {
                            id_guia: id_guia,
                            nome_segurado: nome_segurado,
                            nr_membro: nr_membro,
                            nr_factura: nr_factura,
                            codigo_autorizacao: codigo_autorizacao,
                            valor: valor,
                            data: data
                        },
                        dataType: 'json',
                        success: function(response) {
                            if (response.status_text == 'error') {
                                Swal.fire({
                                    icon: 'error',
                                    title: response.message,
                                    showConfirmButton: true,
                                })
                            }

                            getItemFacturaGlobal('./sessao_facturas/fetch_item_factura_global.php?factura_global_id=' + factura_id ? './sessao_facturas/fetch_item_factura_global.php?factura_global_id=' + factura_id : 0);
                        },
                        error: function(err) {
                            alert("Deu erro");
                            console.log(err)
                        }
                    });
                } else {
                    getItemFacturaGlobal('./sessao_facturas/fetch_item_factura_global.php?factura_global_id=' + factura_id ? './sessao_facturas/fetch_item_factura_global.php?factura_global_id=' + factura_id : 0);
                    Swal.fire({
                        icon: 'error',
                        title: 'O campo nome do segurado não pode estar vazio!',
                        showConfirmButton: true,
                    })
                }
            })
        });

        $(document).on("dblclick", ".td_valor", function() {

            var numerador = $(this).attr("numerador");
            // alert(numerador)
            $(".input_valor" + numerador).attr("hidden", false);
            $(".input_valor" + numerador).focus();
            $(".span_valor" + numerador).attr("hidden", true);

            var factura_id = $(this).attr("factura_id");
            var id_guia = $(this).attr("id_guia");
            // alert(id_guia)

            $('.input_valor').focusout(function() {
                var val = $(".input_valor" + numerador).val();
                $(".span_valor" + numerador).text(val);
                $(".input_valor" + numerador).attr("hidden", true);
                $(".span_valor" + numerador).attr("hidden", false);

                var nome_segurado = $(".input_nome_segurado" + numerador).val();
                var nr_membro = $(".input_nr_membro" + numerador).val();
                var nr_factura = $(".input_nr_factura" + numerador).val();
                var codigo_autorizacao = $(".input_codigo_autorizacao" + numerador).val();
                var valor = $(".input_valor" + numerador).val();
                var data = $(".input_data" + numerador).val();
                // var nr_membro_anterior = $(".input_nr_membro"+numerador).attr('nr_membro_anterior');

                if (valor != "") {
                    $.ajax({
                        url: './API/api/factura_cliente/replace.php?id_guia=' + id_guia,
                        method: 'POST',
                        data: {
                            id_guia: id_guia,
                            nome_segurado: nome_segurado,
                            nr_membro: nr_membro,
                            nr_factura: nr_factura,
                            codigo_autorizacao: codigo_autorizacao,
                            valor: valor,
                            data: data
                        },
                        dataType: 'json',
                        success: function(response) {
                            if (response.status_text == 'error') {
                                Swal.fire({
                                    icon: 'error',
                                    title: response.message,
                                    showConfirmButton: true,
                                })
                            }

                            getItemFacturaGlobal('./sessao_facturas/fetch_item_factura_global.php?factura_global_id=' + factura_id ? './sessao_facturas/fetch_item_factura_global.php?factura_global_id=' + factura_id : 0);
                        },
                        error: function(err) {
                            alert("Deu erro");
                            console.log(err)
                        }
                    });
                } else {
                    getItemFacturaGlobal('./sessao_facturas/fetch_item_factura_global.php?factura_global_id=' + factura_id ? './sessao_facturas/fetch_item_factura_global.php?factura_global_id=' + factura_id : 0);
                    Swal.fire({
                        icon: 'error',
                        title: 'O campo nome do segurado não pode estar vazio!',
                        showConfirmButton: true,
                    })
                }
            })
        });

        $(document).on("dblclick", ".td_data", function() {

            var numerador = $(this).attr("numerador");
            // alert(numerador)
            $(".input_data" + numerador).attr("hidden", false);
            $(".input_data" + numerador).focus();
            $(".span_data" + numerador).attr("hidden", true);

            var factura_id = $(this).attr("factura_id");
            var id_guia = $(this).attr("id_guia");
            // alert(id_guia)

            $('.input_data').focusout(function() {
                var val = $(".input_data" + numerador).val();
                $(".span_data" + numerador).text(val);
                $(".input_data" + numerador).attr("hidden", true);
                $(".span_data" + numerador).attr("hidden", false);

                var nome_segurado = $(".input_nome_segurado" + numerador).val();
                var nr_membro = $(".input_nr_membro" + numerador).val();
                var nr_factura = $(".input_nr_factura" + numerador).val();
                var codigo_autorizacao = $(".input_codigo_autorizacao" + numerador).val();
                var valor = $(".input_valor" + numerador).val();
                var data = $(".input_data" + numerador).val();
                // var nr_membro_anterior = $(".input_nr_membro"+numerador).attr('nr_membro_anterior');

                if (data != "") {
                    $.ajax({
                        url: './API/api/factura_cliente/replace.php?id_guia=' + id_guia,
                        method: 'POST',
                        data: {
                            id_guia: id_guia,
                            nome_segurado: nome_segurado,
                            nr_membro: nr_membro,
                            nr_factura: nr_factura,
                            codigo_autorizacao: codigo_autorizacao,
                            valor: valor,
                            data: data
                        },
                        dataType: 'json',
                        success: function(response) {
                            if (response.status_text == 'error') {
                                Swal.fire({
                                    icon: 'error',
                                    title: response.message,
                                    showConfirmButton: true,
                                })
                            }

                            getItemFacturaGlobal('./sessao_facturas/fetch_item_factura_global.php?factura_global_id=' + factura_id ? './sessao_facturas/fetch_item_factura_global.php?factura_global_id=' + factura_id : 0);
                        },
                        error: function(err) {
                            alert("Deu erro");
                            console.log(err)
                        }
                    });
                } else {
                    getItemFacturaGlobal('./sessao_facturas/fetch_item_factura_global.php?factura_global_id=' + factura_id ? './sessao_facturas/fetch_item_factura_global.php?factura_global_id=' + factura_id : 0);
                    Swal.fire({
                        icon: 'error',
                        title: 'O campo nome do segurado não pode estar vazio!',
                        showConfirmButton: true,
                    })
                }
            })
        });
    })
</script>