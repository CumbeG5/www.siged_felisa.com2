function factura() {
    
    $(function () {
        list(page = 1)
        function list(page) {
            $.ajax({
                url: './ajax/listPagamentoFornecedor.php'
                , type: "GET"
                , data: {
                    data_inicio: function () {
                        return $("#data_inicio").val()
                    },
                    data_fim: function () {
                        return $("#data_fim").val()
                    },
                    nr_factura: function () {
                        return $("#nr_factura").val()
                    },
                    limit: function () {
                        return $("#limit").val()
                    },
                    page: function () {
                        return page
                    }
                }
                , dataType: "html",

                success: function (data) {
                    $(".table-pagamento-fornecedor").html(data)
                },
                error: function (e) {
                    // alert("errorsssss")
                    console.log(e)
                }
            });
        }
        $(document).on("change keyup click", ".search", function () {
            list()
        })
        $(document).on("click", ".changePage", function () {
            list($(this).val())
        })

        //Eliminar pagamento
        $(document).on("click", ".eliminarPagamento", function (e) {
            Swal.fire({
                title: 'Tem certeza que deseja eliminar este pagamento?',
                text: "Atenção! esta acção é irreversível!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#5cb85c',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Sim, eliminar!',
                cancelButtonText: 'Cancelar'
            }).then((result) => {
                if (result.isConfirmed) {
                    fetch('./API/api/pagamento/delete.php', {
                        method: "POST",
                        body: JSON.stringify({ id: $(this).val(), estado: '0' })
                    })
                        .then((response) => {
                            return response.json()
                        })
                        .then((data) => {
                            console.log(data)
                            if (data.Status == 200) {
                                displaySuccessToaster(data.message);
                                table.ajax.reload();
                                // location.assign('sigde.php?go=123' ? 'index.php?go=123' : "");
                            } else if (data.Status == 500) {
                                displayErrorToaster(data.message);
                            }
                        })
                        .catch((error) => {
                            console.log(error)
                        });
                    // deletePayment('./API/api/pagamento_factura_cliente/delete.php',document.getElementById("pagamento_id").value,"0")

                }
            })
        })


    });
}

$(document).on("click", ".viewPagamento", function (event) {
    var id = $(this).attr('value');
    // alert(id);
    $.ajax({
        url: 'modals/detalhePagamentoFornecedor.php',
        type: 'POST',
        dataType: 'html',
        data: { id: id },
    })
        .done(function (response) {
            $(response).modal({
                inverted: false,
                allowMultiple: true
            }).modal('show');
        })
        .fail(function () {
            console.log("error");
        })
        .always(function () {
            console.log("complete");
        });
});
factura();
