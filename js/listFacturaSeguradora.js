function facturaCliente() {
    $(function () {

        list(page = 1)
        function list(page) {
            $.ajax({
                url: './ajax/listFacturaSeguradora.php',
                type: "GET",
                data: {
                    seguradora_id: function () {
                        return $("#seguradora_id").val();
                    },
                    data_inicio: function () {
                        return $("#data_inicio").val();
                    },
                    data_fim: function () {
                        return $("#data_fim").val();
                    },
                    numero: function () {
                        return $("#numero").val();
                    },
                    estado: function () {
                        return $("#estados").val();
                    },
                    limit: function () {
                        return $("#limit").val()
                    },
                    page: function () {
                        return page
                    }
                },
                dataType: "html",

                success: function (data) {
                    $(".table-factura-fornecedor").html(data)
                    // alert(data)
                },
                error: function (e) {
                    // alert("errorsssss")
                    console.log(e)
                }
            })
        }
        // let table = $("#listarFacturasCliente").DataTable({
        //     processing: true,
        //     serverMethod: "GET",
        //     ajax: {
        //         url: './ajax/listFacturaSeguradora.php',
        //         data: {
        //             cliente_id: function () {
        //                 return $("#cliente_id").val();
        //             },
        //             data_inicio: function () {
        //                 return $("#data_inicio").val();
        //             },
        //             data_fim: function () {
        //                 return $("#data_fim").val();
        //             },
        //             numero: function () {
        //                 return $("#numero").val();
        //             },
        //             estado: function () {
        //                 return $("#estados").val();
        //             },
        //         },
        //     },

        //     //colunas da tabela
        //     columnDefs: [
        //         {
        //             searchable: false,
        //             orderable: false,
        //             targets: 0, // aqui é a coluna do id como é a primeira é 0
        //         },
        //         {
        //             targets: [8],
        //             render: function (data, type, row) {
        //                 // alert(row.nota_credito)
        //                 if (row.estado == 1) {
        //                     if (row.valor == row.rem) {
        //                         return (
        //                             "<span class='badge badge-success mb-2'>Aberto</span> " +
        //                             " <span class='badge badge-warning'>não pago</span>"
        //                         );
        //                     } else if (row.rem > 0) {
        //                         return (
        //                             "<span class='badge badge-success mb-2'>Aberto</span> " +
        //                             " <span class='badge badge-info'>parcial</span>"
        //                         );
        //                     } else if (row.valor == row.pago) {
        //                         return (
        //                             "<span class='badge badge-success mb-2'>Aberto</span> " +
        //                             " <span class='badge badge-success'>pago</span>"
        //                         );
        //                     }
        //                 } else if (row.estado == 2) {
        //                     if (row.valor == row.rem) {
        //                         return (
        //                             "<span class='badge badge-danger mb-2'>Fechado</span> " +
        //                             " <span class='badge badge-warning'>não pago</span>"
        //                         );
        //                     } else if (row.rem > 0) {
        //                         return (
        //                             "<span class='badge badge-danger mb-2'>Fechado</span> " +
        //                             " <span class='badge badge-info'>parcial</span>"
        //                         );
        //                     } else if (row.valor == row.pago) {
        //                         return (
        //                             "<span class='badge badge-danger mb-2'>Fechado</span> " +
        //                             " <span class='badge badge-success'>pago</span>"
        //                         );
        //                     }
        //                 } else if (row.estado == 3) {
        //                     return (
        //                         "<span class='badge badge-danger mb-2'>Eliminado</span> "
        //                     );
        //                 }
        //             },
        //         },
        //         {
        //             targets: [9],
        //             render: function (data, type, row) {
        //                 // alert(row.estado);
        //                 var accoes = "";
        //                 accoes +=

        //                     "<td>" +
        //                     '<a href="sigde.php?id=' + row.id + '&go=133" class="btn btn-default" data-tooltip="Pagamentos Pendentes"><i class="ui time icon"></i></a>' +

        //                     '<a href="./class/pdf/documentos/reportFacturaGlobal.php?id=' +
        //                     row.id +
        //                     '" value="' +
        //                     row.id +
        //                     '" target="_blank" class="btn btn-default" data-tooltip="Visualizar Nota Pagamento"><i class="ui print icon"></i></a>';
        //                 // if (row.nota_credito == 0) {
        //                 if (row.estado == 1) {
        //                     accoes +=
        //                         '<a class="btn btn-default" href="sigde.php?id=' +
        //                         row.id +
        //                         '&amp;go=132" data-tooltip="Editar Factura"> <i class="ui edit icon"></i> </a>' +
        //                         "</td>";
        //                 }
        //                 if (row.pago == 0 && row.estado != 3) {
        //                     accoes += '<a id="deleteFacturaGlobal" value="' + row.id + '" class="deleteFacturaGlobal btn btn-default" data-tooltip="Eliminar Factura"><i class="ui trash icon"></i></a>';
        //                 }
        //                 if (row.pago == 0 && row.estado == 2) {
        //                     accoes += '<a id="reabrirFactura" value="' + row.id + '" class="reabrirFactura btn btn-default" data-tooltip="Reabrir Factura"><i class="lock open icon"></i></a>';
        //                 }
        //                 // }
        //                 return accoes;
        //             },
        //         },
        //     ],
        //     select: true,
        //     dom: "Blfrtip",
        //     iDisplayLength: 25,
        //     aLengthMenu: [
        //         [5, 25, 50, 100, -1],
        //         [5, 25, 50, 100, "All"],
        //     ],
        //     dom: "Bfrtip",

        // });
        $(document).on("change keyup", ".search", function () {
            list()
            // alert($("#cliente_id").val())
            // console.log($("#numero").val());
        });

        $(document).on("click", ".changePage", function () {
            list($(this).val())
        })

        $(document).on('click', '.deleteFacturaGlobal', function (e) {
            // alert($(this).attr('value'))
            Swal.fire({
                title: 'Tem certeza?',
                text: "Ao eliminar este registo, não poderá recuperar!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'Cancelar',
                confirmButtonText: 'Sim, eliminar!'
            }).then((result) => {
                if (result.isConfirmed) {
                    // alert($(this).attr('value'))
                    $.ajax({
                        url: './API/api/factura_global/delete.php'
                        , type: "POST"
                        , data: {
                            id: $(this).attr('value')
                        }
                        , dataType: "json",
                        success: function (data) {
                            // alert(data.message)
                            // table.ajax.reload();
                            list()
                            Swal.fire({
                                icon: 'success',
                                title: data.message,
                                showConfirmButton: false,
                                timer: 1500
                            })
                        },
                        error: function (e) {
                            console.log(e)
                        }
                    })
                }
            });
        })

        $(document).on('click', '.reabrirFactura', function (e) {
            // alert($(this).attr('value'))
            Swal.fire({
                title: 'Tem certeza que deseja reabrir a factura?',
                // text: "Ao eliminar este registo, não poderá recuperar!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'Cancelar',
                confirmButtonText: 'Sim, reabrir!'
            }).then((result) => {
                if (result.isConfirmed) {
                    // alert($(this).attr('value'))
                    $.ajax({
                        url: './API/api/factura_global/reabrir.php'
                        , type: "POST"
                        , data: {
                            id: $(this).attr('value')
                        }
                        , dataType: "json",
                        success: function (data) {
                            // alert(data.message)
                            // table.ajax.reload();
                            list()
                            Swal.fire({
                                icon: 'success',
                                title: data.message,
                                showConfirmButton: false,
                                timer: 1500
                            })
                        },
                        error: function (e) {
                            console.log(e)
                        }
                    })
                }
            });
        })
    });
}
facturaCliente();
