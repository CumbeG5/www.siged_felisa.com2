function factura() {
    $(function () {
        list(page = 1)
        // alert()
        function list(page) {
            // alert($("#data_inicio").val())
            // alert()
            $.ajax({
                url: './ajax/listDespesas.php',
                type: "GET",
                data: {
                    data_inicio: $("#data_inicio").val(),
                    data_fim: $("#data_fim").val(),
                    centro_custo: $("#idcentrodecustos").val(),
                    grupo_despesas: $("#grupos").val(),
                    subgrupo: $(".subgrupo_listagem").val(),
                    fornecedor: $("#fornecedorResumo_d").val(),
                    // total: $("#mes_final_d").val(),
                    limit: $("#limit").val(),
                    page: page
                },
                dataType: "html",
                success: function (data) {
                    $("#tabela_despesas_gerais").html(data)
                    displayTotal();
                },
                error: function (e) {
                    // alert("errorsssss")
                    console.log(e)
                }
            });

        }
        
        $(document).on("change keyup click", ".search", function () {
            // alert()
            list()
        })
        $(document).on("click", ".changePage", function () {
            list($(this).val())
        })
        // $(document).on('click', '.deleteFactura', function (e) {
        //     Swal.fire({
        //         title: 'Tem certeza?',
        //         text: "Ao eliminar este registo, não poderá recuperar!",
        //         icon: 'warning',
        //         showCancelButton: true,
        //         confirmButtonColor: '#3085d6',
        //         cancelButtonColor: '#d33',
        //         cancelButtonText: 'Cancelar',
        //         confirmButtonText: 'Sim, eliminar!'
        //     }).then((result) => {
        //         if (result.isConfirmed) {
        //             // alert($(this).attr('value'))
        //             $.ajax({
        //                 url: './ajax/deleteFactura.php'
        //                 , type: "POST"
        //                 , data: {
        //                     id: $(this).attr('value')
        //                 }
        //                 , dataType: "json",
        //                 success: function (data) {
        //                     // alert(data.message)
        //                     console.log(data.message)
        //                     list(1)
        //                     Swal.fire({
        //                         icon: 'success',
        //                         title: data.message,
        //                         showConfirmButton: false,
        //                         timer: 1500
        //                     })
        //                 },
        //                 error: function (e) {
        //                     console.log(e)
        //                 }
        //             })
        //         }
        //     });
        // })
        function displayTotal() {
            var total = $("#total").val();
            var total2 = $("#valor_total_despesas").val();
            total = numeral(total).format('0,0.00');
            total2 = numeral(total2).format('0,0.00');
            $("#totalView").val(total2);
            $("#somaTotal").text(total);
        }

        $('#grupos').on('change', function () {
            var id = $(this).val();
            var centro_custo = $("#idcentrodecustos").val();
            var centro_custos = $("#idcentrodecustos").val();
            // alert(centro_custo);
            if ($(this).val()) {
                $.ajax({
                    url: 'combobox/listSubgrupoDespesa.php',
                    method: 'POST',
                    data: {
                        id: id,
                        centro_custo: centro_custo,
                        centro_custos: centro_custos
                    },
                    dataType: 'html',
                    success: function (html) {
                        console.log(html)
                        $('#subgrupo').html(html);
                        // $('.subgrupo').hide('fast');
                        // $('.subgrupo').removeAttr('hidden');
                        // $('.subgrupo').show('slow');
                    },
                    error: function () {
                        //
                    }
                });
            }
            else {
                $('.subgrupo').find('select').empty();
                $('.subgrupo').hide('slow');
            }
        });
    });
}
factura();
