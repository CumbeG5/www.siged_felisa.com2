function Saldo(valor) {
    document.getElementById('saldo').value = valor;
    // console.log(valor);
}
$(document).on('keyup change', '#valorMask', function () {
    // alert($(this).val());
    $.ajax({
        url: "./function/convert_number.php",
        method: "POST",
        data: { price: $(this).val() },
        dataType: "json",
        success: function (data) {
            Saldo(data);
            document.getElementById('valor').value = data;
            console.log("success: " + data);
        },
        error: function (err) {
            console.log("error: " + err);
            // alert(err);
        }
    })
});
$(document).on("submit", "#frmFactura", function (e) {
    // alert()
    e.preventDefault()
    $.ajax({
        url: './API/api/factura/create.php',
        type: 'POST',
        data: new FormData(this),
        contentType: false,
        processData: false,
        success: function (data) {
            console.log(data)
            window.location = "listFacturaFornecedor.php";
        },
        error: function (error) {
            console.log(error);
            console.log(error.responseJSON);
            if (error.responseJSON.status_text == 'error') {
                if (!error.responseJSON.description) {
                    Swal.fire({
                        icon: error.responseJSON.status_text,
                        title: error.responseJSON.message,
                        // showConfirmButton: true,
                    })
                } else {
                    Swal.fire({
                        icon: error.responseJSON.status_text,
                        title: error.responseJSON.message + "\n" + "Descrição:" + error.responseJSON.description.errorInfo[2],
                        // showConfirmButton: true,
                    })
                }
            }
        }
    });
    return false;
});