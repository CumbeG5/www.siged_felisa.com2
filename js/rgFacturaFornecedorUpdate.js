function Saldo(valor) {
    document.getElementById('saldo').value = valor;
    // console.log(valor);
}
$(document).on('keyup change', '#valorMask', function () {
    // alert($(this).val());
    $.ajax({
        url: "./function/convert_number.php",
        method: "POST",
        data: { price: $(this).val() },
        dataType: "json",
        success: function (data) {
            Saldo(data);
            document.getElementById('valor').value = data;
            console.log("success: " + data);
        },
        error: function (err) {
            console.log("error: " + err);
            // alert(err);
        }
    })
});
$(document).on("submit", "#frmAtualizarFactura", function (e) {
    // alert()
    e.preventDefault()
    $.ajax({
        url: './API/api/factura/update.php',
        type: 'POST',
        data: new FormData(this),
        contentType: false,
        processData: false,
        success: function (data) {
            if (data.Status == 201) {
                Swal.fire({
                    icon: data.status_text,
                    title: data.message,
                    showConfirmButton: false,
                    timer: 1500
                }).then((result) => {
                    window.location = "listFacturaFornecedor.php";
                })
            }
            // console.log(data)

        },
        error: function (error) {
            console.log(error);
            console.log(error.responseJSON);
            if (error.responseJSON.status_text == 'error') {
                if (!error.responseJSON.description) {
                    Swal.fire({
                        icon: error.responseJSON.status_text,
                        title: error.responseJSON.message,
                        // showConfirmButton: true,
                    })
                } else {
                    Swal.fire({
                        icon: error.responseJSON.status_text,
                        title: error.responseJSON.message + "\n" + "Descrição:" + error.responseJSON.description.errorInfo[2],
                        // showConfirmButton: true,
                    })
                }
            }
        }
    });
    return false;
});