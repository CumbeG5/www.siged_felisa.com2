$(document).ready(function () {
    list()
    function list() {
        $.ajax({
            url: './ajax/listGuias.php',
            method: 'GET',
            data: {
                // "estado": $("#id_guia").val(),
                "id": $("#id_guia").val(),
                "numero": $("#nr_factura").val(),
                "data_inicio": $("#data_inicio").val(),
                "data_fim": $("#data_fim").val(),
                "valor": $("#valor").val(),
            },
            dataType: 'html',
            success: function (response) {

                $(".table-factura-guias").html(response);
                // $('#nomeGrupoUpdate').val(response);
            },
            error: function () {
                UIkit.notification({ message: "Error please contact developer", status: 'danger', pos: 'bottom-right' });
            }
        });
    }

    $(document).on("change keyup click", ".search", function () {
        // alert()
        list()
    })
})

