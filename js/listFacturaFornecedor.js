function factura() {
    $(function () {
        list(page = 1)
        // let table = $('#listarfacturas').DataTable({
        //     'processing': true,
        //     'serverMethod': 'GET',
        //     'order': [[ 1, "desc" ]],
        //     'ajax': {
        //         'url': './API/api/factura/select.php',
        //         'data': {
        //             fornecedor_id: function () {
        //                 return $("#fornecedor_id").val()
        //             },
        //             centro_custo_id: function () {
        //                 return $("#centro_custo_id").val()
        //             },
        //             data_inicio: function () {
        //                 return $("#data_inicio").val()
        //             },
        //             data_fim: function () {
        //                 return $("#data_fim").val()
        //             },
        //             estado: function () {
        //                 return $("#estado").val()
        //             },
        //             mes_inicial: function(){
        //                 return $("#mes_inicial").val()
        //             },
        //             mes_final: function(){
        //                 return $("#mes_final").val()
        //             },
        //             nr_factura: function(){
        //                 return $("#nr_factura").val()
        //             }
        //         }
        //     },

        //     //colunas da tabela
        //     'columns': [
        //         { "data": 'id' },
        //         { "data": 'fornecedor' },
        //         { "data": 'nr_factura' },
        //         { "data": 'data_factura' },
        //         { "data": 'farmacia' },
        //         { "data": 'tipo_factura' },
        //         { "data": 'mes_correspondente'},
        //         { "data": 'valor', render: $.fn.dataTable.render.number( '.', ',', 2), className: "text-right" },
        //         // { "data": 'saldo', render: $.fn.dataTable.render.number( '.', ',', 2), className: "text-right"},
        //         {
        //             targets: [1],
        //             render: function (data, type, row) {
        //                 var accoes = ""
        //                 accoes +=
        //                     '<td>' +
        //                     '<a id="viewFactura" value="' + row.id + '" class="viewFactura btn btn-default" data-tooltip="Visualizar Factura"><i class="ui zoom icon"></i></a>' +
        //                     '<a class="btn btn-default" href="index.php?id=' + row.id + '&amp;go=125" data-tooltip="Editar Factura"> <i class="ui edit icon"></i> </a>' +
        //                     '</td>';

        //                 return accoes
        //             }
        //         },

        //     ],
        //     select: true,
        //     dom: 'Blfrtip',
        //     "iDisplayLength": 25,
        //     "aLengthMenu": [
        //         [5, 25, 50, 100, -1],
        //         [5, 25, 50, 100, "All"]
        //     ],

        //     "footerCallback": function(row, data, start, end, display) {
        //         var api = this.api();

        //         // Remove the formatting to get integer data for summation
        //         var intVal = function(i) {
        //             return typeof i === 'string' ?
        //                 i.replace(/[\$,]/g, '') * 1 :
        //                 typeof i === 'number' ?
        //                 i : 0;
        //         };

        //         // Total over all pages
        //         total = api
        //             .column(7)
        //             .data()
        //             .reduce(function(a, b) {
        //                 return intVal(a) + intVal(b);
        //             }, 0);

        //         // Total over this page
        //         pageTotal = api
        //             .column(7, {
        //                 page: 'current'
        //             })
        //             .data()
        //             .reduce(function(a, b) {
        //                 return intVal(a) + intVal(b);
        //             }, 0);

        //         // Update footer
        //         $(api.column(7).footer()).html($.number( total, 2, ',', '.' ) 
        //             // +' ('+ $.number( total, 2, ',', '.' ) +' total)'
        //             // total.toLocaleString('en-US')

        //         );
        //         // console.log(pageTotal);
        //         // alert(pageTotal);
        //     },
        //     dom: 'Bfrtip',
        //     // buttons: [
        //     //     { extend: 'copyHtml5', footer: true },
        //     //     { extend: 'excelHtml5', footer: true },
        //     //     { extend: 'csvHtml5', footer: true },
        //     //     { extend: 'pdfHtml5', footer: true }
        //     // ]            
        //     buttons: [
        //         {
        //             text: 'PDF',
        //             extend: 'pdfHtml5',
        //             footer: true,
        //             filename: 'listagem de facturas',
        //             orientation: 'portrait', //landscape
        //             pageSize: 'A4', //A3 , A5 , A6 , legal , letter
        //             exportOptions: {
        //                 columns: ':visible',
        //                 search: 'applied',
        //                 order: 'applied'
        //             },
        //             customize: function (doc) {
        //                 // console.log(doc.content[1].table.body[0][8])
        //                 // doc.content[1].table.body[0][8].attr("hidden",true)
        //                 // doc.pageMargins = [10,10,10,10];
        //                 doc.defaultStyle.fontSize = 10;
        //                 // doc.styles.tableHeader.fontSize = 12;
        //                 // doc.styles.title.fontSize = 14;
        //                 // Remove spaces around page title
        //                 doc.content[0].text = doc.content[0].text.trim();
        //                 // Create a footer
        //                 doc['footer']=(function(page, pages) {
        //                     return {
        //                         columns: [
        //                             // 'ACSUN',
        //                             {
        //                                 // This is the right column
        //                                 alignment: 'right',
        //                                 text: ['page ', { text: page.toString() },  ' of ', { text: pages.toString() }]
        //                             }
        //                         ],
        //                         margin: [10, 0]
        //                     }
        //                 });
        //             }
        //         },
        //         {
        //             text: 'EXCEL',
        //             extend: 'excelHtml5',
        //             footer: true,
        //             filename: 'listagem de facturas',
        //             orientation: 'portrait', //landscape
        //             pageSize: 'A4', //A3 , A5 , A6 , legal , letter
        //             exportOptions: {
        //                 columns: ':visible',
        //                 search: 'applied',
        //                 order: 'applied'
        //             }
        //         },
        //         // 'excel', 'pageLength',
        //     ]
        // });
        function list(page) {
            $.ajax({
                url: './ajax/listFactura.php'
                , type: "GET"
                , data: {
                    fornecedor_id: function () {
                        return $("#fornecedor_id").val()
                    },
                    centro_custo_id: function () {
                        return $("#centro_custo_id").val()
                    },
                    data_inicio: function () {
                        return $("#data_inicio").val()
                    },
                    data_fim: function () {
                        return $("#data_fim").val()
                    },
                    estado: function () {
                        return $("#estado").val()
                    },
                    mes_inicial: function () {
                        return $("#mes_inicial").val()
                    },
                    mes_final: function () {
                        return $("#mes_final").val()
                    },
                    nr_factura: function () {
                        return $("#nr_factura").val()
                    },
                    limit: function () {
                        return $("#limit").val()
                    },
                    page: function () {
                        return page
                    }
                }
                , dataType: "html",

                success: function (data) {
                    $(".table-factura-fornecedor").html(data)
                    // alert(data)
                },
                error: function (e) {
                    // alert("errorsssss")
                    console.log(e)
                }
            });
        }
        $(document).on("change keyup click", ".search", function () {
            list()
            // alert($(this).val())
            // table.ajax.reload();
            // console.log(document.getElementById('estado'));
            // console.log($("#fornecedor_id").val());
        })
        $(document).on("click", ".changePage", function () {
            list($(this).val())
        })

        $(document).on('click', '.deleteFactura', function (e) {
            // alert($(this).attr('value'))
            Swal.fire({
                title: 'Tem certeza?',
                text: "Ao eliminar este registo, não poderá recuperar!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'Cancelar',
                confirmButtonText: 'Sim, eliminar!'
            }).then((result) => {
                if (result.isConfirmed) {
                    // alert($(this).attr('value'))
                    $.ajax({
                        url: './ajax/deleteFactura.php'
                        , type: "POST"
                        , data: {
                            id: $(this).attr('value')
                        }
                        , dataType: "json",
                        success: function (data) {
                            // alert(data.message)
                            console.log(data.message)
                            list(1)
                            Swal.fire({
                                icon: 'success',
                                title: data.message,
                                showConfirmButton: false,
                                timer: 1500
                            })
                        },
                        error: function (e) {
                            console.log(e)
                        }
                    })
                }
            });
        })

        $(document).on("click", ".viewFactura", function (event) {
            var id = $(this).attr('value');
            // alert(id);
            $.ajax({
                url: './modals/detalheFacturaFornecedor.php',
                type: 'POST',
                dataType: 'html',
                data: { id: id },
            }).done(function (response) {
                // console.log("testeee")
                $(response).modal({
                    inverted: false,
                    allowMultiple: true
                }).modal('show');
            }).fail(function () {
                console.log("error");
            })
        });
    });
}
factura();
