const { search } = window.location;
const params = new URLSearchParams(search);
const listID = params.get('id');

let botao3 = null;
let botao4 = null;
let fetchTotal = null;
// listagem da factura global
$(document).ready(function () {
    let lastID = $("#last_factura_global_id").val();
    getItemFacturaGlobal('./sessao_facturas/fetch_item_factura_global.php?factura_global_id=' + lastID ? './sessao_facturas/fetch_item_factura_global.php?factura_global_id=' + listID : 0);
})

// Registo da factura global
$(document).on("submit", "#formFacturaGlobal", function (e) {
    $("#formFacturaGlobal").removeClass("success");
    e.preventDefault()
    gravarFacturaGlobal(document.getElementById('formFacturaGlobal'), 'factura_global/create.php');
});

async function gravarFacturaGlobal(formularioID, endPoint) {
    let lastID = $("#last_factura_global_id").val();
    const dadosFormulario = new FormData(formularioID);
    // console.log(dadosFormulario)
    const response = await fetch(`./API/api/${endPoint}`, {
        method: "POST",
        body: dadosFormulario
    })
        .then((data) => {
            return data.json();
        })
        .then((response) => {
            // console.log( response);
            let lastID = response.data.id;
            document.getElementById('last_factura_global_id').value = lastID;
            let div = document.getElementById('formSegurado');
            let botao1 = document.getElementById('rgFacturaGlobal');
            let botao2 = document.getElementById('rgFacturaCliente');
            let botao3 = document.getElementById('verFacturaGlogal');
            let botao4 = document.getElementById('verFacturas');


            if (response.Status == 201) {
                getItemFacturaGlobal('./sessao_facturas/fetch_item_factura_global.php?factura_global_id=' + lastID ? './sessao_facturas/fetch_item_factura_global.php?factura_global_id=' + listID : 0);
                div?.removeAttribute('hidden', true);
                botao1?.setAttribute('hidden', true);
                botao2?.removeAttribute('hidden', true);
                botao3?.removeAttribute('hidden', true);
                botao4?.removeAttribute('hidden', true);

                //disabled inputs
                document.querySelector("#data_factura_global").setAttribute('disabled', true);
                document.querySelector("#nr_factura_global").setAttribute('disabled', true);
                // document.querySelector("#factura_global_id").setAttribute('disabled', true);
                document.getElementById("factura_global_id").disabled = true;
            } else {
                // console.log(response);
                displayErrorToaster(response.message);
                div?.setAttribute('hidden', true);
                botao1?.removeAttribute('hidden', true);
                botao2?.setAttribute('hidden', true);
                botao3?.setAttribute('hidden', true);
                botao4?.setAttribute('hidden', true);
            }
        })
        .catch((error) => {
            console.log({ error });
            return {
                message: "Ocorreu um erro na execução da operação",
                error: true,
            };
        })
    return response;
}


// Registo da factura global item
$(document).on("submit", "#formFacturaGlobalCliente", function (e) {
    $("#formFacturaGlobalCliente").removeClass("success");
    e.preventDefault()
    gravarItemFacturaGlobal(document.getElementById('formFacturaGlobalCliente'), 'factura_cliente/create.php');
});

async function gravarItemFacturaGlobal(formularioID, endPoint) {
    let lastID = $("#last_factura_global_id").val();
    const dadosFormulario = new FormData(formularioID);
    const response = await fetch(`./API/api/${endPoint}`, {
        method: "POST",
        body: dadosFormulario
    })
        .then((data) => {
            return data.json();
        })
        .then((response) => {
            // console.log(response);

            if (response.Status == 201) {
                displaySuccessToaster(response.message);
                if (lastID != null) {
                    getItemFacturaGlobal('./sessao_facturas/fetch_item_factura_global.php?factura_global_id=' + lastID);
                } else {
                    getItemFacturaGlobal('./sessao_facturas/fetch_item_factura_global.php?factura_global_id=' + listID);
                }

                document.getElementById('nome_segurado').value = "";
                document.getElementById('nr_membro').value = "";
                document.getElementById('nr_factura').value = "";
                document.getElementById('codigo_autorizacao').value = "";
                document.getElementById('valor').value = "";
                document.getElementById('data').value = "";
                document.getElementById('nome_tecnico').value = "";
                document.getElementById('anexo_factura').value = "";
                // $("#nome_tecnico").val("").change()
            } else if (response.Status == 500) {
                console.log(response)
                displayErrorToaster(response.message);

                if (lastID != null) {
                    getItemFacturaGlobal('./sessao_facturas/fetch_item_factura_global.php?factura_global_id=' + lastID);
                } else {
                    getItemFacturaGlobal('./sessao_facturas/fetch_item_factura_global.php?factura_global_id=' + listID);
                }
            }
        })
        .catch((error) => {
            console.log({ error });
            return {
                message: "Ocorreu um erro na execução da operação",
                error: true,
            };
        })
    return response;
}

async function getItemFacturaGlobal(endPoint) {

    const response = await fetch(`${endPoint}`, {
        method: "GET",
    })
        .then((data) => {
            return data.text()
        })
        .then((response) => {
            // console.log(response);
            let divHidden = document.getElementById('listSegurado');
            // let classdivHidden = document.querySelector('.listSegurado');
            if (divHidden != null) {
                // console.log("Registo: " + divHidden)
                divHidden != "" || divHidden != null ? divHidden.removeAttribute('hidden', true) : "";
                divHidden != "" || divHidden != null ? divHidden.innerHTML = response : "";
            }
            // if (divUpdate != "" || divHidden != null) {
            //     console.log("Update: " + divUpdate)
            //     divUpdate.innerHTML = response;
            // }
            fetchTotal = document.querySelector('#fetchTotal');
            // alert(fetchTotal);
        })
        .catch((error) => {
            console.log({ error })
        })
    return response;
}

$(document).on('click', ".remove_guia", function () {
    Swal.fire({
        title: 'Tem certeza que deseja eliminar a guia?',
        // text: "Ao eliminar este registo, não poderá recuperar!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Sim, elimine!'
    }).then((result) => {
        if (result.isConfirmed) {
            removeFacturaGlobal('./API/api/factura_cliente/delete.php', $(this).attr('value'), $(this).attr('factura_id'));
        }
    })

});

async function removeFacturaGlobal(endPoint, id, factura_id) {
    ``
    let lastID = $("#last_factura_global_id").val();
    const response = await fetch(`${endPoint}`, {
        method: "POST",
        body: JSON.stringify({ id: id, estado: "0", factura_id: factura_id })
    })
        .then((data) => {
            return data.json();
        })
        .then((response) => {
            Swal.fire({
                icon: 'success',
                title: 'Factura removida com sucesso!',
                showConfirmButton: false,
                timer: 1500
            })
            getItemFacturaGlobal('./sessao_facturas/fetch_item_factura_global.php?factura_global_id=' + lastID);
            // getItemFacturaGlobal('./sessao_facturas/fetch_item_factura_global.php?factura_global_id=' + lastID ? './sessao_facturas/fetch_item_factura_global.php?factura_global_id=' + listID : 0);
            // listItemPayments('./sessao_facturas/fetch_factura_cliente_update.php?pagamento_factura_id=' + lastID ? './sessao_facturas/fetch_factura_cliente_update.php?pagamento_factura_id=' + listID : 0);
        })
        .catch((error) => {
            console.log({ error })
        })
    return response;
}
// functions
function displayInfoToaster(msg) {
    toastr.options.timeOut = 1500; // 1.5s 
    toastr.info(msg);
}

function displaySuccessToaster(msg) {
    toastr.options.timeOut = 1500; // 1.5s 
    toastr.success(msg);
}

function displayErrorToaster(msg) {
    toastr.options.timeOut = 1500; // 1.5s 
    toastr.error(msg);
}

function displayWarningToaster(msg) {
    toastr.options.timeOut = 1500; // 1.5s 
    toastr.warning(msg);
}