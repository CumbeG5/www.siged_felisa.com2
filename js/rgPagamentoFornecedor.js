function factura() {
    listarFacturas();
    $(document).on("submit", "#frmPagamento", function () {
        $.ajax({
            url: './API/api/pagamento/create.php',
            type: 'POST',
            data: new FormData(this),
            contentType: false,
            processData: false,
            success: function (response) {
                let params = `scrollbars=no,resizable=no,status=no,location=no,toolbar=no,menubar=no,width=800,height=600,left=100,top=100`;
                open('./class/pdf/documentos/report.php?id=' + response.data.id, 'test', params);
                // window.open('http://localhost/www.siged_felisa.com/class/pdf/documentos/report.php?id=' + response.data.id, '_blank')
                // console.log(response.data.id);
                window.location = "listPagamentoFornecedor.php";
            },
            error: function (error) {
                console.log(error);
                if (error.responseJSON.status_text == 'error') {
                    if (!error.responseJSON.description) {
                        Swal.fire({
                            icon: error.responseJSON.status_text,
                            title: error.responseJSON.message,
                            // showConfirmButton: true,
                        })
                    } else {
                        Swal.fire({
                            icon: error.responseJSON.status_text,
                            title: error.responseJSON.message + "\n" + "Descrição:" + error.responseJSON.description.errorInfo[2],
                            // showConfirmButton: true,
                        })
                    }
                }
            }
        });
        return false;
    });

    ListaTiposPagamentos();
    $(document).on('submit', '#frmRgTiposPagamento', function (e) {
        $("#frmRgTiposPagamento").removeClass("success");
        e.preventDefault()
        // if ($("#idformapagamento").val() != "" && $("#tipo_pagamento_id").val() != "" && $("#valor_pagamento").val() != "" && $("#nr_operacao").val() != "") {
        if ($("#valor_pagamento").val() > 0) {
            $.ajax({
                url: "./sessao_facturas/sessao_tiposPagamentos.php",
                method: "POST",
                data: {
                    proveniencia: $("#proveniencia").val(),
                    tipo_pagamento_id: $("#tipo_pagamento_id").val(),
                    tipo_pagamento_text: $("#tipo_pagamento_id option:selected").text(),
                    idformapagamento: $("#idformapagamento").val(),
                    idformapagamento_text: $("#idformapagamento option:selected").text(),
                    tipo_pagamento_id: $("#tipo_pagamento_id").val(),
                    tipo_pagamento_text: $("#tipo_pagamento_id option:selected").text(),
                    valor: $("#valor_pagamento").val(),
                    data_pagamento: $("#data_pagamento_cliente").val() ? $("#data_pagamento_cliente").val() : "",
                    numerocheque: $("#numerocheque").val(),
                    numerodocumento: $("#numerodocumento").val(),
                    nr_operacao: $("#nr_operacao").val(),
                    caixa: $("#idcaixa").val(),
                    caixa_text: $("#idcaixa option:selected").text(),
                    // farmacia: $("#centro_custo_id option:selected").text(),
                    // idfarmacia: $("#centro_custo_id").val(),

                },
                dataType: "json",
                success: function (data) {
                    if (data.status_text == 'error') {
                        if (!data.description) {
                            Swal.fire({
                                icon: data.status_text,
                                title: data.message,
                                // showConfirmButton: true,
                            })
                        } else {
                            Swal.fire({
                                icon: data.status_text,
                                title: data.message + "\n" + "Descrição:" + data.description,
                                // showConfirmButton: true,
                            })
                        }
                    } else {
                        $("#tipo_pagamento_id").val('').trigger('change');
                        $("#idcaixa").val('').trigger('change');
                        $("#idcaixa").val('').trigger('change');
                        $(".idformapagamento").val('').trigger('change');
                        $("#valor_pagamento").val('');
                        $("#nr_operacao").val('');
                        $("#numerocheque").val('');
                        $("#referencia").val('');
                        $("#data_pagamento_cliente").val('');
                        console.log(data.message);
                        ListaTiposPagamentos();
                    }
                },
                error: function (error) {
                    console.log(error);
                }
            });
        } else {
            displayErrorToaster("O valor a pagar deve ser maior que 0.");
        }
        // } else {
        //     Swal.fire({
        //         icon: 'error',
        //         title: 'Oops...',
        //         text: 'Os campos obrigatórios não foram todos preenchidos',
        //     })
        // }
    });
    $(document).on('submit', '#frmRgFacturaPagamento', function (e) {
        $("#frmRgFacturaPagamento").removeClass("success");
        e.preventDefault()
        let TotalCarrinho = $("#totalValorTiposPagamentos").val();
        let TotalPago = $("#totalValorPagamentos").val();
        let Total = Number($("#remanescente").val()) + Number(TotalPago);
        let Tipo_Factura = $("#tipo_factura").val();
        // alert(Number(Total));
        // alert(Number(TotalCarrinho));
        // if ($("#factura_id").val() != "" && $("#valor_total").val() != "" && $("#remanescente").val() != "") {
        // alert($("#rg_tipo_pagamentos").val() ? Number($("#rg_tipo_pagamentos").val()) : 0)
        // $("#factura_fornecedor_total").val() ? Number($("#factura_fornecedor_total").val()) : 0
        // $("#remanescente").val() ? Number($("#remanescente").val()) : 0;

        if ($("#remanescente").val() <= 0) {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'O valor a pagar deve ser maior que 0',
            })
        } else {
            if (($("#rg_tipo_pagamentos").val() ? Number($("#rg_tipo_pagamentos").val()) : 0) >= (($("#factura_fornecedor_total").val() ? Number($("#factura_fornecedor_total").val()) : 0) + Number($("#tipo_remanescente").val() + $("#remanescente").val()))) {
                // alert(Number($("#tipo_remanescente").val()+$("#remanescente").val()))
                if (Tipo_Factura == 1) {
                    // if (Total <= TotalCarrinho) {
                    $.ajax({
                        url: "./sessao_facturas/sessao_facturas.php",
                        method: "POST",
                        data: {
                            factura_id: $("#factura_id").val(),
                            factura_text: $("#factura_id option:selected").text(),
                            valor: $("#tipo_remanescente").val() + $("#valor_total").val(),
                            remanescente: $("#tipo_remanescente").val() + $("#remanescente").val(),
                            saldo: $("#total").val(),
                            tipo_factura: $("#tipo_factura").val(),
                            // desconto: $("#desconto").val()
                        },
                        dataType: "json",
                        success: function (data) {
                            $("#factura_id").val('').trigger('change');
                            $("#valor_total").val('');
                            $("#remanescente").val('');
                            $("#tipo_remanescente").val('');
                            $("#desconto").val('');
                            $("#total_pago").val('');
                            $("#total_pagamento").val('');
                            $("#total").val('');
                            // console.log(data.error);
                            listarFacturas();
                            if (data.error == true) {
                                if (!data.error) {
                                    Swal.fire({
                                        icon: data.error,
                                        title: data.message,
                                        // showConfirmButton: true,
                                    })
                                } else {
                                    Swal.fire({
                                        icon: data.error,
                                        title: data.message,
                                        // showConfirmButton: true,
                                    })
                                }
                            }
                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });
                    // } else {
                    //     Swal.fire({
                    //         icon: 'warning',
                    //         title: 'O valor a pagar não pode ser superior ao total de pagamento',
                    //         // showConfirmButton: true,
                    //     })
                    //     // alert("O Valor do Pagamento Nao pode ser Superior que o total pago!");
                    // }

                    // alert("O Valor do Pagamento Nao pode ser Superior que o total pago!");
                } else {

                    $.ajax({
                        url: "./sessao_facturas/sessao_facturas.php",
                        method: "POST",
                        data: {
                            factura_id: $("#factura_id").val(),
                            factura_text: $("#factura_id option:selected").text(),
                            valor: $("#tipo_remanescente").val() + $("#valor_total").val(),
                            remanescente: $("#tipo_remanescente").val() + $("#remanescente").val(),
                            saldo: $("#total").val(),
                            tipo_factura: $("#tipo_factura").val(),
                            // desconto: $("#desconto").val()
                        },
                        dataType: "json",
                        success: function (data) {
                            $("#factura_id").val('').trigger('change');
                            $("#valor_total").val('');
                            $("#remanescente").val('');
                            $("#tipo_remanescente").val('');
                            // $("#desconto").val('');
                            $("#total_pago").val('');
                            $("#total_pagamento").val('');
                            $("#total").val('');
                            // console.log(data.error);
                            listarFacturas();
                            if (data.error == true) {
                                if (!data.error) {
                                    Swal.fire({
                                        icon: data.error,
                                        title: data.message,
                                        // showConfirmButton: true,
                                    })
                                } else {
                                    Swal.fire({
                                        icon: data.error,
                                        title: data.message,
                                        // showConfirmButton: true,
                                    })
                                }
                            }
                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });
                }
            } else {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'O valor da factura é maior que os pagamentos realizados.',
                })
            }

        }
        // } 
        // else {
        //     Swal.fire({
        //         icon: 'error',
        //         title: 'Oops...',
        //         text: 'Os campos obrigatórios da factura não foram todos preenchidos',
        //     })
        // }
    });
    $(document).on('click', ".remove_factura", function () {
        var id = $(this).val();
        Swal.fire({
            icon: 'warning',
            title: 'Tem certeza que deseja eliminar essa factura?',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Não',
            confirmButtonText: 'Sim,apague!',
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    url: "./sessao_facturas/remove_factura.php",
                    method: "POST",
                    data: {
                        'id': id,
                    },
                    dataType: 'json',
                    success: function (data) {
                        Swal.fire({
                            icon: 'success',
                            title: 'Factura removida com sucesso!',
                            showConfirmButton: false,
                            timer: 1500
                        }).then((e) => {
                            listarFacturas();

                        })
                    },
                    error: function (err) {
                        alert("Error")
                        console.log(err)
                    }
                })
            }
        });

    });
    function ListaTiposPagamentos() {
        $.ajax({
            url: "./sessao_facturas/fetch_tiposPagamentos.php",
            method: "GET",
            data: {},
            dataType: "html",
            success: function (data) {
                // console.log($("#total_tab").val());
                $("#tabela_tiposPagamentos").html(data);
                let tab = document.getElementById('total_tab')?.value;
                $("#totalValorTiposPagamentos").val(tab);
                document.getElementById('tab_total').value = tab;
                // console.log(tab);
            },
            error: function (error) {
                // console.log(error.message);
                // alert(error);
            }
        });
    }

    function listarFacturas() {
        $.ajax({
            url: "./sessao_facturas/fetch_facturas.php",
            method: "POST",
            data: {},
            dataType: 'html',
            success: function (data) {
                $("#tabela_facturas").html(data)
            },
            error: function (err) {
                // alert("Error")
                console.log(err)
            }
        });
    }
}


factura();
