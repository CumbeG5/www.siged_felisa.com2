<?php
//=====================================
	//chamar o formulario de configuracaoes 
	require_once ('../Connections/conexaosiged.php');
	require_once ("../classes/classes.php");
	@session_start();
	//classe  de conexao		
	$utilizador = "";
	$password_utilizador = ""; 
	$mensagem = '';
	  
	if (isset($_POST['btn_submit']))
	{
	  $utilizador = stripcslashes($_POST['username']);
	  $password_utilizador = stripcslashes($_POST['password']);
	  
	 //$username=$_SESSION["user"];
	 //$passwordEncriptada = md5($password_utilizador);
	  $sql = "SELECT * FROM `tblusuario` WHERE `usuarioNome` = ? AND `usuarioSenha` = md5(?)";

	  $pegaUser = $conexaosiged->prepare($sql);
	  $pegaUser->bindParam(1,$utilizador);
	  $pegaUser->bindParam(2, $password_utilizador);
	  $pegaUser->execute();

	  $result=$pegaUser->rowCount();
	  //verificar se os dados correspondem a valores da base de dados
	  if($result == 0)
	  { 
		 $mensagem = '<div class="alert alert-danger alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
			Senha ou Usuario Incorrecto.
			</div>';
	  }
	  else{
    	$row = $pegaUser->fetch();
    	$_SESSION['MM_idUserSgr']  	= $row['usuarioId'];
    	$_SESSION['MM_nivelAcessoSgr'] = $row['usuNivelAcesso'];
	  	$_SESSION['MM_Username'] 	 	= $row['usuarioNome'];
		$_SESSION['MM_avatarSgr'] 	 	= $row['usuarioAvatar'];
		$_SESSION['MM_nomeSgr'] 	 	= $row['nome'];
		$_SESSION['MM_apelidoSgr'] 	= $row['UsuarioApelido'];
		$_SESSION['MM_email'] 	= $row['UsuarioEmail'];
		$_SESSION['MM_UserGroup'] = 0;

			header("Location:../sigde.php?go=0");
	  }
	}
?>