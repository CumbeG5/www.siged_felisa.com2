<?php
# FileName="Connection_php_mysql.htm"
# Type="MYSQL"
# HTTP="true"
$hostname_conexaosiged = "localhost";
$database_conexaosiged = "siged_felisa";
$username_conexaosiged = "root";
$password_conexaosiged = "";
try {
    $conexaosiged = new PDO("mysql:host=$hostname_conexaosiged;dbname=$database_conexaosiged;charset=utf8", $username_conexaosiged, $password_conexaosiged, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
    // set the PDO error mode to exception
    $conexaosiged->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    //echo "Connected successfully";
}
catch(PDOException $e)
{
    echo "Connection failed: " . $e->getMessage();
}
?>