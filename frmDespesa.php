<?php
@session_start();
include "./classes/classes.php";
include('Connections/conexaosiged.php');
require_once("classes/Geral.php");

$query_comboempresass = "SELECT * FROM fornecedors";
$comboempresass = $conexaosiged->prepare($query_comboempresass);
$comboempresass->execute();

$query_combocentrodecusto = "SELECT * FROM centro_de_custo";
$combocentrodecusto = $conexaosiged->prepare($query_combocentrodecusto);
$combocentrodecusto->execute();

$meses = array(1 => "Janeiro", 2 => "Fevereiro", 3 => "Março", 4 => "Abril", 5 => "Maio", 6 => "Junho", 7 => "Julho", 8 => "Agosto", 9 => "Setembro", 10 => "Outubro", 11 => "Novembro", 12 => "Dezembro");
?>
<!DOCTYPE html>
<html lang="en" dir="">
<meta http-equiv="content-type" content="text/html;charset=utf-8" />

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width,initial-scale=1" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>SIGED | Registar Despesa</title>
    <link href="./fonts.googleapis.com/csse27b.css?family=Nunito:300,400,400i,600,700,800,900" rel="stylesheet" />
    <link href="./dist-assets/css/themes/lite-purple.min.css" rel="stylesheet" />
    <link href="./dist-assets/css/plugins/perfect-scrollbar.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="./dist-assets/css/plugins/fontawesome-5.css" />
    <link href="./dist-assets/css/plugins/metisMenu.min.css" rel="stylesheet" />
    <link href="./dist-assets/flatpickr/dist/flatpickr.min.css" rel="stylesheet" />
    <link href="./dist-assets/fonts/fontawesome/css/fontawesome.min.css" rel="stylesheet" />
</head>

<body class="text-left">
    <div class="app-admin-wrap layout-horizontal-bar">
        <?php include_once './topbar.php' ?>
        <!-- =============== Horizontal bar End ================-->
        <div class="main-content-wrap d-flex flex-column">
            <!-- ============ Body content start ============= -->
            <div class="main-content">
                <div class="breadcrumb">
                    <h1>Registar Execução Despesa</h1>
                </div>
                <div class="separator-breadcrumb border-top"></div>
                <div class="card">

                    <div class=" card-body ">
                        <div class="row">
                            <div class="col-md-4 form-group mb-3">
                                <label class="form-label">Centro de custo:</label>
                                <select name="fornecedor_id" id="fornecedor_id" class="form-control search">
                                    <option value="">selecione aqui...</option>
                                    <?php
                                    foreach ($comboempresass as $row_comboempresass) {
                                    ?>
                                        <option value="<?php echo $row_comboempresass['id'] ?>"><?php echo $row_comboempresass['nome'] ?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-md-4 form-group mb-3">
                                <label for="picker1">Grupo Despesas:</label>
                                <select name="fornecedor_id" id="fornecedor_id" class="form-control search">
                                    <option value="">selecione aqui...</option>
                                    <?php
                                    foreach ($comboempresass as $row_comboempresass) {
                                    ?>
                                        <option value="<?php echo $row_comboempresass['id'] ?>"><?php echo $row_comboempresass['nome'] ?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-md-4 form-group mb-3">
                                <label for="picker1">Subgrupo:</label>
                                <select name="fornecedor_id" id="fornecedor_id" class="form-control search">
                                    <option value="">selecione aqui...</option>
                                    <?php
                                    foreach ($comboempresass as $row_comboempresass) {
                                    ?>
                                        <option value="<?php echo $row_comboempresass['id'] ?>"><?php echo $row_comboempresass['nome'] ?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-md-4 form-group mb-3">
                                <label for="picker1">Fornecedor/Beneficiário:</label>
                                <select name="fornecedor_id" id="fornecedor_id" class="form-control search">
                                    <option value="">selecione aqui...</option>
                                    <?php
                                    foreach ($comboempresass as $row_comboempresass) {
                                    ?>
                                        <option value="<?php echo $row_comboempresass['id'] ?>"><?php echo $row_comboempresass['nome'] ?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-md-4 form-group mb-3">
                                <label for="picker1">Tipo de Documento:</label>
                                <select name="fornecedor_id" id="fornecedor_id" class="form-control search">
                                    <option value="">selecione aqui...</option>
                                    <?php
                                    foreach ($comboempresass as $row_comboempresass) {
                                    ?>
                                        <option value="<?php echo $row_comboempresass['id'] ?>"><?php echo $row_comboempresass['nome'] ?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="field banco" style="display: none">
                                <label>Banco:</label>
                                <select class="uk-select" id="banco" name="banco">
                                    <option value="">Selecione Aqui...</option>
                                    <?php
                                    foreach ($query_banco as $row) {
                                    ?>
                                        <option value="<?php echo $row['id'] ?>"><?php echo $row['nome'] ?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="field numerocheque" style="display: none;">
                                <label class="">Número de cheque:</label>
                                <div class="">
                                    <input name="numerocheque" type="number" id="numerocheque" />
                                </div>
                            </div>
                            <div id='' class="field caixa" style="display: none">
                                <label class=''>Caixa:</label>
                                <div class=''>
                                    <select name="caixa" id="idcaixa" class="uk-select">
                                        <option value="">Selecione Aqui...</option>
                                        <?php
                                        foreach ($combocaixas as $row) {
                                        ?>
                                            <option value="<?php echo $row['id'] ?>"><?php echo $row['nome'] ?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="field transferencia" hidden>
                                <label class="">Transferência Conta Destino:</label>
                                <div class="">
                                    <input name="transfcontadestino" type="text" id="transfcontadestino" />
                                </div>
                            </div>
                            <div class="col-md-4 form-group mb-3">
                                <label for="picker1">Número:</label>
                                <input class="form-control" type="text">
                            </div>
                            <div class="col-md-4 form-group mb-3">
                                <label>Data do Pagamento:</label>
                                <input placeholder="yyyy-mm-dd" style="background: white !important;" name="data_inicio" type="datetime" id="data_inicio" class="data1 search form-control" />
                            </div>
                            <div class="col-md-4 form-group mb-3">
                                <label for="picker1">Quantidade:</label>
                                <input class="form-control" type="number">
                            </div>
                            <div class="col-md-4 form-group mb-3">
                                <label for="picker1">Tipo de unidade:</label>
                                <select name="fornecedor_id" id="fornecedor_id" class="form-control search">
                                    <option value="">selecione aqui...</option>
                                    <?php
                                    foreach ($comboempresass as $row_comboempresass) {
                                    ?>
                                        <option value="<?php echo $row_comboempresass['id'] ?>"><?php echo $row_comboempresass['nome'] ?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-md-4 form-group mb-3">
                                <label>Valor Sem IVA:</label>
                                <input class="form-control" type="text">
                            </div>
                            <div class="col-md-4 form-group mb-3">
                                <label>IVA:</label>
                                <input class="form-control" type="text">
                            </div>
                            <div class="col-md-4 form-group mb-3">
                                <label>Valor Com IVA:</label>
                                <input class="form-control" type="text">
                            </div>
                            <div class="col-md-4 form-group mb-3">
                                <label>Forma de Pagamento:</label>
                                <select name="centro_custo_id" id="centro_custo_id" class="form-control search">
                                    <option value="">Selecione Aqui...</option>
                                    <?php
                                    foreach ($combocentrodecusto as $row_combocentrodecusto) {
                                    ?>
                                        <option value="<?php echo $row_combocentrodecusto['idcentrodecusto'] ?>"><?php echo $row_combocentrodecusto['titulo'] ?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-md-4 form-group mb-3">
                                <label>Descrição da Despesa:</label>
                                <textarea class="form-control" rows="4"></textarea>
                            </div>

                            <div class="col-md-4 form-group mb-3">
                                <label for="picker1">Anexo:</label>
                                <input class="form-control" type="file">
                            </div>

                        </div>
                        <div style="text-align: end;">
                            <button class="btn btn-primary ripple m-1" type="button">Registar Execução</button>
                        </div>

                        <br>
                        <br>

                        <!-- </div> -->
                    </div>
                </div>
                <!-- end of main-content -->
            </div>
            <!-- Footer Start -->
            <div class="flex-grow-1"></div>
        </div>
    </div>

    <!-- ============ Search UI End ============= -->
    <script src="./dist-assets/js/plugins/jquery-3.3.1.min.js"></script>
    <script src="./dist-assets/js/plugins/bootstrap.bundle.min.js"></script>
    <script src="./dist-assets/js/plugins/perfect-scrollbar.min.js"></script>
    <script src="./dist-assets/js/scripts/script.min.js"></script>
    <script src="./dist-assets/js/scripts/sidebar-horizontal.script.js"></script>
    <script src="./dist-assets/flatpickr/dist/flatpickr.min.js"></script>
    <script src="./js/dateformat.js"></script>
    <script src="./dist-assets/fonts/fontawesome/js/all.js"></script>
    <!-- Factura -->
</body>


<!-- Mirrored from demos.ui-lib.com/gull/html/layout3/accordion.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 19 Aug 2021 10:04:12 GMT -->

</html>