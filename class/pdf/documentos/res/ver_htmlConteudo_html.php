<?php session_unset();
session_start();
/**
 * Created by PhpStorm.
 * User: Manacaze
 * Date: 1/2/2018
 * Time: 9:53 PM
 */
$conteudo = $_SESSION['html'];
$title = $_SESSION['title'];
$dataInicio = $_SESSION['dataInicio'];
$dataFim = $_SESSION['dataFim'];
// print_r($_SESSION);
$parametro1 = isset($_SESSION['parametro1']) ? $_SESSION['parametro1'] : "";
$parametro2 = $_SESSION['parametro2'];
$parametro3 = $_SESSION['parametro3'];
$parametro4 = $_SESSION['parametro4'];
?>
<html>
<head>
    <meta charset="utf-8" />
    <link rel="stylesheet" href="../css/style.css" type="text/css" />

</head>

<body style="font-size: 10px; font-family: arial; background: white; padding-top: 0">
<div class="padding white" style="padding-top: 0">
    <div class="">
        <table cellspacing="0" style="width: 100%;">
            <tr>
                <td style="width: 100%;text-align: center; color: #444444;" class="text-center">
                    <div class="invoice-desc" style="text-align: center; padding-top: 0; margin-top: 0">
                        <img style="width: 80px" src="../img/image.png" alt="Logo"><br><br>
                        <!-- FARMÁCIA MARIA ELISA<br/>
                        Nuit: 100469261<br/>
                        Contacto: 871944848 / 847876424<br/>
                        Email: marianomcnobre@gmail.com -->
                    </div>
                </td>
            </tr>
        </table>

        <table cellspacing="0" style="width: 100%;">
            <tr>
                <td style="width: 100%; color: #444444;" class="text-center" style="text-align: left">
                    <h3>Parametros da pesquisa:</h3><br>
                    <?php if(isset($dataInicio) && !empty($dataInicio) && isset($dataFim) && !empty($dataFim)){?>
                        <p><?= $dataInicio ?> <?php echo "  | "; ?> <?= $dataFim?> </p>
                        <?php
                    }
                    ?>
                    <p><?= $parametro1 ?></p>
                    <p><?= $parametro2 ?></p>
                    <p><?= $parametro3 ?></p>
                    <p><?= $parametro4 ?></p>
                </td>
            </tr>
        </table>


        <div class="">

            <div class="text-center" style="text-align:center">
                <h2>
                    <?= $title ?>
                </h2>
            </div>
            <br/>
            <div class="text-left" style="width: 100%;">
                <?=$conteudo?>
            </div>
        </div>
        <br>
    </div>
</div>
</body>
</html>

