<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../main/initialize_class.php';
$database = new Database();
$db = $database->getConnection();

$facturaGlobal = new FacturaGlobal($db);
// $factura_global = new FacturaGlobal($db);

$data = json_decode(file_get_contents("php://input"));
if (empty($data))
    $data = json_decode(json_encode($_POST));

if (!empty($data)) {
    $facturaGlobal->id = $data->id;
    $facturaGlobal->estado = 3;

    try {
        $db->beginTransaction();
        if ($facturaGlobal->delete()) {
            $count_created = 0;
            // $factura_id = $db->lastInsertId();

            // $facturaGlobal->updateSaldoRemas($data->factura_id);

            //create projecto cobertura
            $db->commit();
            echo json_encode(
                array(
                    'message' => 'Factura eliminada com sucesso.',
                    'Status' => 201,
                    'status_text' => 'success',
                    'data' => array("id" => $data->id)
                )
            );
            http_response_code(200);
        } else {
            echo json_encode(
                array(
                    'message' => 'Error creating cards data.',
                    'Status' => 500,
                    'status_text' => 'error'
                )
            );
            http_response_code(500);
        }
    } catch (PDOException $e) {
        echo json_encode(
            array(
                'message' => 'factura_global could not be created.',
                'description' => $e,
                'Status' => 500,
                'status_text' => 'error'
            )
        );
        http_response_code(500);
    }
}
