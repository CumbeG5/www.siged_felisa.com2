<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../main/initialize_class.php';
$database = new Database();
$db = $database->getConnection();

$factura = new FacturaGlobal($db);

$data = json_decode(file_get_contents("php://input"));
if (empty($data))
    $data = json_decode(json_encode($_POST));

if (!empty($data)) {
    $factura->seguradora_id = (int)$data->factura_global_id;
    $factura->data = "'$data->data_factura_global'";
    $factura->numero = "'$data->nr_factura_global'";
    $factura->valor = !isset($data->valor_factura_global) || $data->valor_factura_global=="" ? '0' : (float)$data->valor_factura_global;
    $factura->remanescente = !isset($data->remanescente) || $data->remanescente=="" ? '0' : (float)$data->remanescente;
    $factura->user_id = (int)$data->user_id;
    $factura->id = (int)$data->id;
    // print_r($data);
    // exit();
    try {
        $db->beginTransaction();
        if ($factura->update()) {
            $count_created = 0;
            $factura_id = $db->lastInsertId();

            $factura->updateSaldoRemas($data->id);

            //create projecto cobertura
            $db->commit();
            echo json_encode(
                array(
                    'message' => 'A factura global foi actualizada com sucesso.',
                    'Status' => 201,
                    'status_text' => 'success',
                    'data' => array("id" => $data->id)
                )
            );
            http_response_code(201);
        } else {
            echo json_encode(
                array(
                    'message' => 'Error creating cards data.',
                    'Status' => 500,
                    'status_text' => 'error'
                )
            );
            http_response_code(500);
        }
        // }
    } catch (PDOException $e) {
        echo json_encode(
            array(
                'message' => 'Ocorreu um erro ao tentar registar a factura.<br>Por favor tente novamente!',
                'description' => $e,
                'Status' => 500,
                'status_text' => 'error'
            )
        );
        http_response_code(500);
    }
}
