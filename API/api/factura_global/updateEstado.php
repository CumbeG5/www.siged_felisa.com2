<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../main/initialize_class.php';
$database = new Database();
$db = $database->getConnection();

$factura = new FacturaGlobal($db);

$data = json_decode(file_get_contents("php://input"));
if (empty($data))
    $data = json_decode(json_encode($_GET));

if (!empty($data)) {
    $factura->estado = (int)$data->estado;
    $factura->id = (int)$data->id;

    try {
        if($factura->count_factura_cliente()['total']>0){
            $db->beginTransaction();
            if ($factura->updateEstado()) {
                $count_created = 0;
                $factura_id = $db->lastInsertId();

                $factura->updateSaldoRemas($data->id);

                //create projecto cobertura
                $db->commit();
                echo json_encode(
                    array(
                        'message' => 'A factura foi eliminada com sucesso',
                        'Status' => 201,
                        'status_text' => 'success',
                        'data' => array("id" => $data->id)
                    )
                );
                http_response_code(201);
            } else {
                echo json_encode(
                    array(
                        'message' => 'Error creating cards data.',
                        'Status' => 500,
                        'status_text' => 'error'
                    )
                );
                http_response_code(500);
            }
        }else{
            echo json_encode(
                array(
                    'message' => 'Error. Não pode terminar factura sem guias.',
                    'Status' => 500,
                    'status_text' => 'error'
                )
            );
            http_response_code(500);
        }
    } catch (PDOException $e) {
        echo json_encode(
            array(
                'message' => 'Ocorreu um erro ao tentar registar a factura.<br>Por favor tente novamente!',
                'description' => $e,
                'Status' => 500,
                'status_text' => 'error'
            )
        );
        http_response_code(500);
    }
}
