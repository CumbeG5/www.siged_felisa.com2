<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../main/initialize_class.php';

$database = new Database();
$db = $database->getConnection();

$model = new FacturaGlobal($db);

$data = json_decode(json_encode($_GET));
// print_r($data->provincia_id);
$sqlAdd = " ";

if(!empty($_GET['seguradora'])){
    $seguradora = $_GET['seguradora'];
    $sqlAdd .= " AND fg.seguradora_id = {$seguradora}";
}

// if(!empty($_GET['cliente_id'])){
//     $seguradora_id = $_GET['cliente_id'];
//     $sqlAdd .= " AND fg.seguradora_id = {$seguradora_id}";
// }

if(!empty($_GET['estado'])){
    $estado = $_GET['estado'];
    $sqlAdd .= " AND fg.estado = '{$estado}'";
}

if(!empty($_GET['numero'])){
    $numero = $_GET['numero'];
    $sqlAdd .= " AND fg.numero LIKE '%{$numero}%'";
}

if(!empty($_GET['data_inicio']) && !empty($_GET['data_fim'])){
    $data_inicio = $_GET['data_inicio'];
    $data_fim = $_GET['data_fim'];
    $sqlAdd .= " AND fg.data BETWEEN '{$data_inicio}' AND '{$data_fim}'";
}
$page = $_GET['page'];
$limit = $_GET['limit'];
// exit
$start_from = ($page - 1) * $limit;

// Employees read query
$result = $model->sumRemanescente2($sqlAdd, $start_from, $limit);

// Check if any employee
if (!empty($result)) {
    // Turn to JSON & output
    echo json_encode($result);
    http_response_code(200);
} else {
    // No Data
    echo json_encode(
        array(
            "message' => 'No data found.",
            'Status' => 204,
            'status_text' => 'success',
            'data' => []
        )
    );
    //    http_response_code(204);
}
