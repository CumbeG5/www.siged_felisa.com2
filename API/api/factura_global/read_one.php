<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../main/initialize_class.php';

$database = new Database();
$db = $database->getConnection();

$model = new FacturaGlobal($db);

$data = json_decode(json_encode($_GET));



$model->id = $data->id;

$sqlAdd = "";


if (!empty($_GET['valor'])) {
    $valor = $_GET['valor'];
    $sqlAdd .= " AND f.valor LIKE '%{$valor}%'";
}

if (!empty($_GET['numero'])) {
    $numero = $_GET['numero'];
    $sqlAdd .= " AND f.nr_factura LIKE '%{$numero}%'";
}

if (!empty($_GET['data_inicio']) && !empty($_GET['data_fim'])) {
    $data_inicio = $_GET['data_inicio'];
    $data_fim = $_GET['data_fim'];
    $sqlAdd .= " AND f.data BETWEEN '{$data_inicio}' AND '{$data_fim}'";
}

// Employees read query
$result = $model->read_one($sqlAdd);

// Check if any employee
if (!empty($result)) {
    // Turn to JSON & output
    echo json_encode($result);
    http_response_code(200);
} else {
    // No Data
    echo json_encode(
        array(
            "message' => 'No data found.",
            'Status' => 204,
            'status_text' => 'success',
            'data' => []
        )
    );
    //    http_response_code(204);
}
