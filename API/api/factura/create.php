<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../main/initialize_class.php';
$database = new Database();
$db = $database->getConnection();

$factura = new Factura($db);

$data = json_decode(file_get_contents("php://input"));
if (empty($data))
    $data = json_decode(json_encode($_POST));
    // print_r($data);

if (!empty($data)) {
   
    $factura->fornecedor_id = $data->fornecedor_id;
    $factura->nr_factura = "'$data->nr_factura'";
    $factura->valor = "'$data->valor'";
    $factura->data_factura = "'$data->data_factura'";
    $factura->centro_custo_id = $data->centro_custo_id;
    $factura->tipo_factura_id = $data->tipo_factura_id;
    $factura->saldo = $data->saldo;
    $factura->estado = !isset($data->estado) || $data->estado == "" ? "1" : "'$data->estado'";
    $factura->mes = $data->mes;
    $anexo = handleMultiFileUpload($_FILES, 'execucao');
    $factura->anexo_factura = "'$anexo'";
    $factura->user_id = $data->user_id;
    
    try {
        if ($factura->validateFactura()) {
            echo json_encode(
                array(
                    'message' => 'O número da factura já existe para este fornecedor!',
                    'Status' => 500,
                    'status_text' => 'error'
                )
            );
            http_response_code(500);
        } else {
            $db->beginTransaction();
            if ($factura->create()) {
                $count_created = 0;
                $factura_id = $db->lastInsertId();

                //create projecto cobertura
                $db->commit();
                echo json_encode(
                    array(
                        'message' => 'Factura created successfully.',
                        'Status' => 201,
                        'status_text' => 'success',
                        'data' => array("id" => $factura_id)
                    )
                );
                http_response_code(201);
            } else {
                echo json_encode(
                    array(
                        'message' => 'Error creating cards data.',
                        'Status' => 500,
                        'status_text' => 'error'
                    )
                );
                http_response_code(500);
            }
        }
    } catch (PDOException $e) {
        echo json_encode(
            array(
                'message' => 'Ocorreu um erro ao tentar registar a factura.<br>Por favor tente novamente!',
                'description' => $e,
                'Status' => 500,
                'status_text' => 'error'
            )
        );
        http_response_code(500);
    }
}
