<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../main/initialize_class.php';
$database = new Database();
$db = $database->getConnection();

$factura = new Factura($db);

$data = json_decode(file_get_contents("php://input"));
if (empty($data))
    $data = json_decode(json_encode($_POST));
// print_r($data);
if (!empty($data)) {
    $factura->fornecedor_id = $data->fornecedor_id;
    $factura->nr_factura = "'$data->nr_factura'";
    $factura->valor = $data->valor;
    $factura->data_factura = "'$data->data_factura'";
    $factura->centro_custo_id = $data->centro_custo_id;
    $factura->tipo_factura_id = $data->tipo_factura_id;
    $factura->saldo = $data->valor;
    $factura->estado = !isset($data->estado) || $data->estado == "" ? "1" : "'$data->estado'";
    $factura->mes = $data->mes;
    $factura->id = $data->id;

    try {
        $db->beginTransaction();
        if ($factura->update()) {
            $count_created = 0;
            $factura->updateSaldo();
            $factura_id = $db->lastInsertId();

            //create projecto cobertura
            $db->commit();
            echo json_encode(
                array(
                    'message' => 'Factura created successfully.',
                    'Status' => 201,
                    'status_text' => 'success',
                    'data' => array("id" => $data->id)
                )
            );
            http_response_code(201);
        } else {
            echo json_encode(
                array(
                    'message' => 'Error creating cards data.',
                    'Status' => 500,
                    'status_text' => 'error'
                )
            );
            http_response_code(500);
        }
    } catch (PDOException $e) {
        echo json_encode(
            array(
                'message' => 'Factura could not be created.',
                'description' => $e,
                'Status' => 500,
                'status_text' => 'error'
            )
        );
        http_response_code(500);
    }
}
