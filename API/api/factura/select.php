<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../main/initialize_class.php';

$database = new Database();
$db = $database->getConnection();

$model = new Factura($db);

$data = json_decode(json_encode($_GET));
// print_r($_GET);
$sqlAdd = "";

if(!empty($_GET['data_inicio']) OR !empty($_GET['data_fim'])){
    $data_inicio = $_GET['data_inicio'];
    $data_fim = $_GET['data_fim'];
    $sqlAdd .= " AND f.data_factura BETWEEN '{$data_inicio}' AND '{$data_fim}' ";
}

if(!empty($_GET['fornecedor_id'])){
    $fornecedor_id = $_GET['fornecedor_id'];
    $sqlAdd .= "AND f.fornecedor_id = '{$fornecedor_id}'";
}
if(!empty($_GET['c_custo_id'])){
    $centro_custo_id = $_GET['c_custo_id'];
    $sqlAdd .= "AND f.centro_custo_id = '{$centro_custo_id}'";
}
if(!empty($_GET['estado'])){
    $estado = $_GET['estado'];
    $sqlAdd .= "AND f.estado = '{$estado}'";
}
if(!empty($_GET['mes'])){
    $mes = $_GET['mes'];
    $sqlAdd .= "AND f.mes = '{$mes}'";
}
if(!empty($_GET['mes_inicial']) OR !empty($_GET['mes_final'])){
    $mes_inicial = $_GET['mes_inicial'];
    $mes_final = $_GET['mes_final'];
    $sqlAdd .= " AND f.mes BETWEEN '{$mes_inicial}' AND '{$mes_final}' ";
}
if(!empty($_GET['nr_factura'])){
    $nr_factura = $_GET['nr_factura'];
    $sqlAdd .= "AND f.nr_factura like '%$nr_factura%'";
}

$page = $_GET['page'];
$limit = $_GET['limit'];
$start_from = ($page - 1) * $limit;
// echo $start_from;
// Employees read query
$result = $model->read($sqlAdd,$start_from,$limit);

// Check if any employee
if(!empty($result)) {
    // Turn to JSON & output
    echo json_encode($result);
    // echo json_encode($result);
    http_response_code(200);
} else {
    // No Data
    echo json_encode(
        array(
            "message' => 'No data found.",
            'Status' => 204,
            'status_text' => 'success',
            'data' => []
        )
    );
//    http_response_code(204);
}
?>