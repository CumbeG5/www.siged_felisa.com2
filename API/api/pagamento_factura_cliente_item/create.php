<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../main/initialize_class.php';
$database = new Database();
$db = $database->getConnection();

$pagemento_factura = new Pagamento_Factura_Cliente_Item($db);

$data = json_decode(file_get_contents("php://input"));
if (empty($data))
    $data = json_decode(json_encode($_POST));

if (!empty($data)) {
    $pagemento_factura->factura_cliente_id = $data->factura_cliente_id;
    $pagemento_factura->pagamento_factura_id = $data->pagamento_factura_id;
    $pagemento_factura->valor_pago = $data->valor_pago;
    $pagemento_factura->data = $data->data;
    // $pagemento_factura->id = $data->id;

    try {
        $db->beginTransaction();
        if ($pagemento_factura->existe_guia($data->factura_cliente_id, $data->pagamento_factura_id)) {
            echo json_encode(
                array(
                    'message' => 'Não pode inserir duas guias iguais na mesma factura.',
                    'Status' => 500,
                    'status_text' => 'error'
                )
            );
            http_response_code(500);
        } else {
            if ($pagemento_factura->create()) {
                $count_created = 0;
                $factura_id = $db->lastInsertId();

                //create projecto cobertura
                $db->commit();
                echo json_encode(
                    array(
                        'message' => 'Pagamento Facturas created successfully.',
                        'Status' => 201,
                        'status_text' => 'success',
                        'data' => array("id" => $factura_id)
                    )
                );
                http_response_code(201);
            } else {
                echo json_encode(
                    array(
                        'message' => 'Error creating cards data.',
                        'Status' => 500,
                        'status_text' => 'error'
                    )
                );
                http_response_code(500);
            }
        }
    } catch (PDOException $e) {
        echo json_encode(
            array(
                'message' => 'Pagemento_factura could not be created.',
                'description' => $e,
                'Status' => 500,
                'status_text' => 'error'
            )
        );
        http_response_code(500);
    }
}
