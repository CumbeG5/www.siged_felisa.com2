<?php @session_start();
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../main/initialize_class.php';
$database = new Database();
$db = $database->getConnection();

$factura = new Pagamento_Factura_Cliente_Item($db);

$data = json_decode(file_get_contents("php://input"));
if (empty($data))
    $data = json_decode(json_encode($_POST));

if (!empty($data)) {
    $factura->id = $data->id;
    $factura->valor_pago = $data->valor_pago;


    try {
        $db->beginTransaction();
        if ($factura->update_valor_pago()) {
            //update guia
            $db->commit();
            echo json_encode(
                array(
                    'message' => 'Valor actualizado com sucesso.',
                    'Status' => 200,
                    'status_text' => 'success',
                    // 'data' => array("id" => $data->id_guia)
                )
            );
            http_response_code(200);
        } else {
            echo json_encode(
                array(
                    'message' => 'Error creating cards data.',
                    'Status' => 500,
                    'status_text' => 'error'
                )
            );
            http_response_code(500);
        }
    } catch (PDOException $e) {
        echo json_encode(
            array(
                'message' => 'Factura could not be updated.',
                'description' => $e,
                'Status' => 500,
                'status_text' => 'error'
            )
        );
        http_response_code(500);
    }
}
