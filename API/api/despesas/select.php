<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../main/initialize_class.php';

$database = new Database();
$db = $database->getConnection();

$model = new Despesas($db);
// $data = json_decode(json_encode($_GET));
$sqlAdd = "";

if(!empty($_GET['data_inicio']) OR !empty($_GET['data_fim'])){
    $data_inicio = $_GET['data_inicio'];
    $data_fim = $_GET['data_fim'];
    $sqlAdd .= " AND e.data BETWEEN '{$data_inicio}' AND '{$data_fim}' ";
}
if(!empty($_GET['centro_custo_id'])){
    $centro_custo_id = $_GET['centro_custo_id'];
    $sqlAdd .= " AND e.idcentrodecusto = '{$centro_custo_id}' ";
}
if(!empty($_GET['grupo_despesas'])){
    $grupo_despesas = $_GET['grupo_despesas'];
    $sqlAdd .= " AND e.grupo = '{$grupo_despesas}' ";
}
if(!empty($_GET['subgrupo'])){
    $subgrupo = $_GET['subgrupo'];
    $sqlAdd .= " AND e.subgrupo = '{$subgrupo}' ";
}
if(!empty($_GET['fornecedor_id'])){
    $fornecedor_id = $_GET['fornecedor_id'];
    $sqlAdd .= "AND e.idempresa = '{$fornecedor_id}'";
}

$page = $_GET['page'];
$limit = $_GET['limit'];
$start_from = ($page - 1) * $limit;

$result = $model->read($sqlAdd,$start_from,$limit);

// Check if any employee
if(!empty($result)) {
    // Turn to JSON & output
    echo json_encode($result);
    // echo json_encode($result);
    http_response_code(200);
} else {
    // No Data
    echo json_encode(
        array(
            "message' => 'No data found.",
            'Status' => 204,
            'status_text' => 'success',
            'data' => []
        )
    );
//    http_response_code(204);
}
?>