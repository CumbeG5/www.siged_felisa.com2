<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../main/initialize_class.php';

$database = new Database();
$db = $database->getConnection();

$model = new Pagamento_Facturas_Nota_Credito($db);

$data = json_decode(json_encode($_GET));
// print_r("rdyitkfguyytrx: ",$nr_pagamento," fim");

$sqlAdd = "";

if (!empty($_GET['data_inicio']) or !empty($_GET['data_fim'])) {
    $data_inicio = $_GET['data_inicio'];
    $data_fim = $_GET['data_fim'];
    $sqlAdd .= " AND pf.data_nota BETWEEN '{$data_inicio}' AND '{$data_fim}' ";
}

if (!empty($_GET['nr_pagamento'])) {
    $nr_nota = $_GET['nr_pagamento'];
    $sqlAdd .= "AND pf.nr_nota LIKE '%{$nr_nota}%'";
}

if (!empty($_GET['seguradora_id'])) {
    $seguradora_id = $_GET['seguradora_id'];
    $sqlAdd .= "AND pf.seguradora_id = {$seguradora_id}";
}


// Employees read query
$result = $model->read($sqlAdd);

// Check if any employee
if (!empty($result)) {
    // Turn to JSON & output
    echo json_encode($result);
    // http_response_code(200);
} else {
    // No Data
    echo json_encode(
        array(
            "message' => 'No data found.",
            'Status' => 204,
            'status_text' => 'success',
            'data' => []
        )
    );
    //    http_response_code(204);
}
