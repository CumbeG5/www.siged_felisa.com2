<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../main/initialize_class.php';
$database = new Database();
$db = $database->getConnection();

$pagemento_facturas = new Pagamento_Facturas_Nota_Credito($db);

// $data = json_decode(file_get_contents("php://input"));
// if (empty($data))
//     $data = json_decode(json_encode($_POST));

// if (!empty($data)) {
// $pagemento_facturas->factura_global_id = $_POST["factura_global_id"];
// print_r($_POST["data_pagamento"]);
$pagemento_facturas->data_pagamento = $_POST["data_pagamento"];
$pagemento_facturas->nr_pagamento = $_POST["nr_pagamento"];
$pagemento_facturas->id = $_POST["id"];

try {
    $db->beginTransaction();
    if ($pagemento_facturas->update()) {
        $count_created = 0;
        $factura_id = $db->lastInsertId();

        //create projecto cobertura
        $db->commit();
        echo json_encode(
            array(
                'message' => 'Pagamento_Facturas update successfully.',
                'Status' => 201,
                'status_text' => 'success',
                'data' => array("id" => $_POST["id"])
            )
        );
        http_response_code(201);
    } else {
        echo json_encode(
            array(
                'message' => 'Error creating cards data.',
                'Status' => 500,
                'status_text' => 'error'
            )
        );
        http_response_code(500);
    }
} catch (PDOException $e) {
    echo json_encode(
        array(
            'message' => 'Pagemento_facturas could not be created.',
            'description' => $e,
            'Status' => 500,
            'status_text' => 'error'
        )
    );
    http_response_code(500);
}
// }
