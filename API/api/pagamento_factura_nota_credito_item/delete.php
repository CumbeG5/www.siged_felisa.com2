<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../main/initialize_class.php';
$database = new Database();
$db = $database->getConnection();

$itemPayment = new Pagamento_Factura_Nota_Credito_Item($db);

// $data = json_decode(file_get_contents("php://input"));
// if (empty($data))
//     $data = json_decode(json_encode($_POST));

// if (!empty($data)) {
$itemPayment->id = $_POST["id"];
$itemPayment->estado = $_POST["estado"];

try {
    $db->beginTransaction();
    if ($itemPayment->delete()) {
        $count_created = 0;
        $payment_id = $db->lastInsertId();

        // print_r($itemPayment->updateSaldoItemPayment($_POST["factura_id"]));

        //create projecto cobertura
        $db->commit();
        echo json_encode(
            array(
                'message' => 'Pagamento_Facturas created successfully.',
                'Status' => 201,
                'status_text' => 'success',
                // 'data' => array("id" => $data)
            )
        );
        http_response_code(201);
    } else {
        echo json_encode(
            array(
                'message' => 'Error creating cards data.',
                'Status' => 500,
                'status_text' => 'error'
            )
        );
        http_response_code(500);
    }
} catch (PDOException $e) {
    echo json_encode(
        array(
            'message' => 'factura_global could not be created.',
            'description' => $e,
            'Status' => 500,
            'status_text' => 'error'
        )
    );
    http_response_code(500);
}
// }
