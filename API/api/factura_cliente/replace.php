<?php @session_start();
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../main/initialize_class.php';
$database = new Database();
$db = $database->getConnection();

$factura = new FacturaCliente($db);

$data = json_decode(file_get_contents("php://input"));
if (empty($data))
    $data = json_decode(json_encode($_POST));

if (!empty($data)) {
    $factura->nr_factura = "'$data->nr_factura'";
    $factura->valor = $data->valor;
    $factura->data = "'$data->data'";
    $factura->codigo_autorizacao = "'$data->codigo_autorizacao'";
    $factura->nr_membro = "'$data->nr_membro'";
    $factura->nome_segurado = "'$data->nome_segurado'";
    $factura->user_id = $_SESSION['MM_idUserSgr'];//$data->user_id;
    $factura->id = $data->id_guia;

    try {
        $db->beginTransaction();
        if ($factura->replace()) {
            //update guia
            $db->commit();
            echo json_encode(
                array(
                    'message' => 'Factura updated successfully.',
                    'Status' => 200,
                    'status_text' => 'success',
                    'data' => array("id" => $data->id_guia)
                )
            );
            http_response_code(200);
        } else {
            echo json_encode(
                array(
                    'message' => 'Error creating cards data.',
                    'Status' => 500,
                    'status_text' => 'error'
                )
            );
            http_response_code(500);
        }
    } catch (PDOException $e) {
        echo json_encode(
            array(
                'message' => 'Factura could not be updated.',
                'description' => $e,
                'Status' => 500,
                'status_text' => 'error'
            )
        );
        http_response_code(500);
    }
}
