<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../main/initialize_class.php';
$database = new Database();
$db = $database->getConnection();

$factura = new FacturaCliente($db);

$data = json_decode(file_get_contents("php://input"));
if (empty($data))
    $data = json_decode(json_encode($_POST));

if (!empty($data)) {
    $factura->cliente_id = $data->cliente_id;
    $factura->nr_factura = "'$data->nr_factura'";
    $factura->valor = $data->valor;
    $factura->data = "'$data->data'";
    $factura->tipo_factura_id = $data->tipo_factura_id;
    $factura->estado_id = !isset($data->estado_id) || $data->estado_id=="" ? "1" : "'$data->estado_id'";
    $factura->codigo_autorizacao = "'$data->codigo_autorizacao'";
    $factura->nr_membro = "'$data->nr_membro'";
    $factura->valor_remanescente = !isset($data->saldo) || $data->saldo=="" ? "$data->valor" : "$data->saldo";
    $factura->nome_segurado = "'$data->nome_segurado'";
    $factura->user_id = $data->user_id;
    $factura->id = $data->id;

    try {
        $db->beginTransaction();
        if ($factura->update()) {
            $count_created = 0;
            $factura_id = $db->lastInsertId();

            //create projecto cobertura
            $db->commit();
            echo json_encode(
                array(
                    'message' => 'Factura created successfully.',
                    'Status' => 201,
                    'status_text' => 'success',
                    'data' => array("id" => $data->id)
                )
            );
            http_response_code(201);
        } else {
            echo json_encode(
                array(
                    'message' => 'Error creating cards data.',
                    'Status' => 500,
                    'status_text' => 'error'
                )
            );
            http_response_code(500);
        }
    } catch (PDOException $e) {
        echo json_encode(
            array(
                'message' => 'Factura could not be created.',
                'description' => $e,
                'Status' => 500,
                'status_text' => 'error'
            )
        );
        http_response_code(500);
    }
}
