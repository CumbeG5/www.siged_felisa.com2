<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../main/initialize_class.php';
$database = new Database();
$db = $database->getConnection();

$factura = new FacturaCliente($db);
$facturaGlobal = new FacturaGlobal($db);

$data = json_decode(file_get_contents("php://input"));
if (empty($data))
    $data = json_decode(json_encode($_POST));

if (!empty($data)) {
    $factura->factura_global_id = $data->last_factura_global_id;
    $factura->nr_factura = "'$data->nr_factura'";
    $factura->valor = $data->valor;
    $factura->data = "'$data->data'";
    $factura->tipo_factura_id = !isset($data->tipo_factura_id) || $data->tipo_factura_id == "" ? "NULL" : $data->tipo_factura_id;
    $factura->estado = !isset($data->estado) || $data->estado == "" ? "1" : "'$data->estado'";
    $factura->codigo_autorizacao = "'$data->codigo_autorizacao'";
    $factura->nr_membro = "'$data->nr_membro'";
    $factura->valor_remanescente = 0;
    $factura->nome_segurado = "'$data->nome_segurado'";
    $factura->tecnico_id = empty($data->nome_tecnico) ? 'NULL' : $data->nome_tecnico;
    $anexo = handleMultiFileUpload($_FILES, 'anexos_seguradora');
    $factura->anexo_factura = "'$anexo'";
    $factura->user_id = $data->user_id;

    try {
        if ($factura->validateFacturaNumnero()) {
            echo json_encode(
                array(
                    'message' => 'O número da guia já foi criado na base de dados!',
                    'Status' => 500,
                    'status_text' => 'error'
                )
            );
            http_response_code(500);
        } else if ($factura->validateFacturaCodigo()) {
            echo json_encode(
                array(
                    'message' => 'O codigo de autorização já foi criado na base de dados!',
                    'Status' => 500,
                    'status_text' => 'error'
                )
            );
            http_response_code(500);
        } else {
            $db->beginTransaction();
            if ($factura->create()) {
                $count_created = 0;
                $factura_id = $db->lastInsertId();

                $facturaGlobal->updateSaldoRemas($data->last_factura_global_id);

                //create projecto cobertura
                $db->commit();
                echo json_encode(
                    array(
                        'message' => 'Guia adicionada com sucesso!',
                        'Status' => 201,
                        'status_text' => 'success',
                        'data' => array("id" => $factura_id)
                    )
                );
                http_response_code(201);
            } else {
                echo json_encode(
                    array(
                        'message' => 'Error creating cards data.',
                        'Status' => 500,
                        'status_text' => 'error'
                    )
                );
                http_response_code(500);
            }
        }
    } catch (PDOException $e) {
        echo json_encode(
            array(
                'message' => 'Ocorreu um erro ao tentar registar a factura.<br>Por favor tente novamente!',
                'description' => $e,
                'Status' => 500,
                'status_text' => 'error'
            )
        );
        http_response_code(500);
    }
}
