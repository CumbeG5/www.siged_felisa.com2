<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../main/initialize_class.php';
$database = new Database();
$db = $database->getConnection();

$pagemento = new Pagamento_Factura_Cliente($db);

$data = json_decode(file_get_contents("php://input"));
if (empty($data))
    $data = json_decode(json_encode($_POST));

if (!empty($data)) {
    $pagemento->id = $data->seguradora_id;
    $pagemento->nr_pagamento = "'$data->nr_pagamento'";
    $pagemento->data_pagamento = "'$data->data_pagamento'";

    // print_r($data);
    // print_r($pagemento->id);

    try {
        $db->beginTransaction();
        if (!$pagemento->verifyNrPagamento()) {
            if ($pagemento->update()) {
                $count_created = 0;
                // $factura_id = $db->lastInsertId();

                //create projecto cobertura
                $db->commit();
                echo json_encode(
                    array(
                        'message' => 'Dados seguradora updated successfully.',
                        'Status' => 201,
                        'status_text' => 'success',
                        'data' => array("id" => $data->seguradora_id)
                    )
                );
                http_response_code(201);
            } else {
                echo json_encode(
                    array(
                        'message' => 'Error creating cards data.',
                        'Status' => 500,
                        'status_text' => 'error'
                    )
                );
                http_response_code(500);
            }
        } else {
            echo json_encode(
                array(
                    'message' => 'O numero do pagameto já existe.',
                    'Status' => 500,
                    'status_text' => 'error'
                )
            );
            http_response_code(500);
        }
    } catch (PDOException $e) {
        echo json_encode(
            array(
                'message' => 'Pagemento could not be update.',
                'description' => $e,
                'Status' => 500,
                'status_text' => 'error'
            )
        );
        http_response_code(500);
    }
}
