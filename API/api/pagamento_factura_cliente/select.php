<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../main/initialize_class.php';

$database = new Database();
$db = $database->getConnection();

$model = new Pagamento_Factura_Cliente($db);

$data = json_decode(json_encode($_GET));
// print_r($data->provincia_id);

$sqlAdd = "";

if(!empty($_GET['data_inicio']) OR !empty($_GET['data_fim'])){
    $data_inicio = $_GET['data_inicio'];
    $data_fim = $_GET['data_fim'];
    $sqlAdd .= " AND pf.data_pagamento BETWEEN '{$data_inicio}' AND '{$data_fim}' ";
}

if(!empty($_GET['nr_pagamento'])){
    $nr_pagamento = $_GET['nr_pagamento'];
    $sqlAdd .= "AND pf.nr_pagamento LIKE '%{$nr_pagamento}%'";
}

if(!empty($_GET['seguradora_id'])){
    $seguradora_id = $_GET['seguradora_id'];
    $sqlAdd .= "AND pf.seguradora_id = {$seguradora_id}";
}
$page = $_GET['page'];
$limit = $_GET['limit'];
// exit
$start_from = ($page - 1) * $limit;
// Employees read query
$result = $model->read($sqlAdd, $start_from, $limit);

// Check if any employee
if(!empty($result)) {
    // Turn to JSON & output
    echo json_encode($result);
    // http_response_code(200);
} else {
    // No Data
    echo json_encode(
        array(
            "message' => 'No data found.",
            'Status' => 204,
            'status_text' => 'success',
            'data' => []
        )
    );
//    http_response_code(204);
}
?>