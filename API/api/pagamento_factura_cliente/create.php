<?php
@session_start();
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../main/initialize_class.php';
$database = new Database();
$db = $database->getConnection();

$pagemento = new Pagamento_Factura_Cliente($db);
$factura = new FacturaCliente($db);

$data = json_decode(file_get_contents("php://input"));
if (empty($data))
    $data = json_decode(json_encode($_POST));

if (!empty($data)) {
    // print_r($data);
    $pagemento->cliente_id = "'$data->seguradora_id'";
    $factura_global_id = null;
    // for ($i=0; $i < count($data->factura_global_id); $i++) { 
    //     if(count($data->factura_global_id)-1 == $i){
    //         $factura_global_id .= $data->factura_global_id[$i];
    //     }else{
    //         $factura_global_id .= $data->factura_global_id[$i].",";
    //     }
    // }
    $pagemento->factura_global_id = "'$factura_global_id'";
    $pagemento->data_pagamento = "'$data->data_pagamento'";
    $pagemento->nr_pagamento = "'$data->nr_pagamento'";

    try {
        if ($pagemento->verifyNrPagamento()) {
            echo json_encode(
                array(
                    'message' => 'O número do pagamento já foi criado na base de dados!',
                    'Status' => 500,
                    'status_text' => 'error'
                )
            );
            http_response_code(500);
        } else {
            $db->beginTransaction();
            $i = $pagemento->create();
            if ($i > 0) {
                $count_created = 0;

                if (count($_SESSION['cart_pagamento_item']) > 0) {
                    foreach ($_SESSION['cart_pagamento_item'] as $values) {
                        $rg_Pagamento_Item = new Pagamento_Factura_Cliente_Item($db);
                        $total = (float)$values['remanescente'] - (float)$values['desconto'];
                        $saldo = (float)$values['valor'] - (float)$values['remanescente'];
                        // print_r($_SESSION['cart_pagamento_item']);
                        $rg_Pagamento_Item->pagamento_factura_id = $i;
                        $rg_Pagamento_Item->factura_cliente_id = $values['factura_cliente_id'];
                        $rg_Pagamento_Item->valor_pago = $values['remanescente'];
                        $rg_Pagamento_Item->data = date('Y-m-d');
                        $rg_Pagamento_Item->create();

                        $factura->valor_remanescente = $saldo;
                        $factura->id = $values['factura_cliente_id'];
                        $factura->updateSaldo();
                    }
                }
                if (count($_SESSION['cart_tipos_pagamentos']) > 0) {
                    foreach ($_SESSION['cart_tipos_pagamentos'] as $values) {
                        // print_r($values);
                        $dat = $values['data_pagamento'];
                        $formasPagamento = new Formas_Pagamentos($db);
                        $formasPagamento->pagamento_id = $i;
                        $formasPagamento->proveniencia = $values['proveniencia'];
                        $formasPagamento->tipo_pagamento_id = $values['tipo_pagamento_id'];
                        $formasPagamento->valor = $values['valor'];
                        $formasPagamento->referencia = !isset($values['nr_operacao']) || $values['nr_operacao'] == "" ? "NULL" : $values['nr_operacao'];
                        $formasPagamento->idformapagamento = $values['idformapagamento'];
                        $formasPagamento->caixa = !isset($values['caixa']) || $values['caixa'] == "" ? "NULL" : $values['caixa'];
                        $formasPagamento->numerocheque = !isset($values['numerocheque']) || $values['numerocheque'] == "" ? "NULL" : $values['numerocheque'];
                        $formasPagamento->numero_documento = !isset($values['numerodocumento']) || $values['numerodocumento'] == "" ? "NULL" : $values['numerodocumento'];
                        $formasPagamento->data = date('Y-m-d');
                        $formasPagamento->data_pagamento = empty($dat) ? "NULL" : "'$dat'";
                        $formasPagamento->create();
                    }
                }

                $db->commit();
                unset($_SESSION["cart_pagamento_item"]);
                unset($_SESSION["cart_tipos_pagamentos"]);
                echo json_encode(
                    array(
                        'message' => 'Pagamento created successfully.',
                        'Status' => 201,
                        'status_text' => 'success',
                        'data' => array("id" => $i)
                    )
                );
                http_response_code(201);
            } else {
                echo json_encode(
                    array(
                        'message' => 'Error creating cards data.',
                        'Status' => 500,
                        'status_text' => 'error'
                    )
                );
                http_response_code(500);
            }
        }
    } catch (PDOException $e) {
        echo json_encode(
            array(
                'message' => 'Pagemento could not be created.',
                'description' => $e,
                'Status' => 500,
                'status_text' => 'error'
            )
        );
        http_response_code(500);
    }
}
