<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../main/initialize_class.php';
$database = new Database();
$db = $database->getConnection();

$pagemento_facturas = new Pagamento_Facturas($db);
$factura = new Factura($db);

$data = json_decode(file_get_contents("php://input"));
if (empty($data))
    $data = json_decode(json_encode($_POST));

if (!empty($data)) {
    $pagemento_facturas->status = "'$data->status'";
    $pagemento_facturas->id = $data->id;
    $factura->saldo = $data->valor;
    $factura->id = $data->factura;

    try {
        $db->beginTransaction();
        if ($pagemento_facturas->delete()) {

            //create projecto cobertura
            $factura->updateSaldo3();
            $db->commit();
            echo json_encode(
                array(
                    'message' => 'Pagamento factura eliminado com sucesso.',
                    'Status' => 200,
                    'status_text' => 'success',
                    'data' => array("id" => $data->id)
                )
            );
            http_response_code(201);
        } else {
            echo json_encode(
                array(
                    'message' => 'Error creating cards data.',
                    'Status' => 500,
                    'status_text' => 'error'
                )
            );
            http_response_code(500);
        }
    } catch (PDOException $e) {
        echo json_encode(
            array(
                'message' => 'pagemento_facturas could not be created.',
                'description' => $e,
                'Status' => 500,
                'status_text' => 'error'
            )
        );
        http_response_code(500);
    }
}
