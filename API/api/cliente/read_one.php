<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../main/initialize_class.php';

$database = new Database();
$db = $database->getConnection();

$model = new Cliente($db);

$data = json_decode(json_encode($_GET));
// print_r($data->provincia_id);

$model->id = $data->id;

$sqlAdd = "";

// if(!empty($_GET['fornecedor_id'])){
//     $fornecedor_id = $_GET['fornecedor_id'];
//     $sqlAdd .= "f.fornecedor_id = {$fornecedor_id}";
// }

// Employees read query
$result = $model->read_one();

// Check if any employee
if(!empty($result)) {
    // Turn to JSON & output
    echo json_encode($result);
    http_response_code(200);
} else {
    // No Data
    echo json_encode(
        array(
            "message' => 'No data found.",
            'Status' => 204,
            'status_text' => 'success',
            'data' => []
        )
    );
//    http_response_code(204);
}
?>