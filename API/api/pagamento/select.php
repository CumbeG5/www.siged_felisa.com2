<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../main/initialize_class.php';

$database = new Database();
$db = $database->getConnection();

$model = new Pagamento($db);

$data = json_decode(json_encode($_GET));
// print_r($data->provincia_id);

$sqlAdd = "";

if (!empty($_GET['data_inicio']) or !empty($_GET['data_fim'])) {
    $data_inicio = $_GET['data_inicio'];
    $data_fim = $_GET['data_fim'];
    $sqlAdd .= " AND p.data BETWEEN '{$data_inicio}' AND '{$data_fim}' ";
}

if (!empty($_GET['nr_recibo'])) {
    $nr_recibo = $_GET['nr_recibo'];
    $sqlAdd .= "AND p.nr_recibo LIKE '%{$nr_recibo}%'";
}
$page = $_GET['page'];
$limit = $_GET['limit'];
// exit
$start_from = ($page - 1) * $limit;
// Employees read query
$result = $model->read($sqlAdd, $start_from, $limit);

// Check if any employee
if (!empty($result)) {
    // Turn to JSON & output
    echo json_encode($result);
    http_response_code(200);
} else {
    // No Data
    echo json_encode(
        array(
            "message' => 'No data found.",
            'Status' => 204,
            'status_text' => 'success',
            'data' => []
        )
    );
    //    http_response_code(204);
}
