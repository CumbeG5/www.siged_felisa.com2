<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../main/initialize_class.php';
$database = new Database();
$db = $database->getConnection();

$pagemento = new Pagamento($db);

$data = json_decode(file_get_contents("php://input"));
if (empty($data))
    $data = json_decode(json_encode($_POST));

if (!empty($data)) {
    $pagemento->nr_recibo = "'$data->nr_recibo'";
    $pagemento->data = "'$data->data_pagamento'";
    $pagemento->data_recibo = "'$data->data_recibo'";
    $pagemento->id = $data->id;

    try {
        $db->beginTransaction();
        if ($pagemento->update()) {

            //create projecto cobertura
            $db->commit();
            echo json_encode(
                array(
                    'message' => 'Dados do pagamento com sucesso.',
                    'Status' => 201,
                    'status_text' => 'success',
                    'data' => array("id" => $data->id)
                )
            );
            http_response_code(201);
        } else {
            echo json_encode(
                array(
                    'message' => 'Erro ao actualizar os dados do pagamento.',
                    'Status' => 500,
                    'status_text' => 'error'
                )
            );
            http_response_code(500);
        }
    } catch (PDOException $e) {
        echo json_encode(
            array(
                'message' => 'Não foi possível actualizar o pagamento.',
                'description' => $e,
                'Status' => 500,
                'status_text' => 'error'
            )
        );
        http_response_code(500);
    }
}
