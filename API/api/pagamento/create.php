<?php
@session_start();
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../main/initialize_class.php';
$database = new Database();
$db = $database->getConnection();

$pagamento = new Pagamento($db);
$factura = new Factura($db);

$data = json_decode(file_get_contents("php://input"));
if (empty($data))
    $data = json_decode(json_encode($_POST));

if (!empty($data)) {
    $pagamento->nr_recibo = "'$data->nr_recibo'";
    $pagamento->data = "'$data->data'";
    $pagamento->data_recibo = "'$data->data_recibo'";
    $pagamento->fornecedor_id_pagamento = $data->fornecedor_id_pagamento;
    $pagamento->user_id = $data->user_id;
    // $pagamento->numero_factura = $data->numero_factura;

    try {
        if ($pagamento->validateRecibo()) {
            echo json_encode(
                array(
                    'message' => 'O número do recibo já foi criado na base de dados!',
                    'Status' => 500,
                    'status_text' => 'error'
                )
            );
            http_response_code(500);
        } else {
            $db->beginTransaction();
            $i = $pagamento->create();
            if ($i > 0) {
                $count_created = 0;
                // $pagamento_id = $db->lastInsertId();
                // $desconto = null;
                // print_r($db->lastInsertId());
                if (count($_SESSION['cart_facturas']) > 0) {
                    foreach ($_SESSION['cart_facturas'] as $values) {
                        $pagamento_facturas = new Pagamento_Facturas($db);
                        // print_r($values['remanescente']);
                        $total = (float)$values['remanescente'];
                        $saldo = (float)$values['valor'] - (float)$values['remanescente'];
                        // $desconto = (double)$values['remanescente']-
                        $pagamento_facturas->pagamento_id = $i;
                        $pagamento_facturas->factura_id = $values['factura_id'];
                        $pagamento_facturas->valor = $values['valor'];
                        $pagamento_facturas->remanescente = $values['remanescente'];
                        $pagamento_facturas->saldo = $saldo;
                        $pagamento_facturas->desconto = $values['desconto'] ?? 0;

                        $pagamento_facturas->total = $total;
                        $pagamento_facturas->create();

                        $factura->saldo = $saldo;
                        $factura->id = $values['factura_id'];
                        $factura->updateSaldo();
                    }
                }
                if (count($_SESSION['cart_tipos_pagamentos']) > 0) {
                    foreach ($_SESSION['cart_tipos_pagamentos'] as $values) {
                        $formasPagamento = new Formas_Pagamentos($db);

                        // print_r("redtfgvhbjn");

                        // print_r($values['remanescente']);
                        $formasPagamento->pagamento_id = $i;
                        $formasPagamento->proveniencia = $values['proveniencia'];
                        $formasPagamento->tipo_pagamento_id = $values['tipo_pagamento_id'];
                        $formasPagamento->valor = $values['valor'];
                        $formasPagamento->referencia = !isset($values['nr_operacao']) || $values['nr_operacao'] == "" ? "NULL" : $values['nr_operacao'];
                        $formasPagamento->idformapagamento = $values['idformapagamento'];
                        $formasPagamento->caixa = !isset($values['caixa']) || $values['caixa'] == "" ? "NULL" : $values['caixa'];
                        $formasPagamento->numerocheque = !isset($values['numerocheque']) || $values['numerocheque'] == "" ? "NULL" : $values['numerocheque'];
                        $formasPagamento->data_pagamento = empty($data->data_pagamento) ? "NULL" : "'$data->data_pagamento'";
                        $formasPagamento->data = date('Y-m-d');
                        $formasPagamento->create();
                    }
                }

                $db->commit();
                unset($_SESSION["cart_facturas"]);
                unset($_SESSION["cart_tipos_pagamentos"]);
                echo json_encode(
                    array(
                        'message' => 'Pagamento created successfully.',
                        'Status' => 201,
                        'status_text' => 'success',
                        'data' => array("id" => $i)
                    )
                );
                http_response_code(201);
            } else {
                echo json_encode(
                    array(
                        'message' => 'Error creating cards data.',
                        'Status' => 500,
                        'status_text' => 'error'
                    )
                );
                http_response_code(500);
            }
        }
    } catch (PDOException $e) {
        echo json_encode(
            array(
                'message' => 'Pagamento could not be created.',
                'description' => $e,
                'Status' => 500,
                'status_text' => 'error'
            )
        );
        http_response_code(500);
    }
}
