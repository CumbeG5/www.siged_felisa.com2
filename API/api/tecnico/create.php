<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../main/initialize_class.php';
$database = new Database();
$db = $database->getConnection();

$cliente = new Tecnico($db);

// $data = json_decode(file_get_contents("php://input"));
// if (empty($data))
//     $data = json_decode(json_encode($_POST));

// if (!empty($data)) {
// print_r($_POST["nome"]);
$cliente->nome = $_POST["nome"];
// $cliente->apelido = "'$data->apelido'";

try {
    $db->beginTransaction();
    if ($cliente->create()) {
        $count_created = 0;
        $cliente_id = $db->lastInsertId();

        //create projecto cobertura
        $db->commit();
        echo json_encode(
            array(
                'message' => 'tecnico created successfully.',
                'Status' => 201,
                'status_text' => 'success',
                'data' => array("id" => $cliente_id)
            )
        );
        http_response_code(201);
    } else {
        echo json_encode(
            array(
                'message' => 'Error creating cards data.',
                'Status' => 500,
                'status_text' => 'error'
            )
        );
        http_response_code(500);
    }
} catch (PDOException $e) {
    echo json_encode(
        array(
            'message' => 'Ocorreu um erro ao tentar registar a cliente.<br>Por favor tente novamente!',
            'description' => $e,
            'Status' => 500,
            'status_text' => 'error'
        )
    );
    http_response_code(500);
}
// }
