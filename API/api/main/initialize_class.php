<?php
include_once '../../config/database.php';
include_once '../../class/Factura.php';
include_once '../../class/Pagamento.php';
include_once '../../class/Pagamento_Facturas.php';
include_once '../../class/Formas_Pagamentos.php';
include_once '../../class/Tipo_Factura.php';
include_once '../../class/Gerar_nr_factura.php';
include_once '../../class/FacturaCliente.php';
include_once '../../class/Cliente.php';
include_once '../../class/Pagamento_Facturas_Cliente.php';
include_once '../../class/Pagamento_Facturas_Nota_Credito.php';
include_once '../../class/Pagamento_Factura_Cliente_Item.php';
include_once '../../class/Pagamento_Factura_Nota_Credito_Item.php';
include_once '../../class/FacturaGlobal.php';
include_once '../../class/handle_upload_anexos.php';
include_once '../../class/Tecnico.php';
include_once '../../class/Despesas.php';

