<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../main/initialize_class.php';
$database = new Database();
$db = $database->getConnection();

$formas_pagamentos = new Formas_Pagamentos($db);

$data = json_decode(file_get_contents("php://input"));
if (empty($data))
    $data = json_decode(json_encode($_POST));

if (!empty($data)) {
    $formas_pagamentos->pagamento_id = $data->pagamento_id;
    $formas_pagamentos->proveniencia = $data->proveniencia;
    $formas_pagamentos->tipo_pagamento_id = $data->tipo_pagamento_id;
    $formas_pagamentos->valor = $data->valor_pagamento;
    $formas_pagamentos->referencia = "'$data->nr_operacao'";
    $formas_pagamentos->data = date('Y-m-d');
    $formas_pagamentos->data_pagamento = empty($data->data_pagamento) ? "NULL" : "'$data->data_pagamento'";
    $formas_pagamentos->idformapagamento = $data->idformapagamento;
    $formas_pagamentos->numerocheque = !isset($data->numerocheque) || $data->numerocheque == "" ? "NULL" : $data->numerocheque;
    $formas_pagamentos->caixa = !isset($data->caixa) || $data->caixa == "" ? "NULL" : $data->caixa;

    try {
        $db->beginTransaction();
        if ($formas_pagamentos->create()) {
            $count_created = 0;
            $factura_id = $db->lastInsertId();

            //create projecto cobertura
            $db->commit();
            echo json_encode(
                array(
                    'message' => 'Pagamento_Facturas created successfully.',
                    'Status' => 201,
                    'status_text' => 'success',
                    'data' => array("id" => $factura_id)
                )
            );
            http_response_code(201);
        } else {
            echo json_encode(
                array(
                    'message' => 'Error creating cards data.',
                    'Status' => 500,
                    'status_text' => 'error'
                )
            );
            http_response_code(500);
        }
    } catch (PDOException $e) {
        echo json_encode(
            array(
                'message' => 'formas_pagamentos could not be created.',
                'description' => $e,
                'Status' => 500,
                'status_text' => 'error'
            )
        );
        http_response_code(500);
    }
}
