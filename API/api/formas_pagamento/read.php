<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../main/initialize_class.php';

$database = new Database();
$db = $database->getConnection();

$model = new Formas_Pagamentos($db);

$data = json_decode(json_encode($_GET));
// print_r($_GET['tipo']);


$sqlAdd = "";

if (!empty($_GET['pagamento_id'])) {
    $pagamento_id = $_GET['pagamento_id'];
    $sqlAdd .= "AND f.pagamento_id = {$pagamento_id}";
}
if (!empty($_GET['tipo'])) {
    $sqlAdd .= " AND f.proveniencia='".$_GET['tipo']."'";
}
// Employees read query
if ($_GET['tipo'] == "seguradora") {
    $result = $model->read2($sqlAdd);
} else {
    $result = $model->read($sqlAdd);
}


// Check if any employee
if (!empty($result)) {
    // Turn to JSON & output
    echo json_encode($result);
    http_response_code(200);
} else {
    // No Data
    echo json_encode(
        array(
            "message' => 'No data found.",
            'Status' => 204,
            'status_text' => 'success',
            'data' => []
        )
    );
    //    http_response_code(204);
}
