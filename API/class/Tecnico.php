<?php
class Tecnico
{
    // Connection
    private $conn;
    private $table_name = "tecnico";

    public $id;
    public $nome;
    public $apelido;
    public $estado;

    public function __construct($db)
    {
        $this->conn = $db;
    }

    public function create()
    {
        $sql = "INSERT 
            INTO " . $this->table_name . " 
            SET
            nome = '$this->nome'
            ";
        // print_r($sql);
        $stmt = $this->conn->prepare($sql);

        // $this->nome = htmlspecialchars(strip_tags($this->nome));
        // $this->apelido = htmlspecialchars(strip_tags($this->apelido));

        if ($stmt->execute()) {
            return true;
        }
        return false;
    }

    public function update()
    {
        $sql = "UPDATE 
            " . $this->table_name . "
                SET 
                nome = '$this->nome'
            WHERE 
            id = $this->id";
        // print_r($sql);
        $stmt = $this->conn->prepare($sql);

        // $this->nome = htmlspecialchars(strip_tags($this->nome));
        // $this->apelido = htmlspecialchars(strip_tags($this->apelido));
        // $this->id = htmlspecialchars(strip_tags($this->id));

        if ($stmt->execute()) {
            return true;
        }
        return false;
    }

    public function read()
    {
        $sql = "SELECT * FROM {$this->table_name} WHERE activo='1'";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $qty = $stmt->rowCount();
        if ($qty > 0) {
            $response = array();
            $response['data'] = null;
            $response['total'] = $qty;
            foreach ($stmt as $value) {
                extract($value);
                $emp_item = array(
                    "id" => $id,
                    "nome" => $nome,
                    "apelido" => $apelido
                );
                $response['data'][] = $emp_item;
            }
        } else {
            $response = [];
        }
        return $response;
    }

    public function read_one()
    {
        $sql = "SELECT * FROM " . $this->table_name . " WHERE id=$this->id";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            extract($stmt->fetch(PDO::FETCH_ASSOC));
            $response = array(
                "id" => $id,
                "nome" => $nome,
                "apelido" => $apelido
            );
        } else {
            return $response;
        }
        return $response;
    }

    public function delete()
    {
        $sql = "UPDATE 
            " . $this->table_name . "
                SET 
                activo = '$this->estado'
            WHERE 
            id = $this->id";
        // print_r($sql);

        $stmt = $this->conn->prepare($sql);

        if ($stmt->execute()) {
            return true;
        }
        return false;
    }
}
