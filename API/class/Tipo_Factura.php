<?php
class Tipo_Factura
{
    private $conn;
    private $table_name = "tipo_factura";

    public $id;
    public $descricao;
    public $tipo_operacao;

    public function __construct($db)
    {
        $this->conn = $db;
    }

    public function create()
    {
        $sql = "
            INSERT 
                INTO 
                    " . $this->table_name . "
                SET 
                descricao = $this->descricao,
                tipo_operacao = $this->tipo_operacao";

        $stmt = $this->conn->prepare($sql);
        if ($stmt->execute()) {
            return true;
        } else {
            return false;
        }
    }
    public function update()
    {
        $sql = "
            UPDATE  
                " . $this->table_name . "
                SET 
                descricao = $this->descricao,
                tipo_operacao = $this->tipo_operacao
                WHERE id = $this->id";

        $stmt = $this->conn->prepare($sql);
        if ($stmt->execute()) {
            return true;
        } else {
            return false;
        }
    }
    public function read()
    {
        $sql = "SELECT * FROM " . $this->table_name . "";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $rows = $stmt->rowCount();
        if ($rows > 0) {
            $response = array();
            $response['data'] = null;
            $response['total_results'] = $rows;
            foreach ($stmt as $values) {
                extract($values);
                $emp_items = array(
                    "id" => $id,
                    "descricao" => $descricao,
                    "tipo_operacao" => $tipo_operacao
                );
                $response['data'][] = $emp_items;
            }
        } else {
            $response = [];
        }
        return $response;
    }
    public function read_one()
    {
        $sql = "SELECT * FROM " . $this->table_name . "";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            extract($values->fetch(PDO::FETCH_ASSOC));
            $emp_items = array(
                "id" => $id,
                "descricao" => $descricao,
                "tipo_operacao" => $tipo_operacao
            );
        } else {
            $response = [];
        }
        return $response;
    }
}
