<?php
class Pagamento_Facturas_Nota_Credito
{
    private $conn;
    private $table_name = "pagamento_factura_nota_credito";

    public $id;
    public $cliente_id;
    public $factura_global_id;
    public $data_pagamento;
    public $nr_pagamento;
    public $estado;

    public function __construct($db)
    {
        $this->conn = $db;
    }

    public function create()
    {
        $i = null;
        $generate_number = new Generate_Number($this->conn);

        $ano = date("Y");
        $tipo = 'nota_credito_seguradora';
        $numeracao = $generate_number->getInvoiceNumber($ano, $tipo) + 1;
        $numero_factura = "{$numeracao}/{$ano}";

        $sql = "
            INSERT 
                INTO " . $this->table_name . " 
            SET 
                cliente_id = $this->cliente_id,
                factura_global_id = $this->factura_global_id,
                data_nota = '$this->data_pagamento',
                nr_nota = $this->nr_pagamento,
                nr_nota_credito = '$numero_factura'
                ";
        // print_r($sql);

        $stmt = $this->conn->prepare($sql);
        if ($stmt->execute()) {
            $i = $this->conn->lastInsertId();
            $generate_number->updateInvoiceNumber($ano, $tipo);
        }
        return $i;
    }
    public function update()
    {
        $sql = "
            UPDATE 
                " . $this->table_name . " 
            SET 
                data_nota = '$this->data_pagamento',
                nr_nota = $this->nr_pagamento
                WHERE id= $this->id";

        $stmt = $this->conn->prepare($sql);
        if ($stmt->execute()) {
            return true;
        }
        return false;
    }

    public function read($filter = null)
    {
        $sql = "SELECT pf.id, 
                        pf.cliente_id, 
                        pf.data_nota, 
                        pf.nr_nota, 
                        c.nome as cliente, 
                        pf.nr_nota_credito,
                        pf.estado
                FROM " . $this->table_name . " pf
                LEFT JOIN clientes c ON c.id=pf.cliente_id
                WHERE 1=1 AND estado != '0' {$filter}";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $nr_rows = $stmt->rowCount();
        if ($nr_rows > 0) {
            $response = array();
            $response['data'] = null;
            $response['total_results'] = $nr_rows;

            $dadosPagamentos = new Pagamento_Factura_Nota_Credito_Item($this->conn);

            foreach ($stmt as $value) {
                extract($value);
                $emp_item = array(
                    "id" => $id,
                    "cliente_id" => $cliente_id,
                    "cliente" => $cliente,
                    "data_nota" => $data_nota,
                    "nr_nota" => $nr_nota,
                    "nr_nota_credito" => $nr_nota_credito,
                    "valor" => number_format($this->totalFormaPagamento($id)['valor'], 2, ".", ","),
                    "totalPago" => number_format($dadosPagamentos->SUMPagamentos($id)['total_pago_factura'], 2, ".", ","),
                    "estado" => $estado, //($dadosPagamentos->SUMPagamentos($id)['total_pago_factura'] == 0) ? "Pendente" : ($dadosPagamentos->SUMPagamentos($id)['total_pago_factura'] >=$this->totalFormaPagamento($id)['valor'] ? "Terminado" : "Pendente"),
                );
                $response['data'][] = $emp_item;
            }
        } else {
            $response = [];
        }
        return $response;
    }

    public function delete()
    {
        $sql = "UPDATE " . $this->table_name . " SET estado='{$this->estado}' WHERE id={$this->id}";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            return true;
        }
        return false;
    }

    public function totalFormaPagamento($pagamento_id)
    {
        $sql = "SELECT SUM(fp.valor) as sum FROM formas_pagamento fp
        WHERE 
        pagamento_id = {$pagamento_id} 
        AND fp.status='1'
        ";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            extract($stmt->fetch(PDO::FETCH_ASSOC));
            $response = ["valor" => $sum];
        } else {
            return $response = ["valor" => 0];
        }
        return $response;
    }

    public function read_one()
    {
        $sql =
            "SELECT pf.id, 
            pf.cliente_id, 
            pf.data_nota, 
            pf.nr_nota, 
            c.nome as cliente, 
            pf.nr_nota_credito,
            pf.factura_global_id
        FROM " . $this->table_name . " pf
        LEFT JOIN clientes c ON c.id=pf.cliente_id
        WHERE
        pf.id = $this->id
            ";

        $stmt = $this->conn->prepare($sql);
        // sanitize
        // $this->id = htmlspecialchars(strip_tags($this->id));

        // bind data
        // $stmt->bindParam(":id", $this->id);

        $stmt->execute();
        $row = $stmt->rowCount();
        if ($row > 0) {
            // $formas_pagamento = new Formas_Pagamentos($this->conn);
            $dadosPagamentos = new Pagamento_Factura_Nota_Credito_Item($this->conn);
            extract($stmt->fetch(PDO::FETCH_ASSOC));
            $response = array(
                "id" => $id,
                "cliente_id" => $cliente_id,
                "cliente" => $cliente,
                "factura_global_id" => $factura_global_id,
                "data_nota" => $data_nota,
                "nr_nota" => $nr_nota,
                // "formas_pagamento" => $formas_pagamento->read("AND f.pagamento_id={$id}"),
                "facturasPagas" => $dadosPagamentos->read("AND pagamento_factura_id={$id}")['data'] ?? [],
                // "dadosFactura" => $formas_pagamento->DadosFactura($id, 'seguradora') ?? [],
                "nr_nota_credito" => $nr_nota_credito,
                "totalPago" => number_format($dadosPagamentos->SUMPagamentos($id)['total_pago_factura'], 2, ".", ",") ?? [],
            );
        } else {
            $response = [];
        }
        return $response;
    }

    public function  verifyNrPagamento()
    {
        $sql = "SELECT * FROM pagamento_factura_nota_credito WHERE nr_nota ={$this->nr_pagamento} AND estado != '0'";
        // print_r($sql);
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            return true;
        } else {
            return false;
        }
    }
}
