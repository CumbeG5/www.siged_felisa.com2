<?php
class Pagamento_Factura_Cliente
{
    private $conn;
    private $table_name = "pagamento_factura_cliente";

    public $id;
    public $cliente_id;
    public $factura_global_id;
    public $data_pagamento;
    public $nr_pagamento;
    public $estado;

    public function __construct($db)
    {
        $this->conn = $db;
    }

    public function create()
    {
        $i = null;
        $generate_number = new Generate_Number($this->conn);

        $ano = date("Y");
        $tipo = 'venda_credito';
        $numeracao = $generate_number->getInvoiceNumber($ano, $tipo) + 1;
        $numero_factura = "{$numeracao}/{$ano}";

        $sql = "
            INSERT 
                INTO " . $this->table_name . " 
            SET 
                cliente_id = $this->cliente_id,
                factura_global_id = $this->factura_global_id,
                data_pagamento = $this->data_pagamento,
                nr_pagamento = $this->nr_pagamento,
                nr_nota_pagamento = '$numero_factura'
                ";

        $stmt = $this->conn->prepare($sql);
        if ($stmt->execute()) {
            $i = $this->conn->lastInsertId();
            $generate_number->updateInvoiceNumber($ano, $tipo);
        }
        return $i;
    }
    public function update()
    {
        $sql = "
            UPDATE 
                " . $this->table_name . " 
            SET 
                data_pagamento = $this->data_pagamento,
                nr_pagamento = $this->nr_pagamento
                WHERE id= $this->id";

        $stmt = $this->conn->prepare($sql);
        if ($stmt->execute()) {
            return true;
        }
        return false;
    }

    public function read($filter = null, $start_from, $limit)
    {
        $sql = "SELECT pf.id, 
                        pf.cliente_id, 
                        pf.data_pagamento, 
                        pf.nr_pagamento, 
                        c.nome as cliente, 
                        pf.nr_nota_pagamento,
                        pf.estado
                FROM " . $this->table_name . " pf
                LEFT JOIN clientes c ON c.id=pf.cliente_id
                WHERE 1=1 AND estado != '0' {$filter} ORDER BY pf.id desc";
        $sql .= " limit {$start_from}, {$limit}";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $nr_rows = $stmt->rowCount();
        if ($nr_rows > 0) {
            $response = array();
            $response['data'] = null;
            $response['total_results'] = $nr_rows;

            $dadosPagamentos = new Pagamento_Factura_Cliente_Item($this->conn);

            foreach ($stmt as $value) {
                extract($value);
                $emp_item = array(
                    "id" => $id,
                    "cliente_id" => $cliente_id,
                    "cliente" => $cliente,
                    "data_pagamento" => $data_pagamento,
                    "nr_pagamento" => $nr_pagamento,
                    "nr_nota_pagamento" => $nr_nota_pagamento,
                    "valor" => number_format($this->totalFormaPagamento($id)['valor'], 2, ".", ","),
                    "totalPago" => number_format($dadosPagamentos->SUMPagamentos($id)['total_pago_factura'], 2, ".", ","),
                    "estado" => $estado, //($dadosPagamentos->SUMPagamentos($id)['total_pago_factura'] == 0) ? "Pendente" : ($dadosPagamentos->SUMPagamentos($id)['total_pago_factura'] >=$this->totalFormaPagamento($id)['valor'] ? "Terminado" : "Pendente"),
                );
                $response['data'][] = $emp_item;
            }
        } else {
            $response = [];
        }
        return $response;
    }

    public function delete()
    {
        $sql = "UPDATE " . $this->table_name . " SET estado='{$this->estado}' WHERE id={$this->id}";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            return true;
        }
        return false;
    }

    public function totalFormaPagamento($pagamento_id)
    {
        $sql = "SELECT SUM(fp.valor) as sum FROM formas_pagamento fp
        WHERE 
        pagamento_id = {$pagamento_id} 
        AND fp.status='1'
        ";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            extract($stmt->fetch(PDO::FETCH_ASSOC));
            $response = ["valor" => $sum];
        } else {
            return $response = ["valor" => 0];
        }
        return $response;
    }

    public function read_one()
    {
        $sql =
            "SELECT pf.id, 
            pf.cliente_id, 
            CAST(pf.created_at as DATE) as data_criacao,
            pf.data_pagamento, 
            pf.nr_pagamento, 
            c.nome as cliente, 
            pf.nr_nota_pagamento,
            pf.factura_global_id
        FROM " . $this->table_name . " pf
        LEFT JOIN clientes c ON c.id=pf.cliente_id
        WHERE
        pf.id = $this->id";

        $stmt = $this->conn->prepare($sql);
        // sanitize
        // $this->id = htmlspecialchars(strip_tags($this->id));

        // bind data
        // $stmt->bindParam(":id", $this->id);

        $stmt->execute();
        $row = $stmt->rowCount();
        if ($row > 0) {
            $formas_pagamento = new Formas_Pagamentos($this->conn);
            $dadosPagamentos = new Pagamento_Factura_Cliente_Item($this->conn);
            extract($stmt->fetch(PDO::FETCH_ASSOC));
            $response = array(
                "id" => $id,
                "cliente_id" => $cliente_id,
                "cliente" => $cliente,
                "factura_global_id" => $factura_global_id,
                "data_criacao" => $data_criacao,
                "data_pagamento" => $data_pagamento,
                "nr_pagamento" => $nr_pagamento,
                "formas_pagamento" => $formas_pagamento->read("AND f.pagamento_id={$id}"),
                "facturasPagas" => $dadosPagamentos->read("AND pagamento_factura_id={$id}")['data'] ?? [],
                "dadosFactura" => $formas_pagamento->DadosFactura($id, 'seguradora') ?? [],
                "nr_nota_pagamento" => $nr_nota_pagamento,
                "totalPago" => number_format($dadosPagamentos->SUMPagamentos($id)['total_pago_factura'], 2, ".", ",") ?? [],
            );
        } else {
            $response = [];
        }
        return $response;
    }

    public function  verifyNrPagamento()
    {
        $sql = "SELECT * FROM pagamento_factura_cliente WHERE nr_pagamento={$this->nr_pagamento} AND estado != '0'";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            return true;
        } else {
            return false;
        }
    }
}
