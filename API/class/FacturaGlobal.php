<?php
class FacturaGlobal
{
    private $conn;
    private $table_name = "factura_global";

    public $id;
    public int $seguradora_id;
    public string $data;
    public string $numero;
    public float $valor;
    public float $remanescente;
    public int $user_id;
    public string $estado;

    public function __construct($db)
    {
        $this->conn = $db;
    }

    public function create()
    {
        $sql = "INSERT 
                INTO " . $this->table_name . " 
                    SET 
                    seguradora_id={$this->seguradora_id},
                    data={$this->data},
                    numero={$this->numero},
                    valor={$this->valor},
                    remanescente={$this->remanescente},
                    user_id={$this->user_id}";

        $stmt = $this->conn->prepare($sql);
        if ($stmt->execute()) {
            return true;
        }
        return false;
    }

    public function update()
    {
        $sql = "UPDATE " . $this->table_name . " 
                    SET 
                    seguradora_id={$this->seguradora_id},
                    data={$this->data},
                    numero={$this->numero},
                    valor={$this->valor},
                    remanescente={$this->remanescente},
                    user_id={$this->user_id}
                    WHERE id = {$this->id}";

        $stmt = $this->conn->prepare($sql);
        if ($stmt->execute()) {
            return true;
        }
        return false;
    }

    public function read($filter = null)
    {
        $sumPagamentos = $this->sumRemanescente("AND pfci.estado = '1'");
        $sql = "SELECT fac.id,fac.seguradora_id,
                        cli.nome as seguradora,
                        fac.data,fac.numero,fac.valor,
                        fac.remanescente,
                        fac.estado
                        FROM factura_global fac 
                INNER JOIN clientes cli ON cli.id=fac.seguradora_id
                WHERE 1=1
                {$filter}";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $nr_rows = $stmt->rowCount();
        if ($nr_rows > 0) {
            $response = array();
            $response['data'] = null;
            $response['total_results'] = $nr_rows;
            $fetchGuias = new FacturaCliente($this->conn);
            foreach ($stmt as $values) {
                extract($values);
                $emp = array(
                    "id" => $id,
                    "seguradora_id" => $seguradora_id,
                    "seguradora" => $seguradora,
                    "data" => $data,
                    "numero" => $numero,
                    "valor" => $valor,
                    "remanescente" => $remanescente,
                    "estado" => $estado,
                    "fetchGuias" => $fetchGuias->select("AND f.factura_global_id={$id}"),
                    "sumRemanescente" => $sumPagamentos
                );
                $response['data'][] = $emp;
            }
        } else {
            $response = [];
        }
        return $response;
    }

    public function delete()
    {
        $sql = "UPDATE factura_global SET estado='{$this->estado}' WHERE id={$this->id}";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            return true;
        }
        return false;
    }

    public function updateEstado()
    {
        $sql = "UPDATE " . $this->table_name . " SET estado='{$this->estado}' WHERE id={$this->id}";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            return true;
        }
        return false;
    }

    public function count_factura_cliente()
    {
        $sql = "SELECT COUNT(fc.id) as total FROM factura_global f
        INNER JOIN factura_cliente fc ON fc.factura_global_id=f.id
        WHERE f.id={$this->id}";

        $stmt = $this->conn->prepare($sql);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            extract($stmt->fetch(PDO::FETCH_ASSOC));
            $response = array(
                "total" => $total
            );
        } else {
            $response = [];
        }
        return $response;
    }

    public function read_one($filter = null)
    {
        $sql = "SELECT fac.id,fac.seguradora_id,
                    cli.nome as seguradora,
                    fac.data,fac.numero,fac.valor,
                    fac.remanescente FROM factura_global fac 
        INNER JOIN clientes cli ON cli.id=fac.seguradora_id
        WHERE fac.id={$this->id}";
        // print_r($sql);
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $fetchGuias = new FacturaCliente($this->conn);
            extract($stmt->fetch(PDO::FETCH_ASSOC));
            $response = array(
                "id" => $id,
                "seguradora_id" => $seguradora_id,
                "seguradora" => $seguradora,
                "data" => $data,
                "numero" => $numero,
                "valor" => $valor,
                "remanescente" => $remanescente,
                "fetchGuias" => $fetchGuias->select("AND f.factura_global_id={$id} {$filter}")
            );
        } else {
            $response = [];
        }
        return $response;
    }

    public function updateSaldoRemas($id)
    {
        $itemFacturas = new FacturaCliente($this->conn);
        $totalItem = $itemFacturas->getFacturaGlobal($id);
        $sql = "UPDATE " . $this->table_name . " SET valor={$totalItem}, remanescente={$totalItem} WHERE id={$id}";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            return true;
        }
        return false;
    }

    public function updateSaldo($id, $saldo)
    {
        $sql = "UPDATE " . $this->table_name . " SET saldo={$saldo} WHERE id={$id}";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            return true;
        }
        return false;
    }

    public function updateRemanescente($id, $remanescente = 0)
    {
        $sql = "UPDATE " . $this->table_name . " SET remanescente={$remanescente} WHERE id={$id}";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            return true;
        }
        return false;
    }

    public function sumRemanescente($filter = null)
    {
        $sql = "SELECT
                    fg.id,
                    fg.seguradora_id,
                    cli.nome as seguradora,
                    fg.data,
                    fg.numero,
                    fg.valor,
                    fg.remanescente,
                    fg.estado,
                    fg.valor-IFNULL(SUM(pfci.valor_pago), 0) as rem,
                    (SELECT SUM(f.valor_pago) from pagamento_factura_cliente_item f where f.estado = '1' AND f.id =  fc.id) as pago,
                    IFNULL((SELECT SUM(f.valor_pago) from pagamento_factura_nota_credito_item f where f.estado = '1' AND f.id =  fc.id), 0) as nota_credito,
                    fg.valor as valor,
                    fg.estado_pagamento
                FROM
                    factura_global fg
                    LEFT JOIN factura_cliente fc ON fg.id = fc.factura_global_id
                    LEFT JOIN pagamento_factura_cliente_item pfci ON fc.id = pfci.factura_cliente_id
                    INNER JOIN clientes cli ON cli.id = fg.seguradora_id
                WHERE
                    1 = 1 AND fg.estado IN('1','2') 
                     {$filter}
                GROUP BY fg.id
                ORDER BY fg.id desc";

        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $nr_rows = $stmt->rowCount();
        if ($nr_rows > 0) {
            $saldo = 0;
            $response = array();
            $response['data'] = null;
            $response['total_results'] = $nr_rows;
            $fetchGuias = new FacturaCliente($this->conn);

            foreach ($stmt as $value) {
                extract($value);
                if ((float)$pago > (float)$valor) {
                    $saldo = (float)$pago - (float)$valor;
                    $this->updateSaldo($id, $saldo);
                    $this->updateRemanescente($id, 0);
                } else {
                    $saldo = 0;
                    $this->updateSaldo($id, $saldo);
                    $remanescente = (float)$valor - (float)$pago;
                    $this->updateRemanescente($id, $remanescente);
                }
                $emp_item = array(
                    "id" => $id,
                    "seguradora_id" => $seguradora_id,
                    "seguradora" => $seguradora,
                    "data" => $data,
                    "numero" => $numero,
                    "valor" => $valor,
                    "remanescente" => $remanescente,
                    "estado" => $estado,
                    "estado_pagamento" => $estado_pagamento,
                    "fetchGuias" => $fetchGuias->select("AND f.factura_global_id={$id}"),
                    "rem" => abs($rem),
                    "pago" => (float)$pago,
                    "saldo" => (float)$saldo,
                    "nota_credito" => (float)$nota_credito,
                );
                $response['data'][] = $emp_item;
            }
        } else {
            $response = [];
        }
        return $response;
    }
    public function sumRemanescente2($filter = null, $start_from, $limit)
    {
        $sql = "SELECT
                    fg.id,
                    fg.seguradora_id,
                    cli.nome as seguradora,
                    fg.data,
                    fg.numero,
                    fg.valor,
                    fg.remanescente,
                    fg.estado,
                    fg.valor-IFNULL(SUM(pfci.valor_pago), 0) as rem,
                    (SELECT SUM(f.valor_pago) from pagamento_factura_cliente_item f where f.estado = '1' AND f.id =  fc.id) as pago,
                    IFNULL((SELECT SUM(f.valor_pago) from pagamento_factura_nota_credito_item f where f.estado = '1' AND f.id =  fc.id), 0) as nota_credito,
                    fg.valor as valor,
                    fg.estado_pagamento
                FROM
                    factura_global fg
                    LEFT JOIN factura_cliente fc ON fg.id = fc.factura_global_id
                    LEFT JOIN pagamento_factura_cliente_item pfci ON fc.id = pfci.factura_cliente_id
                    INNER JOIN clientes cli ON cli.id = fg.seguradora_id
                WHERE
                    1 = 1 AND fg.estado IN('1','2') 
                     {$filter}
                GROUP BY fg.id
                ORDER BY fg.id desc ";
        $sql .= " limit {$start_from}, {$limit}";
        // print_r($sql);
        // exit;
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $nr_rows = $stmt->rowCount();
        if ($nr_rows > 0) {
            $saldo = 0;
            $response = array();
            $response['data'] = null;
            $response['total_results'] = $nr_rows;
            $fetchGuias = new FacturaCliente($this->conn);

            foreach ($stmt as $value) {
                extract($value);
                if ((float)$pago > (float)$valor) {
                    $saldo = (float)$pago - (float)$valor;
                    $this->updateSaldo($id, $saldo);
                    $this->updateRemanescente($id, 0);
                } else {
                    $saldo = 0;
                    $this->updateSaldo($id, $saldo);
                    $remanescente = (float)$valor - (float)$pago;
                    $this->updateRemanescente($id, $remanescente);
                }
                $emp_item = array(
                    "id" => $id,
                    "seguradora_id" => $seguradora_id,
                    "seguradora" => $seguradora,
                    "data" => $data,
                    "numero" => $numero,
                    "valor" => $valor,
                    "remanescente" => $remanescente,
                    "estado" => $estado,
                    "estado_pagamento" => $estado_pagamento,
                    "fetchGuias" => $fetchGuias->select("AND f.factura_global_id={$id}"),
                    "rem" => abs($rem),
                    "pago" => (float)$pago,
                    "saldo" => (float)$saldo,
                    "nota_credito" => (float)$nota_credito,
                );
                $response['data'][] = $emp_item;
            }
        } else {
            $response = [];
        }
        return $response;
    }
}
