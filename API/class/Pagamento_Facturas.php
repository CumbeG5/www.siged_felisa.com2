<?php
class Pagamento_Facturas
{
    private $conn;
    private $table_name = "pagamento_facturas";

    public $id;
    public $pagamento_id;
    public $factura_id;
    public $valor;
    public $remanescente;
    public $status;
    public $saldo;
    public $desconto;
    public $total;

    public function __construct($db)
    {
        $this->conn = $db;
    }

    public function create()
    {
        $sql = "
            INSERT 
                INTO " . $this->table_name . " 
            SET 
                pagamento_id = $this->pagamento_id,
                factura_id = $this->factura_id,
                valor = $this->valor,
                remanescente = $this->remanescente,
                saldo = $this->saldo,
                desconto = $this->desconto,
                total = $this->total
                ";
        // print_r($sql);
        $stmt = $this->conn->prepare($sql);
        if ($stmt->execute()) {
            $this->UpdateStatusPagamentoFactura("AND pf.pagamento_id={$this->pagamento_id} AND pf.factura_id={$this->factura_id}");
            return true;
        }
        return false;
    }


    public function update()
    {
        $sql = "
            UPDATE 
                " . $this->table_name . " 
            SET 
                pagamento_id = $this->pagamento_id,
                factura_id = $this->factura_id,
                valor = $this->valor,
                remanescente = $this->remanescente,
                saldo = $this->saldo
                WHERE id= $this->id";

        $stmt = $this->conn->prepare($sql);
        if ($stmt->execute()) {
            return true;
        }
        return false;
    }
    public function read($filter = null)
    {

        $sql =
            "SELECT
                p_f.id,
                p_f.pagamento_id,
                p.nr_recibo AS pagamento,
                p_f.factura_id,
                f.nr_factura AS nr_factura,
                p_f.valor,
                p_f.remanescente,
                p_f.saldo,
                p_f.desconto,
                p_f.total
            FROM
                " . $this->table_name . " p_f
            LEFT JOIN pagamento p ON p_f.pagamento_id = p.id
            LEFT JOIN factura f ON p_f.factura_id = f.id
            WHERE 1=1 
            AND p_f.status = '1'
            {$filter}";

        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $row = $stmt->rowCount();
        if ($row > 0) {

            $facturas = new Factura($this->conn);

            $response = array();
            $response['data'] = null;
            $response['total_results'] = $row;

            foreach ($stmt as $values) {
                extract($values);

                $emp_items = array(
                    "id" => $id,
                    "pagamento_id" => $pagamento_id,
                    "pagamento" => $pagamento,
                    "factura_id" => $factura_id,
                    "nr_factura" => $nr_factura,
                    "valor" => $valor,
                    "remanescente" => $remanescente,
                    "desconto" => $desconto,
                    "total" => $total,
                    "saldoActual" => $saldo,
                    "pago_anterior" => $this->pago_anterior_factura($pagamento_id, $factura_id)['total'] ? $this->pago_anterior_factura($pagamento_id, $factura_id)['total'] : "0",
                    "facturas" => $facturas->read2("AND f.id = {$factura_id}")['data'],
                );
                $response['data'][] = $emp_items;
            }
        } else {
            $response = [];
        }
        return $response;
    }

    public function read_one($filter = null)
    {
        $sql = "SELECT * FROM " . $this->table_name . " WHERE id = $this->id";

        $stmt = $this->conn->prepare($sql);

        // $this->id = htmlspecialchars(strip_tags($this->id));

        // $stmt->bindParam(":id", $this->id);

        $stmt->execute();
        if ($stmt->rowCount() > 0) {

            $facturas = new Factura($this->conn);
            $pagamento = new Pagamento($this->conn);

            extract($stmt->fetch(PDO::FETCH_ASSOC));
            $pagamento->id = $pagamento_id;
            $response = array(
                "id" => $id,
                "pagamento_id" => $pagamento_id,
                "factura_id" => $factura_id,
                "facturas" => $facturas->read2("AND f.id = {$factura_id}")['data'],
                "pagamento" => $pagamento->read_one(),
                "valor" => $valor,
                "remanescente" => $remanescente
            );
        } else {
            $response = [];
        }
        return $response;
    }

    public function search_one($pagamento_id, $factura_id)
    {
        $sql = "SELECT * FROM " . $this->table_name . " WHERE pagamento_id = {$pagamento_id} AND factura_id = {$factura_id} AND status = 1";
        // print($sql);
        $stmt = $this->conn->prepare($sql);
        // $this->id = htmlspecialchars(strip_tags($this->id));

        // $stmt->bindParam(":id", $this->id);

        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $resposta = true;
        } else {
            $resposta = false;
        }
        return $resposta;
    }

    public function pago_anterior_factura($pagamento_id, $factura_id)
    {
        $sql = "SELECT SUM(total) as total FROM " . $this->table_name . " WHERE pagamento_id < {$pagamento_id} AND factura_id = {$factura_id} AND status = 1";
        // print($sql);
        $stmt = $this->conn->prepare($sql);

        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            extract($stmt->fetch(PDO::FETCH_ASSOC));
            $response = ["total" => $total];
        } else {
            $response = ['total' => 0];
        }
        return $response;
    }

    public function delete()
    {
        $factura = new Factura($this->conn);
        $sql = "
            UPDATE  
                " . $this->table_name . "
                SET 
                status = $this->status
                WHERE id = $this->id";

        $stmt = $this->conn->prepare($sql);
        if ($stmt->execute()) {
            $resultado = $this->read_one();
            $somatorio = $this->SUMPagamentos($resultado["pagamento_id"]);
            // print_r("Som: ".$somatorio);
            // print_r("Val: ".$resultado["valor"]);
            if ($somatorio ? (double)$somatorio : 0 == 0) {
                $factura->updateEstado2("1", $resultado["factura_id"]);
            } elseif ((double)$somatorio > 0 &&  $somatorio < $resultado["valor"]) {
                $factura->updateEstado2("3", $resultado["factura_id"]);
            } elseif ((double)$somatorio >= $resultado["valor"]) {
                $factura->updateEstado2("4", $resultado["factura_id"]);
            }
            // $this->UpdateStatusPagamentoFactura("AND pf.pagamento_id={$resultado["pagamento_id"]} AND pf.factura_id={$resultado["factura_id"]}");
            // print_r($resultado["factura_id"]);
            // print_r("SEP");
            // print_r($resultado["pagamento_id"]);
            return true;
        } else {
            return false;
        }
    }

    public function SUMPagamentos($pagamento_id)
    {
        $sql = "SELECT SUM(total) as sum FROM `pagamento_facturas`
        INNER JOIN factura ON factura.id=pagamento_facturas.factura_id
        WHERE pagamento_id = {$pagamento_id}";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            extract($stmt->fetch(PDO::FETCH_ASSOC));
            $response = ["total_pago_factura" => $sum];
        } else {
            return $response = [];
        }
        return $response;
    }

    public function UpdateStatusPagamentoFactura($filter = null)
    {
        $factura = new Factura($this->conn);

        $sql = "SELECT f.fornecedor_id, f.id as factura_id, f.valor as valor_factura, 
        SUM(IFNULL(pf.total,0)) as valor_pago 
        FROM `factura` f 
        LEFT JOIN pagamento_facturas pf ON pf.factura_id=f.id 
        WHERE 1=1 AND pf.status='1' {$filter}
        GROUP BY f.id";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $row = $stmt->rowCount();
        if ($row > 0) {
            $response = array();
            $response['data'] = null;
            $response['total_resulys'] = $row;
            foreach ($stmt as $data) {
                extract($data);
                // actualiza o saldo
                $factura->updateSaldo2((float)$valor_factura - (float)$valor_pago, $factura_id);

                if ((float)$valor_factura == (float)$valor_pago) {
                    // print_r("aqui");
                    $factura->updateEstado2("4", $factura_id);
                } elseif ((float)$valor_pago != 0 && ((float)$valor_factura > (float)$valor_pago)) {
                    // print_r("aqui2");
                    $factura->updateEstado2("3", $factura_id);
                } else {
                    // print_r("aqui3");
                    $factura->updateEstado2("2", $factura_id);
                }
            }
        }
    }
}
