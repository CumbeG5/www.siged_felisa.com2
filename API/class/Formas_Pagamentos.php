<?php
class Formas_Pagamentos
{
    private $conn;
    private $table_name = "formas_pagamento";

    public $id;
    public $proveniencia;
    public $pagamento_id;
    public $tipo_pagamento_id;
    public $valor;
    public $referencia;
    public $data;
    public $data_pagamento;
    public $idformapagamento;
    public $numerocheque;
    public $numero_documento;
    public $caixa;
    public $status;

    public function __construct($db)
    {
        $this->conn = $db;
    }

    public function create()
    {
        $sql = "
            INSERT 
                INTO 
                    " . $this->table_name . " 
                    SET
                proveniencia = '$this->proveniencia',
                pagamento_id = $this->pagamento_id,
                tipo_pagamento_id = $this->tipo_pagamento_id,
                valor = $this->valor,
                referencia = $this->referencia,
                data = '$this->data',
                data_pagamento = $this->data_pagamento,
                idformapagamento = '$this->idformapagamento',
                numerocheque = '$this->numerocheque',
                numero_documento = '$this->numero_documento',
                caixa = $this->caixa";
        $stmt = $this->conn->prepare($sql);
        if ($stmt->execute()) {
            return true;
        } else {
            return false;
        }
    }

    public function update()
    {
        $sql = "
            UPDATE  
                " . $this->table_name . "
                SET 
                pagamento_id = $this->pagamento_id,
                tipo_pagamento_id = $this->tipo_pagamento_id,
                valor = $this->valor,
                referencia = $this->referencia,
                data = $this->data,
                data = $this->data_pagamento
                WHERE id = $this->id";

        $stmt = $this->conn->prepare($sql);
        if ($stmt->execute()) {
            return true;
        } else {
            return false;
        }
    }
    public function read($filter = null)
    {
        $sql =
            "SELECT
            f.id,
            f.proveniencia,
            f.pagamento_id,
            f.numerocheque,
            f.numero_documento,
            f.caixa,
            forma.titulo,
            p.nr_recibo,
            f.tipo_pagamento_id,
            b.nome AS banco,
            f.valor,
            f.referencia,
            f.data,
            f.data_pagamento,
            forma.titulo
        FROM
            " . $this->table_name . " f
            INNER JOIN pagamento p ON f.pagamento_id = p.id
            INNER JOIN banco b ON f.tipo_pagamento_id = b.id
            INNER JOIN formadepagamento forma ON f.idformapagamento=forma.idformapagamento
        WHERE
            1 = 1
        AND 
            status = '1'
            {$filter}";
        // print_r($sql);
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $rows = $stmt->rowCount();
        if ($rows > 0) {
            $response = array();
            $response['data'] = null;
            $response['total_results'] = $rows;
            foreach ($stmt as $values) {
                extract($values);
                $emp_items = array(
                    "id" => $id,
                    "proveniencia" => $proveniencia,
                    "pagamento_id" => $pagamento_id,
                    "numerocheque" => $numerocheque,
                    "numero_documento" => $numero_documento,
                    "caixa" => $caixa,
                    "nr_recibo" => $nr_recibo,
                    "tipo_pagamento_id" => $tipo_pagamento_id,
                    "nome" => $banco,
                    "valor" => $valor,
                    "referencia" => $referencia,
                    "data" => $data,
                    "data_pagamento" => $data_pagamento,
                    "titulo" => $titulo
                );

                $response['data'][] = $emp_items;
            }
        } else {
            $response = [];
        }
        return $response;
    }

    public function read2($filter = null)
    {
        $sql =
            "SELECT
             f.id,
            f.proveniencia,
            f.pagamento_id,
            f.numerocheque,
            f.numero_documento,
            f.caixa,
            forma.titulo,
            p.nr_pagamento,
            f.tipo_pagamento_id,
            b.nome AS banco,
            f.valor,
            f.referencia,
            f.data,
            f.data_pagamento,
            forma.titulo
        FROM
            " . $this->table_name . " f
           INNER JOIN pagamento_factura_cliente p ON f.pagamento_id = p.id
           INNER JOIN banco b ON f.tipo_pagamento_id = b.id
           INNER JOIN formadepagamento forma ON f.idformapagamento=forma.idformapagamento
        WHERE
            1 = 1
        AND 
            status = '1'
            {$filter}";
        // print_r($sql);
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $rows = $stmt->rowCount();
        if ($rows > 0) {
            $response = array();
            $response['data'] = null;
            $response['total_results'] = $rows;
            foreach ($stmt as $values) {
                extract($values);
                $emp_items = array(
                    "id" => $id,
                    "proveniencia" => $proveniencia,
                    "pagamento_id" => $pagamento_id,
                    "numerocheque" => $numerocheque,
                    "numero_documento" => $numero_documento,
                    "caixa" => $caixa,
                    "tipo_pagamento_id" => $tipo_pagamento_id,
                    "nome" => $banco,
                    "valor" => $valor,
                    "referencia" => $referencia,
                    "data" => $data,
                    "data_pagamento" => $data_pagamento,
                    "titulo" => $titulo
                );

                $response['data'][] = $emp_items;
            }
        } else {
            $response = [];
        }
        return $response;
    }
    public function read_one()
    {
        $sql = "SELECT * FROM " . $this->table_name . " WHERE 1=1";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            extract($values->fetch(PDO::FETCH_ASSOC));
            $response = array(
                "id" => $id,
                "proveniencia" => $proveniencia,
                "pagamento_id" => $pagamento_id,
                "tipo_pagamento_id" => $tipo_pagamento_id,
                "valor" => $valor,
                "referencia" => $referencia,
                "data" => $data,
                "data_pagamento" => $data_pagamento
            );
        } else {
            $response = [];
        }
        return $response;
    }


    public function delete()
    {
        $sql = "
            UPDATE  
                " . $this->table_name . "
                SET 
                status = $this->status
                WHERE id = $this->id";

        $stmt = $this->conn->prepare($sql);
        if ($stmt->execute()) {
            return true;
        } else {
            return false;
        }
    }

    public function DadosFactura($pagamento_id, $proveniencia)
    {
        $sqlQuery =
            "SELECT DISTINCT(fpagamento.descricao), form.referencia, b.nome as banco, form.valor as valor,form.data_pagamento FROM formas_pagamento form 
            INNER JOIN formadepagamento fpagamento ON fpagamento.idformapagamento=form.idformapagamento
            INNER JOIN banco b ON form.tipo_pagamento_id=b.id
            WHERE form.pagamento_id = {$pagamento_id} AND form.proveniencia='{$proveniencia}' AND form.status='1'";

        $stmt = $this->conn->prepare($sqlQuery);

        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            // extract($stmt->fetchall(PDO::FETCH_ASSOC));
            // $response = array("descricao" => $descricao, "referencia"=>$referencia, "banco"=>$banco);
            foreach ($stmt as $value) {
                extract($value);
                $emp_item = array(
                    "descricao" => $descricao,
                    "referencia" => $referencia,
                    "banco" => $banco,
                    "valor" => $valor,
                    "data_pagamento" => $data_pagamento
                );
                $response['data'][] = $emp_item;
            }
        } else {
            $response = [];
        }
        return $response;
    }
}
