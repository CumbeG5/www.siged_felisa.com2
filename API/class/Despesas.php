<?php
class Despesas
{
    // Connection
    private $conn;
    private $table_name = "execucaodespesas";

    public $id;
    public $descricaodespesa;
    public $idrequisicaodefundos;
    public $data;
    public $mes;
    public $ano;
    public $tipo_factura_id;
    public $saldo;
    public $factura_id;
    public $estado;
    // public $mes;
    public $anexo_factura;
    public $user_id;

    public function __construct($db)
    {
        $this->conn = $db;
    }

    // public function create()
    // {
    //     $sql = "INSERT 
    //         INTO " . $this->table_name . " 
    //         SET
    //         fornecedor_id = $this->fornecedor_id,
    //         nr_factura = $this->nr_factura,
    //         valor = $this->valor,
    //         data_factura = $this->data_factura,
    //         centro_custo_id = $this->centro_custo_id,
    //         tipo_factura_id = $this->tipo_factura_id,
    //         saldo = $this->saldo,
    //         estado = $this->estado,
    //         mes = $this->mes,
    //         anexo_factura = $this->anexo_factura,
    //         user_id = $this->user_id";

    //     $stmt = $this->conn->prepare($sql);

    //     $this->fornecedor_id = htmlspecialchars(strip_tags($this->fornecedor_id));
    //     $this->nr_factura = htmlspecialchars(strip_tags($this->nr_factura));
    //     $this->valor = htmlspecialchars(strip_tags($this->valor));
    //     $this->data_factura = htmlspecialchars(strip_tags($this->data_factura));
    //     $this->centro_custo_id = htmlspecialchars(strip_tags($this->centro_custo_id));
    //     $this->tipo_factura_id = htmlspecialchars(strip_tags($this->tipo_factura_id));
    //     $this->saldo = htmlspecialchars(strip_tags($this->saldo));
    //     $this->estado = htmlspecialchars(strip_tags($this->estado));
    //     $this->mes = htmlspecialchars(strip_tags($this->mes));

    //     if ($stmt->execute()) {
    //         $pagamento_facturas = new Pagamento_Facturas($this->conn);
    //         $pagamento_facturas->UpdateStatusPagamentoFactura(" AND f.nr_factura = {$this->nr_factura}");
    //         return true;
    //     }
    //     return false;
    // }

    // public function update()
    // {
    //     $sql = "UPDATE 
    //         " . $this->table_name . "
    //             SET 
    //         fornecedor_id = $this->fornecedor_id,
    //         nr_factura = $this->nr_factura,
    //         valor = $this->valor,
    //         data_factura = $this->data_factura,
    //         centro_custo_id = $this->centro_custo_id,
    //         tipo_factura_id = $this->tipo_factura_id,
    //         estado = $this->estado,
    //         mes = $this->mes
    //         WHERE 
    //         id = $this->id";
    //     // saldo = $this->saldo,
    //     $stmt = $this->conn->prepare($sql);

    //     $this->fornecedor_id = htmlspecialchars(strip_tags($this->fornecedor_id));
    //     $this->nr_factura = htmlspecialchars(strip_tags($this->nr_factura));
    //     $this->valor = htmlspecialchars(strip_tags($this->valor));
    //     $this->data_factura = htmlspecialchars(strip_tags($this->data_factura));
    //     $this->centro_custo_id = htmlspecialchars(strip_tags($this->centro_custo_id));
    //     $this->tipo_factura_id = htmlspecialchars(strip_tags($this->tipo_factura_id));
    //     $this->saldo = htmlspecialchars(strip_tags($this->saldo));
    //     $this->estado = htmlspecialchars(strip_tags($this->estado));
    //     $this->mes = htmlspecialchars(strip_tags($this->mes));
    //     $this->id = htmlspecialchars(strip_tags($this->id));

    //     if ($stmt->execute()) {
    //         $pagamento_facturas = new Pagamento_Facturas($this->conn);
    //         $pagamento_facturas->UpdateStatusPagamentoFactura(" AND f.id = {$this->id}");
    //         return true;
    //     }
    //     return false;
    // }

    // public function updateSaldo()
    // {
    //     $sql = "UPDATE 
    //         " . $this->table_name . "
    //             SET
    //         saldo = $this->saldo
    //         WHERE 
    //         id = $this->id";

    //     $stmt = $this->conn->prepare($sql);

    //     $this->saldo = htmlspecialchars(strip_tags($this->saldo));
    //     $this->id = htmlspecialchars(strip_tags($this->id));

    //     if ($stmt->execute()) {
    //         return true;
    //     }
    //     return false;
    // }

    // public function updateSaldo2($saldo, $factura_id)
    // {
    //     $sql = "UPDATE 
    //         " . $this->table_name . "
    //             SET
    //         saldo = {$saldo}
    //         WHERE 
    //         id = {$factura_id}";

    //     $stmt = $this->conn->prepare($sql);

    //     if ($stmt->execute()) {
    //         return true;
    //     }
    //     return false;
    // }

    // public function updateSaldo3()
    // {
    //     $sql = "UPDATE 
    //         " . $this->table_name . "
    //             SET
    //         saldo = (saldo - $this->saldo)
    //         WHERE 
    //         id = $this->id";

    //     $stmt = $this->conn->prepare($sql);

    //     $this->saldo = htmlspecialchars(strip_tags($this->saldo));
    //     $this->id = htmlspecialchars(strip_tags($this->id));

    //     if ($stmt->execute()) {
    //         return true;
    //     }
    //     return false;
    // }


    public function read($filter = null, $start_from, $limit)
    {
        // $pagamento_facturas = new Pagamento_Facturas($this->conn);
        // $pagamento_facturas->UpdateStatusPagamentoFactura($filter);
        $sql =
            "SELECT e.idexecucaodespesas,c.titulo centro_custo,e.idcentrodecusto,e.grupo,
                    g.nome grupo, sg.nome sub_grupo,e.subgrupo, e.descricaodespesa,emp.descricaoempresa,
                    e.idempresa, e.data,e.valortotal 
            FROM
                " . $this->table_name . " e
                INNER JOIN centro_de_custo c ON e.idcentrodecusto = c.idcentrodecusto
                INNER JOIN grupo g ON e.grupo = g.id
                INNER JOIN subgrupo sg ON sg.id = e.subgrupo
                INNER JOIN empresas emp ON emp.idempresa = e.idempresa
                {$filter} 
                ORDER BY e.idexecucaodespesas DESC";
        $sql .= " limit {$start_from}, {$limit}";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();

        $row = $stmt->rowCount();
        if ($row > 0) {
            // $meses = array(1 => "Janeiro", 2 => "Fevereiro", 3 => "Março", 4 => "Abril", 5 => "Maio", 6 => "Junho", 7 => "Julho", 8 => "Agosto", 9 => "Setembro", 10 => "Outubro", 11 => "Novembro", 12 => "Dezembro");
            $response = array();
            $response['data'] = null;
            $response['total_resulys'] = $row;
            foreach ($stmt as $data) {
                extract($data);
                // $pagamento_facturas = new Pagamento_Facturas($this->conn);
                // $pagamento_facturas->UpdateStatusPagamentoFactura(" AND f.id = {$id}");
                $emp_data = array(
                    "id" => $idexecucaodespesas,
                    "centro_custo" => $centro_custo,
                    "grupo" => $grupo,
                    "sub_grupo" => $sub_grupo,
                    "beneficiario" => $descricaoempresa,
                    "descr_despesa" => $descricaodespesa,
                    "data_exec" => $data,
                    "val_total" => $valortotal,
                );
                $response['data'][] = $emp_data;
            }
        } else {
            $response = [];
        }
        return $response;
    }

    // public function read2($filter = null)
    // {
    //     $sql =
    //         "SELECT
    //             f.id,
    //             f.fornecedor_id,
    //             forn.nome AS Fornecedor,
    //             f.nr_factura,
    //             f.valor,
    //             f.data_factura,
    //             f.centro_custo_id,
    //             c.titulo AS Farmacia,
    //             f.tipo_factura_id,
    //             tipo.descricao AS Tipo_Factura,
    //             f.saldo,
    //             f.estado,
    //             f.ativo,
    //             f.mes
    //         FROM
    //             " . $this->table_name . " f
    //             INNER JOIN fornecedors forn ON forn.id = f.fornecedor_id
    //             INNER JOIN centro_de_custo c ON c.idcentrodecusto = f.centro_custo_id
    //             INNER JOIN tipo_factura tipo ON tipo.id = f.tipo_factura_id
    //         WHERE	
    //             1=1
    //         {$filter} ORDER BY f.id DESC";
    //     // print_r($sql);
    //     $stmt = $this->conn->prepare($sql);
    //     $stmt->execute();

    //     $row = $stmt->rowCount();
    //     if ($row > 0) {
    //         $meses = array(1 => "Janeiro", 2 => "Fevereiro", 3 => "Março", 4 => "Abril", 5 => "Maio", 6 => "Junho", 7 => "Julho", 8 => "Agosto", 9 => "Setembro", 10 => "Outubro", 11 => "Novembro", 12 => "Dezembro");
    //         $response = array();
    //         $response['data'] = null;
    //         $response['total_resulys'] = $row;
    //         foreach ($stmt as $data) {
    //             extract($data);
    //             $pagamento_facturas = new Pagamento_Facturas($this->conn);
    //             $pagamento_facturas->UpdateStatusPagamentoFactura(" AND f.id = {$id}");
    //             $emp_data = array(
    //                 "id" => $id,
    //                 "fornecedor_id" => $fornecedor_id,
    //                 "fornecedor" => $Fornecedor,
    //                 "nr_factura" => $nr_factura,
    //                 "mes" => $mes,
    //                 "mes_correspondente" => @$meses[$mes],
    //                 "valor" => $valor,
    //                 "data_factura" => $data_factura,
    //                 "centro_custo_id" => $centro_custo_id,
    //                 "farmacia" => $Farmacia,
    //                 "tipo_factura_id" => $tipo_factura_id,
    //                 "tipo_factura" => $Tipo_Factura,
    //                 "saldo" => $saldo,
    //                 "estado" => $estado,
    //                 "ativo" => $ativo
    //             );
    //             $response['data'][] = $emp_data;
    //         }
    //     } else {
    //         $response = [];
    //     }
    //     return $response;
    // }

    // public function readByFornecedor($filter = null)
    // {
    //     $sql =
    //         "SELECT
    //             f.id,
    //             f.fornecedor_id,
    //             forn.nome AS Fornecedor,
    //             f.nr_factura,
    //             f.valor as valor,
    //             f.data_factura,
    //             f.centro_custo_id,
    //             c.titulo AS Farmacia,
    //             f.tipo_factura_id,
    //             tipo.descricao AS Tipo_Factura,
    //             f.saldo,
    //             f.estado,
    //             f.mes
    //         FROM
    //             " . $this->table_name . " f
    //             INNER JOIN fornecedors forn ON forn.id = f.fornecedor_id
    //             INNER JOIN centro_de_custo c ON c.idcentrodecusto = f.centro_custo_id
    //             INNER JOIN tipo_factura tipo ON tipo.id = f.tipo_factura_id
    //         WHERE	
    //             1=1 AND estado IN ('1','2','3')
    //         {$filter} 
    //         ORDER BY f.id DESC";
    //     $stmt = $this->conn->prepare($sql);
    //     $stmt->execute();

    //     $row = $stmt->rowCount();
    //     if ($row > 0) {
    //         $meses = array(1 => "Janeiro", 2 => "Fevereiro", 3 => "Março", 4 => "Abril", 5 => "Maio", 6 => "Junho", 7 => "Julho", 8 => "Agosto", 9 => "Setembro", 10 => "Outubro", 11 => "Novembro", 12 => "Dezembro");
    //         $response = array();
    //         $response['data'] = null;
    //         $response['total_resulys'] = $row;
    //         foreach ($stmt as $data) {
    //             extract($data);
    //             $pagamento_facturas = new Pagamento_Facturas($this->conn);
    //             $pagamento_facturas->UpdateStatusPagamentoFactura(" AND f.id = {$id}");
    //             $emp_data = array(
    //                 "id" => $id,
    //                 "fornecedor_id" => $fornecedor_id,
    //                 "fornecedor" => $Fornecedor,
    //                 "nr_factura" => $nr_factura,
    //                 "mes" => $mes,
    //                 "mes_correspondente" => @$meses[$mes],
    //                 "valor" => $valor,
    //                 "data_factura" => $data_factura,
    //                 "centro_custo_id" => $centro_custo_id,
    //                 "farmacia" => $Farmacia,
    //                 "tipo_factura_id" => $tipo_factura_id,
    //                 "tipo_factura" => $Tipo_Factura,
    //                 "saldo" => $saldo,
    //                 "estado" => $estado
    //             );
    //             $response['data'][] = $emp_data;
    //         }
    //     } else {
    //         $response = [];
    //     }
    //     return $response;
    // }

    // public function readExtract($filter = null)
    // {
    //     $sql =
    //         "SELECT
    //             f.id,
    //             f.fornecedor_id,
    //             e.descricaoempresa AS Fornecedor,
    //             f.nr_factura,
    //             f.valor,
    //             f.data_factura,
    //             f.centro_custo_id,
    //             c.titulo AS Farmacia,
    //             f.tipo_factura_id,
    //             tipo.descricao AS Tipo_Factura,
    //             f.saldo
    //         FROM
    //             " . $this->table_name . " f
    //             INNER JOIN empresas e ON e.idempresa = f.fornecedor_id
    //             INNER JOIN centro_de_custo c ON c.idcentrodecusto = f.centro_custo_id
    //             INNER JOIN tipo_factura tipo ON tipo.id = f.tipo_factura_id
    //         WHERE	
    //             1=1
    //         {$filter}";

    //     $stmt = $this->conn->prepare($sql);
    //     $stmt->execute();

    //     $row = $stmt->rowCount();
    //     if ($row > 0) {
    //         $response = array();
    //         $response['data'] = null;
    //         $response['total_resulys'] = $row;



    //         foreach ($stmt as $data) {
    //             extract($data);
    //             $pagamento_facturas = new Pagamento_Facturas($this->conn);
    //             $pagamento_facturas->UpdateStatusPagamentoFactura(" AND f.id = {$id}");
    //             $emp_data = array(
    //                 "id" => $id,
    //                 "fornecedor_id" => $fornecedor_id,
    //                 "fornecedor" => $Fornecedor . " - " . $nr_factura,
    //                 "nr_factura" => $nr_factura,
    //                 "valor" => $valor,
    //                 "data_factura" => $data_factura,
    //                 "centro_custo_id" => $centro_custo_id,
    //                 "farmacia" => $Farmacia,
    //                 "tipo_factura_id" => $tipo_factura_id,
    //                 "tipo_factura" => $Tipo_Factura,
    //                 // "valorPago" => $ValorPago->read("AND p_f.factura_id = {$id}"),
    //                 "saldo" => $saldo,
    //                 "sumV_Pago" => $SomaValorPago->readSUM($id)['sum'],
    //                 "saldoActual" => $SomaValorPago->readSUM($id)['saldoActual']
    //             );
    //             $response['data'][] = $emp_data;
    //         }
    //     } else {
    //         $response = [];
    //     }
    //     return $response;
    // }

    // public function readSUM($factura_id)
    // {
    //     $pagamento_facturas = new Pagamento_Facturas($this->conn);
    //     $pagamento_facturas->UpdateStatusPagamentoFactura(" AND f.id = {$factura_id}");

    //     $sql = "SELECT SUM(remanescente) as sum, SUM(saldo) saldo FROM pagamento_facturas WHERE factura_id = {$factura_id}";

    //     $stmt = $this->conn->prepare($sql);

    //     $this->factura_id = htmlspecialchars(strip_tags($this->factura_id));

    //     $stmt->bindParam(":factura_id", $this->factura_id);

    //     $stmt->execute();

    //     if ($stmt->rowCount() > 0) {
    //         extract($stmt->fetch(PDO::FETCH_ASSOC));
    //         $response = [
    //             "sum" => $sum,
    //             "saldoActual" => $saldo,
    //         ];
    //     } else {
    //         $response = [];
    //     }
    //     return $response;
    // }
    public function read_one()
    {
        $sql =
            "SELECT
                *
            FROM
                " . $this->table_name . " d
            WHERE	
                f.id = $this->id ";

        $stmt = $this->conn->prepare($sql);

        $this->id = htmlspecialchars(strip_tags($this->id));

        // $stmt->bindParam(":id", $this->id);

        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $total_pagos = new Pagamento_Facturas($this->conn);
            extract($stmt->fetch(PDO::FETCH_ASSOC));
            $calcula = (float)$valor;
            $pagamento_facturas = new Pagamento_Facturas($this->conn);
            $pagamento_facturas->UpdateStatusPagamentoFactura(" AND f.id = {$id}");
            $response = array(
                "id" => $id,
                "fornecedor_id" => $fornecedor_id,
                "fornecedor" => $Fornecedor,
                "nr_factura" => $nr_factura,
                "valor" => number_format($valor, 2, '.', ''),
                "data_factura" => $data_factura,
                "centro_custo_id" => $centro_custo_id,
                "farmacia" => $Farmacia,
                "tipo_factura_id" => $tipo_factura_id,
                "tipo_factura" => $Tipo_Factura,
                "saldo" => (float)$saldo,
                "mes" => $mes,
                "total_pagamento" => $calcula,
                "estado" => $estado,
                "anexo_factura" => $anexo_factura,
                "created_at" => $created_at,
                "total_pagos" => $valor_pago //(float)$total_pagos->SUMPagamentos($id)['total_pago_factura']

            );
        } else {
            $response = [];
        }
        return $response;
    }
    // public function validateFactura($filter = null)
    // {
    //     $sql = "SELECT * FROM " . $this->table_name . " WHERE nr_factura={$this->nr_factura} AND fornecedor_id={$this->fornecedor_id} AND centro_custo_id={$this->centro_custo_id} AND tipo_factura_id={$this->tipo_factura_id} AND estado != '0'";
    //     $stmt = $this->conn->prepare($sql);
    //     $stmt->execute();
    //     if ($stmt->rowCount() > 0) {
    //         return true;
    //     } else {
    //         return false;
    //     }
    // }

    // public function updateEstado()
    // {
    //     $ativo = new Factura($this->conn);
    //     $estado = $ativo->updateAtivo($this->id);
    //     // print_r($estado);
    //     $sql = "UPDATE " . $this->table_name . " SET estado='{$this->estado}', ativo = {$estado} WHERE id={$this->id}";
    //     $stmt = $this->conn->prepare($sql);
    //     $stmt->execute();
    //     if ($stmt->rowCount() > 0) {
    //         return True;
    //     }
    //     return False;
    // }

    // public function updateEstado2($estado, $factura_id)
    // {
    //     $sql = "UPDATE " . $this->table_name . " SET estado='{$estado}'  WHERE id={$factura_id}";
    //     $stmt = $this->conn->prepare($sql);
    //     $stmt->execute();
    //     if ($stmt->rowCount() > 0) {
    //         return true;
    //     }
    //     return false;
    // }

    // public function VerifyDiscount($id)
    // {
    //     $sql = "SELECT * FROM " . $this->table_name . " WHERE id={$id} AND desconto > 0";
    //     $stmt = $this->conn->prepare($sql);
    //     $stmt->execute();
    //     if ($stmt->rowCount() > 0) {
    //         extract($stmt->fetch(PDO::FETCH_ASSOC));
    //         $response = ["desconto" => $desconto];
    //     } else {
    //         return $response = [];
    //     }
    //     return $response;
    // }

    // public function updateAtivo($id)
    // {
    //     $read_all = new Factura($this->conn);
    //     $dados = $read_all->read2(" AND f.id ={$id} ");
    //     // print_r($dados["data"][0]["fornecedor_id"]);


    //     $dados2 = $read_all->read2(" AND f.fornecedor_id ={$dados["data"][0]["fornecedor_id"]} 
    //     AND f.nr_factura ='{$dados["data"][0]["nr_factura"]}' 
    //     AND f.centro_custo_id ={$dados["data"][0]["centro_custo_id"]} 
    //     AND f.tipo_factura_id ={$dados["data"][0]["tipo_factura_id"]}");

    //     // print_r($dados2);
    //     $object = array_reduce($dados2["data"], function ($a, $b) {
    //         return $a['ativo'] < $b['ativo'] ? $a : $b;
    //     }, array_shift($dados2["data"]));

    //     return ($object["ativo"] - 1);
    // }
}
