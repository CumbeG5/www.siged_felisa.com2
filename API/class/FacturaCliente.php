<?php
class FacturaCliente
{
    // Connection
    private $conn;
    private $table_name = "factura_cliente";

    public $id;
    public $factura_global_id;
    public $nr_factura;
    public $valor;
    public $data;
    public $tipo_factura_id;
    public $saldo;
    public $estado;
    public $codigo_autorizacao;
    public $nr_membro;
    public $valor_remanescente;
    public $nome_segurado;
    public $tecnico_id;
    public $factura_id;
    public $user_id;
    public $anexo_factura;

    public function __construct($db)
    {
        $this->conn = $db;
    }

    public function create()
    {
        $sql = "INSERT 
            INTO " . $this->table_name . " 
            SET
            factura_global_id = $this->factura_global_id,
            nr_factura = $this->nr_factura,
            valor = $this->valor,
            data = $this->data,
            tipo_factura_id = $this->tipo_factura_id,
            estado = $this->estado,
            codigo_autorizacao = $this->codigo_autorizacao,
            nr_membro = $this->nr_membro,
            valor_remanescente = $this->valor_remanescente,
            nome_segurado = $this->nome_segurado,
            tecnico_id = $this->tecnico_id,
            anexo = $this->anexo_factura,
            user_id = $this->user_id
            ";

        $stmt = $this->conn->prepare($sql);

        $this->factura_global_id = htmlspecialchars(strip_tags($this->factura_global_id));
        $this->nr_factura = htmlspecialchars(strip_tags($this->nr_factura));
        $this->valor = htmlspecialchars(strip_tags($this->valor));
        $this->data = htmlspecialchars(strip_tags($this->data));
        $this->tipo_factura_id = htmlspecialchars(strip_tags($this->tipo_factura_id));
        $this->saldo = htmlspecialchars(strip_tags($this->saldo));
        $this->estado = htmlspecialchars(strip_tags($this->estado));
        $this->codigo_autorizacao = htmlspecialchars(strip_tags($this->codigo_autorizacao));

        if ($stmt->execute()) {
            return true;
        }
        return false;
    }
    public function update()
    {
        $sql = "UPDATE 
            " . $this->table_name . "
                SET 
                factura_global_id = $this->factura_global_id,
                nr_factura = $this->nr_factura,
                valor = $this->valor,
                data = $this->data,
                tipo_factura_id = $this->tipo_factura_id,
                estado = $this->estado,
                codigo_autorizacao = $this->codigo_autorizacao,
                nr_membro = $this->nr_membro,
                valor_remanescente = $this->valor_remanescente,
                nome_segurado = $this->nome_segurado,
                tecnico_id = $this->tecnico_id,
                user_id = $this->user_id
            WHERE 
            id = $this->id";

        $stmt = $this->conn->prepare($sql);

        $this->factura_global_id = htmlspecialchars(strip_tags($this->factura_global_id));
        $this->nr_factura = htmlspecialchars(strip_tags($this->nr_factura));
        $this->valor = htmlspecialchars(strip_tags($this->valor));
        $this->data = htmlspecialchars(strip_tags($this->data));
        $this->tipo_factura_id = htmlspecialchars(strip_tags($this->tipo_factura_id));
        $this->saldo = htmlspecialchars(strip_tags($this->saldo));
        $this->estado = htmlspecialchars(strip_tags($this->estado));
        $this->codigo_autorizacao = htmlspecialchars(strip_tags($this->codigo_autorizacao));
        $this->id = htmlspecialchars(strip_tags($this->id));

        if ($stmt->execute()) {
            return true;
        }
        return false;
    }
    public function updateSaldo()
    {
        $sql = "UPDATE 
            " . $this->table_name . "
                SET
            valor_remanescente = $this->valor_remanescente
            WHERE 
            id = $this->id";

        $stmt = $this->conn->prepare($sql);

        $this->valor_remanescente = htmlspecialchars(strip_tags($this->valor_remanescente));
        $this->id = htmlspecialchars(strip_tags($this->id));

        if ($stmt->execute()) {
            return true;
        }
        return false;
    }
    public function select($filter = null)
    {
        $dadosFacturaItens = new Pagamento_Factura_Cliente_Item($this->conn);

        $sql =
            "SELECT
                f.id,
                f.factura_global_id,
                f.nr_factura,
                f.valor,
                f.data,
                f.tipo_factura_id,
                tipo.descricao AS Tipo_Factura,
                f.valor_remanescente,
                f.codigo_autorizacao,
                f.nr_membro,
                f.nome_segurado,
                f.tecnico_id,
                f.anexo,
                tec.nome AS nome_do_tecnico,
                f.estado_pagamento
            FROM
                " . $this->table_name . " f
                LEFT JOIN tipo_factura tipo ON tipo.id = f.tipo_factura_id
                LEFT JOIN tecnico tec ON tec.id = f.tecnico_id
            WHERE 
                1=1
            AND f.estado = '1'
            {$filter}";

        $stmt = $this->conn->prepare($sql);
        $stmt->execute();

        $row = $stmt->rowCount();
        if ($row > 0) {
            $response = array();
            $response['data'] = null;
            $response['total_resuls'] = $row;
            $nota = new Pagamento_Factura_Nota_Credito_Item($this->conn);
            foreach ($stmt as $data) {
                extract($data);
                if ((float)$dadosFacturaItens->SUMPagamentos2($id)['total_pago_factura'] > 0) {
                    $this->replace_valor_remanescente((float)$dadosFacturaItens->SUMPagamentos2($id)['total_pago_factura'], $id);
                }
                $emp_data = array(
                    "id" => $id,
                    "factura_global_id" => $factura_global_id,
                    "nr_factura" => $nr_factura,
                    "nome_segurado" => $nome_segurado,
                    "tecnico_id" => $tecnico_id,
                    "nome_do_tecnico" => $nome_do_tecnico,
                    "anexo" => $anexo,
                    "nr_membro" => $nr_membro,
                    "codigo_autorizacao" => $codigo_autorizacao,
                    "valor" => $valor,
                    "data" => $data,
                    "tipo_factura_id" => $tipo_factura_id,
                    "tipo_factura" => $Tipo_Factura,
                    "estado_pagamento" => $estado_pagamento,
                    "saldo" => (float)$valor - (float)$dadosFacturaItens->SUMPagamentos2($id)['total_pago_factura'], //$valor_remanescente,
                    "nota_credito" => (float)$nota->SUMPagamentos2($id)['total_pago_factura'],
                    "dadosFacturaGlobal" => $dadosFacturaItens->valorPago($id)['total'] ?? 0
                );
                $response['data'][] = $emp_data;
            }
        } else {
            $response = [];
        }
        return $response;
    }
    public function selectGuias($filter = null)
    {
        $dadosFacturaItens = new Pagamento_Factura_Cliente_Item($this->conn);

        $sql =
            "SELECT
                f.id,
                f.factura_global_id,
                f.nr_factura,
                f.valor,
                f.data,
                f.tipo_factura_id,
                tipo.descricao AS Tipo_Factura,
                f.valor_remanescente,
                f.codigo_autorizacao,
                f.nr_membro,
                f.nome_segurado
            FROM
                " . $this->table_name . " f
                LEFT JOIN tipo_factura tipo ON tipo.id = f.tipo_factura_id
            WHERE 
                1=1
            AND f.estado = '1'
            {$filter}";

        $stmt = $this->conn->prepare($sql);
        $stmt->execute();

        $row = $stmt->rowCount();
        if ($row > 0) {
            $response = array();
            $response['data'] = null;
            $response['total_resuls'] = $row;

            foreach ($stmt as $data) {
                extract($data);
                if ((float)$dadosFacturaItens->SUMPagamentos2($id)['total_pago_factura'] > 0) {
                    $this->replace_valor_remanescente((float)$dadosFacturaItens->SUMPagamentos2($id)['total_pago_factura'], $id);
                }
                if ((float)$dadosFacturaItens->valorPago($id)['total'] < (float)$valor || (float)$dadosFacturaItens->valorPago($id)['total'] == 0) {
                    $emp_data = array(
                        "id" => $id,
                        "factura_global_id" => $factura_global_id,
                        "nr_factura" => $nr_factura,
                        "nome_segurado" => $nome_segurado,
                        "nr_membro" => $nr_membro,
                        "codigo_autorizacao" => $codigo_autorizacao,
                        "valor" => $valor,
                        "data" => $data,
                        "tipo_factura_id" => $tipo_factura_id,
                        "tipo_factura" => $Tipo_Factura,
                        "saldo" => (float)$valor - (float)$dadosFacturaItens->SUMPagamentos2($id)['total_pago_factura'], //$valor_remanescente,
                        "dadosFacturaGlobal" => $dadosFacturaItens->valorPago($id)['total'] ?? 0
                    );
                    $response['data'][] = $emp_data;
                }
            }
        } else {
            $response = [];
        }
        return $response;
    }
    public function readSUM($factura_id)
    {
        $sql = "SELECT SUM(remanescente) as sum, SUM(saldo) saldo FROM pagamento_facturas WHERE factura_id = {$factura_id}";

        $stmt = $this->conn->prepare($sql);

        $this->factura_id = htmlspecialchars(strip_tags($this->factura_id));

        $stmt->bindParam(":factura_id", $this->factura_id);

        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            extract($stmt->fetch(PDO::FETCH_ASSOC));
            $response = [
                "sum" => $sum,
                "saldoActual" => $saldo,
            ];
        } else {
            $response = [];
        }
        return $response;
    }
    public function read_one()
    {
        $sql =
            "SELECT
                f.id,
                f.factura_global_id,
                cli.nome AS cliente,
                f.nr_factura,
                f.valor,
                f.data,
                f.tipo_factura_id,
                tipo.descricao AS Tipo_Factura,
                f.valor_remanescente,
                f.nr_membro,
                f.codigo_autorizacao,
                f.nome_segurado,
                f.tecnico_id,
                tec.nome AS tecnico
            FROM
                " . $this->table_name . " f
                LEFT JOIN clientes cli ON cli.id = f.factura_global_id
                LEFT JOIN tipo_factura tipo ON tipo.id = f.tipo_factura_id
                LEFT JOIN tecnico tec ON tec.id = f.tecnico_id
            WHERE 
                f.id = $this->id
            AND f.estado = '1'";

        $stmt = $this->conn->prepare($sql);
        $stmt->execute();

        $row = $stmt->rowCount();
        if ($row > 0) {
            $total_pagos = new Pagamento_Factura_Cliente_Item($this->conn);
            extract($stmt->fetch(PDO::FETCH_ASSOC));
            $calcula = (float)$valor;
            $response = array(
                "id" => $id,
                "factura_global_id" => $factura_global_id,
                "cliente" => $cliente,
                "nr_factura" => $nr_factura,
                "nome_segurado" => $nome_segurado,
                "valor" => $valor,
                "data" => $data,
                "tipo_factura_id" => $tipo_factura_id,
                "tipo_factura" => $Tipo_Factura,
                "saldo" => $valor_remanescente,
                "total_pagamento" => $calcula,
                "codigo_autorizacao" => $codigo_autorizacao,
                "nr_membro" => $nr_membro,
                "tecnico" => $tecnico,
                "tecnico_id" => $tecnico_id,
                "total_pagos" => (float)$total_pagos->SUMPagamentos2($id)['total_pago_factura'],
                "remanescente" => (float)$valor - (float)$total_pagos->SUMPagamentos2($id)['total_pago_factura'],
            );
        } else {
            $response = [];
        }
        return $response;
    }

    public function readByNumeroFactura($nr_factura)
    {
        $sql =
            "SELECT
                f.id,
                f.factura_global_id,
                cli.nome AS cliente,
                f.nr_factura,
                f.valor,
                f.data,
                f.tipo_factura_id,
                tipo.descricao AS Tipo_Factura,
                f.valor_remanescente,
                f.nr_membro,
                f.codigo_autorizacao,
                f.nome_segurado
            FROM
                " . $this->table_name . " f
                LEFT JOIN clientes cli ON cli.id = f.factura_global_id
                LEFT JOIN tipo_factura tipo ON tipo.id = f.tipo_factura_id
            WHERE 
                f.nr_factura = '{$nr_factura}'
            AND f.estado = '1'";

        $stmt = $this->conn->prepare($sql);
        $stmt->execute();

        $row = $stmt->rowCount();
        if ($row > 0) {
            $total_pagos = new Pagamento_Factura_Cliente_Item($this->conn);
            extract($stmt->fetch(PDO::FETCH_ASSOC));
            $calcula = (float)$valor;
            $response = array(
                "id" => $id,
                "factura_global_id" => $factura_global_id,
                "cliente" => $cliente,
                "nr_factura" => $nr_factura,
                "nome_segurado" => $nome_segurado,
                "valor" => $valor,
                "data" => $data,
                "tipo_factura_id" => $tipo_factura_id,
                "tipo_factura" => $Tipo_Factura,
                "saldo" => $valor_remanescente,
                "total_pagamento" => $calcula,
                "codigo_autorizacao" => $codigo_autorizacao,
                "nr_membro" => $nr_membro,
                "total_pagos" => (float)$total_pagos->SUMPagamentos2($id)['total_pago_factura'],
                "remanescente" => (float)$valor - (float)$total_pagos->SUMPagamentos2($id)['total_pago_factura'],
            );
        } else {
            $response = [];
        }
        return $response;
    }

    public function validateFacturaNumnero($filter = null)
    {
        $sql = "SELECT * FROM " . $this->table_name . " WHERE nr_factura={$this->nr_factura} AND factura_global_id={$this->factura_global_id} AND estado='1'";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            return true;
        } else {
            return false;
        }
    }
    public function validateFacturaCodigo($filter = null)
    {
        $sql = "SELECT * FROM " . $this->table_name . " WHERE codigo_autorizacao={$this->codigo_autorizacao} AND factura_global_id={$this->factura_global_id} AND estado='1'";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            return true;
        } else {
            return false;
        }
    }
    public function VerifyDiscount($id)
    {
        $sql = "SELECT * FROM " . $this->table_name . " WHERE id={$id} AND desconto > 0";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            extract($stmt->fetch(PDO::FETCH_ASSOC));
            $response = ["desconto" => $desconto];
        } else {
            return $response = [];
        }
        return $response;
    }

    public function getFacturaGlobal($id)
    {
        $sql = "SELECT SUM(valor) as valor FROM " . $this->table_name . " WHERE factura_global_id={$id} AND estado='1'";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            extract($stmt->fetch(PDO::FETCH_ASSOC));
            $response = ["valor" => $valor];
        } else {
            $response = [];
        }
        return $valor ?? 0;
    }

    public function actualiza_estado_pagamento_guia($factura_cliente_id, $valor_pago)
    {
        $this->id = $factura_cliente_id;
        if ((float)$this->read_one()['valor'] >= (float)$valor_pago) {
            $sql = "UPDATE 
            " . $this->table_name . "
                SET
                estado_pagamento = '1'
            WHERE 
            factura_cliente.id = {$factura_cliente_id}";
            $stmt = $this->conn->prepare($sql);
            $stmt->execute();
        } elseif ((float)$this->read_one()['valor'] < (float)$valor_pago) {
            $sql = "UPDATE 
            " . $this->table_name . "
                SET
                estado_pagamento = '0'
            WHERE 
            factura_cliente.id = {$factura_cliente_id}";
            $stmt = $this->conn->prepare($sql);
            $stmt->execute();
        }
    }

    public function replace_valor_remanescente($valor_remanescente, $id)
    {
        $sql = "UPDATE 
            " . $this->table_name . "
                SET
                valor_remanescente = $valor_remanescente
            WHERE 
            id = $id";

        $stmt = $this->conn->prepare($sql);
        $this->id = htmlspecialchars(strip_tags($this->id));

        if ($stmt->execute()) {
            return true;
        }
        return false;
    }
    public function replace()
    {
        $sql = "UPDATE 
            " . $this->table_name . "
                SET 
                nr_factura = $this->nr_factura,
                valor = $this->valor,
                data = $this->data,
                codigo_autorizacao = $this->codigo_autorizacao,
                nr_membro = $this->nr_membro,
                nome_segurado = $this->nome_segurado,
                user_id = $this->user_id
            WHERE 
            id = $this->id";

        $stmt = $this->conn->prepare($sql);

        $this->nr_factura = htmlspecialchars(strip_tags($this->nr_factura));
        $this->valor = htmlspecialchars(strip_tags($this->valor));
        $this->data = htmlspecialchars(strip_tags($this->data));
        $this->codigo_autorizacao = htmlspecialchars(strip_tags($this->codigo_autorizacao));
        $this->nr_membro = htmlspecialchars(strip_tags($this->nr_membro));
        $this->nome_segurado = htmlspecialchars(strip_tags($this->nome_segurado));
        $this->id = htmlspecialchars(strip_tags($this->id));

        if ($stmt->execute()) {
            return true;
        }
        return false;
    }

    public function delete()
    {
        $sql = "UPDATE " . $this->table_name . " SET estado='{$this->estado}' WHERE id={$this->id}";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            return true;
        }
        return false;
    }
}
