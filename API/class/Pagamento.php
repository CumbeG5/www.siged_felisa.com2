<?php
class Pagamento
{
    private $conn;
    private $table_name = "pagamento";

    public $id;
    public $nr_recibo;
    public $data;
    public $data_recibo;
    public $banco_id;
    public $fornecedor_id_pagamento;
    public $numero_factura;
    public $user_id;
    public $estado;

    public function __construct($db)
    {
        $this->conn = $db;
    }

    public function delete()
    {
        $sql = "UPDATE " . $this->table_name . " SET estado='{$this->estado}' WHERE id={$this->id}";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            return true;
        }
        return false;
    }

    public function create()
    {
        $i = 0;
        $generate_number = new Generate_Number($this->conn);

        $ano = date("Y");
        $tipo = 'nota_credito';
        $numeracao = $generate_number->getInvoiceNumber($ano, $tipo) + 1;
        $numero_factura = "{$numeracao}/{$ano}";

        $sql = "INSERT 
            INTO " . $this->table_name . "
            SET
            nr_recibo = $this->nr_recibo,
            data = $this->data,
            data_recibo = $this->data_recibo,
            fornecedor_id = $this->fornecedor_id_pagamento,
            nr_pagamento = '$numero_factura',
            user_id = $this->user_id
        ";

        $stmt = $this->conn->prepare($sql);
        if ($stmt->execute()) {
            $i = $this->conn->lastInsertId();
            $generate_number->updateInvoiceNumber($ano, $tipo);
        }
        return $i;
    }
    public function update()
    {
        $sql = "UPDATE 
            " . $this->table_name . "
            SET
            nr_recibo = $this->nr_recibo,
            data = $this->data,
            data_recibo = $this->data_recibo
            WHERE id = $this->id
        ";

        $stmt = $this->conn->prepare($sql);
        if ($stmt->execute()) {
            return true;
        }
        return false;
    }
    public function read($filter = null, $start_from, $limit)
    {
        $sql =
            "SELECT
                p.id,
                p.fornecedor_id,
                forn.nome AS fornecedor,
                p.nr_recibo,
                p.data,
                p.data_recibo,
                p.created_at,
                p.nr_pagamento
            FROM
                " . $this->table_name . " p
            INNER JOIN fornecedors forn ON p.fornecedor_id=forn.id 
            WHERE
                1=1 AND estado != '0'
            {$filter} ORDER BY p.id desc";
        $sql .= " limit {$start_from}, {$limit}";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $row = $stmt->rowCount();
        if ($row > 0) {
            $response = array();
            $response['data'] = null;
            $response['total_results'] = $row;

            $dadosPagamentos = new Pagamento_Facturas($this->conn);

            foreach ($stmt as $values) {
                extract($values);
                $emp_items = array(
                    "id" => $id,
                    "nr_recibo" => $nr_recibo,
                    "fornecedor" => $fornecedor,
                    "data" => $data,
                    "data_recibo" => $data_recibo,
                    "nr_pagamento" => $nr_pagamento,
                    "totalPago" => number_format($dadosPagamentos->SUMPagamentos($id)['total_pago_factura'], 2, ".", ",") ?? [],
                    // "nr_factura"=>$dadosPagamentos->read("AND p_f.pagamento_id={$id}")
                );
                $response['data'][] = $emp_items;
            }
        } else {
            $response = [];
        }
        return $response;
    }
    public function read_one()
    {
        $sql =
            "SELECT
                p.id,
                p.nr_recibo,
                CAST(p.created_at as DATE) as data_criacao,
                p.data,
                p.data_recibo,
                p.created_at,
                p.fornecedor_id,
                forn.nome as descricaoempresa,
                p.nr_pagamento
            FROM
                " . $this->table_name . " p
                INNER JOIN fornecedors forn ON p.fornecedor_id=forn.id
                WHERE
                p.id = $this->id
            ";

        $stmt = $this->conn->prepare($sql);
        // sanitize
        $this->id = htmlspecialchars(strip_tags($this->id));

        // bind data
        // $stmt->bindParam(":id", $this->id);

        $stmt->execute();
        $row = $stmt->rowCount();
        if ($row > 0) {
            $formas_pagamento = new Formas_Pagamentos($this->conn);
            $facturas = new Pagamento_Facturas($this->conn);
            $sumP = new Pagamento_Facturas($this->conn);
            extract($stmt->fetch(PDO::FETCH_ASSOC));
            $response = array(
                "id" => $id,
                "fornecedor_id" => $fornecedor_id,
                "descricaoempresa" => $descricaoempresa,
                "nr_recibo" => $nr_recibo,
                "data_criacao" => $data_criacao,
                "data" => $data,
                "data_recibo" => $data_recibo,
                "nr_pagamento" => $nr_pagamento,
                "formas_pagamento" => $formas_pagamento->read("AND f.pagamento_id={$id} "),
                "facturas" => isset($facturas->read("AND p_f.pagamento_id={$id}")['data']) && !empty($facturas->read("AND p_f.pagamento_id={$id}")['data']) ? $facturas->read("AND p_f.pagamento_id={$id}")['data'] : [],
                "dadosFactura" => $formas_pagamento->DadosFactura($id, 'fornecedor'),
                "totalPago" => $sumP->SUMPagamentos($id)['total_pago_factura'] ?? []
            );
        } else {
            $response = [];
        }
        return $response;
    }
    public function validateRecibo($filter = null)
    {
        $sql = "SELECT * FROM " . $this->table_name . " WHERE nr_recibo={$this->nr_recibo} AND fornecedor_id={$this->fornecedor_id_pagamento}";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            return true;
        } else {
            return false;
        }
    }
}
