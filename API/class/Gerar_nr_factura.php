<?php
class Generate_Number
{
    private $conn;
    private $table_name = "numeracao";

    public $ano;
    public $tipo;
    public $numeracao;
    public $numero_factura;

    public function __construct($db)
    {
        $this->conn = $db;
    }

    public function getInvoiceNumber($ano, $tipo)
    {
        $numero = 0;
        $numeros = "SELECT * FROM numeracao WHERE ano={$ano} AND tipo='{$tipo}'";
        $stmt = $this->conn->prepare($numeros);
        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            foreach ($stmt as $numero2) {
                extract($numero2);
                $response = array(
                    "numero" => $numero,
                );
                $response['data'][] = $response;
            }
            // print_r($numero);
        }else{
            $response = [];
        }
        return $numero;
        // $numeros = DB::table('numeracao')
        //     ->select("numero")

        //     ->where(['ano' => $ano, 'tipo' => "$tipo"])->get();
    }

    public function updateInvoiceNumber($ano, $tipo)
    {
        $numero = $this->getInvoiceNumber($ano, $tipo);
        if (!empty($numero)) {
            $numero+=1;
            $numeracao_factura = "UPDATE numeracao SET numero=".$numero." WHERE tipo='{$tipo}'";
            $stmt = $this->conn->prepare($numeracao_factura);
            $stmt->execute();
            // $numeracao_factura = DB::table('numeracao')->where('tipo', $tipo)->update(['numero' => $numero + 1]);
        } else {
            $numeracao_factura = "INSERT INTO numeracao SET numero=1, ano='{$ano}', tipo='{$tipo}'";
            $stmt = $this->conn->prepare($numeracao_factura);
            $stmt->execute();
            // $numeracao_factura = DB::table('numeracao')->insertGetId(['numero' => 1, 'ano' => $ano, 'tipo' => $tipo]);
        }
        return $numeracao_factura;
    }
}
