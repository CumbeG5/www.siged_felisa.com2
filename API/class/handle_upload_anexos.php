<?php

function handleMultiFileUpload($fields, $uploadDir)
{
    $result = null;
    foreach ($fields as $fieldName => $field) {
        if (isset($field['name'])) {
            if(empty($field['name'])){
                continue;
            }
            $allowedFileType = array('jpg', 'png', 'jpeg', 'gif', 'pdf', 'doc', 'docx', 'xls', 'xlsx', 'ppt', 'pptx');
            $extension = strtolower(pathinfo($field['name'], PATHINFO_EXTENSION));
            $tempFile = $field['tmp_name'];
            $fileName = $field['name'];
            if (in_array($extension, $allowedFileType)) {
                if ($field['size'] <= 35485760) {
                    $novoNome = uniqid() . ".$extension";

                    $targetFile = "../../../".$uploadDir . '/' . $novoNome;
                    $caminho = $uploadDir . '/' . $novoNome;

                    move_uploaded_file($tempFile, $targetFile);
                    $result = $caminho;
                    // array_push($result, $anexosComite);
                } else {
                    echo json_encode(
                        array(
                            'message' => 'Error uploading file, the file is large.',
                            'Status' => 500,
                            'status_text' => 'error'
                        )
                    );
                    exit();
                }
            } else {
                echo json_encode(
                    array(
                        'message' => 'Error uploading file, video or image are acceptable.',
                        'Status' => 500,
                        'status_text' => 'error',
                        'ext' => $extension
                    )
                );
                exit();
            }
        }
    }
    return $result;

    // return $anexosComite;
}