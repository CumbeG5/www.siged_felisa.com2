<?php
class Pagamento_Factura_Nota_Credito_Item
{
    private $conn;
    private $table_name = "pagamento_factura_nota_credito_item";

    public $id;
    public $factura_cliente_id;
    public $pagamento_factura_id;
    public $valor_pago;
    public $data;
    public $estado;

    public function __construct($db)
    {
        $this->conn = $db;
    }

    public function getInvoiceID($factura_cliente_id)
    {
        $sql = "SELECT * FROM " . $this->table_name . " WHERE factura_cliente_id={$this->factura_cliente_id}";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            foreach ($stmt as $key) {
                extract($key);
                $response = array(
                    "factura_cliente_id" => $factura_cliente_id
                );
                $response['data'][] = $response;
            }
        } else {
            $response = [];
        }
        return $factura_cliente_id;
    }

    public function updateInvoiceNumber()
    {
        $key = $this->getInvoiceID($this->factura_cliente_id);
        if (!empty($key)) {
            $this->create();
        } else {
            $this->update();
        }
        return $this;
    }

    public function create()
    {

        $sql = "
            INSERT 
                INTO " . $this->table_name . " 
            SET 
                factura_cliente_id = $this->factura_cliente_id,
                pagamento_factura_id = $this->pagamento_factura_id,
                valor_pago = $this->valor_pago,
                data = '$this->data'
                ";

        $stmt = $this->conn->prepare($sql);
        if ($stmt->execute()) {
            return true;
        }
        return false;
    }

    public function update()
    {
        $sql = "
            UPDATE 
                " . $this->table_name . " 
            SET 
                factura_cliente_id = $this->factura_cliente_id,
                pagamento_factura_id = $this->pagamento_factura_id,
                valor_pago = $this->valor_pago,
                data = $this->data";

        $stmt = $this->conn->prepare($sql);
        if ($stmt->execute()) {
            return true;
        }
        return false;
    }

    public function SUMPagamentos($pagamento_factura_id)
    {
        $sql = "SELECT SUM(valor_pago) as sum FROM " . $this->table_name . " WHERE pagamento_factura_id = {$pagamento_factura_id} AND estado='1'";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            extract($stmt->fetch(PDO::FETCH_ASSOC));
            $response = ["total_pago_factura" => $sum];
        } else {
            return $response = ["total_pago_factura" => 0];
        }
        return $response;
    }

    public function SUMValorGuias($pagamento_factura_id)
    {
        $sql = "SELECT SUM(fc.valor) as sum FROM " . $this->table_name . " pci
        INNER JOIN factura_cliente fc ON fc.id=pci.factura_cliente_id
        WHERE 
        pagamento_factura_id = {$pagamento_factura_id} 
        AND pci.estado='1'
        ";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            extract($stmt->fetch(PDO::FETCH_ASSOC));
            $response = ["total_valor_guias" => $sum];
        } else {
            return $response = ["total_valor_guias" => 0];
        }
        return $response;
    }

    public function SUMPayments($pagamento_factura_id)
    {
        $sql = "SELECT SUM(valor_pago) as sum FROM " . $this->table_name . " WHERE factura_cliente_id = {$pagamento_factura_id} AND estado='1'";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            extract($stmt->fetch(PDO::FETCH_ASSOC));
            $response = ["total_pago_factura" => $sum];
        } else {
            return $response = [];
        }
        return $response;
    }

    public function SUMPagamentos2($factura_cliente_id)
    {
        $sql = "SELECT SUM(valor_pago) as sum FROM " . $this->table_name . " WHERE estado='1' AND factura_cliente_id = {$factura_cliente_id}";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            extract($stmt->fetch(PDO::FETCH_ASSOC));
            $response = ["total_pago_factura" => $sum];
        } else {
            return $response = [];
        }
        return $response;
    }

    public function read($filter = null)
    {
        $sql = "SELECT * FROM " . $this->table_name . " p_f_i WHERE 1=1 AND p_f_i.estado = '1' {$filter}";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $rows_num = $stmt->rowCount();
        if ($rows_num > 0) {
            $response = array();
            $response['data'] = null;
            $response['total_results'] = $rows_num;

            $facturas = new FacturaCliente($this->conn);
            foreach ($stmt as $value) {
                extract($value);
                $emp_item = array(
                    "id" => $id,
                    "factura_cliente_id" => $factura_cliente_id,
                    "pagamento_factura_id" => $pagamento_factura_id,
                    "valor_pago" => $valor_pago, //$this->SUMPagamentos2($factura_cliente_id)['total_pago_factura'],//,
                    "data" => $data,
                    "factura" => $facturas->select("AND f.id={$factura_cliente_id}")['data'] ?? [],
                    "totalPago" => $this->SUMPagamentos2($factura_cliente_id)['total_pago_factura'] ?? [],
                );
                $response['data'][] = $emp_item;
            }
        } else {
            $response = [];
        }
        return $response;
    }
    public function existe_guia($factura)
    {
        $sql = "SELECT * FROM " . $this->table_name . " p_f_i WHERE p_f_i.estado = '1' AND p_f_i.factura_cliente_id = {$factura}";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $rows_num = $stmt->rowCount();
        if ($rows_num > 0) {
            return True;
        } else {
            return False;
        }
    }

    public function valorPago($factura_cliente_id)
    {
        $sql = "SELECT valor_pago FROM " . $this->table_name . " WHERE factura_cliente_id={$factura_cliente_id} AND estado='1'";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            extract($stmt->fetch(PDO::FETCH_ASSOC));
            $response = ["total" => $valor_pago];
        } else {
            $response = ["total" => 0];
        }
        return $response;
    }

    public function delete()
    {
        $sql = "UPDATE " . $this->table_name . " SET estado='{$this->estado}' WHERE id={$this->id}";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            return true;
        }
        return false;
    }


    public function updateSaldoItemPayment($id)
    {
        // $itemFacturas = new FacturaCliente($this->conn);
        $totalItem = $this->getFacturaGlobalItem($id);
        $sql = "UPDATE factura_global SET valor={$totalItem}, remanescente={$totalItem} WHERE id={$id}";
        print_r($sql);
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            return true;
        }
        return false;
    }

    public function getFacturaGlobalItem($id)
    {
        $sql = "SELECT SUM(valor_pago) as valor FROM " . $this->table_name . " WHERE pagamento_factura_id={$id} AND estado='1'";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            extract($stmt->fetch(PDO::FETCH_ASSOC));
            $response = ["valor" => $valor];
        } else {
            $response = [];
        }
        return $valor ?? 0;
    }
}
