<?php
include_once "../API/callAPI.php";
include_once "../API/include.php";

$response = CallApi($URL . 'api/factura/read_one.php?id=' . (isset($_POST['id']) ? $_POST['id'] : 0), $METHOD = "GET");

// print_r($response->anexo_factura);
?>

<div id="modalDespesa" class="ui small modal">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 style="font-size: 1rem;font-weight: bolder;" class="modal-title" id="exampleModalCenterTitle">DETALHES DA FACTURA</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table style="font-size: 1rem;" class="table table-striped">
                        <tbody>
                            <tr hidden>
                                <td><strong>Id:</strong></td>
                                <td><?= $response->id ?></td>
                            </tr>
                            <tr>
                                <td><strong>Fornecedor:</strong></td>
                                <td><?= $response->fornecedor ?? "" ?></td>
                            </tr>
                            <tr>
                                <td><strong>Centro de custo:</strong></td>
                                <td><?= $response->farmacia ?? "" ?></td>
                            </tr>
                            <tr>
                                <td><strong>Data da Factura:</strong></td>
                                <td><?= $response->data_factura ?? "" ?></td>
                            </tr>
                            <tr>
                                <td><strong>Tipo de Factura:</strong></td>
                                <td><?= $response->tipo_factura ?? "" ?></td>
                            </tr>
                            <tr>
                                <td><strong>Valor:</strong></td>
                                <td><?= number_format($response->valor, 2, ",", ".") ?? "" ?></td>
                            </tr>
                            <tr>
                                <td><strong>Saldo:</strong></td>
                                <td><?= number_format($response->saldo, 2, ",", ".") ?? "" ?></td>
                            </tr>
                            <tr>
                                <td><strong>Nº da Factura:</strong></td>
                                <td><?= $response->nr_factura ?? "" ?></td>
                            </tr>
                            <tr>
                                <td><strong>Anexo:</strong></td>
                                <td><a href="<?= !empty($response->anexo_factura) ? $response->anexo_factura : "" ?>" target="_blank" rel="noopener noreferrer"><?= !empty($response->anexo_factura) ? "Anexo" : "Sem anexo disponivel" ?></a></td>
                            </tr>
                            <tr>
                                <td><strong>Data hora de criação:</strong></td>
                                <td><?= $response->created_at ?? "" ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-danger" type="button" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
    <div class="actions">
        <div class="ui negative labeled icon button">Fechar <i class="ui times icon"></i></div>
    </div>
</div>