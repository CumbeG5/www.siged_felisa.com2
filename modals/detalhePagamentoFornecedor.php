<?php
include_once "../API/callAPI.php";
include_once "../API/include.php";

$response = CallApi($URL . 'api/pagamento/read_one.php?id=' . (isset($_POST['id']) ? $_POST['id'] : 0), $METHOD = "GET");

// print_r($response);
?>

<div id="modalDespesa" class="ui small modal">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 style="font-size: 1rem;font-weight: bolder; text-transform: uppercase;" class="modal-title" id="exampleModalCenterTitle">Detalhes de Pagamento</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table style="font-size: 1rem;" class="table table-striped">
                        <tbody>
                            <tr hidden>
                                <td><strong>Id:</strong></td>
                                <td><?= $response->id ?></td>
                            </tr>
                            <tr>
                                <td><strong>Numero de Recibo:</strong></td>
                                <td><?= $response->nr_recibo ?? "" ?></td>
                            </tr>
                            <tr>
                                <td><strong>Data Pagamento:</strong></td>
                                <td><?= $response->data ?? "" ?></td>
                            </tr>
                            <tr>
                                <td><strong>Data do Recibo:</strong></td>
                                <td><?= $response->data_recibo ?? "" ?></td>
                            </tr>
                            <tr>
                                <td><strong>Numero Nota de Pagamento:</strong></td>
                                <td><?= $response->nr_pagamento ?? "" ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-danger" type="button" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
    <!-- <div class="actions">
        <div class="ui negative labeled icon button">Fechar <i class="ui times icon"></i></div>
    </div> -->
</div>