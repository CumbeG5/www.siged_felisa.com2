<?php
//Data de mocambique
setlocale(LC_ALL, 'pt', 'pt.utf-8', 'pt.utf-8', 'portuguese');
date_default_timezone_set('Africa/Maputo');

//Classes
require_once('Connection.php');
require_once('Geral.php');
require_once("cadastro_utilizadores.php");
require_once("Rotinas.php");

function getMesPortugues($diaMes)
{
	$mesInicial = $diaMes;
	$mesResult = '';
	if ($mesInicial == 1)
		$mesResult = 'Janeiro';
	else if ($mesInicial == 2)
		$mesResult = 'Fevereiro';
	elseif ($mesInicial == 3)
		$mesResult = 'Mar&ccedil;o';
	elseif ($mesInicial == 4)
		$mesResult = 'Abril';
	elseif ($mesInicial == 5)
		$mesResult = 'Maio';
	elseif ($mesInicial == 6)
		$mesResult = 'Junho';
	elseif ($mesInicial == 7)
		$mesResult = 'Julho';
	elseif ($mesInicial == 8)
		$mesResult = 'Agosto';
	elseif ($mesInicial == 9)
		$mesResult = 'Setembro';
	elseif ($mesInicial == 10)
		$mesResult = 'Outubro';
	elseif ($mesInicial == 11)
		$mesResult = 'Novembro';
	elseif ($mesInicial == 12)
		$mesResult = 'Dezembro';

	return $mesResult;
}

function mes_descricao($id)
{
	if ($id == 1)
		echo "Janeiro";
	if ($id == 2)
		echo "Fevereiro";
	if ($id == 3)
		echo "Março";
	if ($id == 4)
		echo "Abril";
	if ($id == 5)
		echo "Maio";
	if ($id == 6)
		echo "Junho";
	if ($id == 7)
		echo "Julho";
	if ($id == 8)
		echo "Agosto";
	if ($id == 9)
		echo "Setembro";
	if ($id == 10)
		echo "Outubro";
	if ($id == 11)
		echo "Novembro";
	if ($id == 12)
		echo "Dezembro";
}

////Classe para levar a fotografia da empresa
function getAvatarComp()
{
	$idEmp = 1;
	global $conexaosiged;
	$sql = "SELECT `empresaLogo` FROM `paramempresa` WHERE `empresaId`=?";
	$result = $conexaosiged->prepare($sql);
	$result->bindParam(1, $idEmp);
	$result->execute();
	$row = $result->fetch();
	return $row['empresaLogo'];
}
function getRow($table, $line, $reference, $valueLike)
{
	global $conexaosiged;
	$sql = "SELECT {$line} FROM {$table} WHERE {$reference} = {$valueLike}";
	$result = $conexaosiged->prepare($sql);
	$result->execute();
	$row = $result->fetch();
	return $row[$line];
}
function alterRow($table, $line, $value, $reference, $valueLike)
{
	global $conexaosiged;
	$sql = "UPDATE ? SET ? = ? WHERE ? = ?";
	$result = $conexaosiged->prepare($sql);
	$result->bindParam(1,  $table);
	$result->bindParam(2,  $line);
	$result->bindParam(3,  $value);
	$result->bindParam(4,  $reference);
	$result->bindParam(5,  $valueLike);
	return $result;
}
