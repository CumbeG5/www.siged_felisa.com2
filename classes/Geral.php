<?php

    function evitarSqlInjection($valor){ //Evitar sql injection
        return mysqli_real_escape_string($this->con(), $valor);
    }

    function number_formatter($number){
        return number_format($number,2,',','.');
    }

?>