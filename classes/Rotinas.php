<?php
/**
 * Created by PhpStorm.
 * User: Manacaze
 * Date: 1/22/2018
 * Time: 9:10 AM
 */

class Rotinas extends Connection
{
    private $ano, $mes, $data;

    function __construct()
    {
        $this->setPresenteAno();
        $this->setPresenteMes();
        $this->setPresenteData();
    }

    private function setPresenteAno(){
        $this->ano = date("Y");
    }
    private function setPresenteMes(){
        $this->mes = date("m");
    }
    private function setPresenteData(){
        $this->data = date("Y-m-d");
    }

    public function desativaServicosExpirados(){
        $sql = "SELECT * FROM `servico` WHERE `anofim`< '".$this->ano."' AND `estadoservico` != 0 ";
        $query = mysqli_query($this->con(), $sql);
        $arrayResult = array();
        foreach ($query as $row){
            if($this->setEstadoServico($row['idservico'], 0)){
                $arrayResult[]=1;
            }
        }
        if(count($arrayResult) != 0){
            return true;
        }else{
            return false;
        }
    }
    public function desativaTurmasExpiradas(){
        $sql = "SELECT * FROM `turma` WHERE `datafim`< '".$this->data."' AND `estado` != 0 ";
        $query = mysqli_query($this->con(), $sql);
        $arrayResult = array();
        $matriculas = new Matricula();
        foreach ($query as $row){
            if($this->setEstadoTurma($row['idturma'], 0)){
                $matriculas->encerarMatriculasTurma($row['idturma']);
                $arrayResult[]=1;
            }
        }
        if(count($arrayResult) != 0){
            return true;
        }else{
            return false;
        }
    }
    public function desativaMatriculasExpiradas(){
        $sql = "SELECT `idmatricula` FROM `turma`, `matricula` WHERE `turma`.`idturma` LIKE `matricula`.`idturma` AND `turma`.`estado` = 0 AND `matricula`.`estado` != 0";
        $query = mysqli_query($this->con(), $sql);
        $arrayResult = array();
        foreach ($query as $row){
            if($this->setEstadoMatricula($row['idmatricula'], 0)){
                $arrayResult[]=1;
            }
        }
        if(count($arrayResult) != 0){
            return true;
        }else{
            return false;
        }
    }
    public function setEstadoServico($servico, $estado){
        $sql = "UPDATE `servico` SET `estadoservico`= '$estado' WHERE `idservico` = '$servico'";
        return mysqli_query($this->con(), $sql);
    }
    public function setEstadoTurma($turma, $estado){
        $sql = "UPDATE `turma` SET `estado`= '$estado', `abertaturma`=2 WHERE `idturma` = '$turma'";
        return mysqli_query($this->con(), $sql);
    }
    public function setEstadoMatricula($matricula, $estado){
        $sql = "UPDATE `matricula` SET `estado`= '$estado' WHERE `idmatricula` = '$matricula'";
        return mysqli_query($this->con(), $sql);
    }
}